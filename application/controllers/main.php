<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
    }

    public function index() {
        $this->load->model('rbc_category_model');
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $data['categories'] = $this->rbc_category_model->get_by_category();
        $data['sub_categories'] = $this->rbc_category_model->get_by_sub_category();

        foreach ($this->product_master_model->get_all_main() as $product) {
                    if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $products_array[] = $product;
                    }
                }
        $data['products'] = $products_array;
//        echo '<pre>';
//        print_r($data);
//        exit();
        $this->session->set_userdata("product_ids","");
        $this->session->set_userdata("compare_count",0);
        $this->load->view('index', $data);
    }

    public function login_user() {
        $this->load->view('login');
    }

    public function register_user() {


        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_user_model');
        $this->load->model('member_master_model');
        $this->load->model('company_master_model');
        $this->load->model('company_privacy_setting_model');
        $data['countries'] = $this->rbc_countries_model->get_all();
        $this->load->view('register', $data);

        //print_r($check_email);exit();
        if ($this->input->post('submit_register')) {

            $this->load->helper(array('form', 'url'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('user_email', 'Email', 'required|callback_uniqueemail');
            if ($this->form_validation->run($this) == FALSE ) {
                $this->load->view('register');
            }
            if($this->input->post('g-recaptcha-response') == ""){
                $this->session->set_flashdata('captcha_error', 'Please verify that You are not a robot');
                redirect('main/register_user');
            }
            else {
                $data = $_POST;
                $data['operational_location'] = $this->input->post('registration_location');
                $data['hash_key'] = $this->random_string(10);
                $data['user_password'] = $this->input->post('create_password');
                $data['user_name'] = $this->input->post('seller_name');
                $data['company_id'] = $this->company_master_model->create($data);
                $data['member_id'] = $this->member_master_model->create($data);
                $this->rbc_user_model->create($data);
                $this->company_privacy_setting_model->create($data);
                $message = '<h1>Congratulations</h1><br>You have successfully signed up to rbctrade<br>Your Use Name is "' . $data['user_email'] . '" and you password is "' . $data['user_password'] . '"<br>Please use this link to sign in ' . base_url() . 'index.php/login/check?key='.$data['hash_key'].'&user='.$data['user_email'].'&pass='.$data['user_password'].'';
                $to = $data['user_email'];
                if($this->send_email($message,$to,"(RBC)Activate Account"))
                {
                    $this->session->set_flashdata('signed_up', 'Signed up successfully, please check your email for login');
                }else{
                    $this->session->set_flashdata('signed_up_failed', 'Signed up Failed, please try gain later');
                }
                redirect('login/');
            }
        }
    }
    public function uniqueemail($str){
        return $this->rbc_user_model->mail_exists($str);
    }
    public function contact_us() {
        $this->load->view('contact_us');
        if($this->input->post('contact_us_submit')){
            $fname = $this->input->post('fname');
            $lname = $this->input->post('lname');
            $email = $this->input->post('email');
            $message = $this->input->post('message');
            $message_for_user = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta name="viewport" content="width=device-width, initial-scale=1.0"/><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><title data-form="subject">Thank you for shopping with us!</title><style type="text/css">
                          body.email-template img { max-width: 100%; } td[class="questions"] a { color: #9aa0a6; } body.email-template .padding { padding: 2% 0; } body.email-template table { border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; } body.email-template table p, ul, ol { font-weight: normal; font-size: 14px; } body.email-template table ul li, ol li { margin-left: 5px; list-style-position: inside; } body.email-template .main-container { display: block; max-width: 600px; margin: 0 auto; clear: both; width: 80%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; } body.email-template .content { max-width: 600px; margin: 0 auto; display: block; } body.email-template .content table { width: 100%; } .questions-p a { color: #9aa0a6; }
                          @media screen and (max-width:640px) { h2.template-message { font-size: 18px !important; }}
                          @media only screen and (max-width:480px) { table[class="body-wrap"]{ margin-top: 0% !important; width: 100% !important; } td[class="container"]{ width: 100% !important; } td[class="main-container"]{ width: 95% !important; padding: 15px 2.5% 20px 2.5% !important; } td[class="total-title"]{ font-size: 11px !important; letter-spacing: 0px !important; text-align: left !important; } td[class="questions"] { float: left !important; width: 100% !important; margin-bottom: 10px !important; text-align: center !important; } table[class="flexible"] { width: 100% !important; float: left !important; margin-bottom: 20px !important; border: 0 !important; text-align: center !important; } table[class="flexible"] td { text-align: center !important; } td[class="address"] { float: left !important; width: 100% !important; text-align: center !important; } td[class="footer-container"] { padding: 25px 2.5% 10px 2.5% !important; width: 95% !important; } h2.template-message { font-size: 16px !important; } img.template-logo { min-width: 100px !important; }}
                          @media print { table, td { box-shadow: none !important; margin: 0 auto !important; } h1, h2, h3, h4, h5, h6, p, td, tr, table, a { color: #000000 !important; } h3, h4, p { font-size: 12px !important; } img { width: 50% !important; margin: 0 auto !important; -webkit-filter: grayscale(100%) !important; -moz-filter: grayscale(100%) !important; filter: grayscale(100%) !important; } table.brand-wrap img { width: 15% !important; } td.brand { height: 10px !important; } td.brand-text { display: none !important; } td.footer-container { padding: 0 !important; } table.flexible tr td { font-size: 9px !important; } table.flexible img { width: 100% !important; }}
                        </style>
                        </head>
                        <body class="email-template" bgcolor="#e4e7e9" style="margin: 0;padding: 0;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;width: 100%;height: 100%;"><table class="bg" style="margin:0;padding:0;border-collapse:collapse;"><tr><td class="bg-gray" width="2560" height="100%" bgcolor="#e4e7e9" align="center"><table class="body-wrap" align="center" style="margin: 0;border-collapse: collapse;width: 100%;margin-top: 3%;padding: 0 10%;"><tr><td></td><td class="main-container" bgcolor="#FFFFFF" align="center" style="margin:0 auto;padding:20px 40px 20px 40px;display:block;max-width:600px;clear:both;width:80%;-webkit-border-top-left-radius: 3px;-webkit-border-top-right-radius: 3px;-moz-border-radius-topleft: 3px;-moz-border-radius-topright: 3px;border-top-left-radius: 3px;border-top-right-radius: 3px;-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.1);-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.1);box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.1);"><table width="100%"><tr><td align="center"><table><tr><td colspan="3" width="100%" height="20"></td></tr><tr><td width="5%"></td><td width="80%" align="center" valign="middle">


                              <!-- 1. Add link to location of your here - replace '.base_url('images/logo.png').' in the image source below with your logo location  -->
                              <img class="template-img template-logo" style="max-width:350px;width:100%;min-width:350px;" data-form="logo" src="http://localhost/rbctrades/images/logo.png" />
                              <!-- Step 1 Complete -->


                        </td><td width="5%"></td></tr><tr><td colspan="3" width="100%" height="20"></td></tr><tr><td width="10%"></td><td width="80%" align="center" valign="middle"><h2 class="template-h2 template-from" style="margin:0;padding:0;font-size: 28px;line-height: 1.3;color: #32373b;font-weight: 500;word-break: break-word;font-family: ,Helvetica,Arial,,sans-serif;">


                              <!-- 2. Amend Thank You title -->
                              Thank you for contacting us!
                              <!-- Step 2 Complete -->


                        </h2></td><td width="10%"></td></tr><tr><td colspan="3" width="100%" height="10"></td></tr><tr><td colspan="3" width="100%" align="center" valign="middle"><h2 class="template-h2 template-message" style="margin: 0;padding: 0;font-family: ,Helvetica,Arial,sans-serif;font-size:20px;line-height:1.5;color:#32373b;font-weight:300;letter-spacing:0px;word-break: break-word;">


                              <!-- 3. Write the message you would like to display on the email -->
                              RBC support received your enquiry, all information received will always remain confidential. We will contact you as soon as we review your message.
                              <!-- Step 3 Complete -->


                        </h2></td></tr><tr><td colspan="3" width="100%" height="20"></td></tr></table></td></tr></table></td><td></td></tr></table><table class="footer-wrap" align="center" style="margin: 0;border-collapse: collapse;width: 100%;margin-top: 0;padding: 0 10%;"><tr><td></td><td class="footer-container" style="margin: 0 auto; 3px;-moz-border-radius-bottomleft: 3px;-moz-border-radius-bottomright: 3px;border-bottom-left-radius: 3px;border-bottom-right-radius: 3px;-webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.1);-moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.1);box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.1);"><table style="margin: 0;padding: 0;border-collapse: collapse;width: 100%;"><tr><td align="center"><table class="flexible" style="width:49%;" align="left"><tr><td width="100%" style="font-family: , Helvetica, Arial, sans-serif;font-size: 12px;line-height: 1.6;color: #ffffff;text-transform: uppercase;font-weight: 800;letter-spacing: 0.5px;text-align:left;word-break: break-word;">

                        </body>
                        </html>';
        $message_admin = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

                        <!-- Facebook sharing information tags -->
                        <meta property="og:title" content="*|MC:SUBJECT|*" />

                        <title>*|MC:SUBJECT|*</title>
                                <style type="text/css">
                                        /* Client-specific Styles */
                                        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
                                        body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
                                        body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

                                        /* Reset Styles */
                                        body{margin:0; padding:0;}
                                        img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
                                        table td{border-collapse:collapse;}
                                        #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

                                        /* Template Styles */

                                        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */

                                        /**
                                        * @tab Page
                                        * @section background color
                                        /* @tip Set the background color for your email. You may want to choose one that matches your company.
                                        * @theme page
                                        */
                                        body, #backgroundTable{
                                                /*@editable*/ background-color:#FAFAFA;
                                        }

                                        /**
                                        * @tab Page
                                        * @section email border
                                        * @tip Set the border for your email.
                                        */
                                        #templateContainer{
                                                /*@editable*/ border: 1px solid #DDDDDD;
                                        }

                                        /**
                                        * @tab Page
                                        * @section heading 1
                                        * @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
                                        * @style heading 1
                                        */
                                        h1, .h1{
                                                /*@editable*/ color:#202020;
                                                display:block;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:34px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ line-height:100%;
                                                margin-top:0;
                                                margin-right:0;
                                                margin-bottom:10px;
                                                margin-left:0;
                                                /*@editable*/ text-align:left;
                                        }

                                        /**
                                        * @tab Page
                                        * @section heading 2
                                        * @tip Set the styling for all second-level headings in your emails.
                                        * @style heading 2
                                        */
                                        h2, .h2{
                                                /*@editable*/ color:#202020;
                                                display:block;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:30px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ line-height:100%;
                                                margin-top:0;
                                                margin-right:0;
                                                margin-bottom:10px;
                                                margin-left:0;
                                                /*@editable*/ text-align:left;
                                        }

                                        /**
                                        * @tab Page
                                        * @section heading 3
                                        * @tip Set the styling for all third-level headings in your emails.
                                        * @style heading 3
                                        */
                                        h3, .h3{
                                                /*@editable*/ color:#202020;
                                                display:block;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:26px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ line-height:100%;
                                                margin-top:0;
                                                margin-right:0;
                                                margin-bottom:10px;
                                                margin-left:0;
                                                /*@editable*/ text-align:left;
                                        }

                                        /**
                                        * @tab Page
                                        * @section heading 4
                                        * @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
                                        * @style heading 4
                                        */
                                        h4, .h4{
                                                /*@editable*/ color:#202020;
                                                display:block;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:22px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ line-height:100%;
                                                margin-top:0;
                                                margin-right:0;
                                                margin-bottom:10px;
                                                margin-left:0;
                                                /*@editable*/ text-align:left;
                                        }

                                        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */

                                        /**
                                        * @tab Header
                                        * @section header style
                                        * @tip Set the background color and border for your email.
                                        * @theme header
                                        */
                                        #templateHeader{
                                                /*@editable*/ background-color:#FFFFFF;
                                                /*@editable*/ border-bottom:0;
                                        }

                                        /**
                                        * @tab Header
                                        * @section header text
                                        * @tip Set the styling for your emails header text. Choose a size and color that is easy to read.
                                        */
                                        .headerContent{
                                                /*@editable*/ color:#202020;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:34px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ line-height:100%;
                                                /*@editable*/ padding:0;
                                                /*@editable*/ text-align:center;
                                                /*@editable*/ vertical-align:middle;
                                        }

                                        /**
                                        * @tab Header
                                        * @section header link
                                        * @tip Set the styling for your emails header links. Choose a color that helps them stand out from your text.
                                        */
                                        .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
                                                /*@editable*/ color:#336699;
                                                /*@editable*/ font-weight:normal;
                                                /*@editable*/ text-decoration:underline;
                                        }

                                        #headerImage{
                                                height:auto;
                                                max-width:600px !important;
                                        }

                                        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */

                                        /**
                                        * @tab Body
                                        * @section body style
                                        * @tip Set the background color for your emails body area.
                                        */
                                        #templateContainer, .bodyContent{
                                                /*@editable*/ background-color:#FFFFFF;
                                        }

                                        /**
                                        * @tab Body
                                        * @section body text
                                        * @tip Set the styling for your emails main content text. Choose a size and color that is easy to read.
                                        * @theme main
                                        */
                                        .bodyContent div{
                                                /*@editable*/ color:#505050;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:14px;
                                                /*@editable*/ line-height:150%;
                                                /*@editable*/ text-align:left;
                                        }

                                        /**
                                        * @tab Body
                                        * @section body link
                                        * @tip Set the styling for your emails main content links. Choose a color that helps them stand out from your text.
                                        */
                                        .bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
                                                /*@editable*/ color:#336699;
                                                /*@editable*/ font-weight:normal;
                                                /*@editable*/ text-decoration:underline;
                                        }

                                        /**
                                        * @tab Body
                                        * @section data table style
                                        * @tip Set the background color and border for your emails data table.
                                        */
                                        .templateDataTable{
                                                /*@editable*/ background-color:#FFFFFF;
                                                /*@editable*/ border:1px solid #DDDDDD;
                                        }

                                        /**
                                        * @tab Body
                                        * @section data table heading text
                                        * @tip Set the styling for your emails data table text. Choose a size and color that is easy to read.
                                        */
                                        .dataTableHeading{
                                                /*@editable*/ background-color:#D8E2EA;
                                                /*@editable*/ color:#336699;
                                                /*@editable*/ font-family:Helvetica;
                                                /*@editable*/ font-size:14px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ line-height:150%;
                                                /*@editable*/ text-align:left;
                                        }

                                        /**
                                        * @tab Body
                                        * @section data table heading link
                                        * @tip Set the styling for your emails data table links. Choose a color that helps them stand out from your text.
                                        */
                                        .dataTableHeading a:link, .dataTableHeading a:visited, /* Yahoo! Mail Override */ .dataTableHeading a .yshortcuts /* Yahoo! Mail Override */{
                                                /*@editable*/ color:#FFFFFF;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ text-decoration:underline;
                                        }

                                        /**
                                        * @tab Body
                                        * @section data table text
                                        * @tip Set the styling for your emails data table text. Choose a size and color that is easy to read.
                                        */
                                        .dataTableContent{
                                                /*@editable*/ border-top:1px solid #DDDDDD;
                                                /*@editable*/ border-bottom:0;
                                                /*@editable*/ color:#202020;
                                                /*@editable*/ font-family:Helvetica;
                                                /*@editable*/ font-size:12px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ line-height:150%;
                                                /*@editable*/ text-align:left;
                                        }

                                        /**
                                        * @tab Body
                                        * @section data table link
                                        * @tip Set the styling for your emails data table links. Choose a color that helps them stand out from your text.
                                        */
                                        .dataTableContent a:link, .dataTableContent a:visited, /* Yahoo! Mail Override */ .dataTableContent a .yshortcuts /* Yahoo! Mail Override */{
                                                /*@editable*/ color:#202020;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ text-decoration:underline;
                                        }

                                        /**
                                        * @tab Body
                                        * @section button style
                                        * @tip Set the styling for your emails button. Choose a style that draws attention.
                                        */
                                        .templateButton{
                                                -moz-border-radius:3px;
                                                -webkit-border-radius:3px;
                                                /*@editable*/ background-color:#336699;
                                                /*@editable*/ border:0;
                                                border-collapse:separate !important;
                                                border-radius:3px;
                                        }

                                        /**
                                        * @tab Body
                                        * @section button style
                                        * @tip Set the styling for your emails button. Choose a style that draws attention.
                                        */
                                        .templateButton, .templateButton a:link, .templateButton a:visited, /* Yahoo! Mail Override */ .templateButton a .yshortcuts /* Yahoo! Mail Override */{
                                                /*@editable*/ color:#FFFFFF;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:15px;
                                                /*@editable*/ font-weight:bold;
                                                /*@editable*/ letter-spacing:-.5px;
                                                /*@editable*/ line-height:100%;
                                                text-align:center;
                                                text-decoration:none;
                                        }

                                        .bodyContent img{
                                                display:inline;
                                                height:auto;
                                        }

                                        /* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */

                                        /**
                                        * @tab Footer
                                        * @section footer style
                                        * @tip Set the background color and top border for your emails footer area.
                                        * @theme footer
                                        */
                                        #templateFooter{
                                                /*@editable*/ background-color:#FFFFFF;
                                                /*@editable*/ border-top:0;
                                        }

                                        /**
                                        * @tab Footer
                                        * @section footer text
                                        * @tip Set the styling for your emails footer text. Choose a size and color that is easy to read.
                                        * @theme footer
                                        */
                                        .footerContent div{
                                                /*@editable*/ color:#707070;
                                                /*@editable*/ font-family:Arial;
                                                /*@editable*/ font-size:12px;
                                                /*@editable*/ line-height:125%;
                                                /*@editable*/ text-align:center;
                                        }

                                        /**
                                        * @tab Footer
                                        * @section footer link
                                        * @tip Set the styling for your emails footer links. Choose a color that helps them stand out from your text.
                                        */
                                        .footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
                                                /*@editable*/ color:#336699;
                                                /*@editable*/ font-weight:normal;
                                                /*@editable*/ text-decoration:underline;
                                        }

                                        .footerContent img{
                                                display:inline;
                                        }

                                        /**
                                        * @tab Footer
                                        * @section utility bar style
                                        * @tip Set the background color and border for your emails footer utility bar.
                                        * @theme footer
                                        */
                                        #utility{
                                                /*@editable*/ background-color:#FFFFFF;
                                                /*@editable*/ border:0;
                                        }

                                        /**
                                        * @tab Footer
                                        * @section utility bar style
                                        * @tip Set the background color and border for your emails footer utility bar.
                                        */
                                        #utility div{
                                                /*@editable*/ text-align:center;
                                        }

                                        #monkeyRewards img{
                                                max-width:190px;
                                        }
                                </style>
                        </head>
                    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
                        <center>
                                <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
                                <tr>
                                        <td align="center" valign="top" style="padding-top:20px;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                                                <tr>
                                                <td align="center" valign="top">
                                                    <!-- // Begin Template Header \\ -->
                                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">

                                                    </table>
                                                    <!-- // End Template Header \\ -->
                                                </td>
                                            </tr>
                                                <tr>
                                                <td align="center" valign="top">
                                                    <!-- // Begin Template Body \\ -->
                                                        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                                        <tr>
                                                            <td valign="top">

                                                                <!-- // Begin Module: Standard Content \\ -->
                                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">

                                                                    <tr>
                                                                        <td valign="top" style="padding-top:0; padding-bottom:0;">
                                                                          <table border="0" cellpadding="10" cellspacing="0" width="100%" class="templateDataTable">
                                                                              <tr>

                                                                                  <th scope="col" valign="top" width="25%" class="dataTableHeading" mc:edit="data_table_heading01">
                                                                                   Sender s Name
                                                                                  </th>
                                                                                  <th scope="col" valign="top" width="50%" class="dataTableHeading" mc:edit="data_table_heading02">
                                                                                    Sender s Email
                                                                                  </th>
                                                                              </tr>
                                                                              <tr mc:repeatable>

                                                                                  <td valign="top" class="dataTableContent" mc:edit="data_table_content01">
                                                                                      ' . $fname . '  ' . $lname . '
                                                                                  </td>
                                                                                  <td valign="top" class="dataTableContent" mc:edit="data_table_content02">
                                                                                    ' . $email . '
                                                                                  </td>
                                                                              </tr>
                                                                          </table>
                                                                        </td>
                                                                    </tr>


                                                                </table>
                                                                <!-- // End Module: Standard Content \\ -->



                                                            </td>
                                                                   <!-- // Begin Module: Standard Content \\ -->
                                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">

                                                                    <tr>
                                                                        <td valign="top" style="padding-top:0; padding-bottom:0;">
                                                                          <table border="0" cellpadding="10" cellspacing="0" width="100%" class="templateDataTable">
                                                                              <tr>

                                                                                  <th scope="col" valign="top" width="25%" class="dataTableHeading" mc:edit="data_table_heading01">
                                                                                   Sender s Message
                                                                                  </th>

                                                                              </tr>
                                                                              <tr mc:repeatable>

                                                                                  <td valign="top" class="dataTableContent" mc:edit="data_table_content01">
                                                                                ' . $message . '
                                                                                  </td>

                                                                              </tr>
                                                                          </table>
                                                                        </td>
                                                                    </tr>


                                                                </table>
                                                                <!-- // End Module: Standard Content \\ -->
                                                        </tr>
                                                    </table>
                                                    <!-- // End Template Body \\ -->


                                                </td>
                                            </tr>

                                        </table>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </center>
                    </body>
                </html>';
            $this->send_email($message_for_user, $email,"(RBC) Thank You for Feedback");
            $this->send_email($message_admin, "master@rbctrades.com","User Feedback");
            $this->session->set_flashdata("email_sent","Your Query sent to RBC Trades, Our Suport Team will contact you soon");
            redirect('main/contact_us');
            //print_r($user_email);
            
        }
    }

    public function how_to_buy() {
        $this->load->view('how_to_buy');
    }

    public function how_to_sell() {
        $this->load->view('how_to_sell');
    }

    public function terms_conditions() {
        $this->load->view('terms_conditions');
    }

//START Zain
    public function category_listing() {
        $this->load->model('rbc_category_model');
        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['all_category'] = $this->rbc_category_model->get_by_sub_category();
        $this->load->view('category_listing', $data);
    }

    public function all_category_listing_filtration() {

        $this->load->model('rbc_category_model');

        $nodelist = $this->fetch_recursive($this->rbc_category_model->get_all(), 1);

        echo json_encode($nodelist);
    }

    public function fetch_recursive($src_arr, $id, $parentfound = false, $cats = array()) {
        foreach ($src_arr as $row) {
            if ((!$parentfound && $row['category_id'] == $id) || $row['parent_id'] == $id) {
                $rowdata = array();
                foreach ($row as $k => $v)
                    $rowdata[$k] = $v;
                $cats[] = $rowdata;
                if ($row['parent_id'] == $id)
                    $cats = array_merge($cats, $this->fetch_recursive($src_arr, $row['category_id'], true));
            }
        }
        return $cats;
    }

    public function get_product_search() {
        // print_r($_POST);
        $this->load->model('product_master_model');
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->product_master_model->get_search_result($q);
        }
    }
    public function get_supplier_search(){
        $this->load->model('company_master_model');
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->company_master_model->get_supplier_search($q);
        }
    }
    public function get_buyer_search(){
        $this->load->model('rfq_details_model');
        if (isset($_GET['term'])) {
            $q = strtolower($_GET['term']);
            $this->rfq_details_model->get_buyer_search($q);
        }
    }
    public function live_search(){
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_category_model');

        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['categories'] = $this->rbc_category_model->get_by_category();
        $data['term'] = $_POST['term'];
        $data['search_by'] = $_POST['search_by'];
        
//        echo '<pre>';
//        print_r($data);
//        exit();
        
        
        $this->load->view('search',$data);

    }

    public function pagination_product_seraching() {

        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $this->load->model('rfq_master_model');
        $page_number = $this->input->post('page_number');
        $term = $this->input->post('term');
        $search_by = $this->input->post('search_by');
        $item_par_page = 3;
        $position = ($page_number * $item_par_page);

        if($search_by === "products"){
//            echo '<pre>';
//             print_r($page_number);
//             print_r($term);
//             print_r($search_by);
//            exit();
            $result_set = $this->product_master_model->pagination_search_product($item_par_page, $position,$term);
            $total = $this->product_master_model->pagination_search_product_count($term)->num_rows();
                
        }
        else if($search_by === "buyers"){
            $result_set = $this->rfq_master_model->pagination_search_quotation($item_par_page, $position,$term);
            $total = $this->rfq_master_model->pagination_search_quotation_count($term)->num_rows();
        }
        else
        {
            
            print_r("ELSE HERE");
            exit();
        }

        $total_set = $result_set->num_rows();
        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            foreach ($result_set->result() as $product) {

                if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $entries[] = $product;
                    }

            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }
    public function forgot_password(){
        $this->load->model('rbc_user_model');
    
        if ($this->input->post('forgot_password'))
        {
             $email = $_POST['username'];
             $result = $this->rbc_user_model->get_by_email($email);
//            print_r($result);
//            exit();
            if($result !=NULL || $result !=""){

                
                
                $data['hash_key'] = $this->random_string(10);
                $data['user_email'] = $email;
                
                $this->rbc_user_model->update_hash($email,$data['hash_key']);
                $message = 'Please use the following link to change your password.<br>' . base_url() . 'index.php/main/change_password?key='.$data['hash_key'].'&user='.$data['user_email'].'';
                $to = $email;
                $subject = "(RBC)Password change request";
                if($this->send_email($message,$to,$subject))
                {
                    $this->session->set_flashdata('msg', 'Password change request has been sent to your email. Please check it.');
                }else{
                    
                    $this->session->set_flashdata('msg', 'Email could not be sent Please try again latter.');
                }
                
                redirect('/main/forgot_password/');
                
            }
            else{
        $this->session->set_flashdata('msg', 'No such Email');
        redirect('/main/forgot_password/');
            }
        }
    else{
        $this->load->view('forgot_password');
    }
    }
    public function change_password(){
        $this->load->model('rbc_user_model');
        
        $data['hash_key'] =$_GET['key'];
        $data['email'] =$_GET['user'];;
                
       if ($this->input->post('change_password'))
        {
                      
           $hash_key = $_POST['hash_key'];
           $email = $_POST['email'];
           $password = $_POST['password'];
           
//           print_r($_POST);
//           exit();
           $result = $this->rbc_user_model->get_by_email($email);
           $pre_date = new DateTime($result->hash_key_time);
            $now = new DateTime();
            $diff = date_diff($pre_date, $now);
            
            
           if($result->hash_key === $hash_key && $diff->d<1){
               
            $this->rbc_user_model->change_password($email,$password);
            $this->session->set_flashdata('msg', 'Password Changed');
                redirect('/login/');
            
           }
           else{
               $this->session->set_flashdata('msg', 'Unauthorized Access.');
               redirect('/main/forgot_password/');
               
           }
          }
        
       else if(($_GET['key'] !=NULL || $_GET['key'] !="") && ($_GET['user'] !=NULL || $_GET['user'] !="")){ 
           
       $this->load->view('change_password',$data);
       
       }
       else{
       redirect('/main/index/');
       
       }
    }
    
    

//END Zain
//START Asad
    public function register_fb_user() {
        //print_r($_POST);exit();
        $this->load->model('rbc_user_model');
        $this->load->model('member_master_model');
        $this->load->model('company_master_model');
        $this->load->model('company_privacy_setting_model');
        $data = $_POST;
        $data['member_type'] = "both";
        $data['user_password'] = $this->random_string(6);
        $data['company_id'] = $this->company_master_model->create($data);
        $data['member_id'] = $this->member_master_model->create($data);
        $data['user_id'] = $this->rbc_user_model->create($data);
        $this->company_privacy_setting_model->create($data);
        $user = $this->rbc_user_model->get_by_id($data['user_id']);
        $company_id = $this->member_master_model->get_company_id($user->member_id);
        $user_name = explode(' ', $user->user_name);
        $session = array(
            'user_name' => $user_name[0],
            'user_id' => $user->user_id,
            'member_id' => $user->member_id,
            'company_id' => $company_id->company_id,
            'is_logged_in' => TRUE,
            'product_ids' => "",
            'compare_count'=>0
        );
        $this->session->set_userdata($session);
        redirect(base_url());
    }

    public function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function send_email($message,$to,$subject) {
        
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.rbctrades.com',
            'smtp_port' => 587,
            'smtp_user' => 'master@rbctrades.com',
            'smtp_pass' => 'rbctrade123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('master@rbctrades.com', 'RBC Trade');
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $path = $this->config->item('server_root');
        $file = $path . 'codeignator';
        if ($this->email->send()) {
            return true;
        } else {
            //show_error($this->email->print_debugger());
            return false;
        }
    }
    public function compare_product(){

        //print_r($this->session->userdata);exit();
        if($this->session->userdata('product_ids')){
            $compare_id = explode("|",$this->session->userdata('product_ids'));

            $this->load->model('product_master_model');
            foreach($compare_id as $product_id){
                if(!empty($product_id)){
                    $data['product_details'][] = $this->product_master_model->get_product_by_id($product_id);
                }
            }

        }
//        echo '<pre>';
//       print_r($data['product_details']);exit();
        $this->session->set_userdata('product_ids',"");
        $this->session->set_userdata('compare_count',0);
        $this->load->view('shop_product_comparison',$data);
    }
    public function set_compare_session(){
        $product_id = $this->session->userdata('product_ids');
        $count = $this->session->userdata('compare_count');
        if(empty($count)){
            $count = 0;
        }
        if($count<4){
            $product_id = $product_id.'|'.$this->input->post('id');
            ++$count;
            $data = array (
                'product_ids' => $product_id,
                'compare_count' => $count
            );
            $this->session->set_userdata($data);

            print_r($this->session->userdata);
        }
        if($count==3 || $count>3){
            echo "1";
        }
    }
    public function remove_session_value(){

        $product_id = $this->session->userdata('product_ids');
        $count = $this->session->userdata('compare_count');
        $find_id = '|'.$this->input->post('id');
        $id = str_replace($find_id,"",$product_id);
        $count = --$count;
        $data = array(
            'product_ids' => $id,
            'compare_count' => $count
        );
        $this->session->set_userdata($data);
        print_r($this->session->userdata);
    }
    public function quotation_listing() {
        $this->load->model('rfq_master_model');
//        $this->load->model('rfq_details_model');
        $this->load->model('rbc_countries_model');
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['quots'] = $this->rfq_master_model->get_all_data();
//        echo '<pre>';
//        print_r($data);
//                exit();

        $this->load->view('quotation_listing', $data);
    }

    public function quotation_listing_quantity_filtration() {
        $this->load->model('rfq_master_model');
        $page_number = $this->input->post('page_number');
        $min_qty = $this->input->post('min_qty');
        $max_qty = $this->input->post('max_qty');

        $item_par_page = 1;
        $position = ($page_number * $item_par_page);
        if ($min_qty != 0 && $max_qty != 0) {

            $result_set = $this->rfq_master_model->get_all_by_quantity_range($item_par_page, $position, $min_qty, $max_qty);
            $total = $this->rfq_master_model->get_all_by_quantity_range_count($min_qty, $max_qty)->num_rows();

        } else if ($min_qty != 0 && ($max_qty == 0 || $max_qty == null)) {

            $result_set = $this->rfq_master_model->get_all_by_quantity_minimum($item_par_page, $position, $min_qty);
            $total = $this->rfq_master_model->get_all_by_quantity_min_count($min_qty)->num_rows();
        } else if (($max_qty != 0 && ($min_qty == 0 || $min_qty == null))) {

            $result_set = $this->rfq_master_model->get_all_by_quantity_maximum($item_par_page, $position, $max_qty);
            $total = $this->rfq_master_model->get_all_by_quantity_max_count($max_qty)->num_rows();
        } else {
            $result_set = $this->rfq_master_model->get_all_data_pagination($item_par_page, $position);
            $total = $this->rfq_master_model->get_all_data_all()->num_rows();
        }
        $total_set = $result_set->num_rows();
//        $page = $this->rfq_master_model->get_all_data_all();

        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            foreach ($result_set->result() as $row) {
                $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }

    public function quotation_listing_country_filtration() {

        $this->load->model('rfq_master_model');
        $page_number = $this->input->post('page_number');
        $country_id = $this->input->post('country');



        $item_par_page = 1;
        $position = ($page_number * $item_par_page);
        //$user_email = $this->session->userdata('email');

        if ($country_id>0) {
//            print_r("IF");
//                    print_r($country_id);
//                    exit();
        $result_set = $this->rfq_master_model->get_by_country_pagination($item_par_page, $position,$country_id);
        $total = $this->rfq_master_model->get_by_country_pagination_count($country_id)->num_rows();

        }  else {

            $result_set = $this->rfq_master_model->get_all_data_pagination($item_par_page, $position);
            $total = $this->rfq_master_model->get_all_data_all()->num_rows();
        }
        $total_set = $result_set->num_rows();
        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            foreach ($result_set->result() as $row) {
                $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }

    public function quotation_listing_open_quotes_filtration() {
        $this->load->model('rfq_master_model');
        $page_number = $this->input->post('page_number');
        $quote = $this->input->post('quotes');


        $item_par_page = 1;
        $position = ($page_number * $item_par_page);
        //$user_email = $this->session->userdata('email');

        if ($quote=='true') {
        $result_set = $this->rfq_master_model->get_all_open_quotes_pagination($item_par_page, $position);
            $total = $this->rfq_master_model->get_all_open_quotes_pagging()->num_rows();

        }  else {
            $result_set = $this->rfq_master_model->get_all_data_pagination($item_par_page, $position);
            $total = $this->rfq_master_model->get_all_data_all()->num_rows();
        }
        $total_set = $result_set->num_rows();
        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            foreach ($result_set->result() as $row) {
                $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }
    public function quotation_listing_all_quotes_filtration() {
        $this->load->model('rfq_master_model');
        $page_number = $this->input->post('page_number');

        $item_par_page = 1;
        $position = ($page_number * $item_par_page);
        //$user_email = $this->session->userdata('email');
        $result_set = $this->rfq_master_model->get_all_data_pagination($item_par_page, $position);
        $total_set = $result_set->num_rows();
        $page = $this->rfq_master_model->get_all_data_all();
        $total = $page->num_rows();
        //break total recoed into pages
        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            foreach ($result_set->result() as $row) {
                $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }
    public function quotation_detail() {
        $this->load->model('rfq_master_model');
        $this->load->model('rfq_details_model');
        $this->load->model('quotation_master_model');
        
        $quot_id = $this->uri->segment(3);
        $quote = $this->rfq_master_model->get_by_id($quot_id);
        $details = $this->rfq_details_model->get_by_quot($quot_id);
        
        $quote->items = $details;
        $data ['quote'] = $quote;
        $data ['seller_details'] = $this->quotation_master_model->get_quote_by_id($quot_id);
//        $now = new DateTime();
//        $future_date = new DateTime(strtotime($quote->rfq_expiry_date,$now));

//        $interval = $now->diff($future_date);

//echo $interval->format("%d days, %h hours, %i minutes, %s seconds");
//        $quote->time_remaining = $interval->format("%m month,%d days, %h hours, %i minutes, %s seconds");
//        echo '<pre>';
//        print_r($interval->format("%m month,%d days, %h hours, %i minutes, %s seconds"));
        
//        print_r($future_date);
//        
//         echo '<pre>';
//        print_r($data);
//                exit();

        $this->load->view('quotation_detail', $data);
    }
    public function help_us(){
        $this->load->model('rbc_help_us_model');
        $data = $_POST;
        $data['user_id'] = $this->session->userdata('user_id');
        if($this->rbc_help_us_model->create($data)){
            echo "1";
        }else{
            echo "0";
    }
    }
    public function site_map() {
        $this->load->view('site_map');
    }
    public function regenerate_hash_key(){
        $email = $this->uri->segment(3);
        $data['hash_key'] = $this->random_string(10);
        $this->rbc_user_model->update_hash_key($email,$data);
        $this->session->set_flashdata('signed_up', 'Verification Link Sent successfully, please check your email for login');
        redirect('login/');
    }
//END Asad
//START Fayyaz
//END Fayyaz
}
