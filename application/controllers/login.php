<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
    }

    private function is_logged_in() {
        return $this->session->userdata('is_logged_in');
    }

    public function index() {
        if (!$this->is_logged_in()) {

            if($this->session->userdata('referred_from') == 'http://'.$_SERVER['HTTP_HOST'].'/bitbucket/rbctrades/index.php/buyer/pagination_product'){

                $this->session->set_userdata('referred_from', 'http://'.$_SERVER['HTTP_HOST'].'/bitbucket/rbctrades/index.php/buyer/product_listing');
            }
            // print_r($this->session->userdata('referred_from'));exit();
            $this->load->view('login');
        } else {
            redirect(base_url());
        }
//        $redirect_url = $this->session->userdata('redirect_back');
//        echo $redirect_url;
    }

    public function check() {
        //print_r($_POST);exit();
        $hash_key = $this->input->get('key');
        $this->load->model('member_master_model');
        $this->load->library('form_validation');
        if ($_POST) {
            $this->form_validation->set_rules('username', 'User Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            //exit($this->input->post('user'));
            if ($this->form_validation->run() == FALSE && $this->input->post('g-recaptcha-response') == "") {
                redirect('/login/error');
            } else {

                $user = $this->rbc_user_model->check_user($this->input->post('username'), $this->input->post('password'));
                $company_id = $this->member_master_model->get_company_id($user->member_id);
                if ($user != NULL) {
                    $this->session->unset_userdata('attempt');
                    $this->session->unset_userdata('captcha');
                    $user_name = explode(' ', $user->user_name);
                    $data = array(
                        'user_name' => $user_name[0],
                        'user_id' => $user->user_id,
                        'member_id' => $user->member_id,
                        'company_id' => $company_id->company_id,
                        'email' => $user->user_email,
                        'name' => $user->user_name,
                        'member_type' => $company_id->member_type,
                        'is_logged_in' => TRUE,
                        'product_ids' => "",
                        'compare_count'=>0
                    );
                    $this->session->set_userdata($data);
                    $referred_from = $this->session->userdata('referred_from');
                    if($referred_from == base_url('index.php/buyer/pagination_product_by_category')){
                        $referred_from = base_url('index.php/buyer/product_by_category_listing');
                    }
                    redirect($referred_from, 'refresh');
                } else {

                    $login_attempt = $this->session->userdata('attempt');
                    if (empty($login_attempt)) {
                        $login_attempt = 0;
                    }


                    if ($login_attempt < 10) {
                        $login_attempt = ++$login_attempt;
                        $data = array(
                            "attempt" => $login_attempt,
                            "captcha" => 0
                        );
                    } else {
                        $data = array(
                            "attempt" => $login_attempt++,
                            "captcha" => 1
                        );
                    }
                    $this->session->set_userdata($data);
                    $this->session->set_flashdata('msg', 'Username or password incorrect');
                    redirect('/login/');
                }
            }
        } else if ($_GET['key'] && $_GET['user'] && $_GET['pass']) {
            $key = $_GET['key'];
            $user = htmlspecialchars(pg_escape_string($_GET['user']));
            $password = htmlspecialchars(pg_escape_string($_GET['pass']));
            $data = $this->rbc_user_model->get_hash_key_time($key);
            $pre_date = new DateTime($data->hash_key_time);
            $now = new DateTime();
            $diff = date_diff($now, $pre_date);
            //print_r($diff);
            //exit();
            if ($diff->h < 24 && $diff->h > 0) {
                $user = $this->rbc_user_model->check_user($user, $password);
                $company_id = $this->member_master_model->get_company_id($user->member_id);
                if ($user != NULL) {
                    $user_name = explode(' ', $user->user_name);
                    $data = array(
                        'user_name' => $user_name[0],
                        'user_id' => $user->user_id,
                        'member_id' => $user->member_id,
                        'company_id' => $company_id->company_id,
                        'email' => $user->user_email,
                        'name' => $user->user_name,
                        'member_type' => $company_id->member_type,
                        'is_logged_in' => TRUE
                    );
                    $this->session->set_userdata($data);
                    $referred_from = $this->session->userdata('referred_from');
                    $status = 1;
                    //print_r($user);
                    $this->rbc_user_model->update_user_status($user->user_id,$status);
                    $this->rbc_user_model->delete_hash($user->user_id);
                    redirect($referred_from, 'refresh');
                } else {
                    redirect('login/error');
                }
            } else {
                //session expired please signup again
                $this->session->set_flashdata('invalid_key', 'Your Session Has been Expired Please Click <a href='.base_url('index.php/main/regenerate_hash_key/'.$user.'').'>Here</a> to Regenerate verify link');
                redirect('login/error');
            }
        }
    }

    public function error() {
        $this->load->view('login', array('error' => TRUE));
    }

    public function logout() {
        $this->session->set_userdata('redirect_back', $this->agent->referrer());
        $redirect_url = $this->session->userdata('redirect_back');
        $this->session->unset_userdata('redirect_back');
        $this->session->set_userdata(array('is_logged_in' => FALSE));
        $this->session->sess_destroy();
        redirect($redirect_url);
    }

//START Zain
//END Zain
//START Asad
    public function check_facebook_user() {
        $this->load->model('member_master_model');
        $data['facebook_uid'] = $this->session->userdata('uid');
        $data['user_name'] = $this->session->userdata('name');
        $data['user_email'] = $this->session->userdata('email');
        $data['user_location'] = $this->session->userdata('location');
        $data['user_image'] = $this->session->userdata('image');
        $data['urls'] = $this->session->userdata('urls');
        $user = $this->rbc_user_model->check_facebook_user($data['facebook_uid']);
        if ($user != NULL) {
            $company_id = $this->member_master_model->get_company_id($user->member_id);
            $user_name = explode(' ', $user->user_name);
            $data = array(
                'user_name' => $user_name[0],
                'user_id' => $user->user_id,
                'member_id' => $user->member_id,
                'company_id' => $company_id->company_id,
                'is_logged_in' => TRUE,
                'email' => $user->user_email,
                'name' => $user->user_name,
                'member_type' => $company_id->member_type,
                'product_ids' => "",
                'compare_count'=>0
            );
            $this->session->set_userdata($data);
            $referred_from = $this->session->userdata('referred_from');
            redirect($referred_from, 'refresh');
        } else {
            $this->load->model('rbc_countries_model');
            $data['countries'] = $this->rbc_countries_model->get_all();
            $this->load->view('register_facebook_user', $data);
        }
    }

//END Asad
//START Fayyaz
//END Fayyaz
}
