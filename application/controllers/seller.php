<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Seller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
        if (!$this->is_logged_in()) {
            $this->session->set_userdata('referred_from', current_url());
            redirect('login/');
        }
    }

    private function is_logged_in() {
        return $this->session->userdata('is_logged_in');
    }

    public function index() {
        $this->load->view('buying_leeds');
    }

    public function view_buying_leeds() {

        $data['categories'] = $this->rbc_category_model->get_by_category();
        $data['sub_categories'] = $this->rbc_category_model->get_by_sub_category();
        $this->load->view('buying_leeds', $data);
    }

    public function manage_products() {
        //  $this->load->model('rbc_products_model');
        // $data['products']=$this->rbc_products_model->get_all();
        // $data['active_count'] = $this->rbc_products_model->active_product_count($this->session->userdata('uid'));
        $this->load->model('product_master_model');
        $this->load->model('product_member_model');
        $this->load->model('member_master_model');
        $data['owner'] = $this->member_master_model->get_by_id($this->session->userdata('member_id'));
        $products = $this->product_member_model->get_products_of_user($this->session->userdata('member_id'));
        //print_r($products);exit();
        foreach ($products as $product) {
            $pending_products = $this->product_master_model->get_all_pending_products($product->product_id);
            if ($pending_products) {
                $data['pending_products'][] = $pending_products;
            }
            $active_products = $this->product_master_model->get_all_active_products($product->product_id);
            if ($active_products) {
                $data['active_products'][] = $active_products;
            }
        }
        $this->load->view('manage_products', $data);
    }

    public function message_center() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $user_email = $this->session->userdata('email');
        $data['inbox_inquies'] = $this->message_center_model->get_information($user_email);
        $this->load->view('message_center', $data);
    }

    public function pagination() {
        $page_number = $this->input->post('page_number');
        $item_par_page = 3;
        $position = ($page_number * $item_par_page);
        $user_email = $this->session->userdata('email');
        $result_set = $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '1' AND message_center_mapped.main_user_email = '" . $user_email . "' LIMIT " . $position . "," . $item_par_page);
        $total_set = $result_set->num_rows();
        $page = $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '1' AND message_center_mapped.main_user_email = '" . $user_email . "'");
        $total = $page->num_rows();
        //break total recoed into pages
        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            foreach ($result_set->result() as $row) {
                $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }

    public function add_new_product() {
        $this->load->model('product_member_model');
        $this->load->model('product_master_model');
        $this->load->model('rbc_category_model');
        $this->load->model('product_images_model');
        $this->load->model('rbc_uom_model');
        $this->load->model('rbc_currency_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_payment_type_model');
        $this->load->model('product_specification_model');
        $data['currency'] = $this->rbc_currency_model->get_all();
        $data['payment_types'] = $this->rbc_payment_type_model->get_all();
        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['all_category'] = $this->rbc_category_model->get_by_sub_category();
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['uom'] = $this->rbc_uom_model->get_all();
//            echo '<pre>';
//            print_r($data['uom']);exit();
        $this->load->view('new_product', $data);
        if ($this->input->post('add_product')) {

//                echo '<pre>';
//                echo print_r($_FILES);
//                 echo print_r($_POST);exit();
            //$this->rbc_user_model->get_by_id($this->session->userdata('uid'));
            //print_r($_post);exit();
            $payment_type = $this->input->post('product_payment_type');
            $payment_type_value = "";
            foreach ($payment_type as $values) {
                if (!empty($values)) {
                    $payment_type_value .= $values;
                    $payment_type_value .= "|";
                }
            }
            //print_r($payment_type_value);exit();
            $product = $_POST;
            
//            echo '<pre>';
//            print_r($product);
//            
            //print_r($data);
            //exit();
            $product['product_detailed_description'] = $_POST['editor1'];
            $product['product_payment_type'] = $payment_type_value;
            $data['member_id'] = $this->session->userdata('member_id');
            $product_name = explode(" ",strtolower($product['product_name']));
            //print_r($product_name);
            for($j=0; $j<5;$j++){
                $product_slug .= $product_name[$j]."-";
            }
            if(sizeof($product_slug) >35){
                $product_slug = substr($product_slug, 0, 35);
            }
            $product_slug .= $this->session->userdata('member_id');
            $product_slug .= $this->random_string();
            $product['product_slug'] = $product_slug;
            //print_r($product_slug);exit();
            $product_id = $this->product_master_model->create($product);
            $count = 0;
            $main_img = 1;
            $upload_path = 'uploads/product_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }
            for ($i = 0; $i < sizeof($_FILES['product_image']['name']); $i++) {
                if (!empty($_FILES['product_image']['name'][$i]) && $_FILES['product_image']['error'][$i] == 0) {
                    $type = explode(".", $_FILES['product_image']['name'][$i]);
                    //echo $_FILES['product_image']['name'][$i];
                    $_FILES['product_image']['name'][$i] = $this->random_string() . "." . $type[1];
                    //echo $_FILES['product_image']['name'][$i];
                    move_uploaded_file($_FILES['product_image']['tmp_name'][$i], $upload_path . $_FILES['product_image']['name'][$i]);
                    $images['image_url'] = base_url() . $upload_path . $_FILES['product_image']['name'][$i];
                    $images['product_id'] = $product_id;
                    $images['is_main'] = $main_img;
                    $this->product_images_model->create($images);
                    $main_img = 0;
                    //redirect('/seller/manage_products/');
                }
            }
            $data['product_id'] = $product_id;
            $this->product_member_model->create($data);
            
            for($j=0; $j<=sizeof($_POST['extra_details_attributes']);$j++){
                if(!empty($_POST['extra_details_attributes'][$j]) && !empty($_POST['extra_details_values'][$j])){
                    $data['attribute'] = $_POST['extra_details_attributes'][$j];
                    $data['value'] = $_POST['extra_details_values'][$j];
                    $this->product_specification_model->create($data);
                }
                
            }
            $this->session->set_flashdata('msg', 'Product Added Successfully');
            redirect('/seller/manage_products');
        }
    }

    public function edit_product() {

        $this->load->model('rbc_category_model');
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $this->load->model('rbc_uom_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_payment_type_model');
        $this->load->model('rbc_currency_model');
        $this->load->model('product_specification_model');
        $data['currency'] = $this->rbc_currency_model->get_all();
        $data['payment_types'] = $this->rbc_payment_type_model->get_all();
        $data['countries'] = $this->rbc_countries_model->get_all();
        $product_id = $this->uri->segment(3);
        $data['category'] = $this->rbc_category_model->get_by_category();
        $data['all_category'] = $this->rbc_category_model->get_by_sub_category();
        $data['product'] = $this->product_master_model->get_by_id($product_id);
        $data['uom'] = $this->rbc_uom_model->get_all();
        //print_r($product_id);
        $data['images'] = $this->product_images_model->get_by_product_id($product_id);
        $data['extra_detail'] = $this->product_specification_model->get_by_id($product_id);
        //echo print_r($data['images']);
//        echo '<pre>';
//              print_r($data);exit();
        $this->load->view('edit_product', $data);

        if ($this->input->post('update')) {
            $payment_type = $this->input->post('product_payment_type');
            $payment_type_value = "";
            foreach ($payment_type as $values) {
                if (!empty($values)) {
                    $payment_type_value .= $values;
                    $payment_type_value .= "|";
                }
            }
            $product = $_POST;
//            echo '<pre>';
//            print_r($product);exit();
            $product_name = explode(" ",strtolower($product['product_name']));
            //print_r($product_name);
            for($j=0; $j<5;$j++){
                $product_slug .= $product_name[$j]."-";
            }
            if(sizeof($product_slug) >35){
                $product_slug = substr($product_slug, 0, 35);
            }
            $product_slug .= $this->session->userdata('member_id');
            $product_slug .= $this->random_string();
            $product['product_slug'] = $product_slug;
            $product['product_payment_type'] = $payment_type_value;
            $this->product_master_model->update($product_id, $product);
            $count = 0;
            $main_img = 1;
            $upload_path = 'uploads/product_images/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777, TRUE);
            }
            for ($i = 0; $i < sizeof($_FILES['product_image']['name']); $i++) {
                if (!empty($_FILES['product_image']['name'][$i]) && $_FILES['product_image']['error'][$i] == 0) {
                    $type = explode(".", $_FILES['product_image']['name'][$i]);
                    echo $_FILES['product_image']['name'][$i];
                    $_FILES['product_image']['name'][$i] = $this->random_string() . "." . $type[1];
                    echo $_FILES['product_image']['name'][$i];
                    move_uploaded_file($_FILES['product_image']['tmp_name'][$i], $upload_path . $_FILES['product_image']['name'][$i]);
                    $images['image_url'] = base_url() . $upload_path . $_FILES['product_image']['name'][$i];
                    $images['product_id'] = $product_id;
                    $images['is_main'] = $main_img;
                    $this->product_images_model->create($images);
                    $main_img = 0;
                    //redirect('/seller/manage_products/');
                }
            }
            for($j=0; $j<=sizeof($_POST['extra_details_attributes']);$j++){
                if(!empty($_POST['extra_details_attributes'][$j]) && !empty($_POST['extra_details_values'][$j]) && !empty($_POST['extra_detail_id'][$j])){
                    $row['attribute'] = $_POST['extra_details_attributes'][$j];
                    $row['value'] = $_POST['extra_details_values'][$j];
                    $this->product_specification_model->update($_POST['extra_detail_id'][$j],$row);
                }
                
            }
            $row['product_id'] = $product_id;
            for($j=0; $j<=sizeof($_POST['add_extra_details_attributes']);$j++){
                if(!empty($_POST['add_extra_details_attributes'][$j]) && !empty($_POST['add_extra_details_values'][$j])){
                    $row['attribute'] = $_POST['add_extra_details_attributes'][$j];
                    $row['value'] = $_POST['add_extra_details_values'][$j];
                    $this->product_specification_model->create($row);
                }
                
            }
            $this->session->set_flashdata("msg","Your Product Edited Successfully");
            redirect('/seller/manage_products/');
        }
    }
    public function delete_product_extra_detail(){
        $this->load->model('product_specification_model');
        $id = $this->input->post('id');
        $this->product_specification_model->delete($id);
        echo "1";
    }
    public function delete_product() {
        $this->load->model('product_master_model');
        $product_id = $this->uri->segment(3);
//            print_r($product_id);exit();
        $this->product_master_model->delete($product_id);
        redirect('/seller/manage_products/');
    }

    public function product_listing() {
        $this->load->view('product_listing');
    }

    public function product_details() {
        $this->load->view('product_detail');
    }

    public function verify_company() {
        $this->load->model('company_master_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_user_model');
        if ($_POST['submit'] == 'submit') {
            $product = $_POST;
//                $product['c_name'] = $this->input->post('c_name');
//                $product['c_registration'] = $this->input->post('c_registration');
            $product['user_id'] = $this->session->userdata('uid');
            $this->rbc_companies_model->create($product);
        }
        //print_r($this->session->userdata);exit();
        //$user_id = $this->session->userdata('user_id');
        $data['user'] = $this->rbc_user_model->get_by_id($this->session->userdata('user_id'));
        $data['company'] = $this->company_master_model->get_by_id($this->session->userdata('company_id'));
        $data['countries'] = $this->rbc_countries_model->get_all();
        $this->load->view('verify_company', $data);
    }

    public function manage_company() {
        $this->load->model('member_master_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_user_model');
        $this->load->model('business_type_model');
        $this->load->model('company_master_model');
        $this->load->model('company_certification_model');
        $this->load->model('company_partner_model');
        $data['business_types'] = $this->business_type_model->get_all();
//        print_r($this->session->userdata);
        $data['user'] = $this->session->userdata('member_id');
        //$data['company'] = $this->member_master_model->get_by_id($this->session->userdata('member_id'));
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['company'] = $this->company_master_model->get_by_id($this->session->userdata('company_id'));
        $data['company_certificate'] = $this->company_certification_model->get_by_company_id($data['company']->id);
        $data['partner_factories'] = $this->company_partner_model->get_by_company_id($data['company']->id);
//        echo '<pre>';
//        print_r($data);exit();
        $data['company']->main_products = explode("|", $data['company']->main_products);
        $data['company']->other_products = explode("|", $data['company']->other_products);
        $data['company']->export_port = explode("|", $data['company']->export_port);
        $data['company']->delievery_terms = explode("|", $data['company']->delievery_terms);
        $data['company']->support_currency = explode("|", $data['company']->support_currency);
        $data['company']->payment_methods = explode("|", $data['company']->payment_methods);
        $data['company']->language_skills = explode("|", $data['company']->language_skills);
        $data['company']->business_type = explode("|", $data['company']->business_type);
        //echo '<pre>';print_r($this->session->userdata);exit();
        $this->load->view('manage_company', $data);
        if ($this->input->post('update_basic_company_details')) {
            $company = $_POST;
            $company['business_type'] = "";
            $company['main_products'] = "";
            $company['other_products'] = "";
            $id = $this->input->post('company_id');
            if ($this->input->post('business_type')) {

                $company['business_type'] = $this->insert_pipe_sign($this->input->post('business_type'));
            }
            if ($this->input->post('main_products')) {

                $company['main_products'] = $this->insert_pipe_sign($this->input->post('main_products'));
            }
            if ($this->input->post('other_products')) {

                $company['other_products'] = $this->insert_pipe_sign($this->input->post('other_products'));
            }
            $this->company_master_model->update_basic_details($id, $company);
            redirect('seller/manage_company/');
        }

        if ($this->input->post('update_trade_details')) {
            $company = $_POST;
            $company['delievery_terms'] = "";
            $company['delievery_terms'] = "";
            $company['support_currency'] = "";
            $company['payment_methods'] = "";
            $company['language_skills'] = "";
            $company['export_port'] = "";
            $id = $this->input->post('company_id');
            if ($this->input->post('delievery_terms')) {

                $company['delievery_terms'] = $this->insert_pipe_sign($this->input->post('delievery_terms'));
            }
            if ($this->input->post('support_currency')) {

                $company['support_currency'] = $this->insert_pipe_sign($this->input->post('support_currency'));
            }
            if ($this->input->post('payment_methods')) {

                $company['payment_methods'] = $this->insert_pipe_sign($this->input->post('payment_methods'));
            }
            if ($this->input->post('language_skills')) {

                $company['language_skills'] = $this->insert_pipe_sign($this->input->post('language_skills'));
            }
            if ($this->input->post('export_port')) {

                $company['export_port'] = $this->insert_pipe_sign($this->input->post('export_port'));
            }
            $this->company_master_model->update_trade_details($id, $company);
            redirect('seller/manage_company/');
        }

        if ($this->input->post('add_partner_factory')) {
            $company = $_POST;

            if (!empty($_FILES['coorporation_contract']['name']) && $_FILES['coorporation_contract']['error'] == 0) {

                $coorporation_contract = $_FILES['coorporation_contract'];

                $upload_path = 'uploads/documents/';

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0777, TRUE);
                }

                $type = explode(".", $coorporation_contract['name']);

                $coorporation_contract['name'] = $this->random_string() . "." . $type[1];

                move_uploaded_file($coorporation_contract['tmp_name'], $upload_path . $coorporation_contract['name']);

                $company['contract_url'] = base_url() . $upload_path . $coorporation_contract['name'];
            }
            $this->company_partner_model->create($company);
            redirect('seller/manage_company/');
        }

        if ($this->input->post('update_company_intro')) {
            //print_r($_POST);print_r($_FILES);
            $company = $_POST;
            $id = $this->input->post('company_id');
            $image_allowed = array('image/gif', 'image/png', 'image/jpg', 'image/jpeg');
            if (!empty($_FILES['company_logo']['name']) && $_FILES['company_logo']['error'] == 0 && in_array($_FILES['company_logo']['type'], $image_allowed)) {

                $logo_image = $_FILES['company_logo'];

                $upload_path = 'uploads/logos/';

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0777, TRUE);
                }

                $type = explode(".", $logo_image['name']);

                $logo_image['name'] = $this->random_string() . "." . $type[1];

                move_uploaded_file($logo_image['tmp_name'], $upload_path . $logo_image['name']);

                $company['logo_url'] = base_url() . $upload_path . $logo_image['name'];
            }
            //print_r($company);exit();
            $this->company_master_model->update_company_introduction($id, $company);
            redirect('seller/manage_company/');
        }
        if ($this->input->post('add_company_certification')) {

            $company = $_POST;
            $image_allowed = array('image/gif', 'image/png', 'image/jpg', 'image/jpeg');

            if (!empty($_FILES['certification_image_url']['name']) && $_FILES['certification_image_url']['error'] == 0 && in_array($_FILES['certification_image_url']['type'], $image_allowed)) {

                $certification_image_url = $_FILES['certification_image_url'];

                $upload_path = 'uploads/logos/';

                if (!is_dir($upload_path)) {

                    mkdir($upload_path, 0777, TRUE);
                }

                $type = explode(".", $certification_image_url['name']);

                $certification_image_url['name'] = $this->random_string() . "." . $type[1];

                move_uploaded_file($certification_image_url['tmp_name'], $upload_path . $certification_image_url['name']);

                $company['image_url'] = base_url() . $upload_path . $certification_image_url['name'];
            }
            //print_r($company);exit();
            $this->company_certification_model->create($company);
            redirect('seller/manage_company/');
        }
    }

    public function seller_profile() {
        $this->load->model('company_master_model');
        $data['company'] = $this->company_master_model->get_by_id($this->session->userdata('company_id'));
        if ($data['company']) {

            $array = json_decode(json_encode($data['company']), true);
            $profile_completion_count = 0;
            $total_count = 0;
            foreach ($array as $vals) {

                if ($vals != "") {
                    $profile_completion_count++;
                }
                $total_count++;
            }
            $array['profile_completed'] = round(($profile_completion_count / $total_count) * 100);
            $data['company'] = json_decode(json_encode($array));
        }
        $this->load->view('seller_profile', $data);
    }

    public function update_profile() {
        $this->load->model('company_master_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_user_model');
        $data['user'] = $this->rbc_user_model->get_by_id($this->session->userdata('user_id'));
        $data['company'] = $this->company_master_model->get_by_id($this->session->userdata('company_id'));
        $data['countries'] = $this->rbc_countries_model->get_all();
        $this->load->view('update_profile', $data);
        if ($this->input->post('update')) {

            $updated_data = $_POST;
            if ($this->input->post('user_type') == 'supplier') {
                $member_id = $this->input->post('member_id');
                $this->member_master_model->update($member_id, $updated_data);
            }
            $updated_data['user_name'] = $this->input->post('seller_name');
            if (empty($_POST['user_password'])) {
                $this->rbc_user_model->update_without_password($updated_data['user_id'], $updated_data);
            } else {
                $this->rbc_user_model->update($updated_data['user_id'], $updated_data);
            }
        }
    }

    public function product_images() {
        if (isset($_FILES['product_image']['name'])) {
            // total files //
            $count = count($_FILES['product_image']['name']);
            // all uploads //
            $uploads = $_FILES['product_image'];

            for ($i = 0; $i < $count; $i++) {

                if ($uploads['error'][$i] == 0) {
                    move_uploaded_file($uploads['tmp_name'][$i], 'uploads/' . $uploads['name'][$i]);
                    echo $uploads['name'][$i] . "\n";
                    //$insert_data = array(
                    //'product_image' => $uploads['name'][$i]
                    //);
                    //$total_images = $uploads['name'][$i];
                    //echo print_r($insert_data);
                    // exit;
                    //$this->session->set_userdata($insert_data);
                }
            }
        }
    }

    public function register_membership() {
        $this->load->view('member_register_form');
    }

    public function insert_pipe_sign($input = array()) {
        $output = "";
        foreach ($input as $value) {
            if (!empty($value)) {
                $output .= $value;
                $output .= '|';
            }
        }
        return $output;
    }

    public function image_delete() {

        $this->load->model('product_images_model');
        $image_id = $this->input->post('image_id');
        //echo print_r($image_id);exit;
        $get_image_data = $this->product_images_model->get_by_id($image_id);
        $img_url = $get_image_data->image_url;
        $img_name = explode('/', $img_url);
        $image_name = $img_name['6'];
        $image_path = '../rbctrades/uploads/product_images/' . $image_name;
        unlink($image_path);
        $this->product_images_model->delete_image($image_id);
        //redirect('/seller/manage_products/');
        exit();
    }

//START Zain

    public function quotation_builder() {
        $this->load->model('product_master_model');
        $this->load->model('rbc_uom_model');
        $this->load->model('quotation_details_model');
        $data['product'] = $this->product_master_model->get_all();
        $data['uom'] = $this->rbc_uom_model->get_all();


        if ($this->input->post('quotation_submit')) {

//            $quotation_data = $_POST;
            //echo '<pre>';
            //exit();
            for ($i = 0; $i < sizeof($_POST['select']); $i++) {
                //print_r($_POST);
                $data['product_id'] = $_POST['select'][$i];
                $data['description'] = $_POST['selectdescription'][$i];
                $data['unit_price'] = $_POST['rate'][$i];
                $data['quantity'] = $_POST['quantity'][$i];
                $data['uom_id'] = $_POST['selectuom'][$i];

                $this->quotation_details_model->create($data);
            }
            redirect('/seller/quotation_builder/');
        } else {
            $this->load->view('quotation_builder', $data);
        }
    }

    public function seller_quotation() {
        $this->load->model('rfq_master_model');
        $this->load->model('rfq_details_model');
        $this->load->model('quotation_master_model');
        $this->load->model('quotation_details_model');
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('rbc_user_model');
        $quot_id = $this->uri->segment(3);
        $data['quot_main'] = $this->rfq_master_model->get_by_id($quot_id);
        $data['quot_details'] = $this->rfq_details_model->get_by_quot($quot_id);
//            echo '<pre>';
//            print_r($data); exit();
        if ($this->input->post('seller-quotation-submit')) {
//            $quotation_data = $_POST;
//            echo '<pre>';
//            print_r($quotation_data); exit();
            $buyer_id = $_POST['buyer_id'];
            $quot['rfq_id'] = $_POST['quotation_id'];
            $quot['seller_id'] = $this->session->userdata('member_id');
            $quot['quotation_date'] = $_POST['quotation_date'];
            $quot['quotation_validity_date'] = $_POST['quotation_expiry_date'];
            $quot['currency_id'] = $_POST['quotation_currency'];
            $quot['discount'] = $_POST['discount'];
            $quot['tax'] = $_POST['tax'];
            $this->rfq_master_model->update_quotes_left($_POST['quotation_id']);
            $quot_id = $this->quotation_master_model->create_main($quot);

            for ($i = 0; $i < sizeof($_POST['product_name']); $i++) {

                $data['quot_no'] = $quot_id;
                $data['product_name'] = $_POST['product_name'][$i];
                $data['description'] = $_POST['product_description'][$i];
                $data['unit_price'] = $_POST['product_rate'][$i];
                $data['uom_id'] = $_POST['product_uom'][$i];
                $data['quantity'] = $_POST['product_quantity'][$i];
                $this->quotation_details_model->create($data);
            }
            $buyer_data = $this->rbc_user_model->search_buyer_data($buyer_id);
            $buyer_email = $buyer_data->user_email;
            $buyer_name = $buyer_data->user_name;
            $sender_name = $this->session->userdata('name');
            $message['subject'] = "You have received a new quote to .";
            $message['body'] = "<p>".$sender_name ." has sent a quote to ".$buyer_name. ". </p>";
            $message_id = $this->message_center_model->create_quote_message($message);
            $user_email_from = $this->session->userdata('email');
            
            $data['message_id'] = $message_id;
            $data['main_user_email'] = $buyer_email;
            $data['temp_user_email'] = $user_email_from;
            $this->message_center_mapped_model->create($data);
            $data['message_id'] = $message_id;
            $data['main_user_email'] = $user_email_from;
            $data['temp_user_email'] = $buyer_email;
            $this->message_center_mapped_model->create_new($data);
            redirect('/main/quotation_listing/');
        } else {
            $this->load->view('seller_quotation', $data);
        }
    }





    public function seller_buying_requests() {
        $this->load->model('rfq_master_model');

        $seller_id = $this->session->userdata('member_id');

        $data['requests'] = $this->rfq_master_model->get_all_seller_buyying_requests($seller_id);

//        echo '<pre>';
//        print_r($data);
//        exit();
        $this->load->view('seller_buying_requests', $data);
    }

//END Zain
//START Asad
    public function random_string() {
        $length = 6;
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    public function update_my_profile() {
        $data = $_POST;
        $image_allowed = array('image/gif', 'image/png', 'image/jpg', 'image/jpeg');
        $this->load->model('rbc_user_model');
        $this->load->model('company_master_model');
        if (!empty($_POST['phone'][0])) {
            $data['phone'] = implode("-", $_POST['phone']);
        } else {
            $data['phone'] = "";
        }

        if (!empty($_POST['fax'][0])) {
            $data['fax'] = implode("-", $_POST['fax']);
        } else {
            $data['fax'] = "";
        }

        if (isset($_POST['preffered_seller_type'])) {
            $types = "";
            foreach ($_POST['preffered_seller_type'] as $values) {
                $types .= $values;
                $types .= "|";
            }
            $data['preffered_seller_type'] = $types;
        }
        if ($this->input->post('preffered_seller_location')) {
            $locations = "";
            foreach ($_POST['preffered_seller_location'] as $value) {
                $locations .= $value;
                $locations .= "|";
            }
            $data['preffered_seller_location'] = $locations;
        }
        if (!empty($_FILES['company_logo']['name']) && $_FILES['company_logo']['error'] == 0 && in_array($_FILES['company_logo']['type'], $image_allowed)) {

            $company_logo = $_FILES['company_logo'];

            $upload_path = 'uploads/logos/';

            if (!is_dir($upload_path)) {

                mkdir($upload_path, 0777, TRUE);
            }

            $type = explode(".", $company_logo['name']);

            $company_logo['name'] = $this->random_string() . "." . $type[1];

            move_uploaded_file($company_logo['tmp_name'], $upload_path . $company_logo['name']);

            $data['logo_url'] = base_url() . $upload_path . $company_logo['name'];

            $old_url = explode("/", $data['old_logo_name']);
            print_r($old_url);
            unlink($upload_path . $old_url[7]);
        }
        if (empty($_FILES['company_logo']['name'])) {
            $data['logo_url'] = $data['old_logo_name'];
        }
        
        $this->rbc_user_model->update($this->session->userdata('user_id'), $data);
        $this->company_master_model->update($this->session->userdata('company_id'), $data);
        redirect('seller/account_setting/my_profile');
    }

    public function update_member_profile() {
        $data = $_POST;
        //print_r($data);
        $this->load->model('rbc_user_model');
        if (!empty($_POST['phone'][0])) {
            $data['phone'] = implode("-", $_POST['phone']);
        } else {
            $data['phone'] = "";
        }
        if (!empty($_POST['fax'][0])) {
            $data['fax'] = implode("-", $_POST['fax']);
        } else {
            $data['fax'] = "";
        }
        $this->rbc_user_model->update($this->session->userdata('user_id'), $data);
        redirect('seller/account_setting/member_profile/');
    }

    public function update_privacy_setting() {
        $form_data = $_POST;
        $this->load->model('company_privacy_setting_model');
        $this->company_privacy_setting_model->update($form_data['id'], $form_data);
        redirect('seller/account_setting/privacy_setting/');
    }

    public function delete_partner_factory() {
        $id = $this->uri->segment(3);
        $this->load->model('company_partner_model');
        $this->company_partner_model->delete($id);
        redirect('seller/manage_company/');
    }

    public function delete_certificate() {
        $id = $this->uri->segment(3);
        $this->load->model('company_certification_model');
        $this->company_certification_model->delete($id);
        redirect('seller/manage_company/');
    }

//END Asad
//START Fayyaz
    public function account_setting() {
        $setting_option = $this->uri->segment(3);
        $this->load->model('rbc_user_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_security_question_model');
        $data['user'] = $this->rbc_user_model->get_by_id($this->session->userdata('user_id'));
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['user_phone'] = explode("-", $data['user']->phone);
        $data['user_fax'] = explode("-", $data['user']->fax);
        $data['questions'] = $this->rbc_security_question_model->get_all();

        if ($setting_option == 'my_profile') {
            $this->load->model('business_type_model');
            $this->load->model('company_master_model');
            $this->load->model('rbc_purchasing_frequency_model');

            $data['company'] = $this->company_master_model->get_by_id($this->session->userdata('company_id'));
            $data['business_types'] = $this->business_type_model->get_all();
            $data['purchasing_fre'] = $this->rbc_purchasing_frequency_model->get_all();

            //print_r($data['user_phone'][0]);exit();
            if (!empty($data['company']->preffered_seller_location)) {
                $data['company']->preffered_seller_location = explode("|", $data['company']->preffered_seller_location);
            }
            if (!empty($data['company']->preffered_seller_type)) {
                $data['company']->preffered_seller_type = explode("|", $data['company']->preffered_seller_type);
            }
            $this->load->view('account_setting_my_profile', $data);
        } else if ($setting_option == 'member_profile') {
            $this->load->view('account_setting_member_profile', $data);
        } else if ($setting_option == 'privacy_setting') {
            $this->load->model('company_privacy_setting_model');
            $data['privacy'] = $this->company_privacy_setting_model->get_by_member_id($this->session->userdata('member_id'));
            $this->load->view('account_setting_privacy_setting', $data);
        } else if ($setting_option == 'security_question') {
            $this->load->model('security_setting_master_model');
            $this->load->model('rbc_security_question_model');
            $member_id = $this->session->userdata('member_id');
            $data['questions'] = $this->rbc_security_question_model->get_all();
            $data['user_question'] = $this->security_setting_master_model->get_by_id($member_id);
            $this->load->view('account_setting_set_security_question', $data);
            if ($this->input->post('update_question')) {
                $this->form_validation->set_rules('security_question_id', 'Security Question', 'required');
                $this->form_validation->set_rules('security_answer', 'Answer', 'required');
                if ($this->form_validation->run() == FALSE) {
                    redirect('seller/account_setting/security_question/');
                } else {
                    $data = $_POST;
                    $data['member_id'] = $member_id;
                    $this->security_setting_master_model->create($data);
                    redirect('seller/account_setting/security_question/');
                }
            }
        } else if ($setting_option == 'change_password') {
            $this->load->model('rbc_user_model');
            $data['user_question'] = $this->rbc_user_model->get_by_id($this->session->userdata('user_id'));
            $this->load->view('account_setting_change_password', $data);
            if ($this->input->post('update_password')) {
                //print_r($_POST);exit();
                $this->form_validation->set_rules('user_password', 'New Password', 'required');
                $this->form_validation->set_rules('confirm_password', 'Confirm password', 'required');
                $this->form_validation->set_rules('answer', 'Answer', 'required');
                if ($this->form_validation->run() == FALSE || $_POST['answer'] != $data['user_question']->answer) {
                    redirect('seller/account_setting/change_password/');
                } else {
                    $data = $_POST;
                    $this->rbc_user_model->update_password($this->session->userdata('user_id'), $data);
                    redirect('seller/account_setting/my_profile');
                }
            }
        } else if ($setting_option == 'change_email_address') {
            $this->load->model('security_setting_master_model');

            $member_id = $this->session->userdata('member_id');
            $data['user_question'] = $this->security_setting_master_model->get_by_id($member_id);

            $this->load->view('account_setting_change_email', $data);
            if ($this->input->post('update_email')) {
                //print_r($_POST);exit();
                $this->form_validation->set_rules('confirm_email', 'Re Enter New Email', 'required');
                $this->form_validation->set_rules('email', 'New Email', 'required');
                $this->form_validation->set_rules('answer', 'Answer', 'required');
                if ($this->form_validation->run() == FALSE || !strcasecmp((trim($data['user_question']->security_answer)), $_POST['answer'])) {
                    redirect('seller/account_setting/change_email_address/');
                } else {
                    $data = $_POST;
                    $this->rbc_user_model->update_email($member_id, $data);
                    redirect('seller/account_setting/change_email_address/');
                }
            }
        }

        //print_r($data);
    }

//END Fayyaz
}
