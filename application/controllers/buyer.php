<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Buyer extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
        $this->session->set_userdata('referred_from', current_url());
    }

    private function is_logged_in() {
        return $this->session->userdata('is_logged_in');
    }

    public function index() {
        $this->load->view('selling_leeds');
    }

    public function product_by_category_listing() {
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $this->load->model('rbc_countries_model');
        $this->load->model('rbc_category_model');
        $category_id = $this->uri->segment(3);
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['categories'] = $this->rbc_category_model->get_by_category();

//        $data['products'] = $this->product_master_model->get_all_by_category_id($category_id);
        $data['images'] = $this->product_images_model->get_all();
        $nodelist = $this->fetch_recursive($this->rbc_category_model->get_all(), $category_id);
        foreach ($nodelist as $category) {

            $products_data = $this->product_master_model->search_category_product($category['category_id']);
            //print_r($products_data);exit();
            foreach ($products_data as $product) {
                if (!empty($product)) {
                    $product_images = $this->product_images_model->get_main_image_new($product->product_id);

                    if (!empty($product_images)) {
                        $product->image_url = $product_images->image_url;
                        $product->is_main = 1;
                    }
                    $products_array[] = $product;
                }
            }
        }
        while ($category_id != 0) {
            $temp = $this->rbc_category_model->get_by_id($category_id);
            $curms[] = $temp->category_name;
            $links[] = $temp->category_id;
            $category_id = $temp->parent_id;
        }
        $data['products'] = $products_array;
        $data['breadcrums'] = $curms;
        $data['breadcrums_link'] = $links;
//echo '<pre>';
//        print_r($products_array);exit();
        $this->load->view('product_listing', $data);
    }

    public function product_listing() {
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $this->load->model('rbc_countries_model');
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['categories'] = $this->rbc_category_model->get_by_category();
        $data['products'] = $this->product_master_model->get_all();
        $data['images'] = $this->product_images_model->get_all();
        $link[] = 'All Products';
        $data['breadcrums'] = $link;
        //echo '<pre>';
        //print_r($data);
        $this->load->view('product_listing', $data);
    }

    public function pagination_product() {
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $page_number = $this->input->post('page_number');
        $item_par_page = 3;
        $position = ($page_number * $item_par_page);
        //$user_email = $this->session->userdata('email');
        $result_set = $this->product_master_model->get_all_for_pagging($item_par_page, $position);
//        echo '<pre>';
//        print_r($result_set);exit();
        $total_set = $result_set->num_rows();
        $page = $this->product_master_model->get_all_pagination_products();
        $total = $page->num_rows();
        //break total recoed into pages
        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            $producr_id  = explode("|",$this->session->userdata('product_ids'));
            
           
            foreach ($result_set->result() as $product) {
                
                if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                                            }
                
                if(in_array($product->product_id,$producr_id )){
                    $product->in_session = true;
                }else{
                    $product->in_session = false;
                }
                $product->compare_count = $this->session->userdata('compare_count');
                $entries[] = $product;
            }


            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );




            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }

    public function product_detail() {
        $this->load->model('member_master_model');
        $buyer_id = $this->session->userdata('member_id');
        $data['member'] = $this->member_master_model->get_member_by_id($buyer_id);
        $id = $this->uri->segment(3);
        $this->load->model('product_images_model');
        $this->load->model('product_master_model');
        $data['product_images'] = $this->product_images_model->get_by_product_id($id);
        $data['product'] = $this->product_master_model->get_product_by_id($id);
        $payment_type = explode("|",$data['product']->product_payment_type);
       foreach ($payment_type as $values) {
                if (!empty($values)) {
                $payment_type_value[] = $this->product_master_model->get_product_payment_type_name_by_id($values);
                }
            }
        $data['product_payment_type']=$payment_type_value;
        $business_type = explode("|",$data['product']->business_type);
        foreach ($business_type as $values) {
                if (!empty($values)) {
                $business_type_value[] = $this->product_master_model->get_product_business_type_name_by_id($values);
                }
            }
        $data['business_type']=$business_type_value;
        $export_type = explode("|",$data['product']->export_port);

        foreach ($export_type as $values) {
                if (!empty($values)) {
                $export_type_value[] = $values;
                }
            }
        $data['export_type_port']=$export_type_value;

        $company_main_product = explode("|",$data['product']->main_products);

        foreach ($company_main_product as $values) {
                if (!empty($values)) {
                $_company_main_product[] = $values;
                }
            }
        $data['main_products_company']=$company_main_product;

//         echo '<pre>';
//        print_r($data);
//        exit;
        $this->load->view('product_detail', $data);
    }

    public function view_selling_leeds() {

        $data['categories'] = $this->rbc_category_model->get_by_category();
        $data['sub_categories'] = $this->rbc_category_model->get_by_sub_category();
        $this->load->view('selling_leeds', $data);
    }

    public function manage_products() {
        if (!$this->is_logged_in()) {
            redirect('login');
        }
        $this->load->view('manage_products');
    }

    public function company_listing() {
        $this->load->model('company_master_model');
        $this->load->model('rbc_countries_model');
        $data['countries'] = $this->rbc_countries_model->get_all();
        $data['companies'] = $this->company_master_model->get_all_companies();
//        echo '<pre>';
//        print_r($data);
//        exit();
        $this->load->view('company_listing',$data);
    }

    public function product_comparison() {
        $this->load->view('shop_product_comparison');
    }

    public function register_membership() {
        if (!$this->is_logged_in()) {
            redirect('login');
        }
        $this->load->view('member_register_form');
    }

    public function request_for_quotation() {
        if (!$this->is_logged_in()) {
            redirect('login');
        }
        $this->load->model('rbc_uom_model');
        $this->load->model('rfq_master_model');
        $this->load->model('rfq_details_model');

        $data['uom'] = $this->rbc_uom_model->get_all();


        if ($this->input->post('rfq-submit')) {
            $quot['rfq_buyer_id'] = $this->session->userdata('member_id');
            $quot['rfq_quotation_date'] = date("Y-m-d");
            $quot['rfq_expiry_date'] = $_POST['rfq_expiry_date'];
            $quot['rfq_quotation_description'] = $_POST['rfq_quotation_description'];
//            echo '<pre>';
//            print_r($quot);
//            exit();

            $quot_id = $this->rfq_master_model->create_main($quot);

            for ($i = 0; $i < sizeof($_POST['rfq_product_name']); $i++) {

                $data['rfq_id'] = $quot_id;
                $data['rfq_product_name'] = $_POST['rfq_product_name'][$i];
                $data['rfq_product_quantity'] = $_POST['rfq_product_quantity'][$i];
                $data['rfq_product_uom'] = $_POST['rfq_product_uom'][$i];
                $data['rfq_product_description'] = $_POST['rfq_product_description'][$i];
             
                $upload_path = 'uploads/';
                if (!empty($_FILES['rfq_product_image']['name'][$i]) && $_FILES['rfq_product_image']['error'][$i] == 0) {
                $type = explode(".", $_FILES['rfq_product_image']['name'][$i]);

                $_FILES['rfq_product_image']['name'][$i] = $this->random_string() . "." . $type[1];

                move_uploaded_file($_FILES['rfq_product_image']['tmp_name'][$i], $upload_path . $_FILES['rfq_product_image']['name'][$i]);
                $data['rfq_product_image'] = base_url() . $upload_path . $_FILES['rfq_product_image']['name'][$i];
               
            }
            
                $this->rfq_details_model->create($data);
            }
            $this->session->set_flashdata('msg', 'Quotation successfully Posted.');
            redirect('/buyer/request_for_quotation/');
        } else {
            $this->load->view('request_for_quotation', $data);
        }
    }
    PUBLIC function random_string() {
        $length = 6;
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }
    public function view_quotation() {
        if (!$this->is_logged_in()) {
            redirect('login');
        }
        $this->load->model('quotation_master_model');
        $this->load->model('rbc_uom_model');
        $this->load->model('quotation_details_model');
        $qout_id = $this->uri->segment(3);
        $data['quot_main'] = $this->quotation_master_model->get_by_id($qout_id);
        $data['quot_details'] = $this->quotation_details_model->get_by_quot_id($qout_id);
//        echo '<pre>';
//        print_r($data);exit();

        $this->load->view('view_quotation', $data);
    }

    public function category_filtration() {

        $this->load->model('rbc_category_model');
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $category_id = $this->input->post('category_id');
        if ($category_id == 0) {
            $products_data = $this->product_master_model->search_all_category_product();
            //print_r($products_data);
            foreach ($products_data as $product) {
                if (!empty($product)) {
                    $product_images = $this->product_images_model->get_main_image($product->product_id);
                    $product->images_url = $product_images;
                    $products_array[] = $product;
                }
            }
        } else {
            $nodelist = $this->fetch_recursive($this->rbc_category_model->get_all(), $category_id);
            foreach ($nodelist as $category) {

                $products_data = $this->product_master_model->search_category_product($category['category_id']);
                //print_r($products_data);exit();
                foreach ($products_data as $product) {
                    if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $products_array[] = $product;
                    }
                }
            }
        }
        //print_r($products_array);
        echo json_encode($products_array);
    }

    public function fetch_recursive($src_arr, $id, $parentfound = false, $cats = array()) {
        foreach ($src_arr as $row) {
            if ((!$parentfound && $row['category_id'] == $id) || $row['parent_id'] == $id) {
                $rowdata = array();
                foreach ($row as $k => $v)
                    $rowdata[$k] = $v;
                $cats[] = $rowdata;
                if ($row['parent_id'] == $id)
                    $cats = array_merge($cats, $this->fetch_recursive($src_arr, $row['category_id'], true));
            }
        }
        return $cats;
    }

//START Zain

    public function product_listing_country_filtration() {
        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $country_id = $this->input->post('country_id');

        //print_r($products_data);
        if ($country_id == 0) {
            $products_data = $this->product_master_model->get_all();
            if (!empty($products_data)) {
                foreach ($products_data as $product) {
                    if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $products_array[] = $product;
                    }
                }
            }
        } else {
            $products_data = $this->product_master_model->get_all_by_country($country_id);
            if (!empty($products_data)) {
                foreach ($products_data as $product) {
                    if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $products_array[] = $product;
                    }
                }
            }
        }

        echo json_encode($products_array);
    }

    public function pagination_product_by_country() {

        $this->load->model('product_master_model');
        $this->load->model('product_images_model');
        $country_id = $this->input->post('country_id');
        $item_par_page = 3;
        $position = ($page_number * $item_par_page);
        $result_set = $this->product_master_model->get_all_by_country($item_par_page, $position,$country_id);
        $total = $this->product_master_model->get_all_by_country_count($country_id)->num_rows();

        $total_set = $result_set->num_rows();
        $total = ceil($total / $item_par_page);
        if ($total_set > 0) {
            $entries = null;
            // get data and store in a json array
            foreach ($result_set->result() as $product) {

                if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $entries[] = $product;
                    }

            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
    }

    public function product_quotation() {
        if (!$this->is_logged_in()) {
            redirect('login');
        }
        $this->load->model('product_master_model');
        $this->load->model('rfq_master_model');
        $this->load->model('rfq_details_model');
        $this->load->model('rbc_uom_model');
        $this->load->model('product_images_model');

        if ($this->input->post('rfqp-submit')) {
//            echo '<pre>';;
//            print_r($_POST);
//            exit();
            $quot['rfq_seller_id'] = $_POST['rfq_seller_id'];
            $quot['rfq_buyer_id'] = $this->session->userdata('member_id');
            $quot['rfq_quotation_date'] = date("Y-m-d");
            $quot['rfq_expiry_date'] = NULL;
            $quot['rfq_quotation_description'] = NULL;
            $quot['rfq_quotes_left'] = NULL;

            $quot_id = $this->rfq_master_model->create($quot);

            $data['rfq_id'] = $quot_id;
            $data['rfq_product_name'] = $_POST['rfq_product_name'];
            $data['rfq_product_quantity'] = $_POST['rfq_product_quantity'];
            $data['rfq_product_uom'] = $_POST['rfq_product_uom'];
            $data['rfq_product_description'] = $_POST['rfq_product_description'];
            $this->rfq_details_model->create($data);
            $this->session->set_flashdata('msg', 'Quotation successfully sent.');
            redirect('/buyer/product_quotation/'.$quot_id);
        } else {
            $data['uom'] = $this->rbc_uom_model->get_all();
            $product_id = $this->uri->segment(3);
            $data['product_images'] = $this->product_images_model->get_by_product_id($product_id);
//            $data['product_images'] = $this->product_images_model->get_main_image($product_id);
            $data['product'] = $this->product_master_model->get_product_by_id($product_id);
            
            foreach ($this->product_master_model->get_all_main_by_user($data['product']->member_id) as $product) {
                    if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $products_array[] = $product;
                    }
                }
        $data['products'] = $products_array;
                       
//        echo '<pre>';
//        print_r($member_id);
//        exit();
            $this->load->view('product_quotation', $data);
        }
    }

//END Zain
//START Asad
    public function buyer_profile() {
        if (!$this->is_logged_in()) {
            redirect('login');
        }
        $this->load->model('company_master_model');
        $data['company'] = $this->company_master_model->get_by_id($this->session->userdata('company_id'));
        if ($data['company']) {

            $array = json_decode(json_encode($data['company']), true);
            $profile_completion_count = 0;
            $total_count = 0;
            foreach ($array as $vals) {

                if ($vals != "") {
                    $profile_completion_count++;
                }
                $total_count++;
            }
            $array['profile_completed'] = round(($profile_completion_count / $total_count) * 100);
            $data['company'] = json_decode(json_encode($array));
        }
        $this->load->view('buyer_profile', $data);
    }

    public function buying_requests() {
        if (!$this->is_logged_in()) {
            redirect('login');
        }

        $this->load->model('rfq_master_model');
        $data['requests'] = $this->rfq_master_model->get_all_buyer_buying_requests($this->session->userdata('member_id'));
//        echo '<pre>';
//        print_r($data);exit();
        $this->load->view('buying_requests', $data);
    }

    public function pagination_product_by_category() {
        $this->load->model('product_master_model');
        $this->load->model('rbc_category_model');
        $this->load->model('product_images_model');
        $page_number = $this->input->post('page_number');
        $category_id = $this->input->post('id');
        $item_par_page = 1;
        $position = $page_number * $item_par_page;
        $data = '';

        $nodelist = $this->fetch_recursive($this->rbc_category_model->get_all(), $category_id);
        foreach ($nodelist as $category) {

            $products_data = $this->product_master_model->search_category_product($category['category_id']);
            if ($products_data != null) {
                foreach ($products_data as $product) {
                    if (!empty($product)) {
                        $product_images = $this->product_images_model->get_main_image($product->product_id);
                        $product->images_url = $product_images;
                        $products_array[] = $product;
                    }
                }
            }
        }

        if ($products_array != null) {
//        $user_email = $this->session->userdata('email');
//        $result_set = $this->product_master_model->get_all_for_pagging($item_par_page,$position);
//        echo '<pre>';
//        print_r($result_set);
//        exit();
            $total_set = sizeof($products_array);

            $page = sizeof($products_array);
            $total = $page;
            //break total recoed into pages
            $total = ceil($total / $item_par_page);
            if ($total_set > 0) {
                $entries = null;
                // get data and store in a json array
                if ($products_array != null) {
                    //echo'<pre>'; print_r($position);exit;
                    for ($i = $position; $i < $item_par_page * ($position + 1); $i++) {
                        $entries[] = $products_array[$i];
                    }
                    $data = array(
                        'TotalRows' => $total,
                        'Rows' => $entries
                    );
                }
                $this->output->set_content_type('application/json');
                echo json_encode(array($data));
            }
        }
        exit();
    }

    public function build_buyer_profile() {

        $this->load->model('company_master_model');
        $this->load->model('member_master_model');
        if ($this->input->post('build_buyer_profile')) {
            $post = $_POST;
            $this->company_master_model->update_company_name($this->session->userdata('company_id'), $post);
            $this->member_master_model->update_type($this->session->userdata('member_id'));
            //$this->session->set_userdata($data);
            $this->session->set_flashdata("buyer_build","Your Buyer Profile created Successfully");
            $this->session->set_userdata("member_type","both");
            redirect('buyer/buyer_profile');
        }
        $data['company'] = $this->company_master_model->get_by_id($this->session->userdata('company_id'));
        $this->load->view('build_buyer_profile', $data);
    }

//END Asad
//START Fayyaz
//END Fayyaz

}
