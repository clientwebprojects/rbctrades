<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Message_center extends CI_Controller {

    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL & ~E_NOTICE);
        if (!$this->is_logged_in()) {
            redirect('login');
        }
    }
    private function is_logged_in() {
        return $this->session->userdata('is_logged_in');
    }
    public function message_center() {
            $this->load->view('message_center');
    }

    public function message_center_new_inquiry() {
         $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $this->load->view('message_center_new_inquiry');
        if ($this->input->post('message_send_btn')) {
            $mail_data = $_POST;
            $mail_to = $mail_data['user_id_to'];
            $message_id = $this->message_center_model->create($mail_data);
            
            $user_email_from = $this->session->userdata('email');
            $data['message_id'] = $message_id;
            $data['main_user_email'] = $mail_to;
            $data['temp_user_email'] = $user_email_from;
            $this->message_center_mapped_model->create($data);
            $data['message_id'] = $message_id;
            $data['main_user_email'] = $user_email_from;
            $data['temp_user_email'] = $mail_to;
            $this->message_center_mapped_model->create_new($data);
            redirect('/seller/message_center/');
        }
        
    }
    public function message_center_reply_inquiry() {
         $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $message_id = $this->uri->segment(3);
        
        $data['message_data'] = $this->message_center_model->get_information_email($message_id);
        $this->load->view('message_center_reply_inquiry',$data);
        if ($this->input->post('message_reply_btn')) {
            $mail_data = $_POST;
            $mail_to = $mail_data['user_id_to'];
            $message_id = $this->message_center_model->create($mail_data);
            
            $user_email_from = $this->session->userdata('email');
            $data['message_id'] = $message_id;
            $data['main_user_email'] = $mail_to;
            $data['temp_user_email'] = $user_email_from;
            $this->message_center_mapped_model->create($data);
            $data['message_id'] = $message_id;
            $data['main_user_email'] = $user_email_from;
            $data['temp_user_email'] = $mail_to;
            $this->message_center_mapped_model->create_new($data);
            redirect('/seller/message_center/');
        }
        
    }
   public function get_sent_inquiries() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $user_email = $this->session->userdata('email');
        $data['sentbox_inquiries'] = $this->message_center_model->get_sent_inquiries($user_email);
            $this->load->view('message_center_sent_inquiries',$data);
    }
     public function get_important_inquiries() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $user_email = $this->session->userdata('email');
        $data['importantbox_inquiries'] = $this->message_center_model->get_important_inquiries($user_email);
            $this->load->view('message_center_important_inquiries',$data);
    }
     public function get_trash_inquiries() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $user_email = $this->session->userdata('email');
        $data['trashbox_inquiries'] = $this->message_center_model->get_trash_inquiries($user_email);
            $this->load->view('message_center_trash_inquiries',$data);
    }
    public function delete_inquiries_all() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $ids = ( explode( ',', $this->input->get_post('ids') ));
        $this->message_center_model->delete($ids);
        $user_email = $this->session->userdata('email');
        $this->message_center_model->get_sent_inquiries($user_email);
        redirect('refresh');
    }
    public function email_data() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $message_id =  $this->uri->segment(3);
        $user_email = $this->session->userdata('email');
        $this->message_center_mapped_model->update_isread($message_id);
        
        $data['message_data'] = $this->message_center_model->get_information_email($message_id);
        
        $this->load->view('message_center_email_data', $data);
    }
    public function get_important_information_email() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $message_id =  $this->uri->segment(3);
        $user_email = $this->session->userdata('email');
        $this->message_center_mapped_model->update_isread($user_email);
        
        $data['message_data'] = $this->message_center_model->get_important_information_email($message_id);
        
        $this->load->view('message_center_email_data', $data);
    }
    public function email_data_trash() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $message_id =  $this->uri->segment(3);
        $user_email = $this->session->userdata('email');
        $this->message_center_mapped_model->update_isread($user_email);
        
        $data['message_data'] = $this->message_center_model->get_trash_information_email($message_id);
        
        $this->load->view('message_center_email_data', $data);
    }
    public function email_data_sent() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $message_id =  $this->uri->segment(3);
        $data['message_data'] = $this->message_center_model->get_information_email($message_id);
        
        $this->load->view('message_center_email_data_sent', $data);
    }
     public function message_center_delete_inquiry() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $message_id =  $this->uri->segment(3);
        
        $user_email = $this->session->userdata('email');
        
        $this->message_center_model->update_detail_message($message_id,$user_email);
         redirect('/seller/message_center/');
        
    }
    public function delete_inquiries() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $ids = ( explode( ',', $this->input->get_post('ids') ));
        $this->message_center_model->update($ids);
        $user_email = $this->session->userdata('email');
        $data['sentbox_inquiries'] = $this->message_center_model->get_sent_inquiries($user_email);
        $this->load->view('message_center_sent_ajax',$data,true);
    }
    public function delete_inbox_inquiries() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $ids = ( explode( ',', $this->input->get_post('ids') ));
        $this->message_center_model->update($ids);
        $user_email = $this->session->userdata('email');
        $data['importantbox_inquiries'] = $this->message_center_model->get_sent_inquiries($user_email);
        $this->load->view('message_center_ajax_load',$data,true);
    }
    public function message_center_mark_important_inquiry() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $message_id =  $this->uri->segment(3);
        
        $user_email = $this->session->userdata('email');
        
        $this->message_center_model->update_as_important_message($message_id,$user_email);
         redirect('/seller/message_center/');
    }
    public function important_inquiries() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $ids = ( explode( ',', $this->input->get_post('ids') ));
        
        $this->message_center_model->update_important($ids);
       $user_email = $this->session->userdata('email');
        $data['importantbox_inquiries'] = $this->message_center_model->get_important_inquiries($user_email);
        $this->load->view('message_center_ajax_load',$data,true);
    }
    public function mark_as_readed_inquiries() {
        $this->load->model('message_center_model');
        $this->load->model('message_center_mapped_model');
        $this->load->model('message_placeholder_model');
        $ids = ( explode( ',', $this->input->get_post('ids') ));
        
        $this->message_center_model->update_mark_readed($ids);
        redirect('refresh');
//       $user_email = $this->session->userdata('email');
//        $data['importantbox_inquiries'] = $this->message_center_model->get_important_inquiries($user_email);
//        $this->load->view('message_center_ajax_load',$data,true);
    }
    public function pagination_sent(){
        $page_number = $this->input->post('page_number');
        $item_par_page = 3;
        $position = ($page_number*$item_par_page);
        $user_email = $this->session->userdata('email');
        $result_set = $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '3' AND message_center_mapped.main_user_email = '".$user_email."' LIMIT ".$position.",".$item_par_page);
        $total_set =  $result_set->num_rows();
        $page =  $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '3' AND message_center_mapped.main_user_email = '".$user_email."'") ;
        $total =  $page->num_rows();
        //break total recoed into pages
        $total = ceil($total/$item_par_page);
        if($total_set>0){
            $entries = null;
	// get data and store in a json array
            foreach($result_set->result() as $row){
                 $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );  

            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
}
        exit;
        
   }

   public function pagination_important(){
        $page_number = $this->input->post('page_number');
        $item_par_page = 3;
        $position = ($page_number*$item_par_page);
        $user_email = $this->session->userdata('email');
        $result_set = $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '2' AND message_center_mapped.main_user_email = '".$user_email."' LIMIT ".$position.",".$item_par_page);
        $total_set =  $result_set->num_rows();
        $page =  $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '2' AND message_center_mapped.main_user_email = '".$user_email."'") ;
        $total =  $page->num_rows();
        //break total recoed into pages
        $total = ceil($total/$item_par_page);
        if($total_set>0){
            $entries = null;
	// get data and store in a json array
            foreach($result_set->result() as $row){
                 $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );  
            
            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
        
   }
   public function pagination_trash(){
        $page_number = $this->input->post('page_number');
        $item_par_page = 3;
        $position = ($page_number*$item_par_page);
        $user_email = $this->session->userdata('email');
        $result_set = $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '4' AND message_center_mapped.main_user_email = '".$user_email."' LIMIT ".$position.",".$item_par_page);
        $total_set =  $result_set->num_rows();
        $page =  $this->db->query("SELECT * FROM message_center left join message_center_mapped on message_center.message_id = message_center_mapped.message_id left join message_placeholder on message_center_mapped.placeholder_id = message_placeholder.placeholder_id where message_center_mapped.placeholder_id = '4' AND message_center_mapped.main_user_email = '".$user_email."'") ;
        $total =  $page->num_rows();
        //break total recoed into pages
        $total = ceil($total/$item_par_page);
        if($total_set>0){
            $entries = null;
	// get data and store in a json array
            foreach($result_set->result() as $row){
                 $entries[] = $row;
            }
            $data = array(
                'TotalRows' => $total,
                'Rows' => $entries
            );  
            
            $this->output->set_content_type('application/json');
            echo json_encode(array($data));
        }
        exit;
        
   }
}