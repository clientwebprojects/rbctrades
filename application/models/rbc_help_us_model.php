<?php
class Rbc_help_us_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'status' => $item['status'],
			'message' => $item['message'],
                        'user_id' => $item['user_id'],
			 ); 

		if($this->db->insert('rbc_help_us', $data)){
                    return true;
                }else{
                    return false;
                }
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_help_us');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_help_us');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'status' => $item['status'],
			'message' => $item['message']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('rbc_help_us', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_help_us');
	}
}