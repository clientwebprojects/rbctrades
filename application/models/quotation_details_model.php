<?php
class Quotation_details_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'quot_no' => $item['quot_no'],
			'product_name' => $item['product_name'],
			'description' => $item['description'],
			'unit_price' => $item['unit_price'],
			'uom_id' => $item['uom_id'],
			'quantity' => $item['quantity']
			 ); 

		$this->db->insert('quotation_details', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('quotation_details');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('quotation_details');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}
         function get_by_quot_id($id)
	{
		$this->db->select('quotation_details.*,rbc_uom.uom_name');
		$this->db->from('quotation_details');
		$this->db->where('quot_no', $id);
                $this->db->join('rbc_uom', 'quotation_details.uom_id = rbc_uom.uom_id', 'left');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'quot_no' => $item['quot_no'],
			'product_name' => $item['product_name'],
			'description' => $item['description'],
			'unit_price' => $item['unit_price'],
			'uom_id' => $item['uom_id'],
			'quantity' => $item['quantity']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('quotation_details', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('quotation_details');
	}
}