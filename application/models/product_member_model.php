<?php
class Product_member_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'product_id' => $item['product_id'],
			'member_id' => $item['member_id']
			 ); 

		$this->db->insert('product_member', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('product_member');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        
        function get_products_of_user($id)
	{
		$this->db->select('product_id');
		$this->db->from('product_member');
                $this->db->where('member_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        
	function get_all()
	{
		$this->db->select('*');
		$this->db->from('product_member');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}
        
        
	function update($id, $item)
	{
		$data = array(
			'product_id' => $item['product_id'],
			'member_id' => $item['member_id']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('product_member', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('product_member');
	}
}