<?php
class Business_type_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'business_type_name' => $item['business_type_name']
			 ); 

		$this->db->insert('business_type', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('business_type');
		$this->db->where('business_type_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('business_type');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'business_type_name' => $item['business_type_name']
			 ); 

		$this->db->where('business_type_id', $id);
		$this->db->update('business_type', $data);
	}

	function delete($id)
	{
		$this->db->where('business_type_id', $id);
		$this->db->delete('business_type');
	}
}