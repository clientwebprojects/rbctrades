<?php
class Product_specification_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'product_id' => $item['product_id'],
			'attribute' => $item['attribute'],
			'value' => $item['value']
			 ); 

		$this->db->insert('product_specification', $data);
	}

	function get_by_id($id)
	{
		
		$this->db->select('*');
		$this->db->from('product_specification');
		$this->db->where('product_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('product_specification');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'attribute' => $item['attribute'],
			'value' => $item['value']
			 ); 

		$this->db->where('product_id', $id);
		$this->db->update('product_specification', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('product_specification');
	}
}