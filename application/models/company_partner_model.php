<?php
class Company_partner_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'partner_factory_name' => $item['partner_factory_name'],
			'coorporation_contract' => $item['coorporation_contract'],
			'partner_duration' => $item['partner_duration'],
			'partner_annual_amount' => $item['partner_annual_amount'],
                        'company_id' => $item['company_id']
			 ); 

		$this->db->insert('company_partner', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('company_partner');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        function get_by_company_id($id){
                $this->db->select('*');
		$this->db->from('company_partner');
		$this->db->where('company_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
        }

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('company_partner');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'partner_factory_name' => $item['partner_factory_name'],
			'coorporation_contract' => $item['coorporation_contract'],
			'partner_duration' => $item['partner_duration'],
			'partner_annual_amount' => $item['partner_annual_amount']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('company_partner', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('company_partner');
	}
}