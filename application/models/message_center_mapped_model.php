<?php
class Message_center_mapped_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'message_id' => $item['message_id'],
			'main_user_email' => $item['main_user_email'],
			'temp_user_email' => $item['temp_user_email'],
			'placeholder_id' => '1',
			'is_read' => '0'
			 ); 

		$this->db->insert('message_center_mapped', $data);
	}
        function create_new($item)
	{
		$data = array(
			'message_id' => $item['message_id'],
			'main_user_email' => $item['main_user_email'],
			'temp_user_email' => $item['temp_user_email'],
			'placeholder_id' => '3',
			'is_read' => '1'
			 ); 

		$this->db->insert('message_center_mapped', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('message_center_mapped');
		$this->db->where('message_id', $id);
		$this->db->select('*');
		$this->db->from('message_center_mapped');
		$this->db->where('main_user_email', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('message_center_mapped');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'temp_user_email' => $item['temp_user_email'],
			'placeholder_id' => $item['placeholder_id'],
			'is_read' => $item['is_read']
			 ); 

		$this->db->where('main_user_email', $id);
		$this->db->update('message_center_mapped', $data);
	}
        
        function update_isread($id)
	{
		$data = array(
			
			'is_read' => 1
			 ); 

		$this->db->where('message_id', $id);
		$this->db->update('message_center_mapped', $data);
	}
	function delete($id)
	{
		$this->db->where('message_id', $id);
		$this->db->where('main_user_email', $id);
		$this->db->delete('message_center_mapped');
	}
}