<?php
class Message_placeholder_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'placeholder_name' => $item['placeholder_name']
			 ); 

		$this->db->insert('message_placeholder', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('message_placeholder');
		$this->db->where('placeholder_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('message_placeholder');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'placeholder_name' => $item['placeholder_name']
			 ); 

		$this->db->where('placeholder_id', $id);
		$this->db->update('message_placeholder', $data);
	}

	function delete($id)
	{
		$this->db->where('placeholder_id', $id);
		$this->db->delete('message_placeholder');
	}
}