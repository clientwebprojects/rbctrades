<?php
class Rbc_companies_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
                        'user_id'           => $item['user_id'],
			'c_registration'    => $item['c_registration'],
			'c_name'            => $item['c_name'],
                        'c_address'         => $item['c_address'],
                        'c_zip_code'        => $item['c_zip_code'],
                        'c_verify_by'       => $item['c_verify_by'],
                        'c_phone'           => $item['c_phone'],
                        'country_id'            => $item['country_id'],
                        'c_status'          => 0
			 ); 
                //print_r($data);
		$this->db->insert('rbc_companies', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_companies');
		$this->db->where('user_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_companies');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
                        'user_id'           => $item['user_id'],
			'c_registration'    => $item['c_registration'],
			'c_name'            => $item['c_name'],
                        'c_address'         => $item['c_address'],
                        'c_zip_code'        => $item['c_zip_code'],
                        'c_verify_by'       => $item['c_verify_by'],
                        'c_phone'           => $item['c_phone']
			 ); 

		$this->db->where('c_id', $id);
		$this->db->update('rbc_companies', $data);
	}
        

	function delete($id)
	{
		$this->db->where('c_id', $id);
		$this->db->delete('rbc_companies');
	}
}