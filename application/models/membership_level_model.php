<?php
class Membership_level_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'level_name' => $item['level_name']
			 ); 

		$this->db->insert('membership_level', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('membership_level');
		$this->db->where('level_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('membership_level');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'level_name' => $item['level_name']
			 ); 

		$this->db->where('level_id', $id);
		$this->db->update('membership_level', $data);
	}

	function delete($id)
	{
		$this->db->where('level_id', $id);
		$this->db->delete('membership_level');
	}
}