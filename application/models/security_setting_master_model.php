<?php
class Security_setting_master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'member_id' => $item['member_id'],
			'security_question_id' => $item['security_question_id'],
			'security_answer' => $item['security_answer']
			 );

		$this->db->insert('security_setting_master', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('security_setting_master');
		$this->db->where('member_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('security_setting_master');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'security_answer' => $item['security_answer']
			 );

		$this->db->where('security_question_id', $id);
		$this->db->update('security_setting_master', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->where('member_id', $id);
		$this->db->where('security_question_id', $id);
		$this->db->delete('security_setting_master');
	}
}