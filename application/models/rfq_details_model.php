<?php
class Rfq_details_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'rfq_id' => $item['rfq_id'],
			'rfq_product_name' => $item['rfq_product_name'],
			'rfq_product_quantity' => $item['rfq_product_quantity'],
			'rfq_product_uom' => $item['rfq_product_uom'],
			'rfq_product_description' => $item['rfq_product_description'],
			'rfq_product_image' => $item['rfq_product_image']
			 ); 

		$this->db->insert('rfq_details', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rfq_details');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        function get_by_quot($id)
	{
		$this->db->select('rfq_details.*,rbc_uom.uom_name');
		$this->db->from('rfq_details');
		$this->db->where('rfq_id', $id);
                $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        
//        function get_by_quot_id($id)
//	{
//		$this->db->select('rfq_details.*,rfq_master.rfq_quotation_date,rfq_master.rfq_expiry_date,rfq_master.rfq_quotation_description,rbc_uom.uom_name');
//		$this->db->from('rfq_details');
//		$this->db->where('rfq_id', $id);
//                $this->db->join('rfq_master', 'rfq_details.rfq_id = rfq_master.id', 'left');
//                $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
//		$query = $this->db->get();
//
//		if($query->num_rows()<1){
//			return null;
//		}
//		else{
//			return $query->result();
//		}
//	}
        public function get_buyer_search($term) {
        $this->db->select('rfq_details.rfq_product_name');
        $this->db->from('rfq_details');
        $this->db->join('rfq_master', ' rfq_details.rfq_id = rfq_master.id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->like('rfq_product_name', $term);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = htmlentities(stripslashes($row['rfq_product_name'])); //build an array
            }
            //echo $this->db->last_query();exit();
            echo json_encode($row_set); //format the array into json data
        }
        else{
            $row_set[] = "No records found";
            echo json_encode($row_set);
        }
    } 

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rfq_details');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'rfq_id' => $item['rfq_id'],
			'rfq_product_name' => $item['rfq_product_name'],
			'rfq_product_quantity' => $item['rfq_product_quantity'],
			'rfq_product_uom' => $item['rfq_product_uom'],
			'rfq_product_description' => $item['rfq_product_description'],
			'rfq_product_image' => $item['rfq_product_image']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('rfq_details', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rfq_details');
	}
}