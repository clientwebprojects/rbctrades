<?php
class Company_certification_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'certification_type' => $item['certification_type'],
			'reference_no' => $item['reference_no'],
			'name' => $item['name'],
			'issued_by' => $item['issued_by'],
			'starting_date' => $item['starting_date'],
			'ending_date' => $item['ending_date'],
			'image_url' => $item['image_url'],
			'scope' => $item['scope'],
			'company_id' => $item['company_id']
			 ); 

		$this->db->insert('company_certification', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('company_certification');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        
        function get_by_company_id($id)
	{   
		$this->db->select('*');
		$this->db->from('company_certification');
		$this->db->where('company_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('company_certification');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'certification_type' => $item['certification_type'],
			'reference_no' => $item['reference_no'],
			'name' => $item['name'],
			'issued_by' => $item['issued_by'],
			'starting_date' => $item['starting_date'],
			'ending_date' => $item['ending_date'],
			'image_url' => $item['image_url'],
			'scope' => $item['scope'],
			'company_id' => $item['company_id']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('company_certification', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('company_certification');
	}
}