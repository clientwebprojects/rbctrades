<?php

class Rbc_user_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function create($item) {
        $data = array(
            'member_id' => $item['member_id'],
            'user_name' => $item['member_name'],
            'user_password' => md5($item['user_password']),
            'user_email' => $item['user_email'],
            'alternative_email' => $item['alternative_email'],
            'street_adddress' => $item['street_adddress'],
            'city' => $item['city'],
            'province' => $item['province'],
            'country' => $item['country'],
            'zip_code' => $item['zip_code'],
            'phone' => $item['phone'],
            'fax' => $item['fax'],
            'mobile' => $item['mobile'],
            'gender' => $item['gender'],
            'job_title' => $item['job_title'],
            'department' => $item['department'],
            'facebook_uid' => $item['facebook_uid'],
            'hash_key' => $item['hash_key']
        );
        $this->db->set('hash_key_time', 'NOW()', FALSE);
        $this->db->insert('rbc_user', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from('rbc_user');
        $this->db->where('user_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
    function get_by_email($email) {
        $this->db->select('*');
        $this->db->from('rbc_user');
        $this->db->where('user_email', $email);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function search_buyer_data($id) {
        $this->db->select('user_email,user_name');
        $this->db->from('rbc_user');
        $this->db->where('member_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
    function mail_exists($email) {
        $this->db->select('*');
        $this->db->from('rbc_user');
        $this->db->where('user_email', $email);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return false;
        } else {
            return true;
        }
    }

    function get_all() {
        $this->db->select('*');
        $this->db->from('rbc_user');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result_array();
        }
    }

    function update($id, $item) {
        //echo '<pre>';
        //print_r($item);
        $data = array(
            'user_name' => $item['member_name'],
            'city' => $item['city'],
            'province' => $item['province'],
            'country' => $item['country'],
            'zip_code' => $item['zip_code'],
            'phone' => $item['phone'],
            'fax' => $item['fax'],
            'mobile' => $item['mobile'],
            'gender' => $item['gender'],
            'job_title' => $item['job_title'],
            'business_type' => $item['business_type'],
            'department' => $item['department'],
            'street_adddress' => $item['street_adddress'],
        );//print_r($data);exit();
        $this->db->set('hash_key_time', 'NOW()', FALSE);
        $this->db->where('user_id', $id);
        $this->db->update('rbc_user', $data);
    }

    function delete($id) {
        $this->db->where('user_id', $id);
        $this->db->delete('rbc_user');
    }

    function check_user($user, $pwd) {
        $query = $this->db->get_where('rbc_user', array('user_email' => $user, 'user_password' => md5($pwd)));

        if ($query->num_rows() < 1) {
            //print_r($query);
            return null;
        } else {
            return $query->row();
            //print_r($query);
        }
    }

    // Asad <<<<
    function check_facebook_user($fb_uid) {

        $query = $this->db->get_where('rbc_user', array('facebook_uid' => $fb_uid));

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
    function get_hash_key_time($key){
        $this->db->select('hash_key_time');
        $this->db->from('rbc_user');
        $this->db->where('hash_key',$key);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
    function update_email($member_id, $item) {
        $data = array(
            'user_email' => $item['user_email']
        );
        $this->db->where('member_id', $member_id);
        $this->db->update('rbc_user', $data);
    }

    function update_user_status($id,$item){
        $data = array(
            'is_active' => $item
        );
        $this->db->where('user_id', $id);
        $this->db->update('rbc_user', $data);

    }
    function delete_hash($id){
        $data = array(
            'hash_key' => "",
            'hash_key_time' => ""
        );
        $this->db->where('user_id', $id);
        $this->db->update('rbc_user', $data);
    }
    function update_hash($email,$key){
        $data = array(
            'hash_key' => $key
        );
        $this->db->where('user_email', $email);
        $this->db->set('hash_key_time', 'NOW()', FALSE);
        $this->db->update('rbc_user', $data);
}
    function change_password($email,$pass){
        $data = array(
            'user_password' => md5($pass),
            'hash_key' => "",
            'hash_key_time' => ""
            
        );
        $this->db->where('user_email', $email);
        $this->db->update('rbc_user', $data);
    }
    function update_password($email,$key){
        $data = array(
            'hash_key' => $key
        );
        $this->db->where('user_email', $email);
        $this->db->set('hash_key_time', 'NOW()', FALSE);
        $this->db->update('rbc_user', $data);
    }
}
