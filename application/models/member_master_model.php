<?php
class Member_master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'member_name' => $item['member_name'],
			'member_type' => $item['member_type'],
			'company_id' => $item['company_id']
			 );

		$this->db->insert('member_master', $data);
                $insert_id = $this->db->insert_id();
                $this->db->trans_complete();
                return  $insert_id;
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('member_master');
		$this->db->where('member_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('member_master');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'member_name' => $item['member_name'],
			'member_type' => $item['member_type'],
			'company_id' => $item['company_id']
			 );

		$this->db->where('member_id', $id);
		$this->db->update('member_master', $data);
	}
        function update_type($id){
            $data = array(
			'member_type' => "both"
			 );

		$this->db->where('member_id', $id);
		$this->db->update('member_master', $data);
        }
	function delete($id)
	{
		$this->db->where('member_id', $id);
		$this->db->delete('member_master');
	}
        function get_company_id($id)
	{
		$this->db->select('company_id,member_type');
		$this->db->from('member_master');
		$this->db->where('member_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
          function get_member_by_id($id) {

        $this->db->select('member_master.*,rbc_countries.country_name,company_master.operational_location');
        $this->db->from('member_master');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->where('member_master.member_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
}