<?php

class Company_master_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function create($item) {
        $data = array(
            'business_type' => $item['business_type'],
            'company_name' => $item['company_name'],
            'contact_no' => $item['contact_no'],
            'registration_location' => $item['registration_location'],
            'operational_street_address' => $item['operational_street_address'],
            'operational_location' => $item['registration_location'],
            'operational_address_zip_code' => $item['operational_address_zip_code'],
            'main_products' => $item['main_products'],
            'other_products' => $item['other_products'],
            'registered_year' => $item['registered_year'],
            'total_employees' => $item['total_employees'],
            'website_url' => $item['website_url'],
            'legal_owner' => $item['legal_owner'],
            'office_size' => $item['office_size'],
            'company_advantages' => $item['company_advantages'],
            'annual_sales_volume' => $item['annual_sales_volume'],
            'export_percentage' => $item['export_percentage'],
            'exporting_started' => $item['exporting_started'],
            'trade_staff_num' => $item['trade_staff_num'],
            'cross_industry' => $item['cross_industry'],
            'rnd_staff_num_trade' => $item['rnd_staff_num_trade'],
            'qc_staff_num_trade' => $item['qc_staff_num_trade'],
            'export_port' => $item['export_port'],
            'avg_delivery_time' => $item['avg_delivery_time'],
            'have_oversea_office' => $item['have_oversea_office'],
            'delievery_terms' => $item['delievery_terms'],
            'support_currency' => $item['support_currency'],
            'payment_methods' => $item['payment_methods'],
            'language_skills' => $item['language_skills'],
            'logo_url' => $item['logo_url'],
            'photo_url' => $item['photo_url'],
            'company_description' => $item['company_description'],
            'operational_address_city' => $itme['operational_address_city']
        );

        $this->db->insert('company_master', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function get_by_id($company_id) {
        $this->db->select('a.*, a.registration_location, a.operational_location, c.country_id, c.country_name As registration_location_country ,d.country_id,d.country_name as operational_location_country');
        $this->db->from('company_master As a');
        $this->db->join('rbc_countries As c', ' a.registration_location = c.country_id', 'LEFT');
        $this->db->join('rbc_countries As d', ' a.operational_location = d.country_id', 'LEFT');
        $this->db->where('id', $company_id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

//SELECT a.*,a.`registration_location`,a.operational_location,c.country_id, c.country_name as country,d.country_id,d.country_name as cont
//FROM company_master as a
//LEFT JOIN rbc_countries AS c
//  ON a.`registration_location` = c.`country_id`
//LEFT JOIN rbc_countries AS d
//  ON a.operational_location = d.country_id
//WHERE `user_id` = '4'
    function get_all() {
        $this->db->select('*');
        $this->db->from('company_master');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result_array();
        }
    }

    function get_all_companies() {
        $this->db->select('company_master.company_name,company_master.logo_url');
        $this->db->from('company_master');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    public function get_supplier_search($term) {
        $this->db->select('company_name');
        $this->db->from('company_master');
        $this->db->like('company_name', $term);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = htmlentities(stripslashes($row['company_name'])); //build an array
            }
            //echo $this->db->last_query();exit();
            echo json_encode($row_set); //format the array into json data
        }
        else{
            $row_set[] = "No records found";
            echo json_encode($row_set);
        }
    }
//    SELECT c.company_name
//FROM company_master As c
//RIGHT JOIN member_master As m
//ON m.company_id=c.id
//WHERE m.member_type = "both" OR  m.member_type = "supplier" AND c.company_name LIKE 'abc%'
    public function get_buyer_search($term) {
        $this->db->select('company_name');
        $this->db->from('company_master');
        $this->db->like('company_name', $term);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = htmlentities(stripslashes($row['company_name'])); //build an array
            }
            //echo $this->db->last_query();exit();
            echo json_encode($row_set); //format the array into json data
        }
    }

    function update($id, $item) {

        $data = array(
            'company_name' => $item['company_name'],
            'operational_street_address' => $item['operational_street_address'],
            'operational_address_city' => $item['operational_address_city'],
            //'operational_address_city' => $itme['operational_address_city'],
            'operational_location' => $item['operational_location'],
            'annual_purchase_volume' => $item['annual_purchase_volume'],
            'operational_address_zip_code' => $item['operational_address_zip_code'],
            'website_url' => $item['website_url'],
            'preffered_seller_location'=> $item['preffered_seller_location'],
            'preffered_seller_type'=>$item['preffered_seller_type'],
            'purchasing_frequency' =>$item['purchasing_frequency'],
            'in_industry' => $item['in_industry'],
            'logo_url' => $item['logo_url'],
            'company_description' => $item['company_description'],
            'operational_address_province' => $item['operational_address_province'],
            'company_description' => $item['company_description'],
            'business_experiences' => $item['business_experiences']
        );
        //print_r($data)        ;exit();
        $this->db->where('id', $id);
        $this->db->update('company_master', $data);
    }

    public function update_basic_details($id, $item) {
        $data = array(
            'business_type' => $item['business_type'],
            'company_name' => $item['company_name'],
            'contact_no' => $item['contact_no'],
            'registration_location' => $item['registration_location'],
            'operational_street_address' => $item['operational_street_address'],
            'operational_location' => $item['operational_location'],
            'operational_address_zip_code' => $item['operational_address_zip_code'],
            'operational_address_city' => $item['operational_address_city'],
            'main_products' => $item['main_products'],
            'other_products' => $item['other_products'],
            'registered_year' => $item['registered_year'],
            'total_employees' => $item['total_employees'],
            'website_url' => $item['website_url'],
            'legal_owner' => $item['legal_owner'],
            'office_size' => $item['office_size'],
            'company_advantages' => $item['company_advantages'],
        );
        $this->db->where('id', $id);
        $this->db->update('company_master', $data);
    }

    public function update_company_name($id, $item) {
        $data = array(
            'company_name' => 'company_name'
        );
        $this->db->where('id', $id);
        $this->db->update('company_master', $data);
    }

    public function update_trade_details($id, $item) {
        $data = array(
            'annual_sales_volume' => $item['annual_sales_volume'],
            'export_percentage' => $item['export_percentage'],
            'exporting_started' => $item['exporting_started'],
            'trade_staff_num' => $item['trade_staff_num'],
            'cross_industry' => $item['cross_industry'],
            'rnd_staff_num_trade' => $item['rnd_staff_num_trade'],
            'qc_staff_num_trade' => $item['qc_staff_num_trade'],
            'export_port' => $item['export_port'],
            'avg_delivery_time' => $item['avg_delivery_time'],
            'have_oversea_office' => $item['have_oversea_office'],
            'delievery_terms' => $item['delievery_terms'],
            'support_currency' => $item['support_currency'],
            'payment_methods' => $item['payment_methods'],
            'language_skills' => $item['language_skills'],
        );
        $this->db->where('id', $id);
        $this->db->update('company_master', $data);
    }

    public function update_company_introduction($id, $item) {
        $data = array(
            'logo_url' => $item['logo_url'],
            'company_description' => $item['company_description'],
        );
        $this->db->where('id', $id);
        $this->db->update('company_master', $data);
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('company_master');
    }

}
