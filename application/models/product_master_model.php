<?php

class Product_master_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_all_by_category_id($id) {
        $this->db->select('p.*,p_image.image_url,p_image.is_main,cat.category_name');
        $this->db->from('product_master As p,product_images AS p_image,rbc_category as cat');
        $this->db->where('p.product_category', $id);
        $this->db->where('p_image.is_main', '1');
        $this->db->where('p_image.product_id = p.product_id');
        $this->db->where('p.product_category = cat.category_id');

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function create($item) {
        $data = array(
            'product_category' => $item['product_category'],
			'product_code' => $item['product_code'],
			'product_model' => $item['product_model'],
			'brand_id' => $item['brand_id'],
			'images' => $item['images'],
			'product_name' => $item['product_name'],
			'product_description' => $item['product_description'],
			'product_keyword' => $item['product_keyword'],
			'uom_id' => $item['uom_id'],
			'product_status' => "A",
			'product_origin' => $item['product_origin'],
			'product_type' => $item['product_type'],
			'product_minimun_order' => $item['product_minimun_order'],
			'minimun_order_unit' => $item['minimun_order_unit'],
			'product_port' => $item['product_port'],
			'product_payment_type' => $item['product_payment_type'],
			'product_supply_ability' => $item['product_supply_ability'],
			'product_delivery_time' => $item['product_delivery_time'],
			'product_delivery_unit' => $item['product_delivery_unit'],
			'packaging_detail' => $item['packaging_detail'],
			'product_detailed_description' => $item['product_detailed_description'],
                        'product_fob_currency' => $item['product_fob_currency'],
			'product_fob_price_from' => $item['product_fob_price_from'],
			'product_fob_price_to' => $item['product_fob_price_to'],
			'product_fob_unit' => $item['product_fob_unit'],
			'product_slug' => $item['product_slug']
        );
        $this->db->set('product_add_date', 'NOW()', FALSE);
        //print_r($item);exit();
        $this->db->insert('product_master', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function get_all_pending_products($id) {
        $this->db->select('*');
        $this->db->from('product_master');
        $this->db->where('product_status', 'P');
        //$this->db->join('product_member', ' product_master.product_id = product_member.id', 'LEFT');
        $this->db->where('product_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_all_active_products($id) {
        $this->db->select('*');
        $this->db->from('product_master');
        $this->db->where('product_status', 'A');
        $this->db->where('product_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_all_rejected_products($id) {
        $this->db->select('*');
        $this->db->from('product_master');
        $this->db->where('product_status', 'R');
        $this->db->where('product_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from('product_master');
        $this->db->join('rbc_category', 'product_master.product_category = rbc_category.category_id', 'left');
        $this->db->where('product_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

       function get_product_by_id($id) {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,rbc_user.user_email,member_master.company_id,company_master.*,uom.uom_name,rbc_countries.country_name,rbc_currency.*,company_certification.*,product_images.*');
        $this->db->from('product_master');
        $this->db->join('rbc_category', 'product_master.product_category = rbc_category.category_id', 'left');
        $this->db->join('product_member', 'product_master.product_id = product_member.product_id', 'left');
        $this->db->join('rbc_user', 'product_member.member_id = rbc_user.member_id', 'left');
        $this->db->join('member_master', 'rbc_user.member_id = member_master.member_id', 'left');
        $this->db->join('company_master', 'member_master.company_id = company_master.id', 'left');
        $this->db->join('company_certification', 'company_master.id = company_certification.company_id', 'left');
        $this->db->join('rbc_uom as uom', ' product_master.uom_id = uom.uom_id', 'LEFT');
        $this->db->join('rbc_currency', ' product_master.product_fob_currency = rbc_currency.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->join('product_images', 'product_images.product_id=product_master.product_id','LEFT');
        $this->db->where('product_images.is_main', 1);
        $this->db->where('product_master.product_id', $id);
        $this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
    function get_product_payment_type_name_by_id($id) {
        $this->db->select('rbc_payment_type.p_term_description');
        $this->db->from('rbc_payment_type');
        $this->db->where('p_term_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
    function get_product_business_type_name_by_id($id) {
        $this->db->select('business_type.business_type_name');
        $this->db->from('business_type');
        $this->db->where('business_type_id', $id);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }
    function get_all_for_pagging($item_par_page, $position) {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,rbc_user.user_email,member_master.company_id,company_master.*,uom.uom_name,rbc_countries.country_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category', 'product_master.product_category = rbc_category.category_id', 'left');
//        $this->db->join('product_images As i', ' product_master.product_id = i.product_id', 'LEFT');
        $this->db->join('product_member', 'product_master.product_id = product_member.product_id', 'left');
        $this->db->join('rbc_user', 'product_member.member_id = rbc_user.member_id', 'left');
        $this->db->join('member_master', 'rbc_user.member_id = member_master.member_id', 'left');
        $this->db->join('company_master', 'member_master.company_id = company_master.id', 'left');
        $this->db->join('rbc_uom as uom', ' product_master.uom_id = uom.uom_id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.registration_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page, $position);
        $query = $this->db->get();

        return $query;
    }

    function get_all() {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,rbc_user.user_email,member_master.company_id,company_master.*,uom.uom_name,rbc_countries.country_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category', 'product_master.product_category = rbc_category.category_id', 'left');
        $this->db->join('product_member', 'product_master.product_id = product_member.product_id', 'left');
        $this->db->join('rbc_user', 'product_member.member_id = rbc_user.member_id', 'left');
        $this->db->join('member_master', 'rbc_user.member_id = member_master.member_id', 'left');
        $this->db->join('company_master', 'member_master.company_id = company_master.id', 'left');
        $this->db->join('rbc_uom as uom', ' product_master.uom_id = uom.uom_id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.registration_location = rbc_countries.country_id', 'LEFT');
        //$this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
    function get_all_main() {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,rbc_user.user_email,member_master.company_id,uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category', 'product_master.product_category = rbc_category.category_id', 'left');
        $this->db->join('product_member', 'product_master.product_id = product_member.product_id', 'left');
        $this->db->join('rbc_user', 'product_member.member_id = rbc_user.member_id', 'left');
        $this->db->join('member_master', 'rbc_user.member_id = member_master.member_id', 'left');
        $this->db->join('rbc_uom as uom', ' product_master.uom_id = uom.uom_id', 'LEFT');
       //$this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
    function get_all_main_by_user($user_id) {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,rbc_user.user_email,member_master.company_id,uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category', 'product_master.product_category = rbc_category.category_id', 'left');
        $this->db->join('product_member', 'product_master.product_id = product_member.product_id', 'left');
        $this->db->join('rbc_user', 'product_member.member_id = rbc_user.member_id', 'left');
        $this->db->join('member_master', 'rbc_user.member_id = member_master.member_id', 'left');
        $this->db->join('rbc_uom as uom', ' product_master.uom_id = uom.uom_id', 'LEFT');
        $this->db->where('product_member.member_id', $user_id);
       //$this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_by_country($item_par_page,$position,$country_id) {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,member_master.company_id,company_master.*,rbc_countries.country_name,rbc_uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category', ' product_master.product_category = rbc_category.category_id', 'LEFT');
        $this->db->join('product_member', ' product_master.product_id = product_member.product_id', 'LEFT');
        $this->db->join('member_master', ' product_member.member_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->join('rbc_uom', ' product_master.uom_id = rbc_uom.uom_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('company_master.operational_location', $country_id);
//        $this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query;
        }
    }
    function get_all_by_country_count($country_id) {
        $this->db->select('product_master.*,product_member.member_id,member_master.company_id,company_master.operational_location,rbc_uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('product_member', ' product_master.product_id = product_member.product_id', 'LEFT');
        $this->db->join('member_master', ' product_member.member_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_uom', ' product_master.uom_id = rbc_uom.uom_id', 'LEFT');
        $this->db->where('company_master.operational_location', $country_id);
//        $this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query;
        }
    }

    function update($id, $item) {
        $data = array(
            'product_category' => $item['product_category'],
			'product_code' => $item['product_code'],
			'product_model' => $item['product_model'],
			'brand_id' => $item['brand_id'],
			'images' => $item['images'],
			'product_name' => $item['product_name'],
			'product_description' => $item['product_description'],
			'product_keyword' => $item['product_keyword'],
			'uom_id' => $item['uom_id'],
			'product_status' => "A",
			'product_origin' => $item['product_origin'],
			'product_type' => $item['product_type'],
			'product_minimun_order' => $item['product_minimun_order'],
			'minimun_order_unit' => $item['minimun_order_unit'],
			'product_port' => $item['product_port'],
			'product_payment_type' => $item['product_payment_type'],
			'product_supply_ability' => $item['product_supply_ability'],
			'product_delivery_time' => $item['product_delivery_time'],
			'product_delivery_unit' => $item['product_delivery_unit'],
			'packaging_detail' => $item['packaging_detail'],
			'product_detailed_description' => $item['product_detailed_description'],
                        'product_fob_currency' => $item['product_fob_currency'],
			'product_fob_price_from' => $item['product_fob_price_from'],
			'product_fob_price_to' => $item['product_fob_price_to'],
			'product_fob_unit' => $item['product_fob_unit'],
			'product_slug' => $item['product_slug']
        );
     //print_r($data);exit();
        $this->db->set('product_last_updated', 'NOW()', FALSE);
        $this->db->where('product_id', $id);
        $this->db->update('product_master', $data);
    }

    function delete($id) {
        $this->db->where('product_id', $id);
        $this->db->delete('product_master');
    }

    function search_category_product($category_id) {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,member_master.company_id,company_master.*,rbc_countries.country_name,rbc_uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category','product_master.product_category = rbc_category.category_id','left');
         $this->db->join('product_member', ' product_master.product_id = product_member.product_id', 'LEFT');
        $this->db->join('member_master', ' product_member.member_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->join('rbc_uom ', ' product_master.uom_id = rbc_uom.uom_id', 'LEFT');
        $this->db->where('product_category', $category_id);



        //$this->db->where('product_status', 'A');
        //$this->db->join('rbc_category','product_master.product_category = rbc_category.parent_id','left');
        return $this->db->get()->result();
    }

    function search_all_category_product() {
        $this->db->select('product_master.*,rbc_category.category_name,rbc_uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category','product_master.product_category = rbc_category.category_id','left');
        $this->db->join('rbc_uom ', ' product_master.uom_id = rbc_uom.uom_id', 'LEFT');
        $this->db->where('product_status', 'A');
        //$this->db->join('rbc_category','product_master.product_category = rbc_category.parent_id','left');
        return $this->db->get()->result();
    }

    function get_all_pagination_products() {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,rbc_user.user_email,member_master.company_id,company_master.*,uom.uom_name,rbc_countries.country_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category','product_master.product_category = rbc_category.category_id','left');
//        $this->db->join('product_images As i', ' product_master.product_id = i.product_id', 'LEFT');
        $this->db->join('product_member','product_master.product_id = product_member.product_id','left');
        $this->db->join('rbc_user','product_member.member_id = rbc_user.member_id','left');
        $this->db->join('member_master','rbc_user.member_id = member_master.member_id','left');
        $this->db->join('company_master','member_master.company_id = company_master.id','left');
        $this->db->join('rbc_uom as uom', ' product_master.uom_id = uom.uom_id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.registration_location = rbc_countries.country_id', 'LEFT');
        //$this->db->limit(9);
        $query = $this->db->get();


        return $query;
    }

    function get_search_result($q) {
        $this->db->select('product_master.product_name');
        $this->db->from('product_master');
        $this->db->like('product_master.product_name', $q);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result_array() as $row) {
                $row_set[] = htmlentities(stripslashes($row['product_name'])); //build an array
            }
            echo json_encode($row_set); //format the array into json data
        }
        else{
            $row_set[] = "No records found";
            echo json_encode($row_set);
        }
    }
    function pagination_search_product($item_par_page,$position,$term) {
        $this->db->select('product_master.*,rbc_category.category_name,product_member.member_id,member_master.company_id,company_master.*,rbc_countries.country_name,rbc_uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('rbc_category', ' product_master.product_category = rbc_category.category_id', 'LEFT');
        $this->db->join('product_member', ' product_master.product_id = product_member.product_id', 'LEFT');
        $this->db->join('member_master', ' product_member.member_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->join('rbc_uom', ' product_master.uom_id = rbc_uom.uom_id', 'LEFT');
        $this->db->like('product_master.product_name', $term);
        $this->db->limit($item_par_page,$position);

//        $this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query;
}
    }
    function pagination_search_product_count($term) {
        $this->db->select('product_master.*,product_member.member_id,member_master.company_id,company_master.operational_location,rbc_uom.uom_name');
        $this->db->from('product_master');
        $this->db->join('product_member', ' product_master.product_id = product_member.product_id', 'LEFT');
        $this->db->join('member_master', ' product_member.member_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_uom', ' product_master.uom_id = rbc_uom.uom_id', 'LEFT');
        $this->db->like('product_master.product_name', $term);
//        $this->db->where('product_status', 'A');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query;
        }
    }

}
