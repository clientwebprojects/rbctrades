<?php
class Rbc_currency_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'currency_name' => $item['currency_name'],
			'currency_code' => $item['currency_code'],
			'currency_symbol' => $item['currency_symbol']
			 );

		$this->db->insert('rbc_currency', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_currency');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_currency');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'currency_name' => $item['currency_name'],
			'currency_code' => $item['currency_code'],
			'currency_symbol' => $item['currency_symbol']
			 );

		$this->db->where('id', $id);
		$this->db->update('rbc_currency', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_currency');
	}
}