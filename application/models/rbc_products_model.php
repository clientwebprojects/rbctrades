<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rbc_products_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function create($item) {
        $data = array(
            'product_category'      => $item['product_category'],
            'product_code'          => $item['product_model'],
            'product_model'         => $item['product_model'],
            'brand_id'              => $item['brand_id'],
            'images'                => "1",
            'product_name'          => $item['product_name'],
            'product_description'   => $item['editor1'],
            'product_keyword'       => $item['product_keyword'],
            'uom_id'                => "1"
        );
        //print_r($item);exit();
        $this->db->insert('product_master', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    function get_by_id($id) {
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_all() {
        $this->db->select('*');
        $this->db->from('product_master');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function get_all_products_by_id($id) {

        $this->db->select('*');
        $this->db->from('product_master');
        $this->db->where('', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

    function update($id, $item) {
        $data = array(
            'p_name ' => $item['p_name '],
            'p_details ' => $item['p_details '],
            'category_id' => $item['category_id']
        );

        $this->db->where('', $id);
        $this->db->update('product_master', $data);
    }

    function delete($id) {
        $this->db->delete('product_master');
    }

    function active_product_count($id) {

        $this->db->select('COUNT(p_id) as count');
        $this->db->from('product_master');
        $where = "user_id = " . $id . " AND p_status = 'active'";
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }

}
