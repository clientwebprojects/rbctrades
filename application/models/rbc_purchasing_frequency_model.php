<?php
class Rbc_purchasing_frequency_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'purchasing_frequency' => $item['purchasing_frequency']
			 ); 

		$this->db->insert('rbc_purchasing_frequency', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('rbc_purchasing_frequency');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('rbc_purchasing_frequency');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function update($id, $item)
	{
		$data = array(
			'purchasing_frequency' => $item['purchasing_frequency']
			 ); 

		$this->db->where('id', $id);
		$this->db->update('rbc_purchasing_frequency', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rbc_purchasing_frequency');
	}
}