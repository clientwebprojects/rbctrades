<?php

class Message_center_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function create($item) {
        $data = array(
            'subject' => $item['subject'],
            'body' => $item['editor1'],
            'date' => date('y-m-d')
        );
       $this->db->insert('message_center', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function create_quote_message($item) {
        $data = array(
            'subject' => $item['subject'],
            'body' => $item['body'],
            'date' => date('y-m-d')
        );
       $this->db->insert('message_center', $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    function get_by_id($id) {
        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->where('message_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->row();
        }
    }

    function get_all() {
        $this->db->select('*');
        $this->db->from('message_center');
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result_array();
        }
    }
            
            function get_information_email($id) {

        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->join('message_center_mapped', 'message_center.message_id = message_center_mapped.message_id', 'left');
        $this->db->join('message_placeholder', 'message_center_mapped.placeholder_id = message_placeholder.placeholder_id', 'left');
        $this->db->where('message_center_mapped.placeholder_id', '1');
        $this->db->where('message_center_mapped.message_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
    function get_important_information_email($id) {

        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->join('message_center_mapped', 'message_center.message_id = message_center_mapped.message_id', 'left');
        $this->db->join('message_placeholder', 'message_center_mapped.placeholder_id = message_placeholder.placeholder_id', 'left');
        $this->db->where('message_center_mapped.placeholder_id', '2');
        $this->db->where('message_center_mapped.message_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
    function get_trash_information_email($id) {

        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->join('message_center_mapped', 'message_center.message_id = message_center_mapped.message_id', 'left');
        $this->db->join('message_placeholder', 'message_center_mapped.placeholder_id = message_placeholder.placeholder_id', 'left');
        $this->db->where('message_center_mapped.placeholder_id', '4');
        $this->db->where('message_center_mapped.message_id', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
    function get_information($id) {

        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->join('message_center_mapped', 'message_center.message_id = message_center_mapped.message_id', 'left');
        $this->db->join('message_placeholder', 'message_center_mapped.placeholder_id = message_placeholder.placeholder_id', 'left');
        $this->db->where('message_center_mapped.placeholder_id', '1');
        $this->db->where('message_center_mapped.main_user_email', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
    function get_sent_inquiries($id) {

        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->join('message_center_mapped', 'message_center.message_id = message_center_mapped.message_id', 'left');
        $this->db->join('message_placeholder', 'message_center_mapped.placeholder_id = message_placeholder.placeholder_id', 'left');
        $this->db->where('message_center_mapped.placeholder_id', '3');
        $this->db->where('message_center_mapped.main_user_email', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
        function get_important_inquiries($id) {

        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->join('message_center_mapped', 'message_center.message_id = message_center_mapped.message_id', 'left');
        $this->db->join('message_placeholder', 'message_center_mapped.placeholder_id = message_placeholder.placeholder_id', 'left');
        $this->db->where('message_center_mapped.placeholder_id', '2');
        $this->db->where('message_center_mapped.main_user_email', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
    function get_trash_inquiries($id) {

        $this->db->select('*');
        $this->db->from('message_center');
        $this->db->join('message_center_mapped', 'message_center.message_id = message_center_mapped.message_id', 'left');
        $this->db->join('message_placeholder', 'message_center_mapped.placeholder_id = message_placeholder.placeholder_id', 'left');
        $this->db->where('message_center_mapped.placeholder_id', '4');
        $this->db->where('message_center_mapped.main_user_email', $id);
        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
        
    function update($id) {
        $data = array(
            'placeholder_id' => '4'
        );
        $ids = $id;
       $count = 0;
        foreach ($ids as $id){
           $did = intval($id);
            $this->db->where('message_id', $did);
            $this->db->update('message_center_mapped', $data);
            $count = $count+1;
       }
        
    }
    function update_detail_message($id,$user_email) {
        $data = array(
            'placeholder_id' => '4'
        );
        
            $this->db->where('message_id', $id);
            $this->db->where('main_user_email', $user_email);
            $this->db->update('message_center_mapped', $data);
            
       }
       function update_as_important_message($id,$user_email) {
        $data = array(
            'placeholder_id' => '2'
        );
        
            $this->db->where('message_id', $id);
            $this->db->where('main_user_email', $user_email);
            $this->db->update('message_center_mapped', $data);
            
       } 
    function update_important($id) {
        $data = array(
            'placeholder_id' => '2'
        );
        $ids = $id;
       $count = 0;
        foreach ($ids as $id){
           $did = intval($id);
            $this->db->where('message_id', $did);
            $this->db->update('message_center_mapped', $data);
            $count = $count+1;
       }
        
    }
    function update_mark_readed($id) {
        $data = array(
            'is_read' => '1'
        );
        $ids = $id;
       $count = 0;
        foreach ($ids as $id){
           $did = intval($id);
            $this->db->where('message_id', $did);
            $this->db->update('message_center_mapped', $data);
            $count = $count+1;
       }
        
    }
    function delete($id) {
        $ids = $id;
       $count = 0;
        foreach ($ids as $id){
           $did = intval($id);
            $this->db->where('message_id', $did);
            $this->db->delete('message_center_mapped');
            $count = $count+1;
       }
       
    }

}
