<?php
class Company_privacy_setting_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'member_id' => $item['member_id'],
			'basic_information' => 1,
			'contact_information' => 1,
			'sourcing_information' => 1,
			'activity_summary' => 1,
			'watched_industries' => 1,
			'most_searched_keywords' => 1,
			'recently_searched_products' => 1,
			'transactions' => 1
			 ); 

		$this->db->insert('company_privacy_setting', $data);
	}

	function get_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('company_privacy_setting');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        
        function get_by_member_id($id)
        {
                $this->db->select('*');
		$this->db->from('company_privacy_setting');
		$this->db->where('member_id', $id);
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
        }

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('company_privacy_setting');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result_array();
		}
	}

	function update($id, $item)
	{
		$data = array(
			
			'contact_information' => $item['contact_information'],
			'sourcing_information' => $item['sourcing_information'],
			'activity_summary' => $item['activity_summary'],
			'watched_industries' => $item['watched_industries'],
			'most_searched_keywords' => $item['most_searched_keywords'],
			'recently_searched_products' => $item['recently_searched_products'],
			'transactions' => $item['transactions']
			 ); 
		$this->db->where('id', $id);
		$this->db->update('company_privacy_setting', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('company_privacy_setting');
	}
}