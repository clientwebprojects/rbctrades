<?php

class Rfq_master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'rfq_seller_id' => $item['rfq_seller_id'],
			'rfq_buyer_id' => $item['rfq_buyer_id'],
			'rfq_quotation_date' => $item['rfq_quotation_date'],
			'rfq_expiry_date' => $item['rfq_expiry_data'],
			'rfq_quotation_description' => $item['rfq_quotation_description'],
                        'rfq_quotes_left' => $item['rfq_quotes_left']
			 );

		$this->db->insert('rfq_master', $data);
                $insert_id = $this->db->insert_id();
                return $insert_id;
	}

        function create_main($item)
	{
		$data = array(
			'rfq_buyer_id' => $item['rfq_buyer_id'],
			'rfq_quotation_date' => $item['rfq_quotation_date'],
			'rfq_expiry_date' => $item['rfq_expiry_date'],
			'rfq_quotation_description' => $item['rfq_quotation_description']
			 );

		$this->db->insert('rfq_master', $data);
                $insert_id = $this->db->insert_id();
                return $insert_id;
	}

	function get_by_id($id)
	{
		$this->db->select('rfq_master.*,member_master.company_id,company_master.company_name,company_master.operational_street_address,company_master.operational_address_city,company_master.operational_address_province,company_master.operational_location,company_master.logo_url,rbc_countries.country_name,rbc_countries.country_code');
		$this->db->from('rfq_master');
                $this->db->join('member_master', 'rfq_master.rfq_buyer_id = member_master.member_id', 'left');
                $this->db->join('company_master', 'member_master.company_id = company_master.id', 'left');
                $this->db->join('rbc_countries', 'company_master.operational_location = rbc_countries.country_id', 'left');
		$this->db->where('rfq_master.id', $id);
                
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}


	function get_all()
	{
		$this->db->select('r.*,d.*');
		$this->db->from('rfq_master As r');
                $this->db->join('rfq_details As d','r.id = d.rfq_id','LEFT');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

        function get_all_seller_buyying_requests($id)
	{
		$this->db->select('r.*,d.*,u.user_name');
		$this->db->from('rfq_master As r');
                $this->db->join('rfq_details As d','r.id = d.rfq_id','LEFT');
                $this->db->join('rbc_user As u','r.rfq_buyer_id = u.member_id','LEFT');
                $this->db->where('r.rfq_seller_id', $id);

		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        function get_all_buyer_buying_requests($id){
                $this->db->select('r.rfq_buyer_id,r.id,q.quot_no,qd.product_name,qd.description,qd.quantity,q.quotation_date ,m.member_name');
                $this->db->from('rfq_master As r');
                $this->db->join('quotation_master As q','r.id = q.rfq_id','LEFT');
                $this->db->join('quotation_details As qd','q.quot_no=qd.quot_no','LEFT');
                $this->db->join('member_master As m','q.seller_id = m.member_id','LEFT');
                $this->db->where('r.rfq_buyer_id', $id);
                $this->db->where('q.quot_no IS NOT NULL', null, fasle);
                $query = $this->db->get();
                $this->db->last_query();
		if($query->num_rows()<1){
			return null;
		}
		else{
                       // echo $this->db->last_query();
			return $query->result();
		}
        }
//        function get_all_data()
//	{
//		$this->db->select('rfq_master.*,rfq_details.*');
//		$this->db->from('rfq_master');
//                $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
//		$query = $this->db->get();
//
//		if($query->num_rows()<1){
//			return null;
//		}
//		else{
//			return $query->result();
//		}
//	}

	function update($id, $item)
	{
		$data = array(
			'rfq_seller_id' => $item['rfq_seller_id'],
			'rfq_buyer_id' => $item['rfq_buyer_id'],
			'rfq_quotation_date' => $item['rfq_quotation_date'],
			'rfq_expiry_data' => $item['rfq_expiry_data'],
			'rfq_quotation_description' => $item['rfq_quotation_description']
			 );

		$this->db->where('id', $id);
		$this->db->update('rfq_master', $data);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rfq_master');
	}
        function get_all_data() {
        $this->db->select('rfq_master.*,rfq_details.*,rbc_user.country,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('rbc_user', ' rfq_master.rfq_buyer_id = rbc_user.member_id', 'LEFT');
        $this->db->join('rbc_countries', ' rbc_user.country = rbc_countries.country_id', 'LEFT');

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result();
        }
    }
        function get_all_data_pagination($item_par_page,$position) {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $query = $this->db->get();


            return $query;

    }


    function get_all_data_all() {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);

        $query = $this->db->get();


            return $query;

    }
        function get_all_by_country($id) {
       $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->where('rbc_user.country', $id);
        $this->db->where('rfq_master.rfq_seller_id', NULL);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result_array();
        }
    }

    function get_all_by_quantity_range($item_par_page,$position,$min, $max) {
       $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_details.rfq_product_quantity >=', $min);
        $this->db->where('rfq_details.rfq_product_quantity <=', $max);

        $query = $this->db->get();


            return $query;

    }
    function get_all_by_quantity_range_count($min, $max) {
        $this->db->select('rfq_master.id');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_details.rfq_product_quantity >=', $min);
        $this->db->where('rfq_details.rfq_product_quantity <=', $max);

        $query = $this->db->get();


            return $query;

    }
    function get_all_by_quantity_min_count($min) {
        $this->db->select('rfq_master.id');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_details.rfq_product_quantity >=', $min);

        $query = $this->db->get();


            return $query;

    }
    function get_all_by_quantity_max_count($max) {
        $this->db->select('rfq_master.id');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_details.rfq_product_quantity <=', $max);

        $query = $this->db->get();


            return $query;

    }

    function get_all_by_quantity_minimum($item_par_page,$position,$min) {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_details.rfq_product_quantity >=', $min);
        $query = $this->db->get();


            return $query;

    }

    function get_all_by_quantity_maximum($item_par_page,$position,$max) {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_details.rfq_product_quantity <=', $max);

        $query = $this->db->get();


            return $query;

    }
    function get_all_open_quotes() {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_master.rfq_quotes_left >', 0);

        $query = $this->db->get();

        if ($query->num_rows() < 1) {
            return null;
        } else {
            return $query->result_array();
        }
    }
    function get_all_open_quotes_pagination($item_par_page,$position) {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_master.rfq_quotes_left >', 0);
        $query = $this->db->get();
            return $query;
    }
    function get_by_country_pagination($item_par_page,$position,$country_id) {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('company_master.operational_location', $country_id);
        $query = $this->db->get();
            return $query;      
    }
    function get_by_country_pagination_count($country_id) {
        $this->db->select('rfq_master.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name');
        $this->db->from('rfq_master');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('company_master.operational_location', $country_id);

        $query = $this->db->get();

        
            return $query;
       
    }
    function pagination_search_quotation($item_par_page,$position,$term) {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->limit($item_par_page,$position);
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->like('rfq_details.rfq_product_name', $term);
        $query = $this->db->get();
            return $query;      
    }
    function pagination_search_quotation_count($term) {
        $this->db->select('rfq_master.*,rfq_details.rfq_product_name,member_master.company_id,company_master.operational_location,rbc_countries.country_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->like('rfq_details.rfq_product_name', $term);
        
        $query = $this->db->get();

        
            return $query;
       
    }
    function get_all_count() {
        $this->db->select('rfq_master.id');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_master.rfq_quotes_left >', 0);
        $query = $this->db->get();


            return $query;

    }
    function get_all_open_quotes_pagging() {
        $this->db->select('rfq_master.*,rfq_details.*,member_master.company_id,company_master.operational_location,rbc_countries.country_name,rbc_countries.country_code,rbc_uom.uom_name');
        $this->db->from('rfq_master');
        $this->db->join('rfq_details', ' rfq_master.id = rfq_details.rfq_id', 'LEFT');
        $this->db->join('rbc_uom', 'rfq_details.rfq_product_uom = rbc_uom.uom_id', 'left');
        $this->db->join('member_master', ' rfq_master.rfq_buyer_id = member_master.member_id', 'LEFT');
        $this->db->join('company_master', ' member_master.company_id = company_master.id', 'LEFT');
        $this->db->join('rbc_countries', ' company_master.operational_location = rbc_countries.country_id', 'LEFT');
        $this->db->where('rfq_master.rfq_seller_id', NULL);
        $this->db->where('rfq_master.rfq_quotes_left >', 0);
        $query = $this->db->get();
            return $query;
    }
    function update_quotes_left($id) {
        $this->db->where('id', $id);
        $this->db->set('rfq_quotes_left', 'rfq_quotes_left-1', FALSE);
        $this->db->update('rfq_master');
    }

}
