<?php
class Quotation_master_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function create($item)
	{
		$data = array(
			'rfq_id' => $item['rfq_id'],
			'seller_id' => $item['seller_id'],
			'quotation_date' => $item['quotation_date'],
			'quotation_validity_date' => $item['quotation_validity_date'],
			'currency_id' => $item['currency_id'],
			'discount' => $item['discount'],
			'tax' => $item['tax']
			 ); 

		$this->db->insert('quotation_master', $data);
	}
        function create_main($item)
	{
		$data = array(
			'rfq_id' => $item['rfq_id'],
			'seller_id' => $item['seller_id'],
			'quotation_date' => $item['quotation_date'],
			'quotation_validity_date' => $item['quotation_validity_date'],
			'currency_id' => 1,
			'discount' => $item['discount'],
			'tax' => $item['tax']
			 ); 

		$this->db->insert('quotation_master', $data);
                $insert_id = $this->db->insert_id();
                return $insert_id;
	}

	function get_by_id($id)
	{
		$this->db->select('quotation_master.*,rfq_master.rfq_buyer_id,member_master.company_id,company_master.company_name,company_master.operational_street_address,company_master.operational_address_city,company_master.operational_address_province,company_master.operational_location,company_master.logo_url,rbc_countries.country_name,rbc_countries.country_code');
		$this->db->from('quotation_master');
		$this->db->join('rfq_master', 'quotation_master.rfq_id = rfq_master.id', 'left');
                $this->db->join('member_master', 'rfq_master.rfq_buyer_id = member_master.member_id', 'left');
                $this->db->join('company_master', 'member_master.company_id = company_master.id', 'left');
                $this->db->join('rbc_countries', 'company_master.operational_location = rbc_countries.country_id', 'left');
                $this->db->where('quot_no', $id);
                $query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->row();
		}
	}
        function get_quote_by_id($id)
	{
		$this->db->select('quotation_master.*,member_master.member_id,company_master.id,company_master.company_name,company_master.operational_location,company_master.operational_address_city,rbc_countries.country_name');
		$this->db->from('quotation_master');
		$this->db->join('member_master', 'quotation_master.seller_id = member_master.member_id', 'left');
                $this->db->join('company_master', 'member_master.company_id = company_master.id', 'left');
                $this->db->join('rbc_countries', 'company_master.operational_location = rbc_countries.country_id', 'left');
                $this->db->where('quotation_master.rfq_id', $id);
                $query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}

	function get_all()
	{
		$this->db->select('*');
		$this->db->from('quotation_master');
		$query = $this->db->get();

		if($query->num_rows()<1){
			return null;
		}
		else{
			return $query->result();
		}
	}
        
	function update($id, $item)
	{
		$data = array(
			'rfq_id' => $item['rfq_id'],
			'seller_id' => $item['seller_id'],
			'quotation_date' => $item['quotation_date'],
			'quotation_validity_date' => $item['quotation_validity_date'],
			'currency_id' => $item['currency_id'],
			'discount' => $item['discount'],
			'tax' => $item['tax']
			 ); 

		$this->db->where('quot_no', $id);
		$this->db->update('quotation_master', $data);
	}

	function delete($id)
	{
		$this->db->where('quot_no', $id);
		$this->db->delete('quotation_master');
	}
}