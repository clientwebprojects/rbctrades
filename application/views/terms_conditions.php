<?php include 'header.php'; ?>
<style>

    #page_navigation a{
        padding:3px;

        margin:3px;
        color:black;

        text-decoration:none

    }

    .active_page{
        background:#428bca;
        color:white !important;

    }


</style>
<script>
    $(document).ready(function () {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
    });</script>

<script type="text/javascript">
    $(document).ready(function () {

//how much items per page to show
        var show_per_page = 10;
//getting the amount of elements inside content div
        var number_of_items = $('#content').children().size();
//calculate the number of pages we are going to have
        var number_of_pages = Math.ceil(number_of_items / show_per_page);

//set the value of our hidden input fields
        $('#current_page').val(0);
        $('#show_per_page').val(show_per_page);

//now when we got all we need for the navigation let's make it '

        /* 
         what are we going to have in the navigation?
         - link to previous page
         - links to specific pages
         - link to next page
         */
        var navigation_html = '<a class="previous_link" href="javascript:previous();">Prev</a>';
        var current_link = 0;
        while (number_of_pages > current_link) {
            navigation_html += '<a class="page_link" href="javascript:go_to_page(' + current_link + ')" longdesc="' + current_link + '">' + (current_link + 1) + '</a>';
            current_link++;
        }
        navigation_html += '<a class="next_link" href="javascript:next();">Next</a>';

        $('#page_navigation').html(navigation_html);

//add active_page class to the first page link
        $('#page_navigation .page_link:first').addClass('active_page');

//hide all the elements inside content div
        $('#content').children().css('display', 'none');

//and show the first n (show_per_page) elements
        $('#content').children().slice(0, show_per_page).css('display', 'block');

    });

    function previous() {

        new_page = parseInt($('#current_page').val()) - 1;
//if there is an item before the current active link run the function
        if ($('.active_page').prev('.page_link').length == true) {
            go_to_page(new_page);
        }

    }

    function next() {
        new_page = parseInt($('#current_page').val()) + 1;
//if there is an item after the current active link run the function
        if ($('.active_page').next('.page_link').length == true) {
            go_to_page(new_page);
        }

    }
    function go_to_page(page_num) {
//get the number of items shown per page
        var show_per_page = parseInt($('#show_per_page').val());

//get the element number where to start the slice from
        start_from = page_num * show_per_page;

//get the element number where to end the slice
        end_on = start_from + show_per_page;

//hide all children elements of content div, get specific items and show them
        $('#content').children().css('display', 'none').slice(start_from, end_on).css('display', 'block');

        /*get the page link that has longdesc attribute of the current page and add active_page class to it
         and remove that class from previously active page link*/
        $('.page_link[longdesc=' + page_num + ']').addClass('active_page').siblings('.active_page').removeClass('active_page');

//update the current page input field
        $('#current_page').val(page_num);
    }

</script>

<div class="page_wrapper type_2" >

    <div class="container ">
        <div class="col-md-10 mar">
            <div class=" col-md-10 col-lg-offset-4">


            </div> 
        </div>
        <div class="container hi">
            <div class="row">
                <div class="col-lg-11 col-md-5 col-sm-8 col-xs-9 bhoechie-tab-container">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                        <div class="list-group">
                            <a href="#" class="list-group-item active text-center high ">
                                <h4 class=""></h4><br/>Classification Of Rules
                            </a>
                            <a href="#" class="list-group-item text-center high">
                                <h4 class=""></h4><br/>User Agreement
                            </a>
                            <a href="#" class="list-group-item text-center high">
                                <h4 class=""></h4><br/>Posting Rules
                            </a>
                            <a href="#" class="list-group-item text-center high">
                                <h4 class=""></h4><br/>Announcement
                            </a>
                            <a href="#" class="list-group-item text-center high">
                                <h4 class=""></h4><br/>Learning
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                        <!-- flight section -->
                        <div class="bhoechie-tab-content active">

                            <input type='hidden' id='current_page' />
                            <input type='hidden' id='show_per_page' />




                            <div class="list-page" >
                                <ul class="hc-list list"  id="content" >






                                    <li>
                                        <div class="list-content">
                                            <a href="#">Trade Answers - Posting and Moderation Rules</a>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="#">Rules on Search Results Ranking on www.alibaba.com(2014 Edition)</a>

                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Privacy Policy</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Enforcement Actions for Intellectual Property Right Infringement Claims</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Sourcing Transactions General Rules</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Forum Posting and Moderation Rules</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Terms of Use</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Enforcement Actions for Displaying Prohibited and Controlled Items</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC.com Transaction Services Agreement</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC.com Terms and Rules Highlights on Member Conduct</a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="list-content">
                                            <a href="">Trade Assurance Services Rules (the “Rules”)</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Free Membership Agreement</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Product Listing Policy</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Alipay Services Agreement</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Regulation of Duplicate Products and Products without Performance in Relation to Search Results Ranking</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC.com Product Listing Information-Editing Guidelines</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Trade Dispute Rules (“Rules”)</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC.com Gold Supplier Services Agreement</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Intellectual Property Rights (IPR) Protection Policy</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC.com Sourcing Transactions Dispute Rules</a>
                                        </div>
                                    </li>


                                    <li>
                                        <div class="list-content">
                                            <a href="">Agreement on Use of Complaint Center</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Rules against Duplicate Product Listing</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Penalty Points Incurred Under Serious Dispute Cases</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC.com's Copyright Notice</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">The Buying Request Posting Rules</a>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="list-content">
                                            <a href="">Repeated Listing Guideline and Miscategorization Guideline</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Definition of Dispute and Resolution by Alibaba.com</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Protection Scope for Dispute on RBC.com</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Inspection Transactions General Rules</a>
                                        </div>
                                    </li>
                                    <li>

                                        <div class="list-content">
                                            <a href="">RBC.com Inspection Transactions Dispute Rules</a>
                                        </div>

                                    </li>
                                </ul>
                            </div>


                            <!-- An empty div which will be populated using jQuery -->
                            <div id='page_navigation'></div>

                        </div>


                        <!-- train section -->
                        <div class="bhoechie-tab-content">
                            <div class="list-page">
                                <ul class="hc-list list">
                                    <li>
                                        <div class="list-content">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree">Privacy Policy</a>

                                            <div id="collapsethree" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p> Your privacy is important to us and we have taken steps to ensure that we do not collect more information from you than is necessary for us to provide you with our services and to protect your account.</p><br>

                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseone">Terms of Use</a>

                                            <div id="collapseone" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>Welcome to www.RBC.com!  These Terms of Use describe the terms and conditions applicable to your access and use of the websites at www.RBC.com</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">

                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo">Free Membership Agreement</a>

                                            <div id="collapsetwo" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p>RBC.com may amend this Agreement at any time by posting the amended and restated Agreement on the Site. The amended and restated Agreement shall be effective immediately upon posting. Posting by RBC.com of the amended and restated Agreement and your continued use of the Service shall be deemed to be acceptance of the amended terms..</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>

                            </div>
                        </div>

                        <!-- hotel search -->
                        <div class="bhoechie-tab-content">

                            <div class="list-page">
                                <ul class="hc-list list">
                                    <li>
                                        <div class="list-content">
                                            <a href="">Enforcement Actions for Intellectual Property Right Infringement Claims</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Enforcement Actions for Displaying Prohibited and Controlled Items</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Product Listing Policy</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC.com Product Listing Information-Editing Guidelines</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Intellectual Property Rights (IPR) Protection Policy</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Rules against Duplicate Product Listing</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">The Buying Request Posting Rules</a>
                                        </div>
                                    </li>
                                </ul>    



                                <form data-role="pagination-form" style="display:none;" action="" method="post">
                                    <input type="hidden" name="page">
                                    <input name="_csrf_token_" type="hidden" value="n54o9zo19dvz">
                                </form>

                            </div>
                        </div>
                        <div class="bhoechie-tab-content">

                            <div class="list-page">
                                <ul class="hc-list list">
                                    <li>
                                        <div class="list-content">
                                            <a href="">RBC Trade Assurance Notice on Integrity</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Notice of Amendments of Intellectual Property Rights Protection Policy</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Notice of Amendment to Image Copyright Infringement Complaint Submission</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Prohibited listings: Petroleum, Petroleum Products &amp; Petrochemical Products of Iranian Origin</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Prescription Drugs Reference</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Psychotropic Drugs and Narcotics Reference</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Prohibited Chemicals Reference</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Listing of products containing or made of prohibited substances</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Medical Device Restriction Announcement</a>
                                        </div>
                                    </li>
                                </ul>    



                                <form data-role="pagination-form" style="display:none;" action="http://rule.alibaba.com/rule/rule_list/147.htm" method="post">
                                    <input type="hidden" name="page">
                                    <input name="_csrf_token_" type="hidden" value="n54o9zo19dvz">
                                </form>

                            </div>
                        </div>
                        <div class="bhoechie-tab-content">

                            <div class="list-page">
                                <ul class="hc-list list">
                                    <li>
                                        <div class="list-content">
                                            <a href="">Account Safety</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Disputes &amp; Complaints--Offline Order (for buyer)</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Disputes &amp; Complaints-- Express Order (for buyer )</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">Trade Dispute &amp; Fraud</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="list-content">
                                            <a href="">IPR Infringement</a>
                                        </div>
                                    </li>
                                </ul>    



                                <form data-role="pagination-form" style="display:none;" action="" method="post">
                                    <input type="hidden" name="page">
                                    <input name="_csrf_token_" type="hidden" value="n54o9zo19dvz">
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<?php include 'footer.php'; ?>