<?php include 'header.php'; ?>


<div class="page_wrapper type_2" >
    <div class="container" >
        <div class="section_offset">
            <div class="row">
                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px;">
                    <div class="col-md-12 ">
                        <h1 class="How">How To Sell</h1>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12 ">
                            <img src="<?= base_url() ?>images/how_to_sell_1.jpg" alt="..." width="225">
                            <h4 class="mar"><a href="">Join RBC</a></h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12 ">
                            <img src="<?= base_url() ?>images/how_to_sell_2.jpg" alt="..." width="225">
                            <h4 class="mar"><a href="">Get A Website</a></h4>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12 ">
                            <img src="<?= base_url() ?>images/how_to_sell_3.jpg" alt="..." width="225">
                            <h4 class="mar"><a href="">Display Products</a></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="background-color:#FFF;padding-top: 20px;">
                    <div class="caption" style="border-bottom: 2px solid #DFA239">
                        <h4>All Questions</h4>
                    </div>
                </div>
                <div class="col-lg-12" style="background-color:#FFF;padding-top: 20px;">
                    <div class="col-md-4">
                        <div class="col-md-12">

                            <div class="col-md-10 col-lg-pull-1 ">
                                <h4  class="">Searching For Buyers</h4>
                            </div>
                        </div>

                        <li>  <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseseven">How do I search for buyers?</a>
                            </h4>

                            <div id="collapseseven" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>1) Choose the Buyers tab on the top of Search Bar, enter a product name in the search box then click search.</p><br>
                                    <p>2) Refine your search results by Category, and Country</p><br>
                                    <p>3) You can click "Quote Now" and send email to buyers.</p>
                                </div>
                            </div></li>
                        <li> <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseeight">How to sell my products?</a>
                            </h4>
                            <div id="collapseeight" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p> Choose the seller tab on the top of Search Bar, choose display new products to sell your product.</p>
                                </div>
                            </div></li>
                        <li>  <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsenine">How do I find buyers from a specific country?</a>
                            </h4>
                            <div id="collapsenine" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Use our Advanced search to filter buyer information</p>
                                </div>
                            </div></li>

                        <!--<dd class="pull-right"><a href="" >View more <i class="glyphicon glyphicon-triangle-right right"></i></a></dd>-->
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">

                            <div class="col-md-10 col-lg-pull-1 ">
                                <h4 class="">Contacting buyers</h4>
                            </div>
                        </div>

                        <li> <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">How do I contact buyers?</a>
                            </h4>

                            <div id="collapsefour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>1) Choose the Buyers tab on the top of Search Bar, enter a product name in the search box then click search.</p><br>
                                    <p>2) Refine your search results by Category, and Country</p><br>
                                    <p>3) You can click "Quote Now" and send email to buyers.</p>
                                </div>
                            </div></li>
                        <li> <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive">How do I send quotations to buyers?</a>
                            </h4>

                            <div id="collapsefive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Choose the Buyers tab on the top of Search Bar, select Get Quotation</p>
                                </div>
                            </div></li>
                        <li> <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsesix">Why don't I get inquiries from buyers?</a>
                            </h4>

                            <div id="collapsesix" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>1. Have you provided enough product details? Most buyers choose suppliers who provide lots of details about the product on offer</p><br>
                                    <p>2. Regularly update your posted Selling Leads to keep your information at the top of buyer searches</p>
                                </div>
                            </div></li>
                        <!--<dd class="pull-right"><a href="" >View more <i class="glyphicon glyphicon-triangle-right right"> </i></a></dd>-->

                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12">

                            <div class="col-md-10 col-lg-pull-1 ">
                                <h4 class="anss12345">Managing Selling Information</h4>
                            </div>
                        </div>
                        <li>              
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">How do I display products with pictures?</a>
                            </h4>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>1. Sign in to RBC </p><br>
                                    <p>2. In Products section, click Display New Products</p><br>
                                    <p>3. Add your product with picture</p>
                                </div>
                            </div>
                        </li>
                        <li> 
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">How to edit my products?</a>
                            </h4>

                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>1. In Products click Manage Products</p><br>
                                    <p>2. Select a product and click Edit to make changes</p>
                                </div>
                            </div>
                        </li>
                        <li>  <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree">Group and Sort Products</a>
                            </h4>
                            <div id="collapsethree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Product Group helps you place your products into groupings so that they are easily manageable. </p>
                                </div>
                            </div></li>
                        <!--<dd class="pull-right"><a href="" class="link">View more <i class="glyphicon glyphicon-triangle-right right"> </i></a></dd>-->
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

</div>
<?php include 'footer.php'; ?>