<?php
include 'header.php';
//echo '<pre>';
//print_r($product);
//exit();
?>
<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="<?= base_url() ?>">Home</a></li>
            <li>Product</li>

        </ul>

        <div class="row">

            <main class="col-md-9 col-sm-8">

                <!-- - - - - - - - - - - - - - Product images & description - - - - - - - - - - - - - - - - -->

                <section class="section_offset">

                    <div class="clearfix">

                        <div class="single_product">
                            <?php
                            if ($product_images == NULL) {
                                echo '<img src="' . base_url() . 'images/no_image.jpg"/>';
                            } else {
                                ?>
                                <div class="image_preview_container" style="height: 300px;border: 1px solid #eee;padding: 5px;margin-bottom: 15px;overflow-y: hidden;">
                                    <?php
                                    foreach ($product_images as $row) {
                                        if ($row->is_main == 1) {
                                            ?>
                                            <img id="img_zoom" data-zoom-image="<?= $row->image_url ?>" src="<?= $row->image_url ?>" alt="" style="height: 300px;padding: 5px;">

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="product_preview">

                                    <div class="owl_carousel" id="thumbnails">
                                        <?php
                                        foreach ($product_images as $row) {
                                            ?>
                                            <a href="#" data-image="<?= $row->image_url ?>" data-zoom-image="<?= $row->image_url ?>" style="text-align: center;
                                               float: left;
                                               position: relative;
                                               vertical-align: middle;
                                               border: 1px solid #eee;
                                               padding: 5px;border-radius: 3px;">

                                                <img src="<?= $row->image_url ?>" data-large-image="<?= $row->image_url ?>" alt="" style="max-height: 70px;">

                                            </a>
                                        <?php } ?>

                                    </div>

                                </div>
                            <?php } ?>
                        </div>
                        <div class="single_product_description">

                            <h3 style="font-size: 18px;margin-bottom: 10px"><?php echo $product->product_name; ?></h3>
                            <div class="description_section">

                                <table class="product_info">

                                    <tbody>

                                        <tr>
                                            <td class="specs_cell"><p class="color-666 font12">FOB Price:</p> </td>
                                            <td class="color-000 font12" style="padding-top: 4px;"><?php echo $product->currency_code; ?> &nbsp; <?=$product->currency_symbol; ?>&nbsp;<?=$product->product_fob_price_from; ?> - <?=$product->product_fob_price_to; ?> &nbsp;<?=$product->uom_name; ?></td>
                                        </tr>

                                        <tr>

                                            <td class="specs_cell"><p class="color-666 font12">Min.Order Quantity:</p></td>
                                            <td class="color-000 font12" style="padding-top: 4px;"><?php echo $product->product_minimun_order; ?> &nbsp; <?=$product->uom_name; ?>	</td>

                                        </tr>

                                        <tr>

                                            <td class="specs_cell"><p class="color-666 font12">Supply Ability:</p> </td>
                                            <td class="color-000 font12" style="padding-top: 4px;"><?php echo $product->product_supply_ability; ?></td>

                                        </tr>

                                        <tr>

                                            <td class="specs_cell"><p class="color-666 font12">Port:</p> </td>
                                            <td class="color-000 font12" style="padding-top: 4px;"><?php echo $product->product_port; ?></td>

                                        </tr>

                                        <tr>

                                            <td class="specs_cell"><p class="color-666 font12">Payment Terms:</p> </td>
                                            <td class="color-000 font12" style="padding-top: 4px;"><?php

                                        foreach($product_payment_type as $values_p){
                                            echo $values_p->p_term_description;
                                            echo" / ";
                                                            }

                                                ?></td>


                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="buttons_row">

                                                    <a class="button_blue btn-lg" href="<?= base_url() ?>index.php/buyer/product_quotation/<?= $product->product_id ?>">Get Quotation</a>

                                                </div>
                                            </td>
<!--                                            <td>
                                                <div class="buttons_row">

                                                    <a class="button_dark_grey btn-lg" href="<?= base_url() ?>index.php/buyer/product_quotation/<?= $product->product_id ?>">Contact Seller</a>

                                                </div>
                                            </td>-->
                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                            <hr>

                        </div>
                    </div>

                </section>

                <div class="section_offset">

                    <div class="tabs type_2">

                        <!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->

                        <ul class="tabs_nav clearfix">

                            <li><a href="#tab-1">Product Details</a></li>
                            <li><a href="#tab-2">Company Profile</a></li>
                        </ul>
                        <div class="tab_containers_wrap">

                            <div id="tab-1" class="tab_container" style="border: 1px solid #eaeaea;padding:0 10px 15px 5px">
                                <br/>
                                <table class="product_info">

                                    <tbody>

                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">FOB Price:</td>
                                            <td class="color-000 font12" style="width: 70%"><?php echo $product->currency_code; ?> &nbsp; <?=$product->currency_symbol; ?>&nbsp;<?=$product->product_fob_price_from; ?>-<?=$product->product_fob_price_to; ?> &nbsp;<?=$product->uom_name; ?></td>
                                        </tr>
                                        <?php if($product->product_description !="" ||$product->product_description !=NULL){ ?>
                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">Product Description</td>
                                            <td class="color-000 font12" style="width: 70%"><?= $product->product_description ?></td>
                                        </tr>
                                        <?php }?>
                                        <?php if($product->product_type !="" ||$product->product_type !=NULL){ ?>
                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">Product Type</td>
                                            <td class="color-000 font12" style="width: 70%"><?= $product->product_type ?></td>
                                        </tr>
                                        <?php }?>
                                         <?php if($product->product_use !="" ||$product->product_use !=NULL){ ?>
                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">Product Use</td>
                                            <td class="color-000 font12" style="width: 70%"><?= $product->product_use ?></td>
                                        </tr>
                                        <?php }?>
                                        <?php if($product->product_port !="" ||$product->product_port !=NULL){ ?>
                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">Product Port</td>
                                            <td class="color-000 font12" style="width: 70%"><?= $product->product_port ?></td>
                                        </tr>
                                        <?php }?>
                                        <?php if($product->product_supply_ability !="" ||$product->product_supply_ability !=NULL){ ?>
                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">Product Supply Ability</td>
                                            <td class="color-000 font12" style="width: 70%"><?= $product->product_supply_ability ?></td>
                                        </tr>
                                        <?php }?>
                                         <?php if($product->packaging_detail !="" ||$product->packaging_detail !=NULL){ ?>
                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">Packaging Details</td>
                                            <td class="color-000 font12" style="width: 70%"><?= $product->packaging_detail ?></td>
                                        </tr>
                                        <?php }?>
                                        <?php if($product->product_detailed_description !="" ||$product->product_detailed_description !=NULL){ ?>
                                        <tr>
                                            <td class="color-666 font12" style="width: 30% !important;">Product Details</td>
                                            <td class="color-000 font12" style="width: 70%"><?= $product->product_detailed_description ?></td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>

                            </div><!--/ #tab-1-->

                            <div id="tab-2" class="tab_container" style="border: 1px solid #eaeaea;padding:0 10px 15px 5px">
                                <br/>
                                <h3 class="heading">Company Description</h3>
                                <p class="color-000 font12">
                                    <?php echo $product->company_description; ?>
                                </p>

                                <hr style="margin: 15px 0 15px"/>
                                <h3 class="heading">Basic Information</h3>
                                <table class="product_info">
                                    <tbody>
                                        <tr>
                                            <td class="color-666 font12" style="width: 200px !important;vertical-align: top;">Business Type:</td>
                                            <td class="color-000 font12"><?php

                                        foreach($business_type as $values_p){
                                            echo $values_p->business_type_name;
                                            echo"<br/>";
                                                            }

                                                ?></td>
                                        </tr>
                                        <tr>
                                            <td class="color-666 font12" style="vertical-align: top;">Main Products:</td>
                                            <td class="color-000 font12"><?php
                                        echo implode(", ",$main_products_company);
                                        ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="color-666 font12">Number Of Employees:</td>
                                            <td class="color-000 font12"><?php echo $product->total_employees; ?> People</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr style="margin: 15px 0 15px"/>
                                                                <h3 class="heading">Trade & Market</h3>
                                <table class="product_info">

                                    <tbody>

                                        <tr>
                                            <td class="color-666 font12" style="width: 200px !important;">Nearest Port:</td>
                                            <td class="color-000 font12"><?php

                                        echo implode(", ",$export_type_port);
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="color-666 font12">Total Annual Sales Volume:</td>
                                            <td class="color-000 font12"><?php echo $product->annual_sales_volume; ?>  </td>
                                        </tr>
                                        <tr>
                                            <td class="color-666 font12">Export Percentage:</td>
                                            <td class="color-000 font12"><?php echo $product->export_percentage; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                                                <hr style="margin: 15px 0 15px"/>
                                                                <h3 class="heading">Certificate and Trade</h3>
                                <table class="product_info">

                                    <tbody>

                                        <tr>
                                            <td class="color-666 font12" style="width: 200px !important;">Name:</td>
                                            <td class="color-000 font12">
                                                <?php echo $product->name; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="color-666 font12">Type of Certification:</td>
                                            <td class="color-000 font12"><?php echo $product->certification_type; ?></td>
                                        </tr>
                                        <tr>
                                            <td class="color-666 font12">Issued By:</td>
                                            <td class="color-000 font12"><?php echo $product->issued_by; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div><!--/ #tab-2-->

                        </div><!--/ .tab_containers_wrap -->

                    </div><!--/ .tabs-->

                </div><!--/ .section_offset -->

            </main><!--/ [col]-->

            <aside class="col-md-3 col-sm-4">

                <!-- - - - - - - - - - - - - - Seller Information - - - - - - - - - - - - - - - - -->

                <section class="section_offset">

                    <h3 style="font-size: 18px">Seller Information</h3>

                    <div class="theme_box">

                        <div class="seller_info clearfix">
                            <b class="color-gold">Basic Seller</b><br/>
                            <p class="color-999 font12">
                                            <?= $product->company_name; ?>
                            </p>
                            <p class="color-999 font12">
                                            <?php echo $product->country_name." | ".$product->registered_year; ?>
                            </p>
<!--                            <a href="#" class="alignleft photo image_div">

                                <img src="<?= $product->logo_url; ?>" alt="" width="100" height="100">

                            </a>-->

                        </div><!--/ .seller_info-->

                        <ul class="seller_stats">

                            <!--
                                                        <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                        <li><span class="bold">7606</span> Transactions</li>-->

                        </ul>

                        <!--                        <div class="v_centered">

                                                    <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                    <a href="#" class="small_link">Chat Now</a>

                                                </div>-->

                    </div><!--/ .theme_box -->

<!--                    <footer class="bottom_box center-align">

                        <a href="#" class="button_grey middle_btn">View This Seller's Products</a>

                    </footer>-->

                </section>

            </aside><!--/ [col]-->

        </div><!--/ .row-->

    </div>

</div>

<?php include 'footer.php'; ?>