<?php include 'header.php';
?>
<div class="page_wrapper type_2" >
    
    <div class="container">

        <div class="section_offset">

            <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->

            <div class="row">
                <div class="col-md-12 col-sm-3" style="background-color:#FFF;padding-top: 20px;">

                    <h1><?= $category_name->category_name; ?></h1>

                    <div class="col-md-12 col-sm-3" >

                        <div class="col-sm-2 col-md-12 col-lg-12" style="border:1px #ccc dotted;padding: 10px; margin: 10px">

                            <?php
                            $i = 0;

                            echo "<div class='col-md-0'><ul>";
                            foreach ($sub_categories as $category) :
                                if ($i % 5 == 0)
                                    echo "</ul></div><div class='col-md-3' style='border:1px #ccc solid;padding:20px;margin:0 5px;'><ul style='list-style-type: square;'><h4>".$category_name->category_name." Items</h4>";
                                ?>
                                <li><a href="<?php echo base_url(); ?>index.php/seller/product_listing">
                                        <?php echo $category->category_name; ?> </a></li>
                                <?php
                                //if($i % 20 == 0) echo "</ul>";
                                $i += 1;
                            endforeach;
                            echo "</div></ul>";
                            ?>

                            <!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->

                        </div><!--/ .animated.transparent-->


                    </div><!--/ [col]-->

                    <div class="col-md-12 col-sm-3" style="margin-top: 20px;">

                        <h3 class="offset_title">Trending & New</h3>

                        <div class="owl_carousel related_products">

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/china_seed.jpg" alt="" height="173" width="173">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                <div class="label_new">New</div>

                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Leo vel metus nulla facilisi etiam cursus 750mg...</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$44.99</b></p>

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/china_seed.jpg" alt="" height="173" width="173">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Ut pharetra augue nec augue, 200 ea</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$4.99</b></p>

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/china_seed.jpg" alt="" height="173" width="173">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                <div class="label_bestseller">Bestseller</div>

                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Mauris fermentum dictum magna sed laoreet ...</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$17.99</b></p>

                                        <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                        <ul class="rating alignright">

                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li></li>
                                            <li></li>

                                        </ul>

                                        <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/china_seed.jpg" alt="" height="173" width="173">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Vestibulum libero nisl porta vel scelerisque eget...</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$12.59</b></p>

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/product_img_10.jpg" alt="">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                <div class="label_new">New</div>

                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Leo vel metus nulla facilisi etiam cursus 750mg...</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$44.99</b></p>

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/product_img_11.jpg" alt="">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Ut pharetra augue nec augue, 200 ea</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$4.99</b></p>

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/product_img_12.jpg" alt="">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                <div class="label_bestseller">Bestseller</div>

                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Mauris fermentum dictum magna sed laoreet ...</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$17.99</b></p>

                                        <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                        <ul class="rating alignright">

                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li class="active"></li>
                                            <li></li>
                                            <li></li>

                                        </ul>

                                        <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                            <div class="product_item">

                                <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                <div class="image_wrap">

                                    <img src="<?= base_url() ?>images/product_img_14.jpg" alt="">

                                    <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                    <div class="actions_wrap">

                                        <div class="centered_buttons">

                                            <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey quick_view" data-modal-url="modals/quick_view.html">Quick View</a>

                                            <a href="#" class="button_blue add_to_cart">Add to Cart</a>

                                        </div><!--/ .centered_buttons -->

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_wishlist tooltip_container"><span class="tooltip right">Add to Wishlist</span></a>

                                        <a href="#" class="button_dark_grey def_icon_btn add_to_compare tooltip_container"><span class="tooltip left">Add to Compare</span></a>

                                    </div><!--/ .actions_wrap-->

                                    <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                </div><!--/. image_wrap-->

                                <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                <div class="description">

                                    <a href="#">Vestibulum libero nisl porta vel scelerisque eget...</a>

                                    <div class="clearfix product_info">

                                        <p class="product_price alignleft"><b>$12.59</b></p>

                                    </div>

                                </div>

                                <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                            </div><!--/ .product_item-->

                            <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                        </div><!--/ .owl_carousel -->

                    </div><!--/ .section_offset -->

                    <div class="col-md-12 col-sm-3" style="margin-top: 20px;">
                        <h3 class="offset_title">Most Popular Products</h3>
                        <div class="section_offset">

                            <header class="top_box on_the_sides">

                                <div class="left_side clearfix v_centered">

                                    <!-- - - - - - - - - - - - - - Sort by - - - - - - - - - - - - - - - - -->

                                    <div class="v_centered">

                                        <span>Sort by:</span>

                                        <div class="custom_select sort_select">

                                            <select name="">

                                                <option value="Default">Default</option>
                                                <option value="Price">Price</option>
                                                <option value="Name">Name</option>
                                                <option value="Date">Date</option>

                                            </select>

                                        </div>

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of sort by - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Number of products shown - - - - - - - - - - - - - - - - -->

                                    <div class="v_centered">

                                        <span>Show:</span>

                                        <div class="custom_select">

                                            <select name="">

                                                <option value="15">15</option>
                                                <option value="12">12</option>
                                                <option value="9">9</option>
                                                <option value="6">6</option>
                                                <option value="3">3</option>

                                            </select>

                                        </div>

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of number of products shown - - - - - - - - - - - - - - - - -->

                                </div>

                                <div class="right_side">

                                    <!-- - - - - - - - - - - - - - Product layout type - - - - - - - - - - - - - - - - -->

                                    <div class="layout_type buttons_row" data-table-container="#products_container">

                                        <a href="#" data-table-layout="grid_view" class="button_grey middle_btn icon_btn active tooltip_container"><i class="icon-th"></i><span class="tooltip top">Grid View</span></a>

                                        <a href="#" data-table-layout="list_view list_view_products" class="button_grey middle_btn icon_btn tooltip_container"><i class="icon-th-list"></i><span class="tooltip top">List View</span></a>

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product layout type - - - - - - - - - - - - - - - - -->

                                </div>

                            </header>

                            <div class="table_layout" id="products_container">

                                <div class="table_row">

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/pasta.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">ORIGINAL ITALIAN PASTA</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$5.99</b></p>

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating alignright">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Sed ut perspiciatis unde, Liqui-gels 24 capsules</a>

                                                <a href="#" class="product_category">Beauty Clearance</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li><a href="#">3 Review(s)</a></li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$5.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/cranberries.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_bestseller">Bestseller</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Frozen Cranberries </a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$8.99</b></p>

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating alignright">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Nemo enim ipsam voluptatem quia, 4.25 fl oz (126ml)</a>

                                                <a href="#" class="product_category">Bath &amp; Spa</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li><a href="#">3 Review(s)</a></li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$8.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: $11.24/piece</li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/almonds.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Soft Inshell Almond </a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$76.99</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Quis autem vel eum iure reing elit, 2mg</a>

                                                <a href="#" class="product_category">Beauty Clearance</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$76.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                </div><!--/ .table_row -->

                                <div class="table_row">

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/sesame_seed.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_new">New</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Sesame Seed</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$75.39</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Aliquam congue fermentum nisl, 100mg, Softgels 120 ea</a>

                                                <a href="#" class="product_category">Beauty Clearance</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$75.39</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: $16.63/piece</li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                        <!-- - - - - - - - - - - - - - End product - - - - - - - - - - - - - - - - -->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/cranberries.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_offer percentage">

                                                    <div>30%</div>OFF

                                                </div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Frozen Cranberries </a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><s>$99.99</s> <b>$79.99</b></p>

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating alignright">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Praesent justo dolor, Vcaps 60 ea</a>

                                                <a href="#" class="product_category">Gift Sets</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold"><s>$99.99</s> $79.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/pasta.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_new">New</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">ORIGINAL ITALIAN PASTA</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$24.99</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Donec sagittis euismod purus, 12 ea</a>

                                                <a href="#" class="product_category">Hair Care</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$24.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>

                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                </div><!--/ .table_row -->

                                <div class="table_row">

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/almonds.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_new">New</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Soft Inshell Almond </a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$44.99</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Suspendisse sollicitudin velit sed leo, Softgels 120 ea</a>

                                                <a href="#" class="product_category">Hair Care</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$44.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: $5.00/piece</li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/china_seed.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_soldout">Sold Out</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Chia seeds</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$44.99</b></p>

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating alignright">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Quisque diam lorem, interdum vitae</a>

                                                <a href="#" class="product_category">Beauty Clearance</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li><a href="#">5 Review(s)</a></li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$39.39</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="out_of_stock">out of stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/pasta.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_hot">Hot</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">ORIGINAL ITALIAN PASTA</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$44.99</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Vestibulum iaculis lacinia est, 2.5 fl oz (75ml)</a>

                                                <a href="#" class="product_category">Makeup &amp; Accessories</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>5 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$44.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                </div><!--/ .table_row -->

                                <div class="table_row">

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/sesame_seed.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_bestseller">Bestseller</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Sesame Seed</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$35.99</b></p>

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating alignright">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Ut pharetra augue nec augue, Capsules 90 ea</a>

                                                <a href="#" class="product_category">Makeup &amp; Accessories</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li><a href="#">1 Review(s)</a></li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$35.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="out_of_stock">out of stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>

                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/almonds.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_bestseller">Bestseller</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Soft Shell Almonds</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$8.79</b></p>

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating alignright">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Praesent justo dolor, lobortis quis</a>

                                                <a href="#" class="product_category">Beauty Clearance</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li><a href="#">4 Review(s)</a></li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$8.79</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>

                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/cranberries.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_bestseller">Frozen Cranberries </div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Sed ut perspiciatis, 250mg, Chewable Soft Gel Caps, Berry 90 ea</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$12.99</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Sed ut perspiciatis, 250mg, Chewable Soft Gel Caps, Berry 90 ea</a>

                                                <a href="#" class="product_category">Makeup &amp; Accessories</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$12.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                </div><!--/ .table_row -->

                                <div class="table_row">

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/pasta.jpg" alt="" height="243" width="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Label - - - - - - - - - - - - - - - - -->

                                                <div class="label_new">New</div>

                                                <!-- - - - - - - - - - - - - - End label - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Delisious Pasta</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$44.99</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Lorem ipsum dolor sit amet, 750mg, Softgels 120 ea</a>

                                                <a href="#" class="product_category">Hair Care</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$44.99</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/china_seed.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                    </div><!--/ .centered_buttons -->





                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Chine Seeds</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$9.59</b></p>

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating alignright">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Mauris fermentum dictum magna, Caramel Chocolate Crunch 5 ea</a>

                                                <a href="#" class="product_category">Makeup &amp; Accessories</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                    <ul class="rating">

                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li class="active"></li>
                                                        <li></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li><a href="#">8 Review(s)</a></li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$9.59</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                    <div class="table_cell">

                                        <div class="product_item">

                                            <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                            <div class="image_wrap">

                                                <img src="<?= base_url() ?>images/almonds.jpg" alt="" width="243" height="243">

                                                <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                                <div class="actions_wrap">

                                                    <div class="centered_buttons">

                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>


                                                    </div><!--/ .centered_buttons -->

                                                </div><!--/ .actions_wrap-->

                                                <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                            </div><!--/. image_wrap-->

                                            <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                            <div class="description">

                                                <a href="#">Soft Inshell Almond</a>

                                                <div class="clearfix product_info">

                                                    <p class="product_price alignleft"><b>$8.99</b></p>

                                                </div>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="full_description">

                                                <a href="#" class="product_title">Quisque diam lorem, Lemon 4 fl oz (118ml)</a>

                                                <a href="#" class="product_category">Makeup &amp; Accessories</a>

                                                <div class="v_centered product_reviews">

                                                    <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                    <ul class="topbar">

                                                        <li>0 Review(s)</li>
                                                        <li><a href="#">Add Your Review</a></li>

                                                    </ul>

                                                    <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                                </div>

                                                <p>Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.</p>

                                                <a href="#" class="learn_more">Learn More</a>

                                            </div>

                                            <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                            <div class="actions">

                                                <p class="product_price bold">$8.59</p>

                                                <ul class="seller_stats">

                                                    <li>Shipping: <span class="success">Free Shipping</span></li>
                                                    <li>Availability: <span class="success">in stock</span></li>
                                                    <li class="seller_info_wrap">

                                                        Seller: <span class="seller_name">johnsmith</span>

                                                        <div class="seller_info_dropdown">

                                                            <ul class="seller_stats">

                                                                <li>

                                                                    <ul class="topbar">

                                                                        <li>China (Mainland)</li>

                                                                        <li><a href="#">Contact Details</a></li>

                                                                    </ul>

                                                                </li>

                                                                <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                            </ul>

                                                            <div class="v_centered">

                                                                <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                                <a href="#" class="small_link">Chat Now</a>

                                                            </div>

                                                        </div>

                                                    </li>

                                                </ul>


                                            </div>

                                            <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        </div><!--/ .product_item-->

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of product - - - - - - - - - - - - - - - - -->

                                </div><!--/ .table_row -->

                            </div><!--/ .table_layout -->

                            <footer class="bottom_box on_the_sides">

                                <div class="left_side">

                                    <p>Showing 1 to 3 of 45 (15 Pages)</p>

                                </div>

                                <div class="right_side">

                                    <ul class="pags">

                                        <li><a href="#"></a></li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#"></a></li>

                                    </ul>

                                </div>

                            </footer>

                        </div>
                    </div>
                </div><!--/ [col]-->
            </div>
        </div>
    </div>
    
</div>

<?php include 'footer.php'; ?>

