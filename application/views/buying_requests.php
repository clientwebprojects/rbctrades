<?php include 'header3.php'; // echo '<pre>';pr  ?>
<script>
    document.getElementById("buyer_br").className = "active-seller-menu";
</script>
<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">

            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                    <?php include 'inc/buyer_product_menu.php'; ?>
                    <script>
                        document.getElementById("buying_request").className = "active";
                    </script>
                    <div>
                        <section class="col-sm-10">
                            <h3 class="heading">Buying Requests</h3>
                            <!-- - - - - - - - - - - - - - Tabs v2 - - - - - - - - - - - - - - - - -->
                            <div id="successmessage"></div>
                            <div class="tabs type_2">
                                <!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->
                                <ul class="tabs_nav clearfix" style="margin-right: 0px;">
                                    <li class="active"><a href="#tab-1">New Requests (<?php echo sizeof($requests); ?>)</a></li>
                                    <li ><a href="#tab-2">Old (<?php echo sizeof($active_products); ?>)</a></li>

                                </ul>
                                <!-- - - - - - - - - - - - - - End navigation of tabs - - - - - - - - - - - - - - - - -->
                                <!-- - - - - - - - - - - - - - Tabs container - - - - - - - - - - - - - - - - -->
                                <div class="tab_containers_wrap">
                                    <!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->
                                    <div id="tab-1" class="tab_container" style="margin-top: 20px;">
                                        <table class="table_type_1" cellpadding="0" cellspacing="0" border="0" class="display" id="buying_request_table" style="clear: both">
                                            <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Detail</th>
                                                    <th>Quantity</th>
                                                    <th>Buyer</th>
                                                    <th>Request Date</th>
                                                    <th style="width: 110px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count = 0;
                                                foreach ($requests as $req) {
                                                    //                                                   print_r($pending_products);exit();
                                                    ?>
                                                    <tr>
                                                        <td data-title="Products Name"><?= $req->product_name ?></td>
                                                        <td data-title="Detail" ><?= $req->description ?></td>
                                                        <td data-title="Quantity" ><?= $req->quantity ?></td>
                                                        <td data-title="Buyer"><?= $req->member_name ?></td>
                                                        <td data-title="Request Date" ><?= $req->quotation_date ?></td>
                                                        <td data-title="Action" style="text-align: center;">
                                                            <div class="btn-group">
                                                                <a href="<?= base_url() ?>index.php/buyer/view_quotation/<?= $req->quot_no; ?>" class="btn btn-primary btn-xs" title="Edit">View</a>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>

<!--                                        <section class="section_offset pull-right">
                                            <ul class="pags">
                                                <li><a href="#"></a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#"></a></li>
                                            </ul>
                                        </section>-->
                                    </div>
                                    <!--/ #tab-1-->
                                    <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->

                                    <!--/ #tab-1-->
                                    <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                </div><!--/ .tab_containers_wrap -->
                                <!-- - - - - - - - - - - - - - End of tabs containers - - - - - - - - - - - - - - - - -->
                            </div><!--/ .tabs-->
                            <!-- - - - - - - - - - - - - - End of tabs v2 - - - - - - - - - - - - - - - - -->
                        </section><!--/ [col]-->
                        <!--/well-->
                    </div><!--/ .animated.transparent-->
                </div><!--/ [col]-->


            </div><!--/ .row-->

            <section class="section_offset" style="margin-top: 30px;">

                <!-- - - - - - - - - - - - - - Infoblocks v3 - - - - - - - - - - - - - - - - -->

                <div class="row">

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-thumbs-up-1"></i>

                                <h4 class="caption">The Highest<br>Product Quality</h4>

                            </div>

                            <p>Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-money"></i>

                                <h4 class="caption">100% Money<br>Back Guaranteed</h4>

                            </div>

                            <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-lock"></i>

                                <h4 class="caption">Safe &amp; Secure<br>Payment</h4>

                            </div>

                            <p>Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                </div><!--/ .row -->

                <!-- - - - - - - - - - - - - - End of infoblocks v3 - - - - - - - - - - - - - - - - -->

            </section><!--/ .section_offset -->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->




    </div><!--/ .container-->

</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#buying_request_table').dataTable().columnFilter();

    });
</script>