<!doctype html>
<html lang="en">

    <head>

        <title>RBC TRADES</title>
        <meta charset="utf-8">
        <meta name="author" content="">
        <meta name="description" content="">
        <meta name="keywords" content="">

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>/images/favicon.png">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300,300italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="<?= base_url() ?>css/animate.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/fontello.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/bootstrap.css">

        <link rel="stylesheet" href="<?= base_url() ?>js/rs-plugin/css/settings.css">
        <link rel="stylesheet" href="<?= base_url() ?>js/owlcarousel/owl.carousel.css">
        <link href="<?= base_url() ?>/css/formcss.css" rel="stylesheet"/>
        <link rel="stylesheet" href="<?= base_url() ?>css/style.css">

        <link href="<?= base_url() ?>css/prettify.css" rel="stylesheet">
        <link href="<?= base_url() ?>css/formValidation.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>css/datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= base_url() ?>css/invoice.css">

        <script src="<?= base_url() ?>js/jquery-2.1.1.min.js"></script>
<!--        <script src="<?= base_url() ?>js/datatable/jquery-1.4.4.min.js" type="text/javascript"></script>-->
        <link href="<?= base_url() ?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>js/bootstrap-datepicker.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>


        <script type="text/javascript">
            var base_url = '<?= base_url(); ?>';
        </script>
    </head>
    <body class="front_page">
        <div class="wide_layout" id="top">

            <header id="header" class="type_2" style="padding-bottom: 0 !important">

                <div class="bottom_part">

                    <div>

                        <div class="container">

                            <div class="row">

                                <div class="main_header_row">

                                    <div class="col-lg-3 col-md-3 col-sm-12">

                                        <a href="<?= base_url(); ?>" class="logo" style="  padding-left: 15px;">

                                            <img src="<?= base_url(); ?>images/logo.png" alt="">

                                        </a>

                                    </div>

                                    <div class="col-lg-7 col-md-8 col-sm-12">

                                        <nav>

                                            <ul class="topbar" style="padding-top: 20px">

<!--                                                <li><a href="<?= base_url() ?>">Home</a></li>-->
                                                <li class="has_submenu">
                                                    <a >My RBC</a>

                                                    <ul class="theme_menu submenu">

                                                        <li><a href="<?= base_url() ?>index.php/seller/message_center/">Message Center</a></li>
                                                        <li><a href="<?= base_url() ?>index.php/seller/seller_profile/">My Profile</a></li>

                                                    </ul>

                                                </li>
                                                <li class="has_submenu">

                                                    <a>For Buyer</a>

                                                    <ul class="theme_menu submenu">
                                                        <li><a href="<?= base_url() ?>index.php/buyer/request_for_quotation">Get Quotations</a></li>
<!--                                                        <li><a href="<?= base_url() ?>index.php/buyer/company_listing/">Find Sellers</a></li>-->
                                                        <hr style="border-color: #e7e7e7;margin: 4px 7px"/>
                                                        <li><a href="<?= base_url() ?>index.php/buyer/product_listing/">View Selling Leads</a></li>
                                                        <li><a href="<?= base_url() ?>index.php/main/how_to_buy/">How To Buy</a></li>
                                                    </ul>

                                                </li>
                                                <li class="has_submenu">

                                                    <a>For Seller</a>

                                                    <ul class="theme_menu submenu">
<!--                                                        <li><a href="#">Seller Memberships</a></li>-->
<!--                                                        <li><a href="<?= base_url() ?>index.php/seller/register_company/">Verify Your Company</a></li>-->
                                                        <li><a href="<?= base_url() ?>index.php/seller/add_new_product/">Display New Product</a></li>
                                                        <li><a href="<?= base_url() ?>index.php/seller/manage_products/">Manage Products</a></li>
                                                        <hr style="border-color: #e7e7e7;margin: 4px 7px"/>
                                                        <li><a href="<?= base_url() ?>index.php/main/quotation_listing/">View Buying Leads</a></li>
<!--                                                        <li><a href="<?= base_url() ?>index.php/seller/quotation_builder/">Quotations</a></li>-->
                                                        <li><a href="<?= base_url() ?>index.php/main/how_to_sell/">How To Sell</a></li>

                                                    </ul>

                                                </li>
                                                <li class="has_submenu">
                                                    <a >Help</a>
                                                    <ul class="theme_menu submenu">
                                                        <li><a href="<?= base_url() ?>index.php/main/contact_us">Contact us</a></li>
<!--                                                        <li><a href="<?= base_url() ?>index.php/main/terms_conditions">Terms & Conditions</a></li>-->
                                                    </ul>
                                                </li>
                                            </ul>

                                        </nav>

                                        <form class="clearfix search search_form" action="<?= base_url('index.php/main/live_search')?>" method="post">

                                            <input id="search-input" type="text" name="term" tabindex="1" placeholder="Search..." class="alignleft">

                                            <div class="sw_section search_category alignleft">

                                                <div>

                                                    <select id="search-select" name="search_by" style="padding: 11px 8px!important;border: 0!important">

                                                        <option value="products" selected>Products</option>
                                                        <option value="suppliers">Suppliers</option>
                                                        <option value="buyers">Buyers</option>

                                                    </select>

                                                </div>

                                            </div>

                                            <button class="button_blue def_icon_btn alignleft"></button>

                                        </form>

                                    </div>
                                    <div class="col-lg-2 pt20 col-sm-12">

                                        <div class="clearfix"></div>
                                        <div class="align_right v_centered">
                                            <?php if (!$this->session->userdata('is_logged_in')) { ?>
                                                <div class="site_settings alignright v_centered" style="padding:22px 0 7px;">
                                                    <a href="<?= base_url() ?>index.php/login" style="border-right: 1px solid #ccc;padding-right: 10px;">Login</a>
                                                    <a href="<?= base_url() ?>index.php/main/register_user/" style="padding-right: 5px;">Register Free</a>
                                                </div>
                                            <?php } else { ?>
                                                <div class="site_settings alignright v_centered" style="padding:22px 0 7px;">
                                                    Hi <?= $this->session->userdata('user_name') ?>, <a href="<?= base_url() ?>index.php/login/logout" class="logout_btn">Sign Out</a>
                                                </div>
                                            <?php } ?>
                                            <div class="shopping_cart_wrap">

                                                <a class="button_black middle_btn" href="<?= base_url() ?>index.php/buyer/request_for_quotation">
                                                    <i class="icon-money"></i>Get Quotation
                                                </a>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div id="main_navigation_wrap">

                    <div class="container">

                        <div class="row">

                            <div class="col-xs-12">

                                <div class="sticky_inner type_2">

                                    <div class="nav_item">

                                        <nav class="main_navigation">

                                            <ul>
                                                <li id="seller_home"><a href="<?= base_url('index.php/seller/seller_profile/') ?>">Home</a></li>
                                                <li id="buyer_br"><a href="<?= base_url('index.php/buyer/buying_requests');?>">Buying Requests</a></li>
                                                <li id="seller_messages"><a href="">Messages & Contacts</a></li>
                                                <li class="pull-right"><a href="<?= base_url('index.php/seller/seller_profile')?>">Switch to Sellers Account</a></li>

                                            </ul>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </header>
