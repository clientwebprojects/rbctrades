<?php include 'header2.php';
//echo '<pre>';
//print_r($company);exit();
//echo '</pre>';
?>
<script>
    document.getElementById("seller_home").className = "active-seller-menu";
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">

            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-9" >

                    <div class="col-sm-2 col-md-12 col-lg-12" style="border:1px #ccc dotted;padding: 10px; margin: 10px;margin-top: 0px;">

                        <div class="col-sm-2 col-md-12 col-lg-12" style="width:100%;border-bottom: #ccc dashed 1px;margin: 0;padding:0">
                            <div class="col-sm-2 col-md-10 col-lg-10">
                                <h4>To do List</h4>
                            </div>
                            <div class="col-sm-2 col-md-2 col-lg-2">
                                <a href="<?= base_url() ?>index.php/seller/account_setting/my_profile" class="small_link pull-right underline color-blue">Settings</a>
                            </div>

                        </div>

                        <div class="col-sm-2 col-md-12 col-lg-12">

                            <div class ="col-md-3 align_center" style="margin-top:10px">
                                <br>
                                <a href="<?= base_url() ?>index.php/seller/message_center" class="font14"><img src="<?= base_url() ?>images/messages.png" width="50" height="50"/><br>Messages</a>
                            </div>

                        </div>

                        <!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->

                    </div><!--/ .animated.transparent-->
                    <div class="col-sm-2 col-md-12 col-lg-12" style="border:1px #ccc dotted;padding: 10px; margin: 10px">
                        <div class="col-sm-2 col-md-7 col-lg-7">
                            <h4>No Products, No Inquiries!</h4>
                            <p class="font14">As a Basic free member you can display 25 products online<<br/>More than 95% of buyers source by searching products online.</p><br/>
                            <a class="button_blue small_link" href="<?= base_url() ?>index.php/seller/add_new_product/">Display Products New</a>
                        </div>
                        <div class="col-sm-2 col-md-5 col-lg-5" style="float:right;border-left: 1px dotted #ccc">
                            <p><img src="<?= base_url() ?>images/exclusive.png" height="30" width="30"><a >Upgrade to Standard Account</a></p>
                            <ul style="list-style-type: square;margin-left: 20px;">
                                <li>22x more buyer inquiries</li>
                                <li>Top-tier priority listing</li>
                                <li>Instant access to buyers</li>
                            </ul>
                        </div>
                    </div>

                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

                <aside class="col-md-3">

                    <section>



                        <div class="theme_box">

                            <div class="font14">
                                <p class="color-000 font18" href="<?= base_url() ?>index.php/seller/manage_company" style="padding-bottom: 10px;"><?= $company->company_name; ?></p>

                            </div>
                            <div class="dash-line"></div>
                            <!-- - - - - - - - - - - - - - Progress Bar - - - - - - - - - - - - - - - - -->
                            <?php
//                                if ($level_id == 1) {
//                                    echo "Unverified Member";
//                                }
//
                            ?>
                            <div class="image_div right"><div class="img" style="float: none!important;background:url('<?php
                                if ($this->session->userdata('image')) {
                                    echo $this->session->userdata('image');
                                } else if ($company->logo_url != NULL) {
                                    echo $company->logo_url;
                                }
                                else{
                                    echo base_url() . 'images/comment_author_photo.jpg';
                                }
                                ?>')"></div></div>
                            <div class="clearfix"></div>
                            <table>
                                <tbody>
                                    <tr>
                                        <td style="border:0px;padding: 6px 2px;color: #999">Member Name :</td>
                                        <td style="border:0px;padding: 6px 2px;color: #111"><?= $this->session->userdata('name'); ?></td>
                                    </tr>
                                    <tr>
                                        <td style="border:0px;padding: 6px 2px;color: #999">Account Type :</td>
                                        <td style="border:0px;padding: 6px 2px;color: #111">Basic</td>
                                    </tr>
                                    <tr>
                                        <td style="border:0px;padding: 6px 2px;color: #999">Location :</td>
                                        <td style="border:0px;padding: 6px 2px;color: #111"><?= $company->registration_location_country; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="border:0px;padding: 6px 2px;color: #999">Company Profile :</td>
                                        <td style="border:0px;padding: 6px 2px;color: #111"><?php echo $company->profile_completed . '%'; ?>
                                            <div class="progress_bar">
                                                <div class="pb_inner" style="width: <?= $company->profile_completed; ?>%;"></div>
                                            </div></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="theme_box" style="margin-top:15px;">
                            <b class="color-gold">Help Us Improve</b>
                            <p class="pb_title font14">How would you rate your overall satisfaction with this page?</p>


                            <form method="post" id="help_us_form" name="help_us_form">
                                <div id="message"></div>
                                
                                <ul class="checkboxes_list">

                                    <li>
                                        <label class="radio-inline" >
                                            <input checked type="radio" name="status" id="satisfied" value="satisfied" style=" width: 30px;">Satisfied
                                        </label>
                                    </li>

                                    <li>

                                        <label class="radio-inline" >
                                            <input type="radio" name="status" id="neutral" value="neutral" style=" width: 30px;">Neutral
                                        </label>
                                    </li>

                                    <li>
                                        <label class="radio-inline" >
                                            <input  type="radio" name="status" id="dissatisfied" value="dissatisfied" style=" width: 30px;">Dis-satisfied
                                        </label>
                                    </li>
                                </ul>
                                <textarea name="message" class="form-control" rows="7" placeholder="Leave your Comments and Suggestions Here"></textarea><br>
                                <input  type="submit" name="feedback_submit" class="button_blue" value="submit">
                            </form>
                        </div>

                    </section>

                </aside>

            </div>

        </div>
    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>
<script>
    $("#help_us_form").submit(function() {
        console.log("asadasd");
    var url = "<?= base_url('index.php/main/help_us')?>"; // the script where you handle the form input.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#help_us_form").serialize(), // serializes the form's elements.
           dataType : 'text',
           success: function(data)
           {
               if(data=="1"){
                   var success_message = '<div class="alert alert-success" role="alert">'+
                                            '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+
                                            '<span class="sr-only">Error:</span>'+
                                            'Thank you for your feedback '+
                                        '</div>';
                        $("#message").append(success_message);
               }
               else{
                   var error_message = '<div class="alert alert-danger" role="alert">'+
                                            '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+
                                            '<span class="sr-only">Error:</span>'+
                                            'Sorry ,Theres an error Try again'+
                                        '</div>';
                        $("#message").append(error_message);
               }
                   
           }
         });
    return false; // avoid to execute the actual submit of the form.
});
    </script>