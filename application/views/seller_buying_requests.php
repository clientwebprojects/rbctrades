<?php include 'header2.php'; ?>
<script>
    document.getElementById("seller_buying_request").className = "active-seller-menu";
</script>
<script>
// assumes you're using jQuery
    $(document).ready(function () {
        $('.confirm-div').hide();
<?php if ($this->session->flashdata('msg')) { ?>
            $('#successmessage').html("<div class='alert alert-success alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Congrats!</strong><?php echo $this->session->flashdata('msg'); ?>.</div>").show().delay(5000).fadeOut();
        });
<?php } ?>
</script>
<div class="page_wrapper type_2" >
    <div class="container">
        <div class="">
            <div class="row">
                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                    <?php include 'inc/seller_product_menu.php'; ?>
                    <script>
                        document.getElementById("view_rfqs").className = "active";
                    </script>
                    <div>
                        <section class="col-sm-10">
                            <h3 class="heading">Buying Requests</h3>
                            <!-- - - - - - - - - - - - - - Tabs v2 - - - - - - - - - - - - - - - - -->
                            <div id="successmessage"></div>
                            <div class="tabs type_2">
                                <!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->
                                <ul class="tabs_nav clearfix" style="margin-right: 0px;">
                                    <li class="active"><a href="#tab-1">New Requests (<?php echo sizeof($requests); ?>)</a></li>
                                    <li ><a href="#tab-2">Old (<?php echo sizeof($active_products); ?>)</a></li>

                                </ul>
                                <!-- - - - - - - - - - - - - - End navigation of tabs - - - - - - - - - - - - - - - - -->
                                <!-- - - - - - - - - - - - - - Tabs container - - - - - - - - - - - - - - - - -->
                                <div class="tab_containers_wrap">
                                    <!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->
                                    <div id="tab-1" class="tab_container" style="margin-top: 20px;">
                                        <table class="table_type_1" cellpadding="0" cellspacing="0" border="0" class="display" id="table1" style="clear: both">
                                            <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Detail</th>
                                                    <th>Quantity</th>
                                                    <th>Buyer</th>
                                                    <th>Request Date</th>
                                                    <th style="width: 110px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count = 0;
                                                foreach ($requests as $req) {
                                                    //                                                   print_r($pending_products);exit();
                                                    ?>
                                                    <tr>
                                                        <td data-title="Products Name"><?= $req->rfq_product_name ?></td>
                                                        <td data-title="Detail" ><?= $req->rfq_product_description ?></td>
                                                        <td data-title="Quantity" ><?= $req->rfq_product_quantity ?></td>
                                                        <td data-title="Buyer"><?= $req->user_name ?></td>
                                                        <td data-title="Request Date" ><?= $req->rfq_quotation_date ?></td>
                                                        <td data-title="Action" style="text-align: center;">
                                                            <div class="btn-group">
                                                                <a href="<?= base_url() ?>index.php/seller/seller_quotation/<?= $req->rfq_id; ?>" class="btn btn-primary btn-xs" title="Edit">View</a>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>

<!--                                        <section class="section_offset pull-right">
                                            <ul class="pags">
                                                <li><a href="#"></a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#"></a></li>
                                            </ul>
                                        </section>-->
                                    </div>
                                    <!--/ #tab-1-->
                                    <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->

                                    <!--/ #tab-1-->
                                    <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                </div><!--/ .tab_containers_wrap -->
                                <!-- - - - - - - - - - - - - - End of tabs containers - - - - - - - - - - - - - - - - -->
                            </div><!--/ .tabs-->
                            <!-- - - - - - - - - - - - - - End of tabs v2 - - - - - - - - - - - - - - - - -->
                        </section><!--/ [col]-->
                        <!--/well-->
                    </div><!--/ .animated.transparent-->
                </div><!--/ [col]-->
                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
            </div><!--/ .row-->
        </div><!--/ .section_offset-->
        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#table1').dataTable().columnFilter();
        $('#table2').dataTable().columnFilter();

    });
</script>