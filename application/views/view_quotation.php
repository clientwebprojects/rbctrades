<?php
include 'header2.php';
//echo '<pre>';
//print_r($quot_main);exit();
?>
<script>
count = <?=sizeof($quot_details)?>;
</script>

<div class="invoice-container" >
    <form class="form-horizontal" method="post" action="" name="view-quotation" id="view-quotation">
    <header class="invoice-header">
        <h1>Quotation</h1>
        <address contenteditable>
            <textarea class="form-control" id="textarea" name="textarea" readonly style="resize: vertical;height: 130px;"><?=$quot_main->company_name?> &#13; <?=$quot_main->operational_street_address?> &#13; <?=$quot_main->operational_address_city?> &#13; <?=$quot_main->operational_address_province?> &#13; <?=$quot_main->country_name?>  </textarea>

        </address>

        <table class="meta">
            <tr>
                <th><span >Invoice #</span></th>
            <input type="hidden" id="quotation_id" name="quotation_id" value="<?=$quot_main->rfq_id?>">
                <td><span ><?=$quot_main->rfq_id?></span></td>
            </tr>
            <tr>
                <th><span >Currency</span></th>
                <input type="hidden" id="quotation_currency" name="quotation_currency" value="Dollar">
                <td><span >Dollar</span></td>
            </tr>
            <tr>
                <th><span >Date</span></th>
                <input type="hidden" id="quotation_date" name="quotation_date" value="<?=$quot_main->quotation_date?>">
                <td><span ><?=$quot_main->quotation_date?></span></td>
            </tr>
            <tr>
                <th><span >Valid Date</span></th>
                <input type="hidden" id="quotation_expiry_date" name="quotation_expiry_date" value="<?=$quot_main->quotation_validity_date?>">
                <td><span ><?=$quot_main->quotation_validity_date?></span></td>
            </tr>

        </table>
<!--			<span><img alt="" src="<?= base_url() ?>/images/logo.png"></span>-->
    </header>
    
        <article>




            <table class="inventory">
                <thead>
                    <tr>
                        <th><span>Item</span></th>
                        <th><span>Description</span></th>
                        <th><span>Rate</span></th>
                        <th><span>Quantity</span></th>
                        <th><span>UOM</span></th>
                        <th><span>Price</span></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $i=0;
                            foreach ($quot_details as $q) {
                                
                    ?>
                    <tr>
                        <td><input id="product_name<?php echo $i; ?>" name="product_name[]"  type="text" value="<?= $q->product_name?>" readonly ></td>
                        <td><input id="select<?php echo $i; ?>description" name="product_description[]" type="text" value="<?= $q->description?>" readonly></td>
                        <td><input id="rate<?php echo $i; ?>" name="product_rate[]" type="text" value="<?= $q->unit_price?>" readonly></td>
                        <td><input id="quantity<?php echo $i; ?>" name="product_quantity[]"  type="text" value="<?= $q->quantity?>" readonly></td>
                        <td><input id="product_uom<?php echo $i; ?>" type="text" value="<?= $q->uom_name?>" readonly><input type="hidden" name="product_uom[]"  type="text" value="<?= $q->uom_id?>"></td>
                        <td><input id="price<?php echo $i; ?>" name="product_price[]" type="text" disabled></td>
                    </tr>
                    
                    <?php
                           $i++; }
                    ?>
                </tbody>
            </table>
            <table class="balance">
                <tr>
                    <th><span >Sub Total</span></th>
                    <td><input id="total" name="total" type="text" disabled></td>
                </tr>
                <tr>
                    <th><span >Discount %</span></th>
                    <td><input id="discount" name="discount" type="text" value="<?=$quot_main->discount?>" ></td>
                </tr>
                <tr>
                    <th><span >Tax %</span></th>
                    <td><input id="tax" name="tax" type="text" value="<?=$quot_main->tax?>" ></td>
                </tr>
                <tr>
                    <th><span >Grand Total</span></th>
                    <td><input id="balance_due" name="balance_due" type="text" disabled></td>
                </tr>
            </table>
        </article>
        
        <button class="btn btn-primary" id="view-quotation-submit" type="submit" value="view-quotation-submit" name="view-quotation-submit">Accept</button>
        
    </form>
</div>
<script src="<?= base_url() ?>js/invoicescript.js" type="text/javascript"></script>
<?php include 'footer.php'; ?>