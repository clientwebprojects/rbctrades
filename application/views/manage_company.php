<?php include 'header2.php';?>
<script>
    document.getElementById("seller_company").className = "active-seller-menu";
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">

            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px;">
                    <?php include 'inc/seller_company_menu.php'; ?>
                    <script>
                        document.getElementById("manage_company_profile").className = "active";
                    </script>
                    <div class="col-md-10">
                                
                                <section class="col-sm-12">
                                    <div class="tabs type_2">
                                        <!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->
                                        <ul class="tabs_nav clearfix" style="margin-right: 0px;">
                                            <li id="tab-1-btn"><a href="#tab-1" style="font-size: 12px;font-weight: bold">Basic Company Details</a></li>
                                            <li id="tab-2-btn"><a href="#tab-2" style="font-size: 12px;font-weight: bold">Trade Details</a></li>
                                            <li id="tab-3-btn"><a href="#tab-3" style="font-size: 12px;font-weight: bold">Partner Factories</a></li>
                                            <li id="tab-4-btn"><a href="#tab-4" style="font-size: 12px;font-weight: bold">Company Introduction</a></li>
                                            <li id="tab-5-btn"><a href="#tab-5" style="font-size: 12px;font-weight: bold">Certification & Trademark</a></li>
                                        </ul>
                                        <!-- - - - - - - - - - - - - - End navigation of tabs - - - - - - - - - - - - - - - - -->
                                        <!-- - - - - - - - - - - - - - Tabs container - - - - - - - - - - - - - - - - -->
                                        <div class="tab_containers_wrap">
                                            <!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->
                                            <div id="tab-1" class="tab_container" style="margin-top:10px;">
                                                <form class="form-horizontal type_2" id="basic_company_details" action="" method="post" enctype="multipart/form-data">
                                                    <div class="form-group">
                                                        <label class="col-sm-6 col-md-3 control-label"><span style="color:red">*</span>Business Type:</label>
                                                        <div class="col-md-6"><?php foreach($business_types as $type){?>
                                                            <div class="col-sm-12 checkbox">
                                                                <label for="business_<?= $type->business_type_id;?>" style="padding-top:0px;" class="col-sm-12 ">
                                                                    <input  name="business_type[]" id="business_<?= $type->business_type_id; ?>" value="<?= $type->business_type_id; ?>" type="checkbox" <?php if(in_array($type->business_type_id,$company->business_type)){echo "checked=checked";}?> class="bt_chekbox"><?= $type->business_type_name;?>
                                                                </label>
                                                            </div>
                                                            <?php
                                                                  }?>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="company_id" value="<?= $company->id?>"/>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="company_name"><span style="color:red">*</span>Company Name</label>
                                                        <div class="col-md-5">
                                                            <input id="company_name" name="company_name" type="text" placeholder="Complete Name" class="form-control input-md" value="<?php echo $company->company_name;?>">

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="registration_loc"><span style="color:red">*</span>Location of Registration</label>
                                                        <div class="col-md-5">

                                                            <select id="registration_loc" name="registration_location" class="form-control">
                                                                <option value="">---Select Country---</option>
                                                                <?php foreach ($countries as $country) { ?>
                                                                    <option value="<?= $country->country_id; ?>" <?php if($country->country_id==$company->registration_location){echo "selected=selected";}?>>
                                                                        <?= $country->country_name; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>


                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" ><span style="color:red">*</span>Company Operational Address</label>
                                                        <div class="col-md-8">
                                                            <div class="col-md-2"><label class="control-label" >Street</label></div>
                                                            <div class="col-md-6" >
                                                                <input id="operational_street" name="operational_street_address" type="text" placeholder="Street" class="form-control input-md" value="<?= $company->operational_street_address;?>">
                                                            </div>
                                                            <div class="col-md-2" style="clear: left;margin-top: 5px"><label class="control-label" >City</label></div>
                                                            <div class="col-md-6" style="margin-top: 5px;">
                                                                <input id="operational_city" name="operational_address_city" type="text" placeholder="City" class="form-control input-md" value="<?= $company->operational_address_city;?>">
                                                            </div>
                                                            <div class="col-md-2" style="clear: left;margin-top: 5px"><label class="control-label" >Country</label></div>
                                                            <div class="col-md-6" style="margin-top: 5px;">
                                                                <select id="operational_loc" name="operational_location" class="form-control">
                                                                    <option value="">---Country/Region---</option>
                                                                    <?php foreach ($countries as $country) { ?>
                                                                    <option value="<?= $country->country_id; ?>" <?php if($country->country_id==$company->operational_location){ echo "selected=selected";}?>>
                                                                            <?= $country->country_name; ?>
                                                                        </option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2" style="clear: left;margin-top: 5px"><label class="control-label" value="<?= $company->operational_address_zip_code?>">Zip</label></div>
                                                            <div class="col-md-6" style="margin-top: 5px;">
                                                                <input id="operational_zip_code" name="operational_address_zip_code" type="text" placeholder="Zip/postal Code" class="form-control input-md" value="<?= $company->operational_address_zip_code;?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="main_products"><span style="color:red">*</span>Main Products</label>
                                                        <div class="col-md-9" >
                                                            <div class="col-md-2" style="padding:0;margin:0 0;">
                                                                <input id="main_products" name="main_products[]" type="text" class="form-control input-md" value="<?= $company->main_products[0];?>">
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="main_products" name="main_products[]" type="text" class="form-control input-md" value="<?= $company->main_products[1];?>">
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="main_products" name="main_products[]" type="text"class="form-control input-md" value="<?= $company->main_products[2];?>">
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="main_products" name="main_products[]" type="text" class="form-control input-md" value="<?= $company->main_products[3];?>">
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="main_products" name="main_products[]" type="text"  class="form-control input-md" value="<?= $company->main_products[4];?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="other_products">Other Products You Sell</label>
                                                        <div class="col-md-9" >
                                                            <div class="col-md-2" style="padding:0;margin:0 0;">
                                                                <input id="other_products" name="other_products[]" type="text"class="form-control input-md" value="<?= $company->other_products[0];?>">
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="other_products" name="other_products[]" type="text" class="form-control input-md" value="<?= $company->other_products[1];?>" >
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="other_products" name="other_products[]" type="text" class="form-control input-md" value="<?= $company->other_products[2];?>" >
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="other_products" name="other_products[]" type="text"class="form-control input-md" value="<?= $company->other_products[3];?>" >
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px">
                                                                <input id="other_products" name="other_products[]" type="text"class="form-control input-md" value="<?= $company->other_products[4];?>" >
                                                            </div>

                                                            <div class="col-md-2" style="padding:0;margin:0 0;margin-top: 3px;">
                                                                <input id="other_products" name="other_products[]" type="text"class="form-control input-md" value="<?= $company->other_products[5];?>" >
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px;margin-top: 3px;">
                                                                <input id="other_products" name="other_products[]" type="text" class="form-control input-md" value="<?= $company->other_products[6];?>" >
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px;margin-top: 3px;">
                                                                <input id="other_products" name="other_products[]" type="text" class="form-control input-md" value="<?= $company->other_products[7];?>" >
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px;margin-top: 3px;">
                                                                <input id="other_products" name="other_products[]" type="text"class="form-control input-md" value="<?= $company->other_products[8];?>" >
                                                            </div>
                                                            <div class="col-md-2" style="padding:0;margin:0;margin-left: 2px;margin-top: 3px;">
                                                                <input id="other_products" name="other_products[]" type="text"class="form-control input-md" value="<?= $company->other_products[9];?>" >
                                                            </div>
                                                            <div class="col-md-4" style="padding:0;margin:0;margin-left: 2px;margin-top: 3px;">
                                                                One product per box
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="registered_year">Year Company Registered</label>
                                                        <div class="col-md-4">
                                                            <select id="registered_year" name="registered_year" class="form-control">
                                                                <option value="">---Please Select---</option>
                                                                <?php for($i=date("Y"); $i>1950;$i--){?>
                                                                    
                                                                    <option value="<?= $i?>" <?php if($company->registered_year == $i){echo "selected=selected";}?> ><?= $i; ?></option>
                                                                <?php
                                                                }
                                                                ?>
                                                                

                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="total_employees">Total No. Employees</label>
                                                        <div class="col-md-4">
                                                            <select id="total_employees" name="total_employees" class="form-control">
                                                                <option value="">---Please Select---</option>

                                                                <option value="less_5" <?php if($company->total_employees == "less_5"){echo "selected=selected";}?>>Fewer than 5</option>
                                                                <option value="5-10" <?php if($company->total_employees == "5-10"){echo "selected=selected";}?>>5-10 People</option>
                                                                <option value="11-50" <?php if($company->total_employees == "11-50"){echo "selected=selected";}?>>11-50 People</option>
                                                                <option value="51-100" <?php if($company->total_employees == "51-100"){echo "selected=selected";}?>>51-100 People</option>
                                                                <option value="more-100" <?php if($company->total_employees == "more-100"){echo "selected=selected";}?>>More than 100</option>
                                                                
                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="website_url">Company Website URL</label>
                                                        <div class="col-md-5">
                                                            <input id="website_url" name="website_url" type="text" class="form-control input-md" value="<?php if(isset($company->website_url)){echo $company->website_url;}else{ echo "http://";}?>" >

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="legal_owner">Legal Owner</label>
                                                        <div class="col-md-5">
                                                            <input id="legal_owner" name="legal_owner" type="text" class="form-control input-md"  value="<?= $company->legal_owner;?>">

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="office_size">Office Size</label>
                                                        <div class="col-md-4">
                                                            <select id="office_size" name="office_size" class="form-control">
                                                                <option value="">---Please Select---</option>

                                                                <option value="less_5" <?php if($company->office_size == "less_5"){echo "selected=selected";}?>>below 100 square meters</option>
                                                                <option value="5-10" <?php if($company->office_size == "5-10"){echo "selected=selected";}?>>101-500 square meters</option>
                                                                <option value="11-50" <?php if($company->office_size == "11-50"){echo "selected=selected";}?>>501-1000 square meters</option>
                                                                <option value="51-100" <?php if($company->office_size == "51-100"){echo "selected=selected";}?>>1001-10000 square meters</option>

                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="company_advantages">Company Advantages</label>
                                                        <div class="col-md-5">
                                                            <textarea rows="5" class="form-control" name="company_advantages" id="company_advantages"><?= $company->company_advantages;?></textarea>
                                                            <div style="font-size:10px">Please briefly describe your company’s advantages. E.g. “We have twenty years experience of product design.</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <div class="dash-line"></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-9" style="margin-top: 15px;">
                                                                <input type="submit" class="button_blue" name="update_basic_company_details" value="Submit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--/ #tab-1-->
                                            <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                            <div id="tab-2" class="tab_container" style="margin-top:10px; display:none">
                                                <form class="form-horizontal type_2" id="trade-details-form" action="" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="company_id" value="<?= $company->id?>"/>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="annual_sales_volume">Total Annual Sales Volume</label>
                                                        <div class="col-md-4">
                                                            <select id="annual_sales_volume" name="annual_sales_volume" class="form-control">
                                                                <option value="">---Please Select---</option>

                                                                <option value="less_5" <?php if($company->annual_sales_volume == "less_5"){echo "selected=selected";}?>>below US$1 Million</option>
                                                                <option value="5-10" <?php if($company->annual_sales_volume == "5-10"){echo "selected=selected";}?>>US$1 Million - US$2.5 Million</option>
                                                                <option value="11-50" <?php if($company->annual_sales_volume == "11-50"){echo "selected=selected";}?>>US$2.5 Million - US$5 Million</option>
                                                                <option value="51-100" <?php if($company->annual_sales_volume == "51-100"){echo "selected=selected";}?>>US$5 Million - US$10 Million</option>

                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="export_percentage">Export Percentage</label>
                                                        <div class="col-md-4">
                                                            <select id="export_percentage" name="export_percentage" class="form-control">
                                                                <option value="">---Please Select---</option>

                                                                <option value="less_5" <?php if($company->export_percentage == "less_5"){echo "selected=selected";}?>>1%-10%</option>
                                                                <option value="5-10" <?php if($company->export_percentage == "5-10"){echo "selected=selected";}?>>11%-20%</option>
                                                                <option value="11-50" <?php if($company->export_percentage == "11-50"){echo "selected=selected";}?>>21%-30%</option>
                                                                <option value="51-100" <?php if($company->export_percentage == "51-100"){echo "selected=selected";}?>>31%-40%</option>

                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="exporting_started">Year When Your Company Started Exporting</label>
                                                        <div class="col-md-3">
                                                            <select id="exporting_started" name="exporting_started" class="form-control">
                                                                <option value="">-Please Select-</option>
                                                                <?php for ($i = 1910; $i <= 2015; $i++) { ?>
                                                                    <option value="<?= $i ?>" <?php if($company->exporting_started==$i){echo "selected=selected";}?>><?= $i ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>

                                                        </div>
                                                    </div>

    <!--                                                <div class="form-group">
                                                        <label class="col-md-3 control-label" for="sales_volume">Add Customer Details</label>
                                                        <div class="col-md-4">
                                                            <label class="radio-inline" for="customer_details_yes">
                                                                <input type="radio" id="customer_details_yes" class="radio add_customer_details_btn" name="customer_details" value="yes" style="display: block;width:15px"/>Yes
                                                            </label>

                                                            <label class="radio-inline" for="customer_details_no">
                                                                <input checked type="radio" id="customer_details_no" class="radio customer_details_no_btn" name="customer_details" value="no" style="display: block;width:15px"/>No
                                                            </label>
                                                        </div>
                                                    </div>-->
                                                    <div id="customer_detail_div" style="display:none">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="customer_name"><span style="color:red">*</span>Customer</label>
                                                            <div class="col-md-4">

                                                                <input type="text" id="customer_name" class="form-control input-md" name="customer_name"/>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="customer_name"><span style="color:red">*</span>Customer's Country/Region</label>
                                                            <div class="col-md-4">
                                                                <select id="seller_location" name="seller_location" class="form-control">
                                                                    <option value="">---Select Country---</option>
                                                                    <?php foreach ($countries as $country) { ?>
                                                                        <option value="<?= $country->country_id; ?>">
                                                                            <?= $country->country_name; ?>
                                                                        </option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="customer_products"><span style="color:red">*</span>Products You Supply</label>
                                                            <div class="col-md-4">

                                                                <input type="text" id="customer_products" class="form-control input-md" name="customer_products"/>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="customer_turnover">Annual Turnover</label>
                                                            <div class="col-md-4">

                                                                <input type="text" id="customer_turnover" class="form-control input-md" placeholder="$" name="customer_turnover"/>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label" for="customer_document">Transaction Document</label>
                                                            <div class="col-md-4">

                                                                <input type="file" id="customer_document" class="form-control input-md" name="customer_document"/>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="trade_staff_num">No of Employees in trade Department</label>
                                                        <div class="col-md-4">
                                                            <select id="trade_staff_num" name="trade_staff_num" class="form-control">
                                                                <option value="">---Please Select---</option>

                                                                <option value="less_5" <?php if($company->trade_staff_num == "less_5"){echo "selected=selected";}?>>0 People/option>
                                                                <option value="5-10" <?php if($company->trade_staff_num == "5-10"){echo "selected=selected";}?>>1-2 people</option>
                                                                <option value="11-50" <?php if($company->trade_staff_num == "11-50"){echo "selected=selected";}?>>3-5 People</option>
                                                                <option value="51-100" <?php if($company->trade_staff_num == "51-100"){echo "selected=selected";}?>>5-10 People</option>

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="cross_industry">Are you able to source across multiple industries?:</label>
                                                        <div class="col-md-4">
                                                            <label class="radio-inline" for="outsource_yes">
                                                                <input type="radio" id="outsource_yes" class="radio" name="cross_industry" value="1" style="display: block;width:15px" <?php if($company->cross_industry==1){ echo "checked=ckecked";}?>/>Yes
                                                            </label>

                                                            <label class="radio-inline" for="outsource_no">
                                                                <input type="radio" id="outsource_no" class="radio" name="cross_industry" value="0" style="display: block;width:15px" <?php if($company->cross_industry==0){ echo "checked=ckecked";}?>/>No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="rnd_staff_num_trade">No. R&D Staff</label>
                                                        <div class="col-md-4">
                                                            <select id="rnd_staff_num_trade" name="rnd_staff_num_trade" class="form-control">
                                                                <option value="">---Please Select---</option>

                                                                <option value="less_5" <?php if($company->rnd_staff_num_trade == "less_5"){echo "selected=selected";}?>>0 People/option>
                                                                <option value="5-10" <?php if($company->rnd_staff_num_trade == "5-10"){echo "selected=selected";}?>>1-2 people</option>
                                                                <option value="11-50" <?php if($company->rnd_staff_num_trade == "11-50"){echo "selected=selected";}?>>3-5 People</option>
                                                                <option value="51-100" <?php if($company->rnd_staff_num_trade == "51-100"){echo "selected=selected";}?>>5-10 People</option>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="qc_staff_num_trade">No QC Staff</label>
                                                        <div class="col-md-4">
                                                            <select id="qc_staff_num_trade" name="qc_staff_num_trade" class="form-control">
                                                                <option value="">---Please Select---</option>

                                                                <option value="less_5" <?php if($company->qc_staff_num_trade == "less_5"){echo "selected=selected";}?>>0 People/option>
                                                                <option value="5-10" <?php if($company->qc_staff_num_trade == "5-10"){echo "selected=selected";}?>>1-2 people</option>
                                                                <option value="11-50" <?php if($company->qc_staff_num_trade == "11-50"){echo "selected=selected";}?>>3-5 People</option>
                                                                <option value="51-100" <?php if($company->qc_staff_num_trade == "51-100"){echo "selected=selected";}?>>5-10 People</option>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="export_port">Nearest Port</label>
                                                        <div class="col-md-7" style="padding-left:0">
                                                            <div class="col-md-4">
                                                                <input id="export_port" name="export_port[]" type="text" class="form-control input-md" value="<?= $company->export_port[0];?>"/>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <input name="export_port[]" type="text" class="form-control input-md" value="<?= $company->export_port[1];?>"/>
                                                            </div>
                                                            <div class="col-md-4">
                                                            <input name="export_port[]" type="text"  class="form-control input-md" value="<?= $company->export_port[2];?>"/>
                                                            </div>
                                                            <div class="col-md-5">One port name per box</div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="avg_delivery_time">Average Lead Time</label>
                                                        <div class="col-md-4">
                                                            <input id="avg_delivery_time" name="avg_delivery_time" type="text" class="form-control input-md" value="<?= $company->avg_delivery_time;?>"/>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">Days</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="have_oversea_office">Does your company have an overseas office?:</label>
                                                        <div class="col-md-4">
                                                            <label class="radio-inline" for="overseas_office_yes">
                                                                <input type="radio" id="overseas_office_yes" class="radio" name="have_oversea_office" value="1" style="display: block;width:15px" <?php if($company->have_oversea_office==1){echo "checked=checked";}?>/>Yes
                                                            </label>

                                                            <label class="radio-inline" for="overseas_office_no">
                                                                <input type="radio" id="overseas_office_no" class="radio" name="have_oversea_office" value="0" style="display: block;width:15px" <?php if($company->have_oversea_office==0){echo "checked=checked";}?>/>No
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="delievery_terms">Accepted Delivery Terms:</label>
                                                        <div class="col-md-7">
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="fob">
                                                                    <input name="delievery_terms[]" id="fob" value="fob" type="checkbox" style=" width: 30px" <?php if(in_array("fob",$company->delievery_terms)){echo "checked=checked";}?>>FOB
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="cfr">
                                                                    <input name="delievery_terms[]" id="cfr" value="cfr" type="checkbox" style=" width: 30px" <?php if(in_array("cfr",$company->delievery_terms)){echo "checked=checked";}?>>CFR
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="cif">
                                                                    <input name="delievery_terms[]" id="cif" value="cif" type="checkbox" style=" width: 30px" <?php if(in_array("cif",$company->delievery_terms)){echo "checked=checked";}?>>CIF
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="exw">
                                                                    <input name="delievery_terms[]" id="exw" value="exw" type="checkbox" style=" width: 30px" <?php if(in_array("exw",$company->delievery_terms)){echo "checked=checked";}?>>EXW
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="fas">
                                                                    <input name="delievery_terms[]" id="fas" value="fas" type="checkbox" style=" width: 30px" <?php if(in_array("fas",$company->delievery_terms)){echo "checked=checked";}?>>FAS
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="cip">
                                                                    <input name="delievery_terms[]" id="cip" value="cip" type="checkbox" style=" width: 30px" <?php if(in_array("cip",$company->delievery_terms)){echo "checked=checked";}?>>CIP
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="fca">
                                                                    <input name="delievery_terms[]" id="fca" value="fca" type="checkbox" style=" width: 30px" <?php if(in_array("fca",$company->delievery_terms)){echo "checked=checked";}?>>FCA
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="cpt">
                                                                    <input name="delievery_terms[]" id="cpt" value="cpt" type="checkbox" style=" width: 30px" <?php if(in_array("cpt",$company->delievery_terms)){echo "checked=checked";}?>>CPT
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="deq">
                                                                    <input name="delievery_terms[]" id="deq" value="deq" type="checkbox" style=" width: 30px" <?php if(in_array("deq",$company->delievery_terms)){echo "checked=checked";}?>>DEQ
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="ddp">
                                                                    <input name="delievery_terms[]" id="ddp" value="ddp" type="checkbox" style=" width: 30px" <?php if(in_array("ddp",$company->delievery_terms)){echo "checked=checked";}?>>DDP
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="ddu">
                                                                    <input name="delievery_terms[]" id="ddu" value="ddu" type="checkbox" style=" width: 30px" <?php if(in_array("ddu",$company->delievery_terms)){echo "checked=checked";}?>>DDU
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="daf">
                                                                    <input name="delievery_terms[]" id="daf" value="daf" type="checkbox" style=" width: 30px" <?php if(in_array("daf",$company->delievery_terms)){echo "checked=checked";}?>>DAF
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="des">
                                                                    <input name="delievery_terms[]" id="des" value="des" type="checkbox" style=" width: 30px" <?php if(in_array("des",$company->delievery_terms)){echo "checked=checked";}?>>DES
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-5">
                                                                <label class="checkbox-inline" for="express_delievery">
                                                                    <input name="delievery_terms[]" id="express_delievery" value="express_delievery" type="checkbox" style=" width: 30px" <?php if(in_array("express_delievery",$company->delievery_terms)){echo "checked=checked";}?>>Express Delivery
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="support_currency">Accepted Payment Currency:</label>
                                                        <div class="col-md-7">
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="usd">
                                                                    <input name="support_currency[]" id="usd" value="usd" type="checkbox" style=" width: 30px" <?php if(in_array("usd",$company->support_currency)){echo "checked=checked";}?>>USD
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="eur">
                                                                    <input name="support_currency[]" id="eur" value="eur" type="checkbox" style=" width: 30px" <?php if(in_array("eur",$company->support_currency)){echo "checked=checked";}?>>EUR
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="jpy">
                                                                    <input name="support_currency[]" id="jpy" value="jpy" type="checkbox" style=" width: 30px" <?php if(in_array("jpy",$company->support_currency)){echo "checked=checked";}?>>JPY
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="cad">
                                                                    <input name="support_currency[]" id="cad" value="cad" type="checkbox" style=" width: 30px" <?php if(in_array("cad",$company->support_currency)){echo "checked=checked";}?>>CAD
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="aud">
                                                                    <input name="support_currency[]" id="aud" value="aud" type="checkbox" style=" width: 30px" <?php if(in_array("aud",$company->support_currency)){echo "checked=checked";}?>>AUD
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="hkd">
                                                                    <input name="support_currency[]" id="hkd" value="hkd" type="checkbox" style=" width: 30px" <?php if(in_array("hkd",$company->support_currency)){echo "checked=checked";}?>>HKD
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="gbp">
                                                                    <input name="support_currency[]" id="gbp" value="gbp" type="checkbox" style=" width: 30px" <?php if(in_array("gbp",$company->support_currency)){echo "checked=checked";}?>>GBP
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="cny">
                                                                    <input name="support_currency[]" id="cny" value="cny" type="checkbox" style=" width: 30px" <?php if(in_array("cny",$company->support_currency)){echo "checked=checked";}?>>CNY
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="chf">
                                                                    <input name="support_currency[]" id="chf" value="chf" type="checkbox" style=" width: 30px" <?php if(in_array("chf",$company->support_currency)){echo "checked=checked";}?>>CHF
                                                                </label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="payment_methods">Accepted Payment Type:</label>
                                                        <div class="col-md-7">
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="tt">
                                                                    <input name="payment_methods[]" id="tt" value="t/t" type="checkbox" style=" width: 30px" <?php if(in_array("t/t",$company->payment_methods)){echo "checked=checked";}?>>T/T
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="lc">
                                                                    <input name="payment_methods[]" id="lc" value="l/c" type="checkbox" style=" width: 30px" <?php if(in_array("l/c",$company->payment_methods)){echo "checked=checked";}?>>L/C
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="dpda">
                                                                    <input name="payment_methods[]" id="dpda" value="d/p d/a" type="checkbox" style=" width: 30px" <?php if(in_array("d/p d/a",$company->payment_methods)){echo "checked=checked";}?>>D/P D/A
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="money_gram">
                                                                    <input name="payment_methods[]" id="money_gram" value="money gram" type="checkbox" style=" width: 30px" <?php if(in_array("money gram",$company->payment_methods)){echo "checked=checked";}?>>MoneyGram
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="credit_card">
                                                                    <input name="payment_methods[]" id="credit_card" value="credit card" type="checkbox" style=" width: 30px" <?php if(in_array("credit card",$company->payment_methods)){echo "checked=checked";}?>>Credit Card
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="pay_pal">
                                                                    <input name="payment_methods[]" id="pay_pal" value="pay pal" type="checkbox" style=" width: 30px" <?php if(in_array("pay pal",$company->payment_methods)){echo "checked=checked";}?>>PayPal
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="western_union">
                                                                    <input name="payment_methods[]" id="western_union" value="western union" type="checkbox" style=" width: 30px" <?php if(in_array("western union",$company->payment_methods)){echo "checked=checked";}?>>Western Union
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="cash">
                                                                    <input name="payment_methods[]" id="cash" value="cash" type="checkbox" style=" width: 30px" <?php if(in_array("cash",$company->payment_methods)){echo "checked=checked";}?>>Cash
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="escrow">
                                                                    <input name="payment_methods[]" id="escrow" value="escrow" type="checkbox" style=" width: 30px" <?php if(in_array("escrow",$company->payment_methods)){echo "checked=checked";}?>>Escrow
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="language_skills">Language Spoken:</label>
                                                        <div class="col-md-7">
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="english">
                                                                    <input name="language_skills[]" id="english" value="english" type="checkbox" style=" width: 30px" <?php if(in_array("english",$company->language_skills)){echo "checked=checked";}?>>English
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="chinese">
                                                                    <input name="language_skills[]" id="chinese" value="chinese" type="checkbox" style=" width: 30px" <?php if(in_array("chinese",$company->language_skills)){echo "checked=checked";}?>>Chinese
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="spanish">
                                                                    <input name="language_skills[]" id="spanish" value="spanish" type="checkbox" style=" width: 30px" <?php if(in_array("spanish",$company->language_skills)){echo "checked=checked";}?>>Spanish
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="japanese">
                                                                    <input name="language_skills[]" id="japanese" value="japanese" type="checkbox" style=" width: 30px" <?php if(in_array("japanese",$company->language_skills)){echo "checked=checked";}?>>Japanese
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="portuguese">
                                                                    <input name="language_skills[]" id="portuguese" value="portuguese" type="checkbox" style=" width: 30px" <?php if(in_array("portuguese",$company->language_skills)){echo "checked=checked";}?>>Portuguese
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="german">
                                                                    <input name="language_skills[]" id="german" value="german" type="checkbox" style=" width: 30px" <?php if(in_array("german",$company->language_skills)){echo "checked=checked";}?>>German
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="arabic">
                                                                    <input name="language_skills[]" id="arabic" value="arabic" type="checkbox" style=" width: 30px" <?php if(in_array("arabic",$company->language_skills)){echo "checked=checked";}?>>Arabic
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="french">
                                                                    <input name="language_skills[]" id="french" value="french" type="checkbox" style=" width: 30px" <?php if(in_array("french",$company->language_skills)){echo "checked=checked";}?>>French
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="russian">
                                                                    <input name="language_skills[]" id="russian" value="russian" type="checkbox" style=" width: 30px" <?php if(in_array("russian",$company->language_skills)){echo "checked=checked";}?>>Russian
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="korean">
                                                                    <input name="language_skills[]" id="korean" value="korean" type="checkbox" style=" width: 30px" <?php if(in_array("korean",$company->language_skills)){echo "checked=checked";}?>>Korean
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="hindi">
                                                                    <input name="language_skills[]" id="hindi" value="hindi" type="checkbox" style=" width: 30px" <?php if(in_array("hindi",$company->language_skills)){echo "checked=checked";}?>>Hindi
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-2 col-md-4">
                                                                <label class="checkbox-inline" for="italian">
                                                                    <input name="language_skills[]" id="italian" value="italian" type="checkbox" style=" width: 30px" <?php if(in_array("italian",$company->language_skills)){echo "checked=checked";}?>>Italian
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <div class="dash-line"></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-9" style="margin-top: 15px;">
                                                                <input type="submit" class="button_blue" name="update_trade_details" value="Submit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!--/ #tab-2-->
                                            <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                            <div id="tab-3" class="tab_container" style="margin-top:10px; display:none">
                                                
                                                <form class="form-horizontal type_2" id="partner-factory-form" action="" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="company_id" value="<?= $company->id?>"/>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="partner_factories">Do you cooperate with a factory?:</label>
                                                        <div class="col-md-4">
                                                            <label class="radio-inline" for="factories_yes">
                                                                <input checked="" type="radio" id="factories_yes" class="radio partner_factories_btn" name="partner_factories" value="yes" style="display: block;width:30px"/>Yes
                                                            </label>

                                                            <label class="radio-inline" for="factories_no">
                                                                <input type="radio" id="factories_no" class="radio partner_factories_btn_no" name="partner_factories" value="no" style="display: block;width:30px"/>No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div id="partner_factories_yes_div" style="display:block">
                                                        <div class="form-group" >
                                                            <label class="col-md-3 control-label" for="partner_factory_name"><span style="color:red">*</span>Factory Name:</label>
                                                            <div class="col-md-4">
                                                                <input type="text" id="partner_factory_name" class="form-control" name="partner_factory_name" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group" >
                                                            <label class="col-md-3 control-label" for="coorporation_contract">Cooperation Contract:</label>
                                                            <div class="col-md-4">
                                                                <input id="contract_document" type="file" name="coorporation_contract"><br>
                                                            </div>

                                                            <div class="col-md-5" style="margin-top:3px;">Contract should be in .pdf format</div>
                                                        </div>
                                                        
                                                        <div class="form-group" >
                                                            <label class="col-md-3 control-label" for="partner_duration">Years Cooperation:</label>
                                                            <div class="col-md-4">
                                                                <select id="partner_duration" name="partner_duration" class="form-control">
                                                                    <option value="">---Please Select---</option>

                                                                    <option value="1">1 Year</option>
                                                                    <option value="2">2 Years</option>
                                                                    <option value="3">3 Years</option>
                                                                    <option value="4">4 Years</option>

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group" >
                                                            <label class="col-md-3 control-label" for="partner_annual_amount">Total Transaction Amount with the Factory (Previous Year):</label>
                                                            <div class="col-md-4">
                                                                <select id="partner_annual_amount" name="partner_annual_amount" class="form-control">
                                                                    <option value="">---Please Select---</option>

                                                                    <option value="1">Below $100 Thousand</option>
                                                                    <option value="2">$100 Thousand - $300 Thousand</option>
                                                                    <option value="3">$300 Thousand - $500 Thousand</option>
                                                                    <option value="4">$500 Thousand - $1 Million</option>

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" >
                                                            <label class="col-md-3 control-label" >Production Capacity:</label>
                                                            <div class="col-md-9">
                                                                <div class="col-md-12">
                                                                    <label class="col-md-2 control-label" >Product:</label>
                                                                    <div class="col-md-2"><input type="text" class="form-control" name="product_name" /></div>
                                                                    <label class="col-md-4 control-label" >Annual Prod. Volume:</label>
                                                                    <div class="col-md-2" ><input type="text" class="form-control" name="product_name" /></div>
                                                                    <div class="col-md-2">
                                                                        <select id="total_tranactions" name="total_tranactions" class="form-control">
                                                                            <option value="">unit type</option>

                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label class="col-md-2 control-label" >Product:</label>
                                                                    <div class="col-md-2"><input type="text" class="form-control" name="product_name" /></div>
                                                                    <label class="col-md-4 control-label" >Annual Prod. Volume:</label>
                                                                    <div class="col-md-2" ><input type="text" class="form-control" name="product_name" /></div>
                                                                    <div class="col-md-2">
                                                                        <select id="total_tranactions" name="total_tranactions" class="form-control">
                                                                            <option value="">unit type</option>

                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <label class="col-md-2 control-label" >Product:</label>
                                                                    <div class="col-md-2"><input type="text" class="form-control" name="product_name" /></div>
                                                                    <label class="col-md-4 control-label" >Annual Prod. Volume:</label>
                                                                    <div class="col-md-2" ><input type="text" class="form-control" name="product_name" /></div>
                                                                    <div class="col-md-2">
                                                                        <select id="total_tranactions" name="total_tranactions" class="form-control">
                                                                            <option value="">unit type</option>

                                                                            <option value="1">1</option>
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <div class="dash-line"></div>
                                                                <div class="col-md-3"></div>
                                                                <div class="col-md-9" style="margin-top: 15px;">
                                                                    <input type="submit" class="button_blue"  name="add_partner_factory" value="Submit">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </form>
                                                <h4>Partner Factories</h4>
                                                <table class="table_type_1" cellpadding="0" cellspacing="0" border="0" class="display" id="table4" style="clear: both">
                                                    <thead>
                                                        <tr>
                                                            <th>Factory Name</th>
                                                            <th>Contract</th>
                                                            <th>Years</th>
                                                            <th>Transaction Amount</th>
                                                            <th >Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $count = 0;
                                                        foreach ($partner_factories as $partner) {
                                                            ?>
                                                            <tr>
                                                                <td data-title="Products Name"><?= $partner->partner_factory_name ?></td>
                                                                <td data-title="Model" ><img src="<?php if(!empty($partner->coorporation_contract)){ echo $partner->coorporation_contract;}else{echo base_url('images/no_image.jpg');}?>" height="70" width="50"/></td>
                                                                <td data-title="Owner" ><?= $partner->partner_duration ?></td>
                                                                <td data-title="Status"><?= $partner->partner_annual_amount ?></td>
                                                                <td data-title="Action">
                                                                    <div class="btn-group">
                                                                        <a href="<?php echo base_url() ?>index.php/seller/delete_partner_factory/<?= $partner-id ?>" class="btn btn-danger btn-xs" title="delete" onclick="return confirm('Are you sure you want to delete this product?')">Delete</a>
                                                                    </div>
                                                                </td>

                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>

                                            </div>
                                            <!--/ #tab-3-->
                                            <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                            <div id="tab-4" class="tab_container" style="margin-top:10px;display:none">
                                                <form class="form-horizontal type_2" id="company-intro-form" action="" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="company_id" value="<?= $company->id?>"/>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="company_logo">Company logo:</label>
                                                        <div class="col-md-4">
                                                            <input id="company_logo" type="file" name="company_logo"><br>
                                                            <img id="image_upload_preview" src="<?= $company->logo_url;?>" alt="your image" width="70" height="70"/>
                                                        </div>
                                                        <div class="col-md-4">

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="company_description">Company detailed Description:</label>
                                                        <div class="col-md-7">
                                                            <textarea id="company_description" rows="5" class="form-control input-md" name="company_description"><?= $company->company_description;?></textarea>
                                                        </div>
                                                    </div>
                                                    <div style="height:150px"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <div class="dash-line"></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-9" style="margin-top: 15px;">
                                                                <input type="submit" class="button_blue" name="update_company_intro" value="Submit">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                            <!--/ #tab-4-->
                                            <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                            <div id="tab-5" class="tab_container" style="margin-top:10px; display:none">
                                                <form class="form-horizontal type_2" id="certification-form" action="" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="company_id" value="<?= $company->id?>"/>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="type_of_certification"><span style="color:red">*</span>Type of Certification</label>
                                                        <div class="col-md-4">
                                                            <select id="type_of_certification" name="certification_type" class="form-control">
                                                                <option value="">Please Select...</option>

                                                                <option value="msc" <?php if($company_certificate->certification_type=="msc"){echo "selected=selected";}?>>Management System Certifications</option>
                                                                <option value="pc/tr" <?php if($company_certificate->certification_type=="pc/tr"){echo "selected=selected";}?>>Product Certifications/Testing Reports</option>

                                                            </select>

                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="certification_reference_no"><span style="color:red">*</span>Reference No.</label>
                                                        <div class="col-md-4">
                                                            <input id="certification_reference_no" name="reference_no" type="text" placeholder="#" class="form-control input-md" value="<?= $company_certificate->reference_no;?>" >

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="cer-certification_name"><span style="color:red">*</span>Name</label>
                                                        <div class="col-md-4">
                                                            <select id="cercertification_namename" name="name" class="form-control">
                                                                <option value="">-Please Select-</option>
                                                                <option value="apic" <?php if($company_certificate->name=="apic"){echo "selected=selected";}?>>API</option>
                                                                <option value="azme" <?php if($company_certificate->name=="azme"){echo "selected=selected";}?>>ASME</option>
                                                                <option value="azo_free" <?php if($company_certificate->name=="azo_free"){echo "selected=selected";}?>>AZO free</option>
                                                                <option value="cb" <?php if($company_certificate->name=="cb"){echo "selected=selected";}?>>CB</option>
                                                                <option value="ccc" <?php if($company_certificate->name=="ccc"){echo "selected=selected";}?>>CCC</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="certification_issued_by"><span style="color:red">*</span>Issued By</label>
                                                        <div class="col-md-4">
                                                            <select id="certification_issued_by" name="issued_by" class="form-control">
                                                                <option value="">-Please Select-</option>
                                                                <option value="apic" <?php if($company_certificate->issued_by=="apic"){echo "selected=selected";}?>>API</option>
                                                                <option value="azme" <?php if($company_certificate->issued_by=="azme"){echo "selected=selected";}?>>ASME</option>
                                                                <option value="azo_free" <?php if($company_certificate->issued_by=="azo_free"){echo "selected=selected";}?>>AZO free</option>
                                                                <option value="cb" <?php if($company_certificate->issued_byname=="cb"){echo "selected=selected";}?>>CB</option>
                                                                <option value="ccc" <?php if($company_certificate->issued_by=="ccc"){echo "selected=selected";}?>>CCC</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-sm-2 control-label" for="certification_starting_date"><span style="color:red">*</span>Starting Date</label>
                                                        <div class="col-md-3">

                                                            <div class="input-group date"  data-date="" data-date-format="yyyy-mm-dd">
                                                                <input class="form-control" type="text" id="certification_starting_date" value="YYYY-MM-DD" name="starting_date"/>
                                                                <span class="input-group-addon add-on" style="cursor: pointer;"> <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                    
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-sm-2 control-label" for="certification_ending_date">Ending Date</label>
                                                        <div class="col-md-3">

                                                            <div class="input-group date"  data-date="" data-date-format="yyyy-mm-dd">
                                                                <input class="form-control" type="text" id="certification_ending_date" value="YYYY-MM-DD" name="ending_date"/>
                                                                <span class="input-group-addon add-on" style="cursor: pointer;"> <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                                            </div>

                                                        </div>

                                                    </div>
                                                    
                                                    
                                                    
                                                    

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="certification_image_url">Image</label>
                                                        <div class="col-md-4">
                                                            <input id="certification_image_url" name="certification_image_url" type="file" /><br>
                                                            <img id="image_certificate_preview" src="http://placehold.it/100x100" alt="your image" width="70" height="70"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="certification_scope"><span style="color:red">*</span>Scope</label>
                                                        <div class="col-md-6">
                                                            <textarea rows="5" class="form-control" name="scope" id="certification_scope"><?= $company_certificate->scope?></textarea>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                            <div class="dash-line"></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-9" style="margin-top: 15px;">
                                                                <input type="submit" class="button_blue"  name="add_company_certification" value="Submit">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <h4>Previous Certificates</h4>
                                                <table class="table_type_1" cellpadding="0" cellspacing="0" border="0" class="display" id="table3" style="clear: both">
                                                    <thead>
                                                        <tr>
                                                            <th>Certification Type</th>
                                                            <th>Name</th>
                                                            <th>Issued By</th>
                                                            <th>Image</th>
                                                            <th>Scope</th>
                                                            <th >Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $count = 0;
                                                        foreach ($company_certificate as $certificate) {
                                                            //                                                   print_r($pending_products);exit();
                                                            ?>
                                                            <tr>
                                                                <td data-title="Products Name"><?= $certificate->certification_type ?></td>
                                                                <td data-title="Model" ><?= $certificate->name ?></td>
                                                                <td data-title="Owner" ><?= $certificate->issued_by ?></td>
                                                                <td data-title="Status"><?php if(!empty($certificate->image_url)){?><img src="<?= $certificate->image_url?>" height="70" width="50"/><?php }else{echo '<img src="'.base_url('images/no_image.jpg').'" height="70" width="50"/>';}?></td>
                                                                <td data-title="Last Updated" ><?= $certificate->scope ?></td>
                                                                <td data-title="Action">
                                                                    <div class="btn-group">
                                                                        <a href="<?php echo base_url() ?>index.php/seller/delete_certificate/<?= $certificate->id ?>" class="btn btn-danger btn-xs" title="delete" onclick="return confirm('Are you sure you want to delete this product?')">Delete</a>
                                                                    </div>
                                                                </td>

                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                            <!--/ #tab-5-->
                                            <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                        </div><!--/ .tab_containers_wrap -->
                                        <!-- - - - - - - - - - - - - - End of tabs containers - - - - - - - - - - - - - - - - -->
                                    </div><!--/ .tabs-->
                                    <div style="margin-top:30px; margin-bottom: 30px;"></div>
                                </section>
                                
                                

                    </div><!--/ .animated.transparent-->


                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

            </div><!--/ .row-->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->




    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>

<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_certificate_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#certification_image_url").change(function () {
            readURL(this);
        });
    });
</script>
<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_company_preview').attr('src', e.target.result);
                }


                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#company_photo").change(function () {

            //$('#company-photo-div').height( $("#company-photo-div").height() + 50 );
            //$('#company-photo-div').css("clear","both");
            readURL(this);
        });

    });
</script>
<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_upload_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#company_logo").change(function () {
            readURL(this);
        });
    });
</script>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#table3').dataTable().columnFilter();
        $('#table4').dataTable().columnFilter();
        $('#factories_yes').click(function(){
            $('#partner_factories_yes_div').show(500);
        });
        $('#factories_no').click(function(){
            $('#partner_factories_yes_div').hide(500);
        });
    });
</script>
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.date').datepicker();
    });
</script>