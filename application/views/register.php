<?php include 'header.php'; ?>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container">

        <div class="section_offset">

            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-12 " style="background-color:#FFF;">

                    <div >

                        <div class="col-sm-4 col-md-12 col-lg-12">

                            <form class="form-horizontal" id="register-user-form" action="<?= base_url() ?>index.php/main/register_user/" method="post">

                                <fieldset>

                                    <!-- Text input-->
                                    <br>
                                    <h4 style="margin-left: 70px">Create your Account</h4>
                                    <br>
                                    <?php if (validation_errors()) { ?>
                                        <div class="form-group">
                                            <label class="col-sm-2 col-md-4 control-label" ></label>
                                            <div class="col-sm-2 col-md-4">
                                                <div class="alert alert-error">
                                                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                                                    <strong>Error!</strong> <?= validation_errors(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } 
                                          if ($this->session->flashdata('captcha_error')) 
                                          {?>
                                            <div class="alert alert-danger" role="alert">
                                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                <span class="sr-only">Error:</span>
                                                <?= $this->session->flashdata('captcha_error'); ?>
                                            </div>
                                    <?php }?>
                                    <div class="form-group">
                                        <label class="col-sm-2 col-md-4 control-label" for="user_email">Email</label>
                                        <div class="col-sm-2 col-md-4">
                                            <input id="user_email" name="user_email" type="email" placeholder="Enter Your Email" class="form-control input-md" required="" value="<?= $form_data['user_email']?>">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-sm-2 col-md-4 control-label" for="create_password">Create Password</label>
                                        <div class="col-sm-2 col-md-4">
                                            <input id="create_password" name="create_password" type="password" placeholder="Enter Strong Pasword" class="form-control input-md" required="" value="<?=$form_data['create_password'];?>">

                                        </div>
                                    </div>

                                    <!-- File Button -->
                                    <div class="form-group">
                                        <label class="col-sm-2 col-md-4 control-label" for="confirm_password">Confirm Password</label>
                                        <div class="col-sm-2 col-md-4">
                                            <input id="confirm_password" name="confirm_password" type="password" placeholder="Re Enter Password" class="form-control input-md" required="" value="<?=$form_data['confirm_password'];?>">
                                        </div>
                                    </div>
                                    <br>
                                    <hr>
                                    <br>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="registration_location">Business Location</label>
                                        <div class="col-md-4">
                                            <select id="registration_location" name="registration_location" class="form-control">
                                                <option value="">---Select Country---</option>
                                                <?php
                                                foreach ($countries as $country) {?>
                                                   <option value="<?=$country->country_id;?>" <?php if($form_data['registration_location'] == $country->country_id){ echo "selected=selected";}?> ><?=$country->country_name;?> </option>
                                                   <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="member_type">I am a</label>
                                        <div class="col-md-6 control-label" >
                                            <label class="radio-inline" style="padding-top: 1px;">
                                                <input checked type="radio" name="member_type" id="supplier"  value="supplier" style=" width: 30px;margin-top:1px" <?php if($form_data['member_type']=="supplier"){echo "checked=checked";}?>>Supplier
                                            </label>
                                            <label class="radio-inline" style="padding-top: 1px;">
                                                <input checked type="radio" name="member_type" id="buyer"  value="buyer" style="width: 30px;margin-top:1px" <?php if($form_data['member_type']=="buyer"){echo "checked=checked";}?>>Buyer
                                            </label>
                                            <label class="radio-inline" style="padding-top: 1px;">
                                                <input checked type="radio" name="member_type" id="both"  value="both" style="width: 30px;margin-top:1px" <?php if($form_data['member_type']=="both"){echo "checked=checked";}?>>Both
                                            </label>
                                        </div>
                                    </div>

                                    <br>
                                    <h4 style="margin-left: 70px">Add Your Contact Information</h4>
                                    <br>

                                    <!-- Search input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="member_name">Contact Name</label>
                                        <div class="col-md-4">
                                            <input id="member_name" name="member_name" type="text" placeholder="Full Name" class="form-control input-md" value="<?= $form_data['member_name'];?>">

                                        </div>
                                    </div>

                                    <!-- Text input-->

                                    <div class="form-group" style="display: block">
                                        <label class="col-md-4 control-label" for="company_name">Company Name</label>
                                        <div class="col-md-4">
                                            <input id="company_name" name="company_name" type="text" placeholder="Complete Company Name" class="form-control input-md" value="<?= $form_data['company_name']?>">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="contact_no">Contact No</label>
                                        <div class="col-md-4">
                                            <input id="contact_no" name="contact_no" type="text" placeholder="Number" class="form-control input-md" <?= $form_data['contact_no']?>>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Captcha</label>
                                        <div class="col-md-8">
                                            <div class="g-recaptcha" data-sitekey="6Lf3JAcTAAAAANdDjN638WHpEksepoYU6a7Lc5fP"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label"></label>
                                        <div class="col-md-8">
                                            <input type="submit" id="submit_register" name="submit_register" value="Create Account" class="btn button_blue">

                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>

                    </div><!--/ .animated.transparent-->

                </div><!--/ [col]-->

            </div><!--/ .row-->

        </div>
    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>