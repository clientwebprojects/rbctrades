<?php
include 'header2.php';
$var = json_encode($all_category);
$varmain = json_encode($category);
// echo '<pre>';
// print_r($category);
// exit();
?>

<script> var image_counter = 0;</script>
<script type="text/javascript">
    var abc = 0;
    $(document).ready(function () {
        $('#add_more').click(function () {
            $(this).before($("<div/>", {id: 'img-div'}).fadeIn('slow').append(
                    $("<input/>", {name: 'product_image[]', type: 'file', id: 'file'})

                    ));
        });
        $('body').on('change', '#file', function () {
//            if (this.files && this.files[0] && image_counter < 4) {
    if (this.files && this.files[0]) {
                abc += 1; //increementing global variable by 1

                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='image" + abc + "' class='img-div'><img id='previewimg" + abc + "' src=''/></div>");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                image_counter++;
                $(this).hide();
                $("#image" + abc).append($("<img/>", {id: 'imgx', src: '<?= base_url() ?>/images/x.png', alt: 'delete'}).click(function () {
                    $(this).parent().parent().remove();
                }));
            }
        });
    });
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    }
</script>
<script src="<?= base_url() ?>js/ckeditor.js"></script>
<script>

    // The instanceReady event is fired, when an instance of CKEditor has finished
    // its initialization.
    CKEDITOR.on('instanceReady', function (ev) {
        // Show the editor name and description in the browser status bar.
        //document.getElementById('eMessage').innerHTML = 'Instance <code>' + ev.editor.name + '<\/code> loaded.';

        // Show this sample buttons.
        //document.getElementById('eButtons').style.display = 'block';
    });

    function InsertHTML() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        var value = document.getElementById('htmlArea').value;

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg')
        {
            // Insert HTML code.
            // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-insertHtml
            editor.insertHtml(value);
        }
        else
            alert('You must be in WYSIWYG mode!');
    }

    function InsertText() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        var value = document.getElementById('txtArea').value;

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg')
        {
            // Insert as plain text.
            // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-insertText
            editor.insertText(value);
        }
        else
            alert('You must be in WYSIWYG mode!');
    }

    function SetContents() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        var value = document.getElementById('htmlArea').value;

        // Set editor contents (replace current contents).
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-setData
        editor.setData(value);
    }

    function GetContents() {
        // Get the editor instance that you want to interact with.
        var editor = CKEDITOR.instances.editor1;

        // Get editor contents
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-getData
        alert(editor.getData());
    }

    function ExecuteCommand(commandName) {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg')
        {
            // Execute the command.
            // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-execCommand
            editor.execCommand(commandName);
        }
        else
            alert('You must be in WYSIWYG mode!');
    }

    function CheckDirty() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        // Checks whether the current editor contents present changes when compared
        // to the contents loaded into the editor at startup
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-checkDirty
        alert(editor.checkDirty());
    }

    function ResetDirty() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        // Resets the "dirty state" of the editor (see CheckDirty())
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-resetDirty
        editor.resetDirty();
        alert('The "IsDirty" status has been reset');
    }

    function Focus() {
        CKEDITOR.instances.editor1.focus();
    }

    function onFocus() {
        //document.getElementById('eMessage').innerHTML = '<b>' + this.name + ' is focused </b>';
    }

    function onBlur() {
        //document.getElementById('eMessage').innerHTML = this.name + ' lost focus';
    }

</script>
<script>
    var sub_category_object = <?= $var; ?>;
    var category_object = <?= $varmain; ?>;
    var count = 0;
    var catId;
    var catName;
    //console.log(sub_category_object);

</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container">

        <div class="section_offset">

            <div class="row">

                <div class="col-md-12" style="padding-top: 20px;">
                    <?php include 'inc/seller_product_menu.php'; ?>

                    <script>
                        document.getElementById("manage_product").className = "active";
                        document.getElementById("seller_products").className = "active-seller-menu";
                    </script>
                    <div>

                        <div class="col-md-10">


                            <!-- - - - - - - - - - - - - - Revolution slider - - - - - - - - - - - - - - - - -->
                            <form id="edit_product_form" class="form-horizontal" method="post"  enctype="multipart/form-data">

                                <fieldset>
                                    <legend>Edit Product</legend>
                                    <div id="successmessage"></div>


                                    <h4 class="heading">Basic Information <span class="small-desc">Besides helping buyers clearly understand your listing, this information will also influence your product ranking in search results.</span></h4>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_code">Current Category</label>
                                        <div class="col-md-6">
                                            <input   type="text" disabled="readonly" class="form-control input-md" value="<?= $product->category_name; ?>">

                                        </div>
                                    </div>
                                    <input type="hidden" id="newparentid" name="product_category" value="<?= $product->product_category; ?>">

                                      <div style="background-color: #f5f5f5;padding: 20px 0 5px;border: 1px solid #bebebe;border-radius: 3px;margin-bottom: 14px;">
                                        <div class="form-group" id="catSelect">
                                            <label class="col-md-2 control-label">Select Category</label>
                                            <div class="col-md-4">
                                                <select id="0" name="select" class="form-control" size="10" onchange="catFunction(this.value, this.id)" >
                                                    <?php foreach ($category as $values) { ?>
                                                        <option value="<?php echo $values->category_id; ?>" name="<?php echo $values->category_name; ?>"><?php echo $values->category_name; ?></option>

<?php } ?>
                                                </select>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_name">Product Name</label>
                                        <div class="col-md-6">
                                            <input id="product_name" name="product_name" type="text" placeholder="" class="form-control input-md" value="<?= $product->product_name; ?>"  >

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_keyword">Product Keyword</label>
                                        <div class="col-md-6">
                                            <input id="product_keyword" name="product_keyword" type="text" value="<?= $product->product_keyword; ?>" class="form-control input-md"   >

                                        </div>
                                    </div>

                                   <div class="form-group">
                                        <label class="col-md-2 control-label">Product Image</label>
                                        <div class="col-md-6">
                                            <span class="add_more" id="add_more"><i class="btn btn-default">Add more images</i></span><br/>
                                            <span class="font12 color-999" style="width: 100%;display: inline-block;">Note: First image is the product's main image.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="filebutton"></label>
                                        <div class="col-md-10">
                                            <div class="thumbnails_carousel">
                                                <?php
                                                if($images != ''){
                                                foreach ($images as $product_images) {
                                                    ?>
                                                <img class="default-image" id="image_<?= $product_images->image_id; ?>" src="<?= $product_images->image_url; ?>" /><img src="<?= base_url() ?>/images/x.png" id="<?= $product_images->image_id; ?>" onclick="delete_image(this.id)" class="del_image_<?= $product_images->image_id; ?> del_image">

                                                    <?php
                                                }
                                                }  else {


                                                ?>
                                                <img class="default-image" src="<?= base_url() ?>images/default.png"/>
                                                <img class="default-image" src="<?= base_url() ?>images/default.png"/>
                                                <img class="default-image" src="<?= base_url() ?>images/default.png"/>
                                                <img class="default-image" src="<?= base_url() ?>images/default.png"/>

                                                <?php
                                                }
                                                ?>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="dash-line"></div>
                                    <h4 class="heading">Product Details <span class="small-desc">Complete product details help your listing gain more exposure and visibility to potential buyers.</span></h4>
                                    <br>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_model">Model Number</label>
                                        <div class="col-md-4">
                                            <input id="model_number" name="product_model" type="text" placeholder="" value="<?= $product->product_model; ?>" class="form-control input-md">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="product_code">Product Code</label>
                                        <div class="col-md-4">
                                            <input id="product_code" name="product_code" type="text" placeholder="" value="<?= $product->product_code; ?>" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="brand_id">Brand Id</label>
                                        <div class="col-md-4">
                                            <input id="brand_id" name="brand_id" type="text" placeholder="" value="<?= $product->brand_id; ?>" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Select Basic -->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="place_of_origin">Place of Origin</label>
                                        <div class="col-md-4">
                                            <select id="place_of_origin" name="product_origin" class="form-control">
                                                <option value="">Select Country</option>
                                                <?php foreach ($countries as $country) { ?>
                                                    <option value="<?= $country->country_id; ?>" <?php if ($country->country_id == $product->product_origin) echo 'selected="selected"'; ?>><?= $country->country_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="uom">UOM</label>
                                        <div class="col-md-4">
                                            <select id="uom" value="" name="uom_id" class="form-control">
                                                <option value="">Select Unit</option>
                                                <?php foreach ($uom as $unit_of_m) { ?>
                                                    <option value="<?= $unit_of_m->uom_id; ?>" <?php if ($unit_of_m->uom_id == $product->uom_id) echo 'selected="selected"'; ?>><?= $unit_of_m->uom_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Password input-->
                                    <div class="ex_details">
                                                <div class="form-group">
                                                    
                                                    <label class="col-md-2 control-label" for="pakagin_details">More Details</label>
                                                    <div class="col-xs-7">
                                                        <span class="small-desc">For Better customer respond, provide extra details below.</span>
                                                    </div>
                                                </div>
                                        <?php for($i=0;$i<sizeof($extra_detail);$i++){
                                            ?>
                                                <div class="form-group" id="formgroup_<?= $extra_detail[$i]->id;?>">
                                                    <input type="hidden" name="extra_detail_id[]" value="<?= $extra_detail[$i]->id;?>" />
                                                    <label class="col-md-2 control-label" for="pakagin_details"></label>
                                                    <div class="col-xs-3">
                                                        <input type="text" class="form-control" name="extra_details_attributes[]" placeholder="Attribute Name"  value="<?= $extra_detail[$i]->attribute;?>"/>
                                                    </div>
                                                    <div class="col-xs-3">
                                                        <input type="text" class="form-control" name="extra_details_values[]" placeholder="Value" value="<?= $extra_detail[$i]->value;?>" />
                                                    </div>

                                                    <div class="col-xs-1">
                                                        <button type="button" class="btn btn-default remove" style="font-size: 9px;" id="<?= $extra_detail[$i]->id;?>">Remove</button>
                                                    </div>
                                                </div>
                                                
                                            <?php }?>
                                            
                                            <div class="form-group more">
                                                <label class="col-md-2 control-label" for="pakagin_details"></label>
                                               
                                                <div class="col-xs-3">
                                                    <button type="button" class="btn btn-default addButton" style="font-size: 9px;" id="<?= $extra_detail[$i]->id;?>">Add More</button>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="dash-line"></div>
                                    <h4 class="heading">Trade Information <span class="small-desc">Complete trade information helps buyers make better sourcing decisions.</span></h4>
                                    <br>
                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="minimum_order_quantity">Minimum Order Quantity</label>
                                        <div class="col-md-4">
                                            <input id="minimum_order_quantity" name="product_minimun_order" value="<?= $product->product_minimun_order; ?>" type="text" placeholder="" class="form-control input-md">

                                        </div>
                                        <div class="col-md-2">
                                            <select value="" name="minimun_order_unit" class="form-control">
                                                <option value="">Select Unit</option>
                                                <?php foreach ($uom as $unit_of_m) { ?>
                                                    <option value="<?= $unit_of_m->uom_id; ?>" <?php if ($unit_of_m->uom_id == $product->minimun_order_unit) echo 'selected="selected"'; ?>><?= $unit_of_m->uom_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Search input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">FOB Price</label>
                                        <div class="col-md-2">
                                            <select value="" name="product_fob_currency" class="form-control">
                                                <option value="">Currency</option>
                                                <?php foreach ($currency as $curr) { ?>
                                                    <option value="<?= $curr->id ?>" <?php if ($curr->currency_id == $product->product_fob_currency) echo 'selected="selected"'; ?>><?= $curr->currency_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input name="product_fob_price_from" type="text" placeholder="" value="<?= $product->product_fob_price_from; ?>" class="form-control input-md">
                                        </div>
                                        <div class="col-md-2">
                                            <input name="product_fob_price_to" type="text" placeholder="" value="<?= $product->product_fob_price_to; ?>" class="form-control input-md">
                                        </div>
                                        <div class="col-md-2">
                                            <select value="" name="product_fob_unit" class="form-control">
                                                <option value="">Select Unit</option>
                                                <?php foreach ($uom as $unit_of_m) { ?>
                                                    <option value="<?= $unit_of_m->uom_id; ?>" <?php if ($unit_of_m->uom_id == $product->product_fob_unit) echo 'selected="selected"'; ?>><?= $unit_of_m->uom_name; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Password input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="port">Port</label>
                                        <div class="col-md-6">
                                            <input id="port" name="product_port" type="text" placeholder="" value="<?= $product->product_port; ?>" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Multiple Checkboxes (inline) -->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="payment_terms">Payment Terms</label>

                                        <div class="col-md-10" >

                                            <?php
                                            $paymentType = $product->product_payment_type;
                                            $focus = explode('|', $paymentType);
                                            foreach ($payment_types as $payment_type_val) {
                                                ?>
                                                <label class="checkbox-inline" for="">
                                                    <input type="checkbox" name="product_payment_type[]" value="<?= $payment_type_val->p_term_id; ?>" <?php if(in_array("$payment_type_val->p_term_id",$focus)) { ?> checked="checked" <?php } ?>  style=" width: 30px">
                                                    <?= $payment_type_val->p_term_description; ?>
                                                </label>

                                                <?php
                                            }
                                            ?>

                                        </div>

                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="supply_ability">Supply Ability</label>
                                        <div class="col-md-6">
                                            <input id="supply_ability" name="product_supply_ability" type="text" value="<?= $product->product_supply_ability; ?>" placeholder="Quantity in pieces" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- Text input-->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="delievery_time">Delivery Time</label>
                                        <div class="col-md-4">
                                            <input id="delievery_time" name="product_delivery_time" value="<?= $product->product_delivery_time; ?>" type="text" placeholder="In Days" class="form-control input-md">

                                        </div>
                                        <div class="col-md-2">
                                            <select name="product_delivery_unit" class="form-control">
                                                <option value="">Select Unit</option>
                                                <?php foreach ($uom as $unit_of_m) { ?>
                                                    <option value="<?= $unit_of_m->uom_id; ?>" <?php if ($unit_of_m->uom_id == $product->product_delivery_unit) echo 'selected="selected"'; ?>><?= $unit_of_m->uom_name; ?></option>
<?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="col-md-2 control-label" for="pakagin_details">Packaging Details</label>
                                        <div class="col-md-6">
                                            <textarea class="form-control" id="pakagin_details" rows="5" name="packaging_detail"><?= $product->packaging_detail; ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="dash-line"></div>
                                    <h4 class="heading">Detailed Description <span class="small-desc">For the best visual effects, we recommend keeping the image within 750px(width)*800px(height) and table width within 750px.</span></h4>
                                    <br>
                                    <!-- Textarea -->
                                    <div class="form-group">

                                        <div class="col-md-12" id="eButtons">
                                            <textarea cols="100" id="editor1" rows="10" name="product_detailed_description"><?= $product->product_detailed_description; ?></textarea>

                                            <script>
                                                // Replace the <textarea id="editor1"> with an CKEditor instance.
                                                CKEDITOR.replace('editor1', {
                                                    on: {
                                                        focus: onFocus,
                                                        blur: onBlur,
                                                        // Check for availability of corresponding plugins.
                                                        pluginsLoaded: function (evt) {
                                                            var doc = CKEDITOR.document, ed = evt.editor;
                                                            if (!ed.getCommand('bold'))
                                                                doc.getById('exec-bold').hide();
                                                            if (!ed.getCommand('link'))
                                                                doc.getById('exec-link').hide();
                                                        }
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>

                                    <!-- Button (Double) -->
                                    <div class="form-group center" style="clear:left;clear: right">
                                        <label class="col-md-6 control-label" for="update"></label>
                                        <div class="col-md-8">
                                            <input type="submit" id="update" name="update" class="btn button_blue" value="update" />
                                        </div>
                                    </div>
                                </fieldset>
                                <!-- Button -->
                            </form>

                         </div>
                        <!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->

                    </div><!--/ .animated.transparent-->

                </div><!--/ [col]-->
            </div>

        </div>

    </div>

</div>

<?php include 'footer.php'; ?>
<script>
    function catFunction(panelid, id) {

        $("#loader").show();
        setTimeout(function () {
            hasSub = 0;
            catId = panelid;
            document.getElementById('newparentid').setAttribute('value', catId);
//        console.log(catId);
            if (id === '0') {
                $.each(category_object, function (i, v) {
                    if (parseInt(v.category_id) === parseInt(panelid)) {

                        catName = v.category_name;
                    }

                });

            }
            else {
                $.each(sub_category_object, function (i, v) {


                    if (parseInt(v.category_id) === parseInt(panelid)) {

                        catName = v.category_name;
                    }

                });
            }
            if (count > id) {
                i = count;
                while (i > id) {
                    $('#main' + i).remove();
                    i--;
                }
                count = id;

            }
            count++;

            $.each(sub_category_object, function (i, v) {

                if (parseInt(v.parent_id) === parseInt(panelid)) {
                    hasSub++;
                }

            });

            var start_html =
                    '<div class="col-md-4" id="main' + count + '"> ' +
                    '<select id="' + count + '" name="select' + count + '" class="form-control" size="10" onchange="catFunction(this.value,this.id)" >';
            var end_html =
                    '</select>' +
                    '</div>';
            console.log(hasSub);
            if (hasSub > 0) {
                $('#catSelect').append(start_html);

                $.each(sub_category_object, function (i, v) {

                    if (parseInt(v.parent_id) === parseInt(panelid)) {

                        var subCategory_html =
                                '<option value="' + v.category_id + '">' +
                                v.category_name +
                                '</option>';

                        $('#' + count).append(subCategory_html);

                    }

                });
                $('#catSelect').append(end_html);
            }
            $("#loader").hide();
        }, 500)
    }
</script>
<script type="text/javascript">

    function delete_image(id) {
        var image_id = "image_"+id;
        console.log(image_id);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('index.php/seller/image_delete'); ?>", //Relative or absolute path to response.php file
            data: {'image_id': id},
            //dataType: 'json',
            success: function () {


                $("#image_"+id).remove();
                $(".del_image_"+id).remove();

            }
        });
    }


</script>
<script>
    $(document).ready(function() {
        
        var new_row = '<div class="form-group">'+
                            '<label class="col-md-2 control-label" for="pakagin_details"></label>'+
                            '<div class="col-xs-3">'+
                                '<input type="text" class="form-control" name="add_extra_details_attributes[]" placeholder="Attribute Name" />'+
                            '</div>'+
                            '<div class="col-xs-3">'+
                                '<input type="text" class="form-control" name="add_extra_details_values[]" placeholder="Value" />'+
                            '</div>'+

                            '<div class="col-xs-1">'+
                                '<button type="button" class="btn btn-default remove" style="font-size: 9px;">Remove</button>'+
                            '</div>'+
                        '</div>';
        $('.addButton').click(function() {
                $('.more').before(new_row);
            });
            $('.ex_details').on('click','.remove',function() {
                    var id = $(this).attr("id");
                    var el = $(this).parent().parent();
                    if(typeof id === 'undefined'){
                        $(el).remove();
                    }
                    else
                    {    
                        
                        console.log(id);
                        $.ajax({
                            url: '<?= base_url('index.php/seller/delete_product_extra_detail'); ?>',
                            type: 'POST',
                            data: {'id': id}, // An object with the key 'submit' and value 'true;
                            success: function (result) {
                                if(result == "1"){
                                    //$("formgroup_"+id).hide();
                                    $(el).remove();
                                    console.log($(this).parent().parent());
                                    console.log("aaas");
                                }
                            }
                        });
                    }
            });
            
    });
</script>