<?php
include 'header.php';
//echo '<pre>';
//print_r($products);exit();
//echo '</pre>';
?>
<script type="text/javascript">
    $(document).ready(function(e) {
        if (window.location.hash == '#_=_') {
            window.location.hash = ''; // for older browsers, leaves a # behind
            history.pushState('', document.title, window.location.pathname); // nice and clean
            e.preventDefault(); // no page reload
        }
    });
</script>

<div class="page_wrapper type_2">
    <div class="container">
        <div class="section_offset">
            <div class="row">
                <aside class="col-md-3 has_mega_menu">
                    <section>

                        <?php include 'main_menu.php'; ?>
                    </section>

                </aside>

                <?php include 'home_slider.php'; ?>

                <div class="col-md-2 membership-area">
                    <h3 style="margin: 20px 0 35px 20px;font-size: 24px;font-weight: normal">Exclusive<br/>Membership</h3>
                    <a href="#" class="banner">
                        <img src="<?= base_url() ?>images/mem1.jpg" alt="">
                    </a>
                    <a href="#" class="banner">
                        <img src="<?= base_url() ?>images/mem2.jpg" alt="">
                    </a>
                    <a href="#" class="banner">
                        <img src="<?= base_url() ?>images/mem3.jpg" alt="">
                    </a>

                </div>

            </div>

        </div>
    </div>
</div>
<div class="page_wrapper type_2" style="background: #f5f5f5">

    <div class="container">

        <div class="section_offset">

            <div class="row">

                <div class="col-md-3 col-sm-6">

                    <section class="infoblock type_2">

                        <i class="icon-thumbs-up-1"></i>

                        <h4 class="caption"><b>The Highest Product Quality</b></h4>

                        <p>Make a Single inquiry, get multiple quotations. Get fastest response.</p>

                        <a href="#" class="button_dark_grey middle_btn">Read More</a>

                    </section><!--/ .infoblock.type_2-->

                </div><!--/ [col]-->

                <div class="col-md-3 col-sm-6">

                    <section class="infoblock type_2">

                        <i class="icon-lock"></i>

                        <h4 class="caption"><b>Secure Transactions</b></h4>

                        <p>Secure Online Transactions
                            Buy It Now for Fast Dispatch</p>

                        <a href="#" class="button_dark_grey middle_btn">Read More</a>

                    </section><!--/ .infoblock.type_2-->

                </div><!--/ [col]-->

                <div class="col-md-3 col-sm-6">

                    <section class="infoblock type_2">

                        <i class="icon-user-8"></i>

                        <h4 class="caption"><b>Select Verified Suppliers</b></h4>

                        <p>Get products from verified suppliers from around the world.</p>

                        <a href="#" class="button_dark_grey middle_btn">Read More</a>

                    </section><!--/ .infoblock.type_2-->

                </div><!--/ [col]-->

                <div class="col-md-3 col-sm-6">

                    <section class="infoblock type_2">

                        <i class="icon-heart-3"></i>

                        <h4 class="caption"><b>Trade <br/>Services</b></h4>

                        <p>Display your verification status
                            Faster responses from suppliers.</p>

                        <a href="#" class="button_dark_grey middle_btn">Read More</a>

                    </section><!--/ .infoblock.type_2-->

                </div><!--/ [col]-->

            </div><!--/ .row-->

        </div><!--/ .section_offset-->

        <section class="section_offset">

            <h3 class="heading">Today's Deals</h3>

            <div class="tabs type_2 products">

                <!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->

                <ul class="tabs_nav clearfix">

                    <li><a href="#tab-1">Featured Wholesale Products</a></li>
                    <li><a href="#tab-2">Hot Items</a></li>
                    <li><a href="#tab-3">New Deals</a></li>
                </ul>
                <div class="tab_containers_wrap">

                    <div id="tab-1" class="tab_container">

                        <div class="owl_carousel carousel_in_tabs type_2">
                            <?php foreach ($products as $product) { ?>
                                <div class="product_item">
                                    <a href="<?= base_url('index.php/buyer/product_detail/'.$product->product_id.'')?>" class="image_wrap">
                                        <?php
                                        if (!empty($product->images_url)) {
                                            echo "<img src='" . $product->images_url[0]->image_url . "' alt='' >";
                                        } else {
                                            echo "<img src='" . base_url() . "images/no_image.jpg'>";
                                        }
                                        ?>
                                    </a>
                                    <div class="description">

                                        <div class="clearfix product_info">
                                            <a class="font14 alignleft" href="<?= base_url('index.php/buyer/product_detail/'.$product->product_id.'')?>"><?php if (strlen($product->product_name) > 36) {
                                        echo substr($product->product_name, 0, 36) . '...';
                                    } else {
                                        echo $product->product_name;
                                    } ?></a><br/>
                                            <p class="font12 alignleft"><b><?= $product->product_minimun_order ?> </b> <?= $product->uom_name ?> <span class="color-999">(Min. Order)</span></p>
                                        </div>
                                    </div>
                                </div>
<?php } ?>

                        </div>

                    </div>

                    <div id="tab-2" class="tab_container">

                        <div class="owl_carousel carousel_in_tabs type_2">
                                    <?php foreach ($products as $product) { ?>
                                <div class="product_item">
                                    <a href="<?= base_url('index.php/buyer/product_detail/'.$product->product_id.'')?>" class="image_wrap">
                                        <?php
                                        if (!empty($product->images_url)) {
                                            echo "<img src='" . $product->images_url[0]->image_url . "' alt='' >";
                                        } else {
                                            echo "<img src='" . base_url() . "images/no_image.jpg'>";
                                        }
                                        ?>
                                    </a>
                                    <div class="description">

                                        <div class="clearfix product_info">
                                            <a class="font14 alignleft" href="<?= base_url('index.php/buyer/product_detail/'.$product->product_id.'')?>"><?php if (strlen($product->product_name) > 36) {
                                            echo substr($product->product_name, 0, 36) . '...';
                                        } else {
                                            echo $product->product_name;
                                        } ?></a><br/>
                                            <p class="font12 alignleft"><b><?= $product->product_minimun_order ?> </b> <?= $product->uom_name ?> <span class="color-999">(Min. Order)</span></p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>

                    <div id="tab-3" class="tab_container">

                        <div class="owl_carousel carousel_in_tabs type_2">
                                    <?php foreach ($products as $product) { ?>
                                <div class="product_item">
                                    <a href="<?= base_url('index.php/buyer/product_detail/'.$product->product_id.'')?>" class="image_wrap">
    <?php
    if (!empty($product->images_url)) {
        echo "<img src='" . $product->images_url[0]->image_url . "' alt='' >";
    } else {
        echo "<img src='" . base_url() . "images/no_image.jpg'>";
    }
    ?>
                                    </a>
                                    <div class="description">

                                        <div class="clearfix product_info">
                                            <a class="font14 alignleft" href="<?= base_url('index.php/buyer/product_detail/'.$product->product_id.'')?>">
                                                <?php if (strlen($product->product_name) > 36) {
        echo substr($product->product_name, 0, 36) . '...';
    } else {
        echo $product->product_name;
    } ?>
                                            </a><br/>
                                            <p class="font12 alignleft"><b><?= $product->product_minimun_order ?> </b> <?= $product->uom_name ?> <span class="color-999">(Min. Order)</span></p>
                                        </div>
                                    </div>
                                </div>
<?php } ?>

                        </div>
                    </div>


                </div>

            </div>

        </section><!--/ .section_offset-->

        <div class="section_offset">

            <div class="row">

                <div class="col-sm-4">

                    <div>

                        <!-- - - - - - - - - - - - - - Banner - - - - - - - - - - - - - - - - -->

                        <a href="#" class="banner">
                            <img src="<?= base_url() ?>images/banner_img_3.jpg" alt="">
                        </a>

                        <!-- - - - - - - - - - - - - - End banner - - - - - - - - - - - - - - - - -->

                    </div><!--/ .animated.transparent-->

                </div><!--/ [col]-->

                <div class="col-sm-4">

                    <div>
                        <a href="#" class="banner">
                            <img src="<?= base_url() ?>images/banner_img_10.jpg" alt="">
                        </a>

                        <!-- - - - - - - - - - - - - - End banner - - - - - - - - - - - - - - - - -->

                    </div><!--/ .animated.transparent-->

                </div><!--/ [col]-->

                <div class="col-sm-4">

                    <div>

                        <!-- - - - - - - - - - - - - - Banner - - - - - - - - - - - - - - - - -->

                        <a href="#" class="banner">
                            <img src="<?= base_url() ?>images/banner_img_6.jpg" alt="">
                        </a>

                        <!-- - - - - - - - - - - - - - End banner - - - - - - - - - - - - - - - - -->

                    </div><!--/ .animated.transparent-->

                </div><!--/ [col]-->

            </div><!--/ .row-->

        </div>

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>
