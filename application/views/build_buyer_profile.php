<?php include 'header2.php'; ?>

<div class="page_wrapper type_2" >
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                <?php include 'inc/seller_account_menu.php'; ?>
                
                <div class="col-md-10">

                    <form id="build_buyer_profile" class="form-horizontal" method="post" action="<?= base_url('index.php/buyer/build_buyer_profile');?>" name="my_proflie_form" enctype="multipart/form-data">

                        <h4 class="heading" style="background: #f5f5f5;padding-left: 10px;">Buyer Information </h4>

                        <fieldset style="border: 1px dotted #bebebe;border-radius: 3px;padding: 10px">
                            <div id="company_name" class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Company Name</label>
                                <div class="col-md-4">
                                    <input  name="company_name" type="text" class="form-control input-md" value="<?= $company->company_name?>">
                                </div>
                            </div>
                            
                        </fieldset>
                        <br/>
                        
                        
                        <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="dash-line"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9" style="margin-top: 15px;">
                                        <input type="submit" class="button_blue" id="submit_company_information" name="build_buyer_profile" value="Submit">
                                    </div>
                                    </div>
                        </div>
                    </form>
                </div>
            </div><!--/ [col]-->
            <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
        </div><!--/ .row-->
        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->
<script>
    $("#business_1").click(function() {
        $('#job_title').show(500);
        $('#company_name').show();
        $('#business_type').show();
        $('#company_information').show();
    });
    $("#business_2").click(function() {
        $('#job_title').hide(500);
        $('#company_name').hide();
        $('#business_type').hide();
        $('#company_information').hide();
    });
    $("#website_yes").click(function() {
        $('#website_field').show(500);
    });
    $("#website_no").click(function() {
        $('#website_field').hide(500);
    });
</script>
<?php include 'footer.php'; ?>