<?php include 'header.php'; ?>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container">

        <div class="section_offset">

            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px;">

                    <div >

                        <!-- - - - - - - - - - - - - - Revolution slider - - - - - - - - - - - - - - - - -->
                        <div style="text-align: center"> <h1>Verification Information</h1></div>
                        <!-- - - - - - - - - - - - - - Revolution slider - - - - - - - - - - - - - - - - -->

                        <form class="form-horizontal">
                            <fieldset>

                                <!-- Form Name -->
                                <legend>Personal Information</legend>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfFirstNameTextField">First Name</label>
                                    <div class="col-md-4">
                                        <input id="mrfFirstNameTextField" name="mrfFirstNameTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfLastNameTextField">Last Name</label>
                                    <div class="col-md-4">
                                        <input id="mrfLastNameTextField" name="mrfLastNameTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfJobTitleTextField">Job Title</label>
                                    <div class="col-md-4">
                                        <input id="mrfJobTitleTextField" name="mrfJobTitleTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfPhoneNumberTextField">Phone Number</label>
                                    <div class="col-md-4">
                                        <input id="mrfPhoneNumberTextField" name="mrfPhoneNumberTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>



                                <!-- Form Name -->
                                <legend>Company Information</legend>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfFreeTaxNoTextField">NTN/Free Tax Number:</label>
                                    <div class="col-md-4">
                                        <input id="mrfFreeTaxNoTextField" name="mrfFreeTaxNoTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfSaleTaxRegistrationNoTextField">Sale Tax Registration No.(STRN):</label>
                                    <div class="col-md-4">
                                        <input id="mrfSaleTaxRegistrationNoTextField" name="mrfSaleTaxRegistrationNoTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfPTCLAccountIdTextField">PTCL Account ID:</label>
                                    <div class="col-md-4">
                                        <input id="mrfPTCLAccountIdTextField" name="mrfPTCLAccountIdTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfRegisteredCompanyNameTextField">Registered Company Name:</label>
                                    <div class="col-md-4">
                                        <input id="mrfRegisteredCompanyNameTextField" name="mrfRegisteredCompanyNameTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <div style="text-align: center"> <h3>Company Address</h3></div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfCompanyStreetTextField">Street:</label>
                                    <div class="col-md-4">
                                        <input id="mrfCompanyStreetTextField" name="mrfCompanyStreetTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfCompanyCityTextField">City:</label>
                                    <div class="col-md-4">
                                        <input id="mrfCompanyCityTextField" name="mrfCompanyCityTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfCompanyStateTextField">Province/State/County:</label>
                                    <div class="col-md-4">
                                        <input id="mrfCompanyStateTextField" name="mrfCompanyStateTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfCompanyZipCodeTextField">Zip/Postal Code:</label>
                                    <div class="col-md-4">
                                        <input id="mrfCompanyZipCodeTextField" name="mrfCompanyZipCodeTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mrfCompanyTelephoneNoTextField">Company Telephone Number:</label>
                                    <div class="col-md-4">
                                        <input id="mrfCompanyTelephoneNoTextField" style="display: block" name="mrfCompanyTelephoneNoTextField" type="text" placeholder="" class="form-control input-md">

                                    </div>
                                </div>
                                <div>
                               <label class="col-md-2"></label>
                                <div>

                                <input  type="checkbox" name="" id="mrfConfirmCheckInput">
                                <label for="mrfConfirmCheckInput">I Confirm That</label><br>
                                 <label class="col-md-2"></label>
                                 <div><span class="glyphicon glyphicon-asterisk"></span>The information provided is true and accurate.</div>

                                  <label class="col-md-2"></label>
                                 <div>
                                     <span class="glyphicon glyphicon-asterisk"></span>
                                                I am authorized to represent my company to submit the above information for verification.</div>
                                </div>
                                </div>
                                <div class="form-group">

                                    <div class="col-md-12">
                                        <button id="submit" name="submit" class="button_blue huge_btn">Submit</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>





                        <!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->

                    </div><!--/ .animated.transparent-->

                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

            </div><!--/ .row-->

            <section class="section_offset" style="margin-top: 30px;">

                <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->

                <ul class="infoblocks_wrap section_offset five_items">

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-thumbs-up-1"></i>
                            <span class="caption"><b>The Highest Product Quality</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-paper-plane"></i>
                            <span class="caption"><b>Fast &amp; Free Delivery</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-lock"></i>
                            <span class="caption"><b>Safe &amp; Secure Payment</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-diamond"></i>
                            <span class="caption"><b>Get 10% OFF For Reorder</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-money"></i>
                            <span class="caption"><b>100% Money back Guaranted</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                </ul><!--/ .infoblocks_wrap.section_offset.clearfix-->

                <!-- - - - - - - - - - - - - - End of infoblocks - - - - - - - - - - - - - - - - -->

            </section><!--/ .section_offset -->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->




    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>