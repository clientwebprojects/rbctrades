<?php include 'header.php'; ?>
<script type="text/javascript">
    $(document).ready(function(e){
    if (window.location.hash == '#_=_') {
        window.location.hash = ''; // for older browsers, leaves a # behind
        history.pushState('', document.title, window.location.pathname); // nice and clean
        e.preventDefault(); // no page reload
    }
    });
</script>
<div class="page_wrapper type_2" >
    <div class="container" style="background-color:#FFF;padding:40px 20px;">


        <div class="section_offset">

            <div class="row">
                <div class="col-sm-5">
                    <form class="form-horizontal" action="<?= base_url('index.php/login/check/'); ?>" method="post">
                        <fieldset>

                            <!-- Form Name -->
                            <legend>Already Have Account ?</legend>
                            <hr style="margin-bottom:20px;margin-top:20px;">
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="user_email">Email</label>
                                <div class="col-md-8">
                                    <input id="user_email" name="user_email" type="text" placeholder="Email Address or Member Id" class="form-control input-md" required="" value="<?= $user_email; ?>">

                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password">Password</label>
                                <div class="col-md-8">
                                    <input id="password" name="user_password" type="password" placeholder="Password" class="form-control input-md" required="">

                                </div>
                            </div>
                            <?php if ($this->session->userdata('captcha') == true) { ?>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Captcha</label>
                                    <div class="col-md-8">
                                        <div class="g-recaptcha" data-sitekey="6Lf3JAcTAAAAANdDjN638WHpEksepoYU6a7Lc5fP"></div>

                                    </div>
                                </div>

                            <?php } ?>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="submit"></label>
                                <div class="col-md-8">
                                    <button id="submit" name="submit" class=" col-md-12 button_blue middle_btn">Sign In</button>
                                </div>

                            </div>

                        </fieldset>
                    </form>

                </div>
                <div class="col-md-5">
                    <form class="form-horizontal" action="<?= base_url('index.php/main/register_fb_user/'); ?>" method="post">
                        <fieldset>
                            
                            <!-- Form Name -->
                            <legend>Create New</legend>
                            <hr style="margin-bottom:20px;margin-top:20px;">
                            <!-- Text input-->
                            <input name="facebook_uid" type="hidden" value="<?= $facebook_uid;?>"/>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="member_name">Contact Name<span style="color: red">*</span></label>
                                <div class="col-md-8">
                                    <input id="member_name" name="member_name" type="text" placeholder="Full Name" class="form-control input-md" required="" value="<?= $user_name; ?>">

                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="user_email">Email<span style="color: red">*</span></label>
                                <div class="col-md-8">
                                    <input id="user_email" name="user_email" type="text" placeholder="Email Address or Member Id" class="form-control input-md" required="" value="<?= $user_email; ?>">

                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="company_name">Company Name<span style="color: red">*</span></label>
                                <div class="col-md-8">
                                    <input id="company_name" name="company_name" type="text" placeholder="Company Name" class="form-control input-md" required="" >

                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="registration_location">Business Location<span style="color: red">*</span></label>
                                <div class="col-md-8">
                                    <select name="registration_location" id="registration_location">
                                        <?php foreach($countries as $country){?>
                                        <option value="<?=$country->country_id?>" <?php if(strtolower($user_location) == strtolower($country->country_name)){echo 'selected=selected';}?>><?=$country->country_name?></option>
                                        <?php }?>
                                    </select>

                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="submit"></label>
                                <div class="col-md-8">
                                    <button id="submit" name="submit" class=" col-md-12 button_blue middle_btn">Submit</button>
                                </div>

                            </div>

                        </fieldset>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>