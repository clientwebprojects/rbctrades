<?php include 'header3.php';?>
<script>
    document.getElementById("seller_home").className = "active-seller-menu";
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
<script>
// assumes you're using jQuery
    $(document).ready(function () {
        
<?php if ($this->session->flashdata('buyer_build')) { ?>
            $('#successmessage').html("<div class='alert alert-success alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('buyer_build'); ?>.</div>").show().delay(5000).fadeOut();
        });
<?php } ?>
</script>
<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">
            <div class="successmessage"></div>
            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-9" >

                    <div class="col-sm-12 col-md-12 col-lg-12" style="border:1px #ccc dotted;padding: 10px; margin: 10px;margin-top: 0px;">

                        <div class="col-sm-6 col-md-12 col-lg-12" style="width:100%;border-bottom: #ccc dashed 1px;margin: 0;padding:0">
                            <div class="col-sm-6 col-md-10 col-lg-10">
                                <h4>To do List</h4>
                            </div>
                            <div class="col-sm-6 col-md-2 col-lg-2">
                                <a href="" class="small_link pull-right">Settings</a>
                            </div>

                        </div>

                        <div class="col-sm-6 col-md-12 col-lg-12">

                            <div class ="col-md-3" style="margin-top:10px">
                                <br>
                                <a href="<?= base_url() ?>index.php/seller/message_center" class="small_link"><img src="<?= base_url() ?>images/messages.png" width="50" height="50"/><br>&nbsp;&nbsp;Unread <br> Messages</a>
                            </div>

                        </div>

                        <!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->

                    </div><!--/ .animated.transparent-->
                    <div class="col-sm-12 col-md-12 col-lg-12" style="border:1px #ccc dotted;padding: 10px; margin: 10px">
                        <div class="col-sm-6 col-md-7 col-lg-7">
                            <h4>No Products, No Inquiries!</h4>
                            <p>As a free member you can display 50 products online</p>
                            <a class="button_blue small_link" href="<?= base_url() ?>index.php/seller/add_new_product/">Display Products New</a>
                            <p style="color:#ccc;">More than 95% of buyers source by searching products online.</p>
                            <a href="" class="small_link pull-right">Learn More</a>
                        </div>
                        <div class="col-sm-6 col-md-5 col-lg-5" style="float:right;border-left: 1px dotted #ccc">
                            <p><img src="<?= base_url() ?>images/exclusive.png" height="30" width="30"><a >Upgrade to Exclusivev</a></p>
                            <ul style="list-style-type: square;margin-left: 20px;">
                                <li>22x more buyer inquiries</li>
                                <li>Top-tier priority listing</li>
                                <li>Instant access to buyers</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-12" style="border:1px #ccc dotted;padding: 10px; margin: 10px">
                        <h4>Learning and Training Center</h4>
                        <ul style="list-style-type: square;margin-left: 20px;">
                            <li><a href="" class="small_link"> Important to Display More Products</a></li>
                            <li><a href="" class="small_link">Tips for Using Customized Minisite</a></li>
                            <li><a href="" class="small_link">Training Courses: Tips to Reach more Buyers on RBC Trade</a></li>

                        </ul>
                        <a href="" class="small_link pull-right">Learn More</a>
                    </div>

                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

                <aside class="col-md-3 col-sm-12">

                    <section>



                        <div class="theme_box">

                            <h3 ><a class="link urrent open_" href="<?= base_url()?>index.php/seller/manage_company" style="color:#DFA239;cursor:pointer;"><?= $company->company_name; ?></a></h3>
                            <hr style="border:1px #ccc dotted;margin-bottom: 5px;">
                            <!-- - - - - - - - - - - - - - Progress Bar - - - - - - - - - - - - - - - - -->

                            <div style="border:0px"><?php
                                if ($level_id == 1) {
                                    echo "Unverified Member";
                                }
                                ?>
                                Unverified Member
                                <span style="float:right;cursor:pointer;"><a>Upgrade Now</a></span>
                            </div>

                            <div class="progress_bar">

                                <div class="image_div"><div class="img" style="background:url('<?php if($this->session->userdata('image')) { echo $this->session->userdata('image');}else{ echo base_url().'images/comment_author_photo.jpg';}?>')"></div></div>
                                <span style="margin-left:5px;"><?= $this->session->userdata('name'); ?></span>
                                <span style="cursor:pointer;margin-left:5px;"><a href="<?= base_url() ?>index.php/seller/update_profile"><?= $this->session->userdata('email'); ?></a></span>

                            </div>

                            <p class="pb_title">Location :&nbsp;&nbsp;&nbsp;<span style="font-weight:400;color: #000;"><?= $company->registration_location_country;?></span></p>
                            <p class="pb_title">Member Since :&nbsp;&nbsp;&nbsp;<span style="font-weight:400;color: #000;"> 07-05-2015</span></p>
                            <p class="pb_title">Company Profile <?= $company->profile_completed; ?>%</p>
                            <div class="progress_bar">
                                <div class="pb_inner" style="width: <?= $company->profile_completed; ?>%;"></div>
                            </div>
                            <a href="<?= base_url() ?>index.php/seller/manage_company" class="current open_" style="cursor:pointer;float: left;clear:right">Update Company Profile</a>
                            <a href="<?= base_url() ?>index.php/seller/update_profile" class="current open_" style="cursor:pointer;float: left">Update Personal Profile</a>

                        </div>
<!--                        <div class="theme_box" style="margin-top:15px;">
                            <h4 style="color:#DFA239;">Quick Access</h4>
                            <ul style="list-style-type: square; margin-left: 10px;">

                                <li><a href="<?= base_url() ?>index.php/seller/register_company/">Check New Messages</a></li>
                                <li><a href="<?= base_url() ?>index.php/seller/seller_profile/">Display New Products</a></li>
                                <li><a href="<?= base_url() ?>index.php/seller/manage_products/">Find Buying Request</a></li>
                                <li><a href="<?= base_url() ?>index.php/seller/manage_products/">Listing Optimization Tool</a></li>

                            </ul>
                        </div>-->

                        <div class="theme_box" style="margin-top:15px;">
                            <b class="color-gold">Help Us Improve</b>
                            <p class="pb_title font14">How would you rate your overall satisfaction with this page?</p>


                            <form method="post" id="help_us_form" name="help_us_form">
                                <div id="message"></div>
                                
                                <ul class="checkboxes_list">

                                    <li>
                                        <label class="radio-inline" >
                                            <input checked type="radio" name="status" id="satisfied" value="satisfied" style=" width: 30px;">Satisfied
                                        </label>
                                    </li>

                                    <li>

                                        <label class="radio-inline" >
                                            <input type="radio" name="status" id="neutral" value="neutral" style=" width: 30px;">Neutral
                                        </label>
                                    </li>

                                    <li>
                                        <label class="radio-inline" >
                                            <input  type="radio" name="status" id="dissatisfied" value="dissatisfied" style=" width: 30px;">Dis-satisfied
                                        </label>
                                    </li>
                                </ul>
                                <textarea name="message" class="form-control" rows="7" placeholder="Leave your Comments and Suggestions Here"></textarea><br>
                                <input  type="submit" name="feedback_submit" class="button_blue" value="submit">
                            </form>
                        </div>

                    </section>



                    <!-- - - - - - - - - - - - - - Already viewed products - - - - - - - - - - - - - - - - -->



                </aside>

            </div><!--/ .row-->

            <section class="section_offset" style="margin-top: 30px;">

                <!-- - - - - - - - - - - - - - Infoblocks v3 - - - - - - - - - - - - - - - - -->

                <div class="row">

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-thumbs-up-1"></i>

                                <h4 class="caption">The Highest<br>Product Quality</h4>

                            </div>

                            <p>Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-money"></i>

                                <h4 class="caption">100% Money<br>Back Guaranteed</h4>

                            </div>

                            <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-lock"></i>

                                <h4 class="caption">Safe &amp; Secure<br>Payment</h4>

                            </div>

                            <p>Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                </div><!--/ .row -->

                <!-- - - - - - - - - - - - - - End of infoblocks v3 - - - - - - - - - - - - - - - - -->

            </section><!--/ .section_offset -->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->




    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>
<script>
    $("#help_us_form").submit(function() {
        console.log("asadasd");
    var url = "<?= base_url('index.php/main/help_us')?>"; // the script where you handle the form input.
    $.ajax({
           type: "POST",
           url: url,
           data: $("#help_us_form").serialize(), // serializes the form's elements.
           dataType : 'text',
           success: function(data)
           {
               if(data=="1"){
                   var success_message = '<div class="alert alert-success" role="alert">'+
                                            '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+
                                            '<span class="sr-only">Error:</span>'+
                                            'Thank you for your feedback '+
                                        '</div>';
                        $("#message").append(success_message);
               }
               else{
                   var error_message = '<div class="alert alert-danger" role="alert">'+
                                            '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>'+
                                            '<span class="sr-only">Error:</span>'+
                                            'Sorry ,Theres an error Try again'+
                                        '</div>';
                        $("#message").append(error_message);
               }
                   
           }
         });
    return false; // avoid to execute the actual submit of the form.
});
    </script>