<?php include 'header.php'; ?>
<script>
    $(document).ready(function() {
        $("#filter").keyup(function() {

            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(), count = 0;

            // Loop through the comment list
            $(".searchlist li").each(function() {

                // If the list item does not contain the text phrase fade it out
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).show();
                    count++;
                }
            });

            // Update the count
            //var numberItems = count;
            //$("#filter-count").text("Number of Categories = " + count);
        });
    });
</script>

<div class="container " style="margin-bottom: 20px;">
    <br/>
    <div class="row">
        <div class=" col-md-6">
            <h2 class="heading" style="padding-top: 7px; font-size: 18px">Site Map</h2>
        </div>
        <div class=" col-md-6">
            <form id="live-search" action="" class="styled" method="post">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for..." id="filter">
                    <span class="input-group-addon" >
                        Search
                    </span>
                    <span id="filter-count"></span>
                </div>
            </form>
        </div>
    </div>


    <div class="dash-line"></div>



    <div class="box ui-box-content g-category-con clearfix">
        <div class="col-lg-6">

            <ol class="searchlist">
                <dl>
                    <li>  <span class="heading">Product Searches</span> <span class="small-desc"> (There are so many ways to finding products)</span></li>
                    <li>       </li>
                    <li>  <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/main/category_listing" rel="nofollow">Product Categories</a>
                    </dd></li>
                    <li>     <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/buyer/product_listing/" rel="nofollow">Hot Products</a>
                    </dd> </li>

                </dl>

            </ol>
            <br/>
            <ol class="searchlist">
                <dl>
                    <li><span class="heading">For Buyers</span></li>

                    <li><dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/buyer/request_for_quotation" rel="nofollow">Get Quotation</a>
                    </dd></li>
                    <li>        <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/buyer/company_listing/" rel="nofollow">Find Sellers</a>
                    </dd></li>
                    <li>     <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/buyer/view_selling_leeds/" rel="nofollow">View Selling Leads</a>
                    </dd></li>
                    <li>       <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/main/how_to_buy/" rel="nofollow">How to Buy</a>
                    </dd></li>
                </dl>
            </ol>
        </div>
        <div class="col-lg-6">
            <ol class="searchlist">
                <dl>
                    <li>    <span class="heading">RBC Profile Management</span> <span class="small-desc"> (Get insights, hear experts &amp; build relationships here!)</span></li>
                    <li>      </li>
                    <li>     <dd>
                        <a class="font12 link" href="<?php echo base_url() ?>index.php/login" rel="nofollow">Sign in</a> / <a class="font12 link" href="http://localhost/rbctrades/index.php/main/register_user/" rel="nofollow">Join Now!</a>
                    </dd></li>
                <li>      <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/seller/seller_profile/" rel="nofollow">Profile</a>
                    </dd>	</li>
                    <li>    <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/seller/message_center/" rel="nofollow">Message Center</a>
                    </dd></li>



                </dl>
            </ol>
            <br/>
            <ol class="searchlist">
                <dl>
                    <li>    <span class="heading">For Sellers</span></li>

                    <li>    <dd>
                        <a class="font12"href="<?php echo base_url() ?>index.php/seller/seller_profile/" rel="nofollow">Seller Profile</a>
                    </dd></li>
                    <li>    <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/seller/message_center/" rel="nofollow">Message center</a>
                    </dd></li>
                    <li>    <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/seller/add_new_product/" rel="nofollow">Display New Product</a>
                    </dd></li>
                    <li>    <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/seller/manage_products/" rel="nofollow">Manage Products</a>
                    </dd></li>
                    <li>     <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/seller/view_buying_leeds/" rel="nofollow">View Buying Leads</a>
                    </dd></li>
                    <li>    <dd>
                        <a class="font12" href="<?php echo base_url() ?>index.php/main/how_to_sell/" rel="nofollow">How to Sell</a>
                    </dd></li>
                </dl>
            </ol>

        </div>
    </div>
</div>
<?php include 'footer.php'; ?>