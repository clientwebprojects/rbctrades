<?php
include 'header.php';
$varuom = json_encode($uom);
?>
<script>
// assumes you're using jQuery
    $(document).ready(function () {
<?php if ($this->session->flashdata('msg')) { ?>
            $('#successmessage').html("<div class='alert alert-success alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('msg'); ?>.</div>").show().delay(10000).fadeOut();
        });
<?php } ?>
</script>

<script type="text/javascript">
    var abc = 0;
    var count_file = 1;
    $(document).ready(function () {
         
        
        $('body').on('change', '.file', function () {
            if (this.files && this.files[0] ) {
                abc += 1; //increementing global variable by 1

                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='image" + abc + "' class='img-div'><img id='previewimg" + abc + "' src=''/></div>");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                
                $(this).hide();
                $("#image" + abc).append($("<img/>", {id: 'imgx', src: '<?= base_url() ?>/images/x.png', alt: 'delete'}).click(function () {
                    $(this).parent().remove();
                    $('.file').show();
                }));
            }
        });
       
    });
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    }
</script>
<link href="<?= base_url() ?>css/datepicker.css" rel="stylesheet" type="text/css" />
<script>
    var uom = <?= $varuom; ?>;
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container">

        <div class="row">

            <div class="col-md-10 col-md-offset-1">

                <div class="row bs-wizard">

                    <div class="col-xs-3 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum">Post Buying Request</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-3 bs-wizard-step disabled" style="margin-top:0px"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">Seller Matching</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-3 bs-wizard-step disabled" style="margin-top:0px"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">Get Quotations</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-3 bs-wizard-step disabled" style="margin-top:0px"><!-- active -->
                        <div class="text-center bs-wizard-stepnum">Close the Deal</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>
                </div>
                <!-- - - - - - - - - - - - - - Revolution slider - - - - - - - - - - - - - - - - -->

                <form class="form-horizontal" method="post" action="" name="rfq_form" id="rfq_form" enctype="multipart/form-data">

                    <fieldset style="background: #f9f9f9;padding: 10px;border-radius: 3px;">



                        <!-- Form Name -->
                        <div class="col-md-12">
                            <div id="successmessage"></div>
                            <div class="heading">Request For Quotation</div>
                        </div>
                        <div id="rfq_container" class="rfq_container">

                            <div class="form-group">
                                <label class="col-md-3 col-sm-2 control-label" for="rfq_product_name">Product Name <span class="color-red">*</span></label>
                                <div class="col-md-6">
                                    <input  name="rfq_product_name[]" type="text" class="form-control input-md"   >

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-2 control-label" for="rfq_product_description">Product Details <span class="color-red">*</span></label>
                                <div class="col-md-6">
                                    <textarea  name="rfq_product_description[]" type="text" rows="5" class="form-control input-md"></textarea>


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-2 control-label">Attachment</label>
                                <div class="col-md-3">
                                    <input class="btn btn-default btn-sm file" type="file" id="file0" multiple="" name="rfq_product_image[]">
                       
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-2 control-label" for="rfq_product_quantity">Product Quantity <span class="color-red">*</span></label>
                                <div class="col-md-3">
                                    <input  name="rfq_product_quantity[]" type="text" class="form-control input-md">
                                </div>
                                <div class="col-md-3">
                                    <select name="rfq_product_uom[]" class="form-control">
                                        <?php foreach ($uom as $uoms) {
                                            ?>

                                            <option value="<?php echo $uoms->uom_id; ?>" name="<?php echo $uoms->uom_name; ?>"><?php echo $uoms->uom_name; ?></option>

                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                        </div>

                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-3"><a  class="btn btn-primary" onclick="rfqAdd();">+ Add More Products</a></div>
                        </div>
                        <div class="dash-line"></div>
                        <div class="col-md-12">
                            <div class="heading">Additional Quotation Details</div>
                        </div>
                        <!--Date Field-->
                        <div class="form-group">
                            <label class="col-md-3 col-sm-2 control-label" for="rfq_expiry_date">Expiry Date</label>
                            <div class="col-md-3">

                                <div class="input-group date"  data-date="" data-date-format="yyyy-mm-dd">
                                    <input class="form-control" type="text" id="rfq_expiry_date" value="YYYY-MM-DD" name="rfq_expiry_date"/>
                                    <span class="input-group-addon add-on" style="cursor: pointer;"> <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                </div>

                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-sm-2 control-label" for="rfq_quotation_description">Quotation Information</label>
                            <div class="col-md-6">
                                <textarea id="rfq_quotation_description" name="rfq_quotation_description" type="text" rows="5" class="form-control input-md"></textarea>
                            </div>
                        </div>


                        <!-- Button -->


                    </fieldset>
                    <div class="form-group col-md-12 col-sm-12 center">
                        
                        <label class="col-md-9 col-sm-6 checkbox-inline font14 color-333" for="">
                            <input type="checkbox" name="share_info_check" value=""   style=" width: 30px">
                            I agree to share my Business Card with quoted suppliers.
                        </label>
                        <br/>
                        
                        <label class="col-md-9 col-sm-6 checkbox-inline font14 color-333" for="">
                            <input type="checkbox" name="agreement_check" value=""   style=" width: 30px">
                            I have read, understood and agree to abide by the Buying Request Posting Rules
                        </label>
                    </div>
                    <div class="form-group" style="clear:left;clear: right">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <button type='submit' id="add-rfq" value="rfq-submit" name="rfq-submit" class="btn button_blue">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

        </div><!--/ .section_offset-->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<script type="text/javascript">
    function rfqAdd() {

        var selectHTMLUom = "";
        for (i = 0; i < uom.length; i = i + 1) {

            selectHTMLUom += "<option value='" + uom[i].uom_id + "'>" + uom[i].uom_name + "</option>";
        }
        var rfq_product ='<div class="dash-line"></div><div class="form-group">'+
                                '<label class="col-md-3 control-label" for="rfq_product_name">Product Name <span class="color-red">*</span></label>'+
                                '<div class="col-md-6">'+
                                    '<input  name="rfq_product_name[]" type="text" class="form-control input-md"   >'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="col-md-3 control-label" for="rfq_product_description">Product Details <span class="color-red">*</span></label>'+
                                '<div class="col-md-6">'+
                                    '<textarea name="rfq_product_description[]" type="text" rows="5" class="form-control input-md"></textarea>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="col-md-3 control-label">Attachment</label>'+
                                '<div class="col-md-3">'+
                                    '<input class="btn btn-default btn-sm" type="file" name="rfq_product_image[]">'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<label class="col-md-3 control-label" for="rfq_product_quantity">Product Quantity <span class="color-red">*</span></label>'+
                                '<div class="col-md-3">'+
                                    '<input  name="rfq_product_quantity[]" type="text" class="form-control input-md">'+
                                '</div>'+
                                '<div class="col-md-3">'+
                                    '<select name="rfq_product_uom[]" class="form-control">'+ selectHTMLUom +
                                    '</select>'+
                                '</div>'+
                            '</div>';

        $('#rfq_container').append(rfq_product);
        count_file++;
    }
</script>
<?php include 'footer.php'; ?>
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.date').datepicker();
    });
</script>


