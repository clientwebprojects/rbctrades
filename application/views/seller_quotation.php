<?php
include 'header2.php';
?>
<script>
    count = <?= sizeof($quot_details) ?>;
</script>

<div class="invoice-container" >
    <form class="form-horizontal" method="post" action="" name="seller-quotation" id="seller-quotation">
        <header class="invoice-header">
            <h1>Quotation</h1>
            <address >
                <textarea class="form-control" id="textarea" name="textarea" readonly style="resize: vertical;height: 130px;background: none;width: 550px;"><?= $quot_main->company_name ?> &#13 <?= $quot_main->operational_street_address ?> &#13 <?= $quot_main->operational_address_city ?> &#13 <?= $quot_main->operational_address_province ?> &#13 <?= $quot_main->country_name ?> </textarea>

            </address>

            <table class="meta">
                <tr>
                    <th><span >Invoice #</span></th>
                <input type="hidden" id="quotation_id" name="quotation_id" value="<?= $quot_main->id ?>">
                <input type="hidden" id="quotation_id" name="buyer_id" value="<?= $quot_main->rfq_buyer_id ?>">
                <td><span ><?= $quot_main->id ?></span></td>
                </tr>
                <tr>
                    <th><span >Currency</span></th>
                <input type="hidden" id="quotation_currency" name="quotation_currency" value="Dollar">
                <td><span >Dollar</span></td>
                </tr>
                <tr>
                    <th><span >Date</span></th>
                <input type="hidden" id="quotation_date" name="quotation_date" value="<?= $quot_main->rfq_quotation_date ?>">
                <td><span ><?= $quot_main->rfq_quotation_date ?></span></td>
                </tr>
                <tr>
                    <th><span >Valid Date</span></th>
                <input type="hidden" id="quotation_expiry_date" name="quotation_expiry_date" value="<?= $quot_main->rfq_expiry_date ?>">
                <td><span ><?= $quot_main->rfq_expiry_date ?></span></td>
                </tr>

            </table>
    <!--			<span><img alt="" src="<?= base_url() ?>/images/logo.png"></span>-->
        </header>

        <article>




            <table class="inventory">
                <thead>
                    <tr>
                        <th><span>Item</span></th>
                        <th><span>Description</span></th>
                        <th><span>Rate</span></th>
                        <th><span>Quantity</span></th>
                        <th><span>UOM</span></th>
                        <th><span>Price</span></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    foreach ($quot_details as $q) {
                        ?>
                        <tr>
                            <td><input id="product_name<?php echo $i; ?>" name="product_name[]"  type="text" value="<?= $q->rfq_product_name ?>" readonly ></td>
                            <td><input id="select<?php echo $i; ?>description" readonly name="product_description[]" type="text" value="<?= $q->rfq_product_description ?>" ></td>
                            <td><input id="rate<?php echo $i; ?>" name="product_rate[]" type="text" ></td>
                            <td><input id="quantity<?php echo $i; ?>" name="product_quantity[]"  type="text"></td>
                            <td><input id="product_uom<?php echo $i; ?>" type="text" value="<?= $q->uom_name ?>"readonly><input type="hidden" name="product_uom[]"  type="text" value="<?= $q->rfq_product_uom ?>"></td>
                            <td><input id="price<?php echo $i; ?>" name="product_price[]" type="text" disabled></td>
                        </tr>

                        <?php
                        $i++;
                    }
                    ?>
                </tbody>
            </table>
            <div style="float: left;width: 40%;">
                <h3 class="heading">Additional Quotation Details</h3>
                <div class="dash-line"></div>
                <p><?= $quot_main->rfq_quotation_description ?></p>
            </div>
            <table class="balance">
                <tr>
                    <th><span >Sub Total</span></th>
                    <td><input id="total" name="total" type="text" disabled></td>
                </tr>
                <tr>
                    <th><span >Discount %</span></th>
                    <td><input id="discount" name="discount" type="text" ></td>
                </tr>
                <tr>
                    <th><span >Tax %</span></th>
                    <td><input id="tax" name="tax" type="text" ></td>
                </tr>
                <tr>
                    <th><span >Grand Total</span></th>
                    <td><input id="balance_due" name="balance_due" type="text" disabled></td>
                </tr>
            </table>
        </article>

        <button class="btn button_blue" id="seller-quotation-submit" type="submit" value="seller-quotation-submit" name="seller-quotation-submit" style="float: right">Submit Quotation</button>

    </form>
</div>
<?php include 'footer.php'; ?>
<script src="<?= base_url() ?>js/invoicescript.js"></script>