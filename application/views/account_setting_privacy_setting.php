<?php include 'header2.php'; ?>
<script>
    document.getElementById("account_setting").className = "active-seller-menu";</script>
<div class="page_wrapper type_2" >
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                <?php include 'inc/seller_account_menu.php'; ?>
                <script>
                    document.getElementById("privacy_setting").className = "active";</script>
                <div class="col-md-10">

                    <form id="" class="form-horizontal" method="post" action="<?php echo base_url('index.php/seller/update_privacy_setting');?>" name="privacy_setting_form" enctype="">
                        <h4 class="heading">Privacy Settings</h4>
                        <div style="background: #f9f9f9;padding: 4px 2px;border-top:#ccc dotted 1px;">
                            <input type="hidden" value="<?= $privacy->id?>" name="id"/>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000"> Basic Information: </label>
                                <div class="col-md-9">
                                    <label class="control-label privacy_answer">
                                        Anyone can view this
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000"> Contact Information: </label>
                                <div class="col-md-9">
                                    <label class="control-label privacy_answer">
                                        Only suppliers you've exchanged business cards with can view this.
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div style="background: #efefef;padding: 4px 2px;">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000"> Contact Record: </label>
                                <div class="col-md-9">
                                    <label class="control-label privacy_answer">
                                        Anyone can view this
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div style="background: #f9f9f9;padding: 4px 2px;">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000"> Sourcing Information: </label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="sourcing_information" type="radio" value="1" style=" width: 30px" <?php if($privacy->sourcing_information == 1){echo 'checked=checked';}?>/>
                                            Anyone can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="sourcing_information" type="radio" value="2" style=" width: 30px" <?php if($privacy->sourcing_information == 2){echo 'checked=checked';}?>/>
                                            Trusted contacts and verified suppliers can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="sourcing_information" type="radio" value="3" style=" width: 30px" <?php if($privacy->sourcing_information == 3){echo 'checked=checked';}?>/>
                                            Only trusted contacts can see this
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #efefef;padding: 4px 2px;">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000">Activity Summary:</label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="activity_summary" type="radio" value="1" style=" width: 30px" <?php if($privacy->activity_summary == 1){echo 'checked=checked';}?>/>
                                            Anyone can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="activity_summary" type="radio" value="2" style=" width: 30px" <?php if($privacy->activity_summary == 2){echo 'checked=checked';}?>/>
                                            Trusted contacts and verified suppliers can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="activity_summary" type="radio" value="3" style=" width: 30px" <?php if($privacy->activity_summary == 3){echo 'checked=checked';}?>/>
                                            Only trusted contacts can see this
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #f9f9f9;padding: 4px 2px;">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000">Watched Industries:</label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="watched_industries" type="radio" value="1" style=" width: 30px" <?php if($privacy->watched_industries == 1){echo 'checked=checked';}?>/>
                                            Anyone can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="watched_industries" type="radio" value="2" style=" width: 30px" <?php if($privacy->watched_industries == 2){echo 'checked=checked';}?>/>
                                            Trusted contacts and verified suppliers can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="watched_industries" type="radio" value="3" style=" width: 30px" <?php if($privacy->watched_industries == 3){echo 'checked=checked';}?>/>
                                            Only trusted contacts can see this
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #efefef;padding: 4px 2px;">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000">Most Searched Keywords:</label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="most_searched_keywords" type="radio" value="1" style=" width: 30px" <?php if($privacy->most_searched_keywords == 1){echo 'checked=checked';}?>/>
                                            Anyone can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="most_searched_keywords" type="radio" value="2" style=" width: 30px" <?php if($privacy->most_searched_keywords == 2){echo 'checked=checked';}?>/>
                                            Trusted contacts and verified suppliers can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="most_searched_keywords" type="radio" value="3" style=" width: 30px" <?php if($privacy->most_searched_keywords == 3){echo 'checked=checked';}?>/>
                                            Only trusted contacts can see this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="most_searched_keywords" type="radio" value="4" style=" width: 30px" <?php if($privacy->most_searched_keywords == 4){echo 'checked=checked';}?>/>
                                            Make hidden
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #f9f9f9;padding: 4px 2px;">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000">Recently searched keywords and viewed products:</label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="recently_searched_products" type="radio" value="1" style=" width: 30px" <?php if($privacy->recently_searched_products == 1){echo 'checked=checked';}?>/>
                                            Anyone can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="recently_searched_products" type="radio" value="2" style=" width: 30px" <?php if($privacy->recently_searched_products == 2){echo 'checked=checked';}?>/>
                                            Trusted contacts and verified suppliers can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="recently_searched_products" type="radio" value="3" style=" width: 30px" <?php if($privacy->recently_searched_products == 3){echo 'checked=checked';}?>/>
                                            Only trusted contacts can see this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="recently_searched_products" type="radio" value="4" style=" width: 30px" <?php if($privacy->recently_searched_products == 4){echo 'checked=checked';}?>/>
                                            Make hidden
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background: #efefef;padding: 4px 2px;">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-000"> Transactions: </label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="transactions" type="radio" value="1" style=" width: 30px" <?php if($privacy->transactions == 1){echo 'checked=checked';}?>/>
                                            Anyone can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="transactions" type="radio" value="2" style=" width: 30px" <?php if($privacy->transactions == 2){echo 'checked=checked';}?>/>
                                            Trusted contacts and verified suppliers can view this
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3"></label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label class="privacy_answer">
                                            <input name="transactions" type="radio" value="3" style=" width: 30px" <?php if($privacy->transactions == 3){echo 'checked=checked';}?>/>
                                            Only trusted contacts can see this
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12"><div class="dash-line"></div></div>
                            <div class="col-md-12">
                                <div class="col-md-3"></div>
                                <div class="col-md-9"><p class="font12">*Note: Your contact information, activity summary, watched industries and most Searched Keywords will not be visible to blocked users.</p></div>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-9" style="margin-top: 15px;">
                                <input type="submit" class="button_blue" id="submit_company_information" name="submit" value="Submit">
                            </div>
                        </div>

                    </form>
                </div>
            </div><!--/ [col]-->
        </div><!--/ .row-->
        <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
    <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
</div>
<script>
    $("#business_1").click(function () {
        $('#job_title').show(500);
        $('#company_name').show();
        $('#business_type').show();
        $('#company_information').show();
    });
    $("#business_2").click(function () {
        $('#job_title').hide(500);
        $('#company_name').hide();
        $('#business_type').hide();
        $('#company_information').hide();
    });
    $("#website_yes").click(function () {
        $('#website_field').show(500);
    });
    $("#website_no").click(function () {
        $('#website_field').hide(500);
    });
</script>
<?php include 'footer.php'; ?>