<?php include 'header.php'; ?>
    <script>
            $(document).ready(function () {
                $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
                    e.preventDefault();
                    $(this).siblings('a.active').removeClass("active");
                    $(this).addClass("active");
                    var index = $(this).index();
                    $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                    $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
                });
            });</script>

<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">

            <div class="container hi">
                <div class="row">
                    <div class="col-lg-11 col-md-5 col-sm-8 col-xs-9 bhoechie-tab-container">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                            <div class="list-group">
                                <a href="#" class="list-group-item  text-center high">
                                    <h4 class="glyphicon glyphicon-pencil"></h4><br/>Register For Free
                                </a>
                                <a href="#" class="list-group-item active text-center high">
                                    <h4 class="glyphicon glyphicon-road"></h4><br/>Find Products
                                </a>
                                <a href="#" class="list-group-item text-center high">
                                    <h4 class="glyphicon glyphicon-home"></h4><br/>Screen Results
                                </a>
                                <a href="#" class="list-group-item text-center high">
                                    <h4 class="glyphicon glyphicon-user"></h4><br/>Contact Suppliers
                                </a>
                                <a href="#" class="list-group-item text-center high">
                                    <h4 class="glyphicon glyphicon-credit-card"></h4><br/>Trade With Confidence
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                            <!-- flight section -->
                            <div class="bhoechie-tab-content">
                                <div class="tab-content" style="display: block;">
                                    <div class="ques_list">
                                        <dl class="adjust">
                                            <dt><a href="" target="blank">Q: How do I register as a free member?</a></dt>
                                            <dd><span class="ques_a">A: </span><strong>Step 1:</strong> Click "Join Free" in the top-left corner of any Rbctrades.com page; select your country or territory, then select either "Supplier," "Buyer" or "Both";</dd>
                                            <dd><strong>Step 2:</strong> Complete the registration form and click "Create My Account". <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                            <dd><a href="" class="ui-button ui-button-primary" target="blank">Register Now</a></dd>
                                        </dl>
                                    </div>
                                
                                </div>
                            </div>
                            <!-- train section -->
                            <div class="bhoechie-tab-content active">
                                <div class="ques_list">
                                    <dl class="adjust">
                                        <dt><a href=""blank">Q: How do I know the most popular products in my industry?</a></dt>
                                        <dd><span class="ques_a">A: </span> Go to <a target="blank" href="">http://us.my.rbctrades.com/tradealert/myrbctrades/sub_scription_trade_alert.htm</a>,and input the product name you are interested in. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How do I find unique products and 3rd-party certified suppliers?</a></dt>
                                        <dd><span class="ques_a">A: </span> Try Rbctrades.com’s one-stop sourcing center now: <a target="blank" href="">http://expo.rbctrades.com</a> <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How do I find quality, and cheap, products quickly?</a></dt>
                                        <dd><span class="ques_a">A: </span><strong>Step 1:</strong> 1: Go to <a target="blank" href="">http://www.rbctrades.com/groupsourcing</a>, and select the categories you are interested in</dd>
                                        <dd><strong>Step 2:</strong> Choose the product you are interested in. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How do I get verified suppliers to reply within 48 hours?</a></dt>
                                        <dd><span class="ques_a">A: </span><strong>Step 1:</strong>  Go to Rbctrades.com and click Post Buying Request, <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                </div>
                               
                            </div>

                            <!-- hotel search -->
                            <div class="bhoechie-tab-content">
                                <div class="ques_list">
                                    <dl class="adjust">
                                        <dt><a href="" target="blank">Q: How do I find trustworthy suppliers?</a></dt>
                                        <dd><span class="ques_a">A: </span><strong>Step 1:</strong> Enter the product name in the Search Bar.</dd>
                                        <dd><strong>Step 2:</strong> Click Search. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How do I find products from specific countries?</a></dt>
                                        <dd><span class="ques_a">A: </span><strong>Step 1:</strong> Click Advanced Search located under the Search Bar.</dd>
                                        <dd><strong>Step 2:</strong> Enter the product name, select the country you want, and then click Search. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How do I compare product / supplier information?</a></dt>
                                        <dd><span class="ques_a">A: </span><strong>Step 1:</strong> earch products.</dd>
                                        <dd><strong>Step 2:</strong> Check the Compare box on the results page under the product image. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                </div>
                            
                            </div>
                            <div class="bhoechie-tab-content">
                                <div class="ques_list">
                                    <dl class="adjust">
                                        <dt><a href="" target="blank">Q: How do I contact suppliers who are offline?</a></dt>
                                        <dd><span class="ques_a">A: </span> After performing a search, click<img src="" alt="Chat Now!">,the Contact Supplier button near the product image.<a href="http://news.rbctrades.com/article/detail/help/100890197-1-how-do-i-contact-suppliers.html?tracelog=howtobuy_contact_1213_inquiry" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How do I contact suppliers who are online?</a></dt>
                                        <dd><span class="ques_a">A: </span> After performing a search, click the Chat Now button to communicate in real-time with your trading partners.<a href="http://news.rbctrades.com/article/detail/help/100890199-1-how-do-i-contact-suppliers.html?tracelog=howtobuy_contact_1213_atm" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: Where can I find contacts that I have communicated with previously?</a></dt>
                                        <dd><span class="ques_a">A: </span><strong>Step 1:</strong> Click My Rbctrades at the top of page, and then sign in</dd>
                                        <dd><strong>Step 2:</strong> Click Messages &amp; Contacts on the navigation bar at the top of page.</dd>
                                        <dd><strong>Step 3:</strong> Click Contacts. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How can I get an accurate quote from a supplier fast?</a></dt>
                                        <dd><span class="ques_a">A: </span> Click “Get latest price” to get an accurate quote from a supplier fast. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                </div>
                           
                            </div>
                            <div class="bhoechie-tab-content">
                                <div class="ques_list">
                                    <dl class="adjust">
                                        <dt><a href="" target="blank">Q:  How do I find qualified products?</a></dt>
                                        <dd><span class="ques_a">A: </span> You can find qualified products from Verified Main Products with production or trading experience. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How can I find assessed suppliers?</a></dt>
                                        <dd><span class="ques_a">A: </span> Using the search filter to find Assessed Suppliers, <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                    <dl>
                                        <dt><a href="" target="blank">Q: How do I protect my payment?</a></dt>
                                        <dd><span class="ques_a">A: </span> You can choose to pay via Rbctrades.com’s Escrow Service.<br>
                                            Rbctrades.com’s Escrow Service will hold your payment while the order is being processed. Payment will be released to the supplier upon your confirmation. <a href="" class="more" target="blank">See more <i class="arrwos"></i></a></dd>
                                    </dl>
                                </div>
                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
    
<?php include 'footer.php'; ?>