<?php include 'header.php'; ?>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
<script type="text/javascript">
    var base_url = "<?= base_url(); ?>";
</script>
<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="<?= base_url() ?>">Home</a></li>
            <li>Agriculture</li>

        </ul>

        <div class="row">
            <?php include 'inc/product_filter.php'; ?>

            <main class = "col-md-9">

                <div class="section_offset">

                    <header class="top_box on_the_sides">

                        <div class="left_side clearfix v_centered">

                            <!-- - - - - - - - - - - - - - Sort by - - - - - - - - - - - - - - - - -->



                                <span>Filter by:</span>
                                <div class="form-group">
                                <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Trade Assurance</label>
                                <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Gold Supplier</label>
                                <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Assessed Supplier</label>
                            </div>

                        </div>

                        <div class="right_side">



                            <div class="layout_type buttons_row" data-table-container="#products_container">

                                <a href="#" data-table-layout="grid_view" class="button_grey middle_btn icon_btn active tooltip_container"><i class="icon-th"></i><span class="tooltip top">Grid View</span></a>

                                <a href="#" data-table-layout="list_view list_view_products" class="button_grey middle_btn icon_btn tooltip_container"><i class="icon-th-list"></i><span class="tooltip top">List View</span></a>

                            </div>

                        </div>

                    </header>

                    <div class="table_layout" id="products_container">
                        <div id="outerdiv">
                            <div id="product-row" style="border-right: 1px solid #dae2ed;border-left: 1px solid #dae2ed;margin-right: 1px">
                                <div class="table_row" >
                                    <?php
                                    $count = 1; //print_r($products);exit();
                                    $product_id = "";
                                    foreach ($products as $row) {
                                        if ($product_id != $row->product_id) {
                                            ?>


                                            <div class="table_cell">

                                                <div class="product_item">



                                                    <div class="image_wrap">
                                                        <?php
                                                        if (!empty($row->image_url) && $row->is_main == 1) {
                                                            echo "<img src='" . $row->image_url . "' alt='' width='245' height='243'>";
                                                        } else {
                                                            echo "<img src='" . base_url() . "images/no_image.jpg' width='245' height='243'>";
                                                        }
                                                        ?>


                                                        <div class="actions_wrap">

                                                            <div class="centered_buttons">

                                                                <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                            </div>





                                                        </div>



                                                    </div>





                                                    <div class="description">

-                                                        <a href="#"><?= $row->product_name; ?></a>
+                                                        <a href="<?= base_url() ?>index.php/buyer/product_detail"><?= $row->product_name; ?></a>

                                                         <div class="clearfix product_info">
+                                                            <ul class="alignleft">

-                                                            <p class="product_price alignleft"><b>$5.99</b></p>
-
-
+                                                                <span class="color-000 font12">3000 Piece</span> <span class="small-desc">(Min. Order)</span>

-                                                            <ul class="rating alignright">
+                                                            </ul>
+<!--                                                            <ul class="rating alignright">

                                                                 <li class="active"></li>
                                                                 <li class="active"></li>
                                                                 <li class="active"></li>
                                                                 <li></li>

-                                                            </ul>
+                                                            </ul>-->




                                                     <div class="full_description">

-                                                        <a href="#" class="product_title"><?= $row->product_keyword; ?></a>
+                                                        <a href="#" class="product_title"><?= $row->product_name; ?></a>

-                                                        <a href="#" class="product_category"><?= $row->category_name; ?></a>
+                                                        <span class="color-333 font14 bold">3000 Piece</span> <span class="small-desc font14">(Min. Order)</span>

-                                                        <div class="v_centered product_reviews">
+                                                        <table style="border: 0!important">
+                                                            <tbody>
+                                                                <tr>
+                                                                    <td style="border:0!important;padding: 0!important"><span class="font12 color-666">Material:</span><span class="small-desc">100% Polyester</span></td>
+                                                                    <td style="border:0!important;padding: 0!important"></td>
+                                                                </tr>
+                                                            </tbody>
+                                                        </table>


-
-                                                            <ul class="rating">
-
-                                                                <li class="active"></li>
-                                                                <li class="active"></li>
-                                                                <li class="active"></li>
-                                                                <li class="active"></li>
-                                                                <li></li>
-
-                                                            </ul>
-
-
-                                                            <ul class="topbar">
-
-                                                                <li><a href="#">3 Review(s)</a></li>
-                                                                <li><a href="#">Add Your Review</a></li>
-
-                                                            </ul>
-
-
-                                                        </div>
-
-                                                        <p><?= $row->product_description; ?></p>
-
-                                                        <a href="#" class="learn_more">Learn More</a>
-
                                                     </div>

-                                                    <div class="actions" style="border-left: 1px solid #dae2ed;padding-left: 15px;">
+                                                    <div class="actions">

                                                         <p class="seller_info font12"><i class="glyphicon glyphicon-star"></i> Guangzhou Beisheng Trading Co., Ltd.</p>

                                                 </div>

                                             </div>
                                            <?php
                                            if ($count % 3 == 0) {
                                                echo '</div><div class="table_row">';
                                            }
                                            $count++;
                                        }

                                        $product_id = $row->product_id;
                                    }
                                    ?>
                                </div>
                            </div>
                            <!--/ .table_row -->
                        </div>
                    </div><!--/ .table_layout -->

                    <footer class="bottom_box on_the_sides">

                        <div class="left_side">

                            <p>Showing 1 to 3 of 45 (15 Pages)</p>

                        </div>

                        <div class="right_side">

                            <ul class="pags">

                                <li><a href="#"></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#"></a></li>

                            </ul>

                        </div>

                    </footer>

                </div>

                <!-- - - - - - - - - - - - - - End of products - - - - - - - - - - - - - - - - -->

            </main>

        </div><!--/ .row -->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
<script>
    $(document).ready(function() {

        $("#ajaxcall_select_category").on('change', (function(e) {
            var id = $("#ajaxcall_select_category option:selected").val();
            var start_html = '<div id="product-row"> ';
            var end_html = '</div></div>';
            var product_row = '<div class="table_row">';
            var count = 1;
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('index.php/buyer/category_filtration'); ?>", //Relative or absolute path to response.php file
                data: {'category_id': id},
                cache: false,
                dataType: 'json',
                success: function(json) {
                    $('#product-row').remove();
                    $('#outerdiv').append(start_html);
                    $('#product-row').append(product_row);
                    $.each(json, function(i, v) {
                        console.log(json);
                        console.log(i, v, id);
                        var subCategory_html =
                                '<div class="table_cell">' +
                                '<div class="product_item">' +
                                '<div class="image_wrap">' +
                                '<img src="' + image_src(v.images_url) + '" alt="" width="245" height="243">' +
                                '<div class="actions_wrap">' +
                                '<div class="centered_buttons">' +
                                '<a href="' + base_url + 'index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="description">' +
                                '<a href="#">' + v.product_name + '</a>' +
                                '<div class="clearfix product_info">' +
                                '<p class="product_price alignleft"><b>$' + v.product_fob_price + '</b></p>' +
                                '<ul class="rating alignright">' +
                                '<li class="active"></li>' +
                                '<li class="active"></li>' +
                                '<li class="active"></li>' +
                                '<li class="active"></li>' +
                                '<li></li>' +
                                '</ul>' +
                                '</div>' +
                                '</div>' +
                                '<div class="full_description">' +
                                '<a href="#" class="product_title">' + v.product_keyword + '</a>' +
                                '<a href="#" class="product_category">' + v.product_category + '</a>' +
                                '<div class="v_centered product_reviews">' +
                                '<ul class="rating">' +
                                '<li class="active"></li>' +
                                '<li class="active"></li>' +
                                '<li class="active"></li>' +
                                '<li class="active"></li>' +
                                '<li></li>' +
                                '</ul>' +
                                '<ul class="topbar">' +
                                '<li><a href="#">3 Review(s)</a></li>' +
                                '<li><a href="#">Add Your Review</a></li>' +
                                '</ul>' +
                                '</div>' +
                                '<p>' + v.product_description + '</p>' +
                                '<a href="#" class="learn_more">Learn More</a>' +
                                '</div>' +
                                '<div class="actions">' +
                                '<p class="product_price bold">$' + v.product_fob_price + '</p>' +
                                '</div>' +
                                '</div>' +
                                '</div>' + new_row(count);
                        $('#product-row').append(subCategory_html);
                        count++;
                    });
                    $('#outerdiv').append(end_html);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });


        }));

    });

</script>
<script type="text/javascript">
    function image_src(src) {
        if (jQuery.isEmptyObject(src)) {
            return base_url + "images/no_image.jpg";
        } else {
            return src[0].image_url;
        }
    }
    function new_row(count) {
        console.log(count % 3);
        if (count % 3 === 0) {

            return '</div><div class="table-row">';
        } else {
            return "";
        }
    }
</script>
<?php include 'footer.php'; ?>