<?php include 'header.php'; ?>
<script>
// assumes you're using jQuery
    $(document).ready(function () {
        $('.confirm-div').hide();
        <?php if ($this->session->flashdata('msg')) { ?>
                    $('#message').html("<div class='alert alert-danger alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('msg'); ?>.</div>").show().delay(10000).fadeOut();

        <?php } else if($this->session->flashdata('registration_success_msg')) { ?>
                    $('#signed_up_failed').html("<div class='alert alert-error alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('signed_up_failed'); ?>.</div>").show().delay(10000);

        <?php } ?>
    });
</script>
<div class="page_wrapper type_2" >
    <div class="container" style="background-color:#FFF;padding:40px 20px;">


        <div class="section_offset">

            <div class="row">
                <div class="col-sm-6">
                                                <!-- Form Name -->
                            <legend>Sign In</legend>
                            <hr style="margin-bottom:20px;margin-top:20px;">
                            <div id="message" class="col-md-offset-4"></div>
                            <div id="sign_up_failed" class="col-md-offset-4"></div>
                    <form class="form-horizontal" action="<?= base_url(); ?>index.php/login/check/" method="post" name="login_form" id="login-form">
                        <fieldset>
                            <?php if ($this->session->flashdata('signed_up')) { ?>
                                <div class='alert alert-success alert-dismissible' role='alert' >
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close' style="margin-top:8%;margin-right:5%"></button><strong>Congrats!</strong><?= $this->session->flashdata('signed_up'); ?>
                                </div>
                            <?php } else if ($this->session->flashdata('invalid_key')) {
                                ?>
                                <div class='alert alert-info alert-dismissible' role='alert' >
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close' style="margin-top:8%;margin-right:5%"></button><strong>Error!</strong><?= $this->session->flashdata('invalid_key'); ?>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="user_email">Username / Email</label>
                                <div class="col-md-8">
                                    <input id="user_email" name="username" type="text" placeholder="Email Address or Member Id" class="form-control input-md" >

                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password">Password</label>
                                <div class="col-md-8">
                                    <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" >

                                </div>
                            </div>
                            <?php if ($this->session->userdata('captcha') == true) { ?>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Captcha</label>
                                    <div class="col-md-8">
                                        <div class="g-recaptcha" data-sitekey="6Lf3JAcTAAAAANdDjN638WHpEksepoYU6a7Lc5fP"></div>

                                    </div>
                                </div>

                            <?php } ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password"></label>
                                <div class="col-md-8 " >
                                    <a href="<?= base_url('index.php/main/forgot_password')?>" class="pull-right color-blue font12">Forgot Password?</a>

                                </div>
                            </div>
                            <label class="col-md-4 control-label" ></label>
                            <div class="col-md-4">
                                <input type="submit" id="submit_sign_in" name="signin" class="btn col-md-12 button_blue" value="Sign In"/>
                            </div>
                            <div class="col-md-4">
                                <a href="<?php base_url('social/session/facebook') ?>" class="btn social_facebook tooltip_container">Sign in with Facebook<span class="tooltip top">Facebook sign in</span></a>

                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-6">
                    <section class="section register inner-left-xs">
                        <legend>Create New Account</legend>
                        <hr  style="margin-bottom:20px;margin-top:20px;">
                        <p>Create your own Media Center account by clicking <a href="<?= base_url() ?>/index.php/login" class="link">Sign In</a></p>
                        <div class="clearfix" style="margin-top: 10px;"></div>
                        <h2 class="semi-bold"><a href="<?= base_url() ?>/index.php/main/register_user" class="color-gold" style="text-decoration: underline">Sign up</a> today and you'll be able to :</h2>

                        <ul class="list-unstyled list-benefits">
                            <li><i class="fa fa-check primary-color"></i> Speed your way through the checkout</li>
                            <li><i class="fa fa-check primary-color"></i> Track your orders easily</li>
                            <li><i class="fa fa-check primary-color"></i> Keep a record of all your purchases</li>
                        </ul>

                    </section>

                </div>
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>