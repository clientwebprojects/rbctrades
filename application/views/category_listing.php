<?php
include 'header.php';
//echo '<pre>';
//print_r($sub_categories);
//exit();
$count = 0;
?>

<script>
    $(document).ready(function () {
        $("#filter").keyup(function () {

            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(), count = 0;

            // Loop through the comment list
            $(".searchlist li").each(function () {

                // If the list item does not contain the text phrase fade it out
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).show();
                    count++;
                }
            });

            // Update the count
            //var numberItems = count;
            //$("#filter-count").text("Number of Categories = " + count);
        });
    });
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >
    <div class="container">
        <div class="section_offset">
            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->
                <div class="col-md-12" style="background-color:#FFF;">
                    <section id="checkout-page">
                        <div class="container">
                            <div class="col-lg-6" style="float: right;">

                                
                                    <form id="live-search" action="" class="styled" method="post">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search for..." id="filter">
                                            <span class="input-group-addon" >
                                                Search
                                            </span>
                                            <span id="filter-count"></span>
                                        </div>
                                    </form>
                              
                            </div>

                            <div class="col-xs-12 no-margin" id="" >

                                <?php foreach ($category as $values) { ?>

                                    <div class="row row-custom">

                                        <ol class="searchlist">
                                            <h4 class="color-blue bold"><?= $values->category_name ?>
    <!--                                                <span class="font14 color-666"> (123554)</span>-->
                                            </h4>
                                            <div class="col-lg-3">
                                                <div>

                                                    <div class="panel-body panel-primary">
                                                        <ul class="category-list">
                                                            <?php foreach ($all_category as $value) { ?>
                                                                <?php if ($value->parent_id === $values->category_id) { ?>
                                                                    <li class="font12 color-999"><a href="<?= base_url() ?>index.php/buyer/product_by_category_listing/<?= $value->category_id; ?>"> <?= $value->category_name ?></a></li>
                                                                    <?php
                                                                    if ($count == 8) {
                                                                        $count = 0
                                                                        ?>
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">

                                                            <div>

                                                                <div class="panel-body panel-primary">

                                                                    <ul class="category-list">
                                                                        <li>
                                                                            <a href=""  >   <?php
                                                                            } $count++;
                                                                        }
                                                                        ?>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        <?php } ?>
                                                    </div>

                                                </div>

                                            </div>

                                        </ol>
                                    </div>
                                <?php } ?>
                            </div>
                        </div><!-- /.container -->
                    </section><!-- /#checkout-page -->

                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

            </div><!--/ .row-->



        </div><!--/ .section_offset-->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>