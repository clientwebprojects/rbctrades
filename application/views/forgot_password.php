<?php include 'header.php'; ?>
<script>
// assumes you're using jQuery
    $(document).ready(function () {
<?php if ($this->session->flashdata('msg')) { ?>
            $('#successmessage').html("<div class='alert alert-warning alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('msg'); ?>.</div>").show().delay(10000).fadeOut();
        });
<?php } ?>
</script>

<div class="page_wrapper type_2" >
    <div class="container" style="background-color:#FFF;padding:40px 20px;">


        <div class="section_offset">

            <div class="row">
                <div class="col-sm-6">
                    <div id="successmessage"></div>
                                                <!-- Form Name -->
                            <legend>Forgot Password</legend>
                            <hr style="margin-bottom:20px;margin-top:20px;">
                            <div id="message" class="col-md-offset-4"></div>
                            <div id="sign_up_failed" class="col-md-offset-4"></div>
                    <form class="form-horizontal" action="<?= base_url(); ?>index.php/main/forgot_password/" method="post" name="forgot_password_form" id="forgot_password_form">
                        <fieldset>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="user_email">Email</label>
                                <div class="col-md-8">
                                    <input id="user_email" name="username" type="text" placeholder="Email Address or Member Id" class="form-control input-md" >

                                </div>
                            </div>

                            <!-- Text input-->
                           
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Captcha</label>
                                    <div class="col-md-8">
                                        <div class="g-recaptcha" data-sitekey="6Lf3JAcTAAAAANdDjN638WHpEksepoYU6a7Lc5fP"></div>

                                    </div>
                                </div>
 
                            <label class="col-md-4 control-label" ></label>
                            <div class="col-md-4">
                                <input type="submit" id="submit_forgot_password" name="forgot_password" class="btn col-md-12 button_blue" value="Send"/>
                            </div>
                           
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-6">
                    <section class="section register inner-left-xs">
                        <legend>Create New Account</legend>
                        <hr  style="margin-bottom:20px;margin-top:20px;">
                        <p>Create your own Media Center account by clicking <a href="<?= base_url() ?>/index.php/login" class="link">Sign In</a></p>
                        <div class="clearfix" style="margin-top: 10px;"></div>
                        <h2 class="semi-bold"><a href="<?= base_url() ?>/index.php/main/register_user" class="color-gold" style="text-decoration: underline">Sign up</a> today and you'll be able to :</h2>

                        <ul class="list-unstyled list-benefits">
                            <li><i class="fa fa-check primary-color"></i> Speed your way through the checkout</li>
                            <li><i class="fa fa-check primary-color"></i> Track your orders easily</li>
                            <li><i class="fa fa-check primary-color"></i> Keep a record of all your purchases</li>
                        </ul>

                    </section>

                </div>
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>

<script>
    $(document).ready(function () {
$('#forgot_password_form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The email is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Email address is not valid'
                    }
                }
            }

        }
    });
    
});
</script>