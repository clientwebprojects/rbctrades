<?php include 'header2.php'; ?>
<script>
    document.getElementById("seller_messages").className = "active-seller-menu";
</script>
<div class="page_wrapper type_2" >

    <div class="container">

        <div class="section_offset">


            <div class="container" style="background-color:#FFF;padding-top: 20px;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-2">
                            <?php include 'inc/message_center_left.php'; ?>
                        </div>
                        <script>
                            document.getElementById("trash_box").className = "active";
                        </script>
                        <div class="col-sm-10">
                            <?php include 'inc/message_center_top.php'; ?>
                            <br/>
                            <?php if ($trashbox_inquiries == NULL) { ?>
                                <div class="list-group">
                                    <div class="list-group-item">
                                        <p>You don't have any deleted inquiry/message.</p>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div id="outerdiv">
                                    <div class="list-group tb">


                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>



            <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>
<script>
    var page_number = 0;
    var total_page = null;
    var sr = 0;
    var sr_no = 0;

    $(".pagination_footer").hide();
    var getReport = function (page_number) {
        if (page_number == 0) {
            $("#previous").prop('disabled', true);
        }
        else {
            $("#previous").prop('disabled', false);
        }

        if (page_number == (total_page - 1)) {
            $("#next").prop('disabled', true);
        }
        else {
            $("#next").prop('disabled', false);
        }

        $("#page_number").text(page_number + 1);

        $.ajax({
            url: "<?php echo base_url() ?>/index.php/message_center/pagination_trash",
            type: "POST",
            dataType: 'json',
            data: 'page_number=' + page_number,
            success: function (data) {
                $(".pagination_footer").show();
                window.mydata = data;
                total_page = mydata[0].TotalRows;
                $("#total_page").text(total_page);
                var record_par_page = mydata[0].Rows;
                if (page_number == 0) {
            $("#previous").prop('disabled', true);
        }
        else {
            $("#previous").prop('disabled', false);
        }

        if (page_number == (total_page - 1)) {
            $("#next").prop('disabled', true);
        }
        else {
            $("#next").prop('disabled', false);
        }

        $("#page_number").text(page_number + 1);
                $.each(record_par_page, function (key, data) {
                    sr = (key + 1);
                    if (data.is_read == 1) {
                        var bold = "";
                    } else {
                        var bold = "bold";
                    }
                    var message_center = '<div class="list-group-item" style="padding:8px  0!important;color: #333">' +
                            '<div class="checkbox" style="margin: 0!important">' +
                            '<label for="' + data.temp_user_email + '" style="padding-top:0px;" class="col-sm-12 ">' +
                            '<input type="checkbox" name="trash[]" id="checkbox_id" value="' + data.message_id + '" style="width: 15px;">' +
                            '<a href="' + base_url + 'index.php/message_center/email_data/' + data.message_id + '"  id="' + data.temp_user_email + '" class="msg_email '+ bold +' color-333">' + data.temp_user_email + '</a>' +
                            '<span class="color-333 pull-right ">' + data.date + '</span>' +
                            '</label>' +
                            '</div>' +
                            '<div style="padding-left: 35px;">' +
                            '<a href="' + base_url + 'index.php/message_center/email_data_trash/' + data.message_id + '" class="'+ bold +' color-333">' + data.subject + '</a>' +
                            '<span class="msg_body color-666">' + data.body + '</span>' +
                            '</div>' +
                            '</div>';
                    $(".tb").append(message_center);

                });
            }
        });
    };
    $(document).ready(function (e) {

        getReport(page_number);

        $("#next").on("click", function () {
            $(".tb").html("");
            page_number = (page_number + 1);
            getReport(page_number);

        });

        $("#previous").on("click", function () {
            $(".tb").html("");
            page_number = (page_number - 1);
            getReport(page_number);
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#trash_all").on('click', function (e) {
            e.preventDefault();
            var checkValues = $('#checkbox_id:checked').map(function ()
            {
                return $(this).val();
            }).get();
            console.log(checkValues);

            $.each(checkValues, function (i, val) {
                $("#" + val).remove();
            });
//                    return  false;
            $.ajax({
                url: '<?= base_url() ?>/index.php/message_center/delete_inquiries_all',
                type: 'post',
                data: 'ids=' + checkValues
            }).done(function (data) {
                 window.location.href ="<?php echo base_url() ?>/index.php/message_center/get_trash_inquiries";
                $("#outerdiv").html(data);
                //$('#selecctall').attr('checked', false);
                $('#outerdiv').html(data);
            });
        });
    });
    function strip(html)
    {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }
</script>