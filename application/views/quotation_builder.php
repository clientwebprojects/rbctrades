<?php
include 'header2.php';
$var = json_encode($product);
$varuom = json_encode($uom);
?>
<script>
    var product = <?= $var; ?>;
    var uom = <?= $varuom; ?>;
    count = 1;

</script>

<div class="invoice-container" >
    <form class="form-horizontal" method="post" action="" name="new-quotation" id="new-quotation">
    <header class="invoice-header">
        <h1>Quotation</h1>
        <address contenteditable>
            <textarea class="form-control" id="textarea" name="textarea" style="resize: vertical;height: 130px;">Zain Aslam 101 E. Chapman Ave Orange, CA 92866 (800) 555-1234 </textarea>

        </address>

        <table class="meta">
            <tr>
                <th><span contenteditable>Invoice #</span></th>
                <td><span contenteditable>101138</span></td>
            </tr>
            <tr>
                <th><span contenteditable>Currency</span></th>
                <td><span contenteditable>Dollar</span></td>
            </tr>
            <tr>
                <th><span contenteditable>Date</span></th>
                <td><span contenteditable>April 24, 2015</span></td>
            </tr>
            <tr>
                <th><span contenteditable>Valid Date</span></th>
                <td><span contenteditable>April 24, 2015</span></td>
            </tr>

        </table>
<!--			<span><img alt="" src="<?= base_url() ?>/images/logo.png"></span>-->
    </header>
    
        <article>




            <table class="inventory">
                <thead>
                    <tr>
                        <th><span >Item</span></th>
                        <th><span >Description</span></th>
                        <th><span >Rate</span></th>
                        <th><span >Quantity</span></th>
                        <th><span >UOM</span></th>
                        <th><span >Price</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a class="cut">-</a><span >
                                <select id="select0" name="select[]" onchange="description(this.value, this.id)">
                                    <?php foreach ($product as $new) {
                                        ?>

                                        <option value="<?php echo $new->product_id; ?>" name="<?php echo $new->product_name; ?>"><?php echo $new->product_name; ?></option>

                                    <?php } ?>
                                </select>
                            </span></td>
                        <td><input id="select0description" name="selectdescription[]" type="text" ></td>
                        <td><input id="rate0" name="rate[]" type="text"></td>
                        <td><input id="quantity0" name="quantity[]"  type="text"></td>
                        <td id="uom0">                           
                            <select id="selectuom0" name="selectuom[]">
                                <?php foreach ($uom as $uoms) {
                                    ?>

                                    <option value="<?php echo $uoms->uom_id; ?>" name="<?php echo $uoms->uom_name; ?>"><?php echo $uoms->uom_name; ?></option>

<?php } ?>
                            </select>

                        </td>
                        <td><input id="price0" name="price[]" type="text" disabled></td>
                    </tr>
                </tbody>
            </table>
            <a class="add">+</a>
            <table class="balance">
                <tr>
                    <th><span >Sub Total</span></th>
                    <td><input id="total" name="total" type="text" disabled></td>
                </tr>
                <tr>
                    <th><span >Discount %</span></th>
                    <td><input id="discount" name="discount" type="text" ></td>
                </tr>
                <tr>
                    <th><span >Tax %</span></th>
                    <td><input id="tax" name="tax" type="text" ></td>
                </tr>
                <tr>
                    <th><span >Grand Total</span></th>
                    <td><input id="balance_due" name="balance_due" type="text" disabled></td>
                </tr>
            </table>
        </article>
        <aside>
            <h1><span contenteditable>Additional Notes</span></h1>
            <div contenteditable>
                <p>A finance charge of 1.5% will be made on unpaid balances after 30 days.</p>
            </div>
        </aside>
        <button id="submitbtn" class="btn btn-primary" value="quotation_submit" type="submit" name="quotation_submit">Submit</button>
        
    </form>
</div>
<script src="<?= base_url() ?>js/invoicescript.js" type="text/javascript"></script>
<?php include 'footer.php'; ?>