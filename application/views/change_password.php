<?php include 'header.php'; ?>
<script>
// assumes you're using jQuery
    $(document).ready(function () {
<?php if ($this->session->flashdata('msg')) { ?>
            $('#successmessage').html("<div class='alert alert-warning alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('msg'); ?>.</div>").show().delay(10000).fadeOut();
        });
<?php } ?>
</script>

<div class="page_wrapper type_2" >
    <div class="container" style="background-color:#FFF;padding:40px 20px;">


        <div class="section_offset">

            <div class="row">
                <div class="col-sm-6">
                    <div id="successmessage"></div>
                                                <!-- Form Name -->
                            <legend>Forgot Password</legend>
                            <hr style="margin-bottom:20px;margin-top:20px;">
                            <div id="message" class="col-md-offset-4"></div>
                            <div id="sign_up_failed" class="col-md-offset-4"></div>
                    <form class="form-horizontal" action="<?= base_url(); ?>index.php/main/change_password/" method="post" name="change_password_form" id="change_password_form">
                        <fieldset>
                            <input type="hidden" value="<?php echo $hash_key; ?>" name="hash_key">
                            <input type="hidden" value="<?php echo $email; ?>" name="email">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="password">New Password</label>
                                <div class="col-md-8">
                                    <input id="password" name="password" type="password" class="form-control input-md" >

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="confirm_password">Confirm Password</label>
                                <div class="col-md-8">
                                    <input id="confirm_password" name="confirm_password" type="password" class="form-control input-md" >

                                </div>
                            </div>

                            <!-- Text input-->
                           
<!--                                <div class="form-group">
                                    <label class="col-md-4 control-label">Captcha</label>
                                    <div class="col-md-8">
                                        <div class="g-recaptcha" data-sitekey="6Lf3JAcTAAAAANdDjN638WHpEksepoYU6a7Lc5fP"></div>

                                    </div>
                                </div>-->
 
                            <label class="col-md-4 control-label" ></label>
                            <div class="col-md-4">
                                <input type="submit" id="submit_change_password" name="change_password" class="btn col-md-12 button_blue" value="Change"/>
                            </div>
                           
                        </fieldset>
                    </form>
                </div>
                
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
<script>
    $(document).ready(function () {
$('#change_password_form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            password: {
                message: 'The password is not valid',
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 100,
                        message: 'The password must be more than 6 characters long'
                    }
                }
            },
            confirm_password: {
                message: 'The confirm password is not valid',
                validators: {
                    notEmpty: {
                        message: 'The confirm password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'Password Does not match'
                    }
                }
            }

        }
    });
    
});
</script>