<?php include 'header.php'; ?>





<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="index.html">Home</a></li>
            <li>Compare Products</li>

        </ul>

        <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->

        <h1>Compare Products</h1>

        <div class="table_wrap">

            <table class="table_type_1 compare">

                <tbody>
                    <tr>

                        <th class="row_title_col">Product Image</th>

                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>
                            <?php if(!empty($product_details[$i]->image_url)){ ?>
                                <img src="<?= $product_details[$i]->image_url;?>" height="100"/>
                            <?php }else{?>
                                <img src="<?= base_url('images/default.png');?>" height="100"/>
                            <?php
                            }
                            ?>

                        </td>
                        <?php }?>

                    </tr>
                    <tr>

                        <th class="row_title_col">Product Name</th>
                    
                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>

                            <a href="#"><?= $product_details[$i]->product_name;?></a>

                        </td>
                        <?php }?>
                    
                    </tr>    
                    

                    <tr>

                        <th class="row_title_col">Product Origin</th>

                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>

                            <a href="#"><?= $product_details[$i]->country_name;?></a>

                        </td>
                        <?php }?>

                    </tr>

                    <tr>

                        <th class="row_title_col">Product Type</th>

                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>

                            <a href="#"><?= $product_details[$i]->product_type;?></a>

                        </td>
                        <?php }?>

                    </tr>
                    
                    <tr>

                        <th class="row_title_col">Product Model</th>

                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>

                            <a href="#"><?= $product_details[$i]->product_model;?></a>

                        </td>
                        <?php }?>

                    </tr>
                    
                    <tr>

                        <th class="row_title_col">FOB Price</th>

                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td class="total">
                            <?php echo $product_details[$i]->currency_symbol.$product_details[$i]->product_fob_price_from; ?>
                            <span style="font-weight: normal;">&nbsp;&nbsp;To&nbsp;&nbsp;</span>
                            <?php echo $product_details[$i]->currency_symbol.$product_details[$i]->product_fob_price_to; ?>
                        </td>
                        <?php }?>

                    </tr>

                    <tr>

                        <th class="row_title_col">Seller</th>
                        
                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>

                            <?= $product_details[$i]->company_name;?>

                        </td>
                        <?php }?>

                    </tr>

                    <tr>

                        <th class="row_title_col">Minimum Order</th>
                        
                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>
                            <?php echo $product_details[$i]->product_minimun_order.'/'.$product_details[$i]->uom_name?>

                        </td>
                        <?php }?>
                       

                    </tr>

                    <tr>

                        <th class="row_title_col">Delievery Time</th>
                        
                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>
                            <?php echo $product_details[$i]->product_delivery_time.'/'.$product_details[$i]->uom_name?>

                        </td>
                        <?php }?>
                        

                    </tr>

                    <tr>

                        <th class="row_title_col">Pakaging Detail</th>
                        
                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>
                            <?php echo $product_details[$i]->packaging_detail;?>

                        </td>
                        <?php }?>

                    </tr>

                    <tr>

                        <th class="row_title_col">Detailed Description</th>
                        
                        <?php for($i=0;$i<sizeof($product_details);$i++){?>
                        <td>
                            <p><?php echo $product_details[$i]->product_detailed_description;?></p>

                        </td>
                        <?php }?>

                    </tr>


                </tbody>

            </table>

        </div>

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>