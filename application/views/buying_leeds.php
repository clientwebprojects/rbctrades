<?php include 'header.php'; ?>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container">

        <div class="section_offset">

            <ul class="breadcrumbs">

                <li><a href="<?= base_url() ?>">Home</a></li>
                <li>Buying Leeds</li>

            </ul>

            <div class="row">

                <aside class="col-md-3 has_mega_menu">

                    <section class="animated transparent" data-animation="fadeInDown">

                        <?php include 'main_menu.php'; ?>
                    </section><!--/ .animated.transparent-->

                </aside><!--/ [col]-->

                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-9" style="background-color:#FFF;padding-top: 20px;">

                    <div>

                        <!-- - - - - - - - - - - - - - Revolution slider - - - - - - - - - - - - - - - - -->

                        <section>
                            <div class="col-md-12" style="margin-top: 30px;">
                                <h1>TOP BUYING LEEDS</h1>

                                <div class="well-none">
                                    <div id="myCarousel" class="carousel slide">

                                        <!-- Carousel items -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_7.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_4.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_3.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_7.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                            </div>
                                            <!--/item-->
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_3.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_3.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_3.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_3.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                            </div>
                                            <!--/item-->
                                            <div class="item">
                                                <div class="row">
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_4.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_4.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_4.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-xs-6">
                                                        <a href="#x" class="thumbnail"><img src="<?= base_url() ?>images/arrivals_img_4.jpg" alt="Image" class="img-responsive"></a>
                                                        <div class="carousel-caption">
                                                            New item for sell very attractive item lorem lipsium
                                                        </div>
                                                        <div>
                                                            <a href="#" style="z-index: 9990;"><i class="icon-money"></i>US $200-600</a>
                                                            <p>MOQ: 1 Set/Sets</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/row-->
                                            </div>
                                            <!--/item-->
                                        </div>
                                        <!--/carousel-inner-->
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="margin-bottom: 62px;"><i class="fa fa-chevron-left fa-4"></i></a>

                                        <a class="right carousel-control" href="#myCarousel" data-slide="next" style="margin-bottom: 62px;"><i class="fa fa-chevron-right fa-4"></i></a>
                                    </div>
                                    <!--/myCarousel-->
                                </div>
                                <!--/well-->
                            </div>
                        </section>
                        <!--/well-->


                        <!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->

                        <section class="section_offset">

                            <div class="clearfix col-md-12"style="margin-top: 30px;margin-bottom: 30px;">

                                <h1>CHOOSE A SELLER MEMBERSHIP</h1>

                                <!-- - - - - - - - - - - - - - Pricing tables - - - - - - - - - - - - - - - - -->

                                <div class="pricing_tables_container">

                                    <!-- - - - - - - - - - - - - - Pricing table - - - - - - - - - - - - - - - - -->

                                    <div class="pricing_table free">

                                        <header>Free</header>

                                        <div class="pt_price">
                                            <div class="price">$0</div>
                                            per month
                                        </div>

                                        <ul class="pt_list">

                                            <li>Up to 50 users</li>
                                            <li>Limited team members</li>
                                            <li>Limited disk space</li>
                                            <li>Custom Domain</li>
                                            <li>PayPal Integration</li>

                                        </ul>

                                        <footer>

                                            <a href="<?= base_url()?>index.php/seller/register_membership" class="button_blue middle_btn">Sign Up</a>

                                        </footer>

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of pricing table - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Pricing table - - - - - - - - - - - - - - - - -->

                                    <div class="pricing_table">

                                        <header>Basic</header>

                                        <div class="pt_price">
                                            <div class="price">$15</div>
                                            per month
                                        </div>

                                        <ul class="pt_list">

                                            <li>Up to 100 users</li>
                                            <li>Limited team members</li>
                                            <li>Limited disk space</li>
                                            <li>Custom Domain</li>
                                            <li>PayPal Integration</li>

                                        </ul>

                                        <footer>

                                            <a href="<?= base_url()?>index.php/seller/register_membership" class="button_blue middle_btn">Sign Up</a>

                                        </footer>

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of pricing table - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Pricing table - - - - - - - - - - - - - - - - -->

                                    <div class="pricing_table">

                                        <header>
                                            Pro

                                            <div class="label_popular">Popular</div>

                                        </header>

                                        <div class="pt_price">
                                            <div class="price">$40</div>
                                            per month
                                        </div>

                                        <ul class="pt_list">

                                            <li>Up to 200 users</li>
                                            <li>Limited team members</li>
                                            <li>Limited disk space</li>
                                            <li>Custom Domain</li>
                                            <li>PayPal Integration</li>

                                        </ul>

                                        <footer>

                                            <a href="<?= base_url()?>index.php/seller/register_membership" class="button_blue middle_btn">Sign Up</a>

                                        </footer>

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of pricing table - - - - - - - - - - - - - - - - -->

                                    <!-- - - - - - - - - - - - - - Pricing table - - - - - - - - - - - - - - - - -->

                                    <div class="pricing_table">

                                        <header>Premium</header>

                                        <div class="pt_price">
                                            <div class="price">$70</div>
                                            per month
                                        </div>

                                        <ul class="pt_list">

                                            <li>Up to 500 users</li>
                                            <li>Limited team members</li>
                                            <li>Limited disk space</li>
                                            <li>Custom Domain</li>
                                            <li>PayPal Integration</li>

                                        </ul>

                                        <footer>

                                            <a href="<?= base_url()?>index.php/seller/register_membership" class="button_blue middle_btn">Sign Up</a>

                                        </footer>

                                    </div>

                                    <!-- - - - - - - - - - - - - - End of pricing table - - - - - - - - - - - - - - - - -->

                                </div>

                                <!-- - - - - - - - - - - - - - End of pricing tables - - - - - - - - - - - - - - - - -->

                            </div>

                        </section>
                    </div><!--/ .animated.transparent-->

                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

            </div><!--/ .row-->

            <section class="section_offset" style="margin-top: 30px;">

                <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->

                <ul class="infoblocks_wrap section_offset five_items">

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-thumbs-up-1"></i>
                            <span class="caption"><b>The Highest Product Quality</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-paper-plane"></i>
                            <span class="caption"><b>Fast &amp; Free Delivery</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-lock"></i>
                            <span class="caption"><b>Safe &amp; Secure Payment</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-diamond"></i>
                            <span class="caption"><b>Get 10% OFF For Reorder</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                    <li>

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <a href="#" class="infoblock type_1">

                            <i class="icon-money"></i>
                            <span class="caption"><b>100% Money back Guaranted</b></span>

                        </a><!--/ .infoblock-->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </li>

                </ul><!--/ .infoblocks_wrap.section_offset.clearfix-->

                <!-- - - - - - - - - - - - - - End of infoblocks - - - - - - - - - - - - - - - - -->

            </section><!--/ .section_offset -->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->




    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>