<?php include 'header2.php';?>
<script>
    document.getElementById("account_setting").className = "active-seller-menu";
</script>
<div class="page_wrapper type_2" >
    <div class="container">
            <div class="row">
                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                    <?php include 'inc/seller_account_menu.php'; ?>
                    <script>
                        document.getElementById("change_email_address").className = "active";
                    </script>
                    <div class="col-md-10">

                            <form id="account_setting_change_email" class="form-horizontal" method="post" action="" name="member_profile_form" enctype="">

                                    <h4 class="heading">Change Email Address <span class="small-desc"></span></h4>

                                    <fieldset style="border: 1px dotted #bebebe;border-radius: 3px;padding: 10px">


                            <div class="form-group">
                                <label class="col-md-3 control-label color-333">Current Email</label>
                                <div class="col-md-6 color-333 font12" style="padding-top: 3px;">
                                    <?php echo $this->session->userdata('email'); ?>
                                </div>
                            </div>
                            <div id="job_title" class="form-group">
                                <label class="col-md-3 control-label color-333">New Email</label>
                                <div class="col-md-4">
                                    <input name="email" type="text" placeholder="Email" class="form-control input-md"  >
                                </div>
                            </div>
                            <div id="job_title" class="form-group">
                                <label class="col-md-3 control-label color-333">Re Enter New Email</label>
                                <div class="col-md-4">
                                    <input name="confirm_email" type="text" placeholder="Re Enter Email" class="form-control input-md"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label color-333">Security Question</label>
                                <div class="col-md-4 font12">
                                    <select class="form-control" name="security_question_id">
                                        <option value="">--Please Select One</option>
                                        <?php foreach($questions as $values){?>
                                            <option value="<?= $values->id;?>" ><?= $values->security_question;?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div id="job_title" class="form-group">
                                <label class="col-md-3 control-label color-333">Answer</label>
                                <div class="col-md-4">
                                    <input name="security_answer" type="text" placeholder="Answer" class="form-control input-md"  >
                                </div>
                            </div>
                        </fieldset>
                                    <br/>
                                    <div class="form-group">
                                    <div class="col-md-12">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9" style="margin-top: 15px;">
                                        <input type="submit" class="button_blue" id="submit_company_information" name="update_email" value="Submit">
                                    </div>
                                    </div>
                        </div>
                            </form>

                        </div>
                </div><!--/ [col]-->
                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
            </div><!--/ .row-->
        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>