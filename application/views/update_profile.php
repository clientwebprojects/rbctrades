<?php include 'header2.php'; ?>
<script>
    document.getElementById("account_setting").className = "active-seller-menu";
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">

            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-9" style="background-color:#FFF;padding-top: 20px;">

                    <div class="col-sm-2 col-md-12 col-lg-12" style="border:1px #ccc dotted;padding: 10px; margin: 10px">

                        <form class="form-horizontal" id="register-user-form" action="" method="post">

                            <fieldset>
                                <input type="hidden"  name="user_id" value="<?= $user->user_id ?>">
                                <input type="hidden"  name="member_id" value="<?= $user->member_id ?>">
                                <!-- Text input-->
                                <br>
                                <h4 style="margin-left: 70px">Basic Information</h4>
                                <br>
                                <div class="form-group">
                                    <label class="col-sm-2 col-md-4 control-label" for="user_email">Email</label>  
                                    <div class="col-sm-2 col-md-4">
                                        <input id="user_email" name="user_email" type="text" placeholder="Enter Your Email" class="form-control input-md" required="" value="<?= $user->user_email; ?>">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 col-md-4 control-label" for="alternative_email">Alternate Email</label>  
                                    <div class="col-sm-2 col-md-4">
                                        <input id="alternative_email" name="alternative_email" type="text" placeholder="Alternate Email" class="form-control input-md" required="" value="<?= $user->alternative_email; ?>">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-sm-2 col-md-4 control-label" for="product_keyword">Password</label>  
                                    <div class="col-sm-2 col-md-4">

                                        <a class="btn button_blue change-password-btn">Change Password</a>

                                    </div>
                                </div>
                                
                                <div id="change-password-div" style="display: none">
                                    <div class="form-group">
                                        <label class="col-sm-2 col-md-4 control-label" for="product_keyword">Update Password</label>  
                                        <div class="col-sm-2 col-md-4">
                                            <input id="create_password" name="user_password" type="password" placeholder="Enter Strong Pasword" class="form-control input-md">

                                        </div>
                                    </div>

                                    <!-- File Button --> 
                                    <div class="form-group">
                                        <label class="col-sm-2 col-md-4 control-label" for="filebutton">Re Enter Password</label>
                                        <div class="col-sm-2 col-md-4">
                                            <input id="confirm_password" name="confirm_password" type="password" placeholder="Re Enter Password" class="form-control input-md">
                                        </div>
                                    </div>
                                </div>    
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user_type">Gender</label>
                                    <div class="col-md-6 control-label" >
                                        <label class="radio-inline" style="padding-top: 0px">
                                            <input <?php if (strcmp($user->gender, "male") != 1) {
                                                            echo "checked";
                                                        } ?> 
                                                type="radio" name="gender" id="male"  value="female" style="display: block; width: 30px;margin-top: 0px;">Male
                                        </label>
                                        <label class="radio-inline" style="padding-top: 0px">
                                            <input <?php if (strcmp($user->gender, "female") != 1) {
                                                            echo "checked";
                                                        } ?>  
                                                type="radio" name="gender" id="female"  value="male" style="display: block;width: 30px;margin-top: 0px;">Female
                                        </label>
                                    </div>
                                </div>
                                
                                
                                <br>
                                <hr>
                                <br>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="registration_location">Business Location</label>
                                    <div class="col-md-4">
                                        <select id="registration_location" name="registration_location" class="form-control">
                                            <option value="">---Select Country---</option>
                                            <?php foreach ($countries as $country) { ?>
                                                    <option value="<?= $country->country_id; ?>" 
                                                    <?php if ((int) $company->seller_location == (int) $country->country_id) {
                                                            echo "selected";
                                                          } ?>><?= $country->country_name; ?> </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="user_type">I am a</label>
                                    <div class="col-md-6 control-label" >
                                        <label class="radio-inline" style="padding-top: 0px">
                                            <input <?php if (strcmp($user->user_type, "supplier") != 1) {
                                                            echo "checked";
                                                        } ?> 
                                                type="radio" name="member_type" id="supplier"  value="supplier" style="display: block; width: 30px;margin-top: 0px;">Supplier
                                        </label>
                                        <label class="radio-inline" style="padding-top: 0px">
                                            <input <?php if (strcmp($user->user_type, "buyer") != 1) {
                                                            echo "checked";
                                                        } ?>  
                                                type="radio" name="member_type" id="buyer"  value="buyer" style="display: block;width: 30px;margin-top: 0px;">Buyer
                                        </label>
                                        <label class="radio-inline" style="padding-top: 0px">
                                            <input <?php if (strcmp($user->user_type, "both") != 1) {
                                                            echo "checked";
                                                        } ?> 
                                                type="radio" name="member_type" id="both"  value="both" style="display: block;width: 30px;margin-top: 0px;">Both
                                        </label>
                                    </div>
                                </div>

                                <br>
                                <h4 style="margin-left: 70px">Contact Information</h4>
                                <br>

                                <!-- Search input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="member_name">Contact Name</label>
                                    <div class="col-md-4">
                                        <input id="member_name" name="member_name" type="text" placeholder="Full Name" class="form-control input-md" value="<?= $user->user_name; ?>">

                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label class="col-sm-2 col-md-4 control-label" >Address</label>
                                    <div class="col-md-8">
                                        <div class="col-md-2"><label class="control-label" >Street</label></div>
                                        <div class="col-md-6" >
                                            <input id="street_adddress" name="street_adddress" type="text" placeholder="Street" class="form-control input-md" value="<?= $user->street_adddress; ?>">
                                        </div>
                                        <div class="col-md-2" style="clear: left;margin-top: 5px"><label for="city" class="control-label" >City</label></div>
                                        <div class="col-md-6" style="margin-top: 5px;">
                                            <input id="city" name="city" type="text" placeholder="City" class="form-control input-md" value="<?= $user->city; ?>">
                                        </div>
                                        <div class="col-md-2" style="clear: left;margin-top: 5px"><label for="province" class="control-label" >Province</label></div>
                                        <div class="col-md-6" style="margin-top: 5px;">
                                            <input id="province" name="province" type="text" placeholder="City" class="form-control input-md" value="<?= $user->province; ?>">
                                        </div>
                                        <div class="col-md-2" style="clear: left;margin-top: 5px"><label for="country" class="control-label" >Country</label></div>
                                        <div class="col-md-6" style="margin-top: 5px;">
                                            <select id="country" name="country" class="form-control">
                                                <option value="">---Country/Region---</option>
                                                <?php foreach ($countries as $country) { ?>
                                                    <option value="<?= $country->country_id; ?>" <?php if ($country->country_id == $user->country) {
                                                    echo "selected=selected";
                                                } ?>>
                                                    <?= $country->country_name; ?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2" style="clear: left;margin-top: 5px"><label for="zip_code" class="control-label">Zip</label></div>
                                        <div class="col-md-6" style="margin-top: 5px;">
                                            <input id="zip_code" name="zip_code" type="text" placeholder="Zip/postal Code" class="form-control input-md" value="<?= $user->zip; ?>">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 col-md-4 control-label" for="phone">Phone</label>  
                                    <div class="col-sm-2 col-md-4">
                                        <input id="phone" name="phone" type="text" placeholder="Enter Phone No." class="form-control input-md" required="" value="<?= $user->phone; ?>">

                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 col-md-4 control-label" for="fax">Fax</label>  
                                    <div class="col-sm-2 col-md-4">
                                        <input id="fax" name="fax" type="text" placeholder="Fax Number" class="form-control input-md" required="" value="<?= $user->fax; ?>">

                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 col-md-4 control-label" for="mobile">Mobile</label>  
                                    <div class="col-sm-2 col-md-4">
                                        <input id="mobile" name="mobile" type="text" placeholder="Mobile No." class="form-control input-md" required="" value="<?= $user->mobile; ?>">

                                    </div>
                                </div>
                                
                                <!-- Text input-->

                                <div class="form-group" id="supplier-div" style="display: block">
                                    <label class="col-md-4 control-label" for="seller_company">Company Name</label>
                                    <div class="col-md-4">
                                        <input id="seller_company" name="seller_company" type="text" placeholder="Complete Company Name" class="form-control input-md" value="<?= $company->seller_company; ?>">

                                    </div>
                                </div>

                                <!-- Text input-->
<!--                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="seller_contact_no">Contact No</label>  
                                    <div class="col-md-4">
                                        <input id="seller_contact_no" name="seller_contact_no" type="text" placeholder="Number" class="form-control input-md" value="<?= $company->seller_contact_no; ?>"> 

                                    </div>
                                </div>-->

                                <div class="form-group">

                                    <div class="col-md-12 center-align">
                                        <input type="submit" id="submit" name="update" value="Update" class="button_blue middle_btn ">
                                    </div>
                                </div>

                            </fieldset>
                        </form>

                    </div><!--/ .animated.transparent-->


                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

                <aside class="col-md-3">

                    <section>



                        <div class="theme_box">

                            <h3 ><a class="link urrent open_" style="color:#DFA239;cursor:pointer;"><?= $company->seller_company; ?></a></h3>
                            <hr style="border:1px #ccc dotted;margin-bottom: 5px;">   
                            <!-- - - - - - - - - - - - - - Progress Bar - - - - - - - - - - - - - - - - -->

                            <div style="border:0px"><?php
if ($company->level_id == 1) {
    echo "Unverified Member";
}
?>
                                <span style="float:right;cursor:pointer;"><a>Upgrade Now</a></span>
                            </div>

                            <div class="progress_bar">

                                <img width="60" height="60" src="<?= base_url() ?>/images/comment_author_photo.jpg" alt=""/>
                                <span style="margin-left:5px;"><?= $company->seller_name; ?></span>                             

                            </div>

                            <p class="pb_title">Last Signed In:&nbsp;&nbsp;&nbsp;<span style="font-weight:400;color: #000;">Pakistan</span></p>
                            <p class="pb_title"><span style="font-weight:400;color: #000;">2015-4-14 12:11:25 (GMT +5)</span></p>                           
                            <a class="current open_" style="cursor:pointer;float: right">Update Profile</a>

                            <!-- - - - - - - - - - - - - - End of progress bar - - - - - - - - - - - - - - - - -->

                            <!-- - - - - - - - - - - - - - Progress Bar - - - - - - - - - - - - - - - - -->

                        </div>
                        <div class="theme_box" style="margin-top:15px;">
                            <h4 style="color:#DFA239;">Quick Access</h4>
                            <ul style="list-style-type: square; margin-left: 10px;">

                                <li><a href="<?= base_url() ?>index.php/seller/register_company/">Check New Messages</a></li>
                                <li><a href="<?= base_url() ?>index.php/seller/seller_profile/">Display New Products</a></li>
                                <li><a href="<?= base_url() ?>index.php/seller/manage_products/">Find Buying Request</a></li>
                                <li><a href="<?= base_url() ?>index.php/seller/manage_products/">Listing Optimization Tool</a></li>

                            </ul>
                        </div>

                        <div class="theme_box" style="margin-top:15px;">
                            <h4 style="color:#DFA239;">Help Us Improve</h4>
                            <p class="pb_title">How would you rate your overall satisfaction with this page?</p>



                            <ul class="checkboxes_list">

                                <li>
                                    <label class="radio-inline" >
                                        <input checked type="radio" name="help" id="satisfied" value="satisfied" style=" width: 30px;">Satisfied
                                    </label>
                                </li>

                                <li>

                                    <label class="radio-inline" >
                                        <input type="radio" name="help" id="neutral" value="neutral" style=" width: 30px;">Neutral
                                    </label>
                                </li>

                                <li>
                                    <label class="radio-inline" >
                                        <input  type="radio" name="help" id="dissatisfied" value="dissatisfied" style=" width: 30px;">Dis-satisfied
                                    </label>
                                </li>
                            </ul>
                            <textarea class="form-control" rows="7" placeholder="Leave your Comments and Suggestions Here"></textarea><br>
                            <input  type="submit" name="help" class="button_blue" value="submit">
                        </div>

                    </section>



                    <!-- - - - - - - - - - - - - - Already viewed products - - - - - - - - - - - - - - - - -->



                </aside>

            </div><!--/ .row-->

            <section class="section_offset" style="margin-top: 30px;">

                <!-- - - - - - - - - - - - - - Infoblocks v3 - - - - - - - - - - - - - - - - -->

                <div class="row">

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-thumbs-up-1"></i>

                                <h4 class="caption">The Highest<br>Product Quality</h4>

                            </div>

                            <p>Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-money"></i>

                                <h4 class="caption">100% Money<br>Back Guaranteed</h4>

                            </div>

                            <p>Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                    <div class="col-sm-4">

                        <!-- - - - - - - - - - - - - - Infoblock - - - - - - - - - - - - - - - - -->

                        <section class="infoblock type_3">

                            <div class="clearfix">

                                <i class="icon-lock"></i>

                                <h4 class="caption">Safe &amp; Secure<br>Payment</h4>

                            </div>

                            <p>Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. <a href="#">Read More.</a></p>

                        </section><!--/ .infoblock.type_3 -->

                        <!-- - - - - - - - - - - - - - End of infoblock - - - - - - - - - - - - - - - - -->

                    </div><!--/ [col]-->

                </div><!--/ .row -->

                <!-- - - - - - - - - - - - - - End of infoblocks v3 - - - - - - - - - - - - - - - - -->

            </section><!--/ .section_offset -->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->




    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>