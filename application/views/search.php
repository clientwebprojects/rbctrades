<?php include 'header.php';
//echo '<pre>';
//$this->session->unset_userdata('product_ids');$this->session->unset_userdata('compare_count');
//print_r($breadcrums);
//echo '</pre>';
?>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
<script type="text/javascript">
    
    var term = "<?= $term; ?>";
    var search_by = "<?= $search_by; ?>";
    console.log("Term : "+term);
    console.log("Search By : "+search_by);
    
    var base_url = "<?= base_url(); ?>";
    var category_filter = false;
    var category_filter_id = 0;
    var category_search = false;
    var country_filter = false;
    var country_id;
    var pathArray = window.location.pathname.split( '/' );
    var page_number = 0;
    var total_page = null;
    var open_status_id;
//    var comparison_counter = <?= $this->session->userdata('compare_count');?>
</script>
<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="<?= base_url() ?>">Home</a></li>
            <li>Search <?= $term; ?> in <?= strtoupper($search_by); ?></li>
        </ul>

        <div class="row">
            <?php include 'inc/product_filter.php'; ?>

            <main class = "col-md-9 col-sm-9">

                <div class="section_offset">

                    <header class="top_box on_the_sides">

                        <div class="left_side clearfix v_centered">

                            <div class="v_centered">

                                <span>Filter by:</span>
                                <div class="form-group">
                                    <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Trade Assurance</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Gold Supplier</label>
                                    <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Assessed Supplier</label>

                                </div>

                            </div>
                        </div>
                        <div class="right_side">

                            <div class="layout_type buttons_row" data-table-container="#products_container">

                                <a href="#" data-table-layout="grid_view" class="button_grey middle_btn icon_btn tooltip_container"><i class="icon-th"></i><span class="tooltip top">Grid View</span></a>

                                <a href="#" data-table-layout="list_view list_view_products" class="button_grey middle_btn icon_btn active tooltip_container"><i class="icon-th-list"></i><span class="tooltip top">List View</span></a>

                            </div>

                        </div>

                    </header>

                    <div class="table_layout list_view list_view_products" id="products_container">
                        <div class="ajax-content-loading">
                            <img src="<?php echo base_url()?>images/icon_loader.gif" alt="">
                        </div>
                        <div id="outerdiv">

                    </div>
                    </div><!--/ .table_layout -->

                    <footer class="bottom_box on_the_sides" id="footer_id">

                        <div class="left_side">

                            <lable>Page <lable id="page_number"></lable> of <lable id="total_page"></lable></lable>
                        </div>

                        <div class="right_side">

                            <div class="btn-group btn-group-sm">
                                <button type="button" id="previous" class="btn btn-default">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </button>
                                <button type="button" id="next" class="btn btn-default">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </button>
                            </div>

                        </div>

                    </footer>

                        </div>

                <!-- - - - - - - - - - - - - - End of products - - - - - - - - - - - - - - - - -->

            </main>

        </div><!--/ .row -->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->

<!-- - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->
<script>
    $(document).ready(function () {

        $(".country-list").on('click', (function (e) {

           country_id=$(this).attr("id");
           country_filter =  true;
           category_filter = false;
           category_search = false;
           page_number=0;
           getReport(page_number);
       }));



    });

    $(document).ready(function () {

        $("#ajaxcall_select_category").on('change', (function (e) {
            var id = $("#ajaxcall_select_category option:selected").val();
            category_filter = true;
            country_filter =  false;
            category_search = false;
            page_number=0;
            getReport(page_number);

        }));

    });

</script>

<script>

    var start_html = '<div id="product-row"> ';
    var end_html = '</div></div>';
    var product_row = '<div class="table_row">';

    var getReport = function (page_number) {
//        console.log('Total Pages = '+total_page);
//        console.log('Page number = '+page_number);
         if (page_number == 0) {
            $("#previous").prop('disabled', true);
        }
        else {
            $("#previous").prop('disabled', false);
        }

        if (page_number == (total_page - 1)) {
            $("#next").prop('disabled', true);
        }
        else {
            $("#next").prop('disabled', false);
        }

        $("#page_number").text(page_number + 1);

        $(".ajax-content-loading").show();
        $('#product-row').remove();
        $('#outerdiv').empty();
        $('#footer_id').hide();
            var count = 1;
        category_filter_id = $("#ajaxcall_select_category option:selected").val();
        
      if(search_by == "products"){
          console.log("1st if");
        var params = {
            type: "POST",
            cache: false,
            dataType: 'json',
            success: function (data) {
                    $('#outerdiv').append(start_html);
                    $('#product-row').append(product_row);
                    $(".ajax-content-loading").hide();
                $('#footer_id').show();

                window.mydata = data;
                total_page = mydata[0].TotalRows;
                $("#total_page").text(total_page);
                var record_par_page = mydata[0].Rows;
                if (page_number == 0) {
                    $("#previous").prop('disabled', true);
                }
                else {
                    $("#previous").prop('disabled', false);
                }

                if (page_number == (total_page - 1)) {
                    $("#next").prop('disabled', true);
                }
                else {
                    $("#next").prop('disabled', false);
                }

                $("#page_number").text(page_number + 1);
                $.each(record_par_page, function (key, data) {
                    //sr = (key + 1);
                    var product_listing = '<div class="table_cell">' +
                            '<div class="product_item">' +
                            '<a class="image_wrap" href="' + base_url + 'index.php/buyer/product_detail/' + data.product_id + '">' +
                            '<img src="' + image_src(data.images_url) + '" alt="" width="245" height="243">' +
                            '</a>' +
                            '<div class="description">' +
                            '<a href="' + base_url + 'index.php/buyer/product_detail/' + data.product_id + '">' + data.product_name + '</a>' +
                                '<li class="seller_info_wrap" style="list-style-type:none">'+
                                        '<span class="pull-right">'+
                                            '<a class="site_settings compare_product '+check_session(data.in_session,data.product_id,'add')+'" id=gridadd_' + data.product_id + '><i class="glyphicon glyphicon-plus"></i></a>'+
                                            '<a class="site_settings remove '+check_session(data.in_session,data.product_id,'remove')+'" id=gridremove_' + data.product_id + '><i class="glyphicon glyphicon-minus"></i></a>'+
                                            '<a class="btn btn-primary btn-xs comparre_status_btn" id="gridbtn_'+ data.product_id +'" title="compare" style="color:#FFF;z-index:999">Compare</a>'+
                                        '</span>'+
                                       '<div class="seller_info_dropdown" id="gridstatus_'+ data.product_id +'" style="padding: 11px 34px 10px;width: 160px;top: 30px;left: 14px;">'+
                                            '<a class="close_status" id="close_'+data.product_id+'"><img src="<?= base_url('images/x.png')?>" width="20" height="20" style="left:20px"/></a>'+
                                           '<ul class="seller_stats compare_status">'+
                                               '<li id="comparison_counter'+data.product_id+'">'+
                                                   ''+data.compare_count+'/3 selected'+
                                               '</li>'+
                                           '</ul>'+
                                           '<div class="v_centered">'+ 
                                               '<a href="<?= base_url('index.php/main/compare_product')?>" class="btn btn-xs button_blue compare-now '+check_count(data.compare_count)+'" style="color:#FFF">Compare Now</a>'+
                                           '</div>'+
                                       '</div>'+
                                   '</li>'+
                            '<div class="clearfix product_info">' +
                            '<p class="alignleft"><span class="color-000 font12">' + data.minimun_order_unit + '' + data.uom_name + '</span> <span class="small-desc">(Min. Order)</span></p>' +
                            '</div>' +
                            '</div>' +
                            '<div class="full_description">' +
                            '<a class="product_title" href="' + base_url + 'index.php/buyer/product_detail/' + data.product_id + '">' + data.product_name + '</a>' +
                            '<p class="product_category">' + data.category_name + '</p>' +
                            '<p>' + data.product_description + '</p>' +
                            '</div>' +
                            '<div class="actions">' +
                            '<ul class="seller_stats">' +
                            '<li class="color-gold">' + data.company_name + '</li>' +
                            '<li>' + data.country_name + '(' + data.operational_address_city + ')' + ' </li>' +
//                            '<li><span class="bold">99.8%</span> <span class="font12">Positive Feedback</span></li>' +
                            '<li><br/></li>' +
                            '<li><a href="" class="btn btn-xs btn-primary">Contact Seller</a></li>' +
                                    '<li class="seller_info_wrap" style="list-style-type:none">'+
                                        '<span >'+
                                            '<a class="site_settings compare_product '+check_session(data.in_session,data.product_id,'add')+'" id=listadd_' + data.product_id + '><i class="glyphicon glyphicon-plus"></i></a>'+
                                            '<a class="site_settings remove '+check_session(data.in_session,data.product_id,'remove')+'" id=listremove_' + data.product_id + '><i class="glyphicon glyphicon-minus"></i></a>'+
                                            '<a class="btn btn-primary btn-xs comparre_status_btn" id="listbtn_'+ data.product_id +'" title="compare" style="color:#FFF;z-index:999">Compare</a>'+
                                        '</span>'+
                                       '<div class="seller_info_dropdown" id="liststatus_'+ data.product_id +'" style="padding: 11px 34px 10px;width: 160px;top: 204px;left: 14px;">'+
                                           '<ul class="seller_stats compare_status">'+
                                               '<li id="comparison_counter'+data.product_id+'">'+
                                                   ''+data.compare_count+'/3 selected'+
                                               '</li>'+
                                           '</ul>'+
                                           '<div class="v_centered">'+ 
                                               '<a href="<?= base_url('index.php/main/compare_product')?>" class="btn btn-xs button_blue compare-now '+check_count(data.compare_count)+'" style="color:#FFF">Compare Now</a>'+
                                           '</div>'+
                                       '</div>'+
                                   '</li>'+
                            '</ul>' +
                            '</div>' +
                            '</div>' +
                            '</div>' + new_row(count);
                    $("#product-row").append(product_listing);
                    count++;
                });
                $("#outerdiv").append(end_html);
                },
                error: function () {
                            var error_msg = '<div style="padding:30px 0 30px 10px;border:1px solid #eaeaea"> <b>Sorry, we could not find any results that matched your query.</b><br/>Use the options above or main menu to redefine your search. </div>';
                            $(".ajax-content-loading").hide();
                            $("#outerdiv").append(error_msg);
                }


        }; }
        
        else {
         console.log("else");
        var params = {
            type: "POST",
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#outerdiv').append(start_html);
                $('#product-row').append(product_row);
                $(".ajax-content-loading").hide();
                $('#footer_id').show();
                window.mydata = data;
                total_page = mydata[0].TotalRows;
//                console.log('Total Pages = '+total_page);
//                console.log('Total Pages = '+data);
                $("#total_page").text(total_page);
                var record_par_page = mydata[0].Rows;
                if (page_number == 0) {
                    $("#previous").prop('disabled', true);
                }
                else {
                    $("#previous").prop('disabled', false);
                }

                if (page_number == (total_page - 1)) {
                    $("#next").prop('disabled', true);
                }
                else {
                    $("#next").prop('disabled', false);
                }

                $("#page_number").text(page_number + 1);

                $.each(record_par_page, function (i, v) {
//                    console.log(v);
                      var subCategory_html =
                                '<div class="table_cell">' +
                                '<div class="product_item">' +
                                '<div class="image_wrap">' +
                                '<img src="' + image_quote_src(v.rfq_product_image) + '" alt="" width="245" height="243">' +
                                '<div class="actions_wrap">' +
                                '<div class="centered_buttons">' +
                                '<a href="' + base_url + 'index.php/main/quotation_detail/' + v.rfq_id + '" class="btn btn-sm btn-default quick_view" data-modal-url="modals/quick_view.html">View</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="full_description">' +
                                '<a href="#" class="product_title">' + v.rfq_product_name + '</a>' +
                                '<p class="font12 color-333"> ' + v.rfq_product_description + '</p>' +
                                '<p class="font12 color-999">Quantity Required: <span class="color-333">' + v.rfq_product_quantity + ' ' + v.uom_name + '</span></p>' +
                                '<span class="font12 color-999">Date Posted: ' + v.rfq_quotation_date + '</span>' +
                                '</div>' +
                                '<div class="actions">' +
                                '<ul class="seller_stats">' +
                                '<li><img src="' +base_url+'images/flags/'+ v.country_code + '.png"/> ' + v.country_name + '</li>' +
                                '<li><a href="' + base_url + 'index.php/seller/seller_quotation/' + v.rfq_id + '"'+ allowed(v.rfq_quotes_left)+'class="btn btn-xs btn-primary">Quote Now</a></li>' +
                                '<li class="font12"> Quotes Left :  <span class="success" style="font-weight:bold">' + v.rfq_quotes_left + ' </span></li>' +
                                '</ul>' +
                                '</div>' +
                                '</div>' +
                                '</div>' + new_row(count);
                        $('#product-row').append(subCategory_html);
                        count++;


                    $('#outerdiv').append(end_html);
                });

            },
            error: function () {
                            var error_msg = '<div style="padding:30px 0 30px 10px;border:1px solid #eaeaea"> <b>Sorry, we could not find any results that matched your query.</b><br/>Use the options above or main menu to redefine your search. </div>';
                            $(".ajax-content-loading").hide();
                            $("#outerdiv").append(error_msg);
                }
        };
        
        }

        
            params.url = "<?php echo base_url('index.php/main/pagination_product_seraching'); ?>";
            var data = {"page_number": page_number,"term": term, "search_by": search_by};
            params.data = data;

      

        $.ajax(params);
//        console.log('Total Pages = '+total_page);
//        console.log('Page number = '+page_number);






    };

    $(document).ready(function (e) {

    $(".ajax-content-loading").hide();

var isnum = /^\d+$/.test(pathArray[6]);

    if(isnum===false){
            getReport(page_number);
        }
    else{
        category_search=true;
            getReport(page_number);
        }


        $("#next").on("click", function () {
            $("#product-row").html("");
            page_number = (page_number + 1);
            getReport(page_number);

        });

        $("#previous").on("click", function () {
            $("#product-row").html("");
            page_number = (page_number - 1);
            getReport(page_number);
        });
    });
</script>

<script type="text/javascript">
    function image_src(src) {
        if (jQuery.isEmptyObject(src)) {
            return base_url + "images/no_image.jpg";
        } else {
            return src[0].image_url;
        }
    }
    function image_quote_src(src) {
        if (jQuery.isEmptyObject(src)) {
            return base_url + "images/no_image.jpg";
        } else {
            return src;
        }
    }
    function allowed(value) {
        if (value>0) {
            return "";
        } else {
            return "disabled ";
        }
    }
    function new_row(count) {
        if (count % 4 === 0) {

            return '</div><div class="table-row">';
        } else {
            return "";
        }
    }
    function check_session(session,id,value){
        if( value==='remove'){
            if(session==true)
            {
                return "";

            }else{
                return "hidden";
            }

        }
        if( value==='add'){
            if(session==true)
            {
                return "hidden";
            }
            else{
                return "";
            }
        }
    }
    function check_count(count){
        //console.log(count);
        if(count<2){
            return "disabled";
        }
    }
</script>
<script>
    $(document).ready(function () {
        $(document).on('click', '.compare_product', function () {
            var id = $(this).attr("id");
            //var name = $(this).prev('.p_name').html();
            var id_array = id.split("_");
            console.log(id);
            //console.log(id);
            $.ajax({
                url: '<?= base_url('index.php/main/set_compare_session'); ?>',
                type: 'POST',
                data: {'id': id_array[1]}, // An object with the key 'submit' and value 'true;
                success: function (result) {
                    $('#listadd_'+id_array[1]).addClass('hidden');
                    $('#gridadd_'+id_array[1]).addClass('hidden');
                    $('#listremove_'+id_array[1]).removeClass('hidden');
                    $('#gridremove_'+id_array[1]).removeClass('hidden');
                    $('#liststatus_'+id_array[1]).css("display","block");
                    $('#gridtstatus_'+id_array[1]).css("display","block");
                    comparison_counter++;
                    $('#comparison_counter'+id_array[1]).text(comparison_counter+"/3 Selected");
                    console.log(result);
                    if (result === "1") {
                        location.href = "<?= base_url('index.php/main/compare_product'); ?>";

                    }

                }
            });
        });
        $(document).on('click','.remove',function(){
            var id = $(this).attr("id");
            var id_array = id.split("_");
            $.ajax({
                url: '<?= base_url('index.php/main/remove_session_value'); ?>',
                type: 'POST',
                data: {'id': id_array[1]}, // An object with the key 'submit' and value 'true;
                success: function (result) {
                    $("#listadd_"+id_array[1]).removeClass('hidden');
                    $("#gridadd_"+id_array[1]).removeClass('hidden');
                    $("#liststatus_"+id_array[1]).removeClass('hidden');
                    $("#gridstatus_"+id_array[1]).removeClass('hidden');
                    $("#listremove_"+id_array[1]).addClass('hidden');
                    $("#gridremove_"+id_array[1]).addClass('hidden');
                    comparison_counter--;
                    $('#comparison_counter'+id_array[1]).text(comparison_counter+"/3 Selected");
                    console.log(comparison_counter);
                }
            });

        });
        $(document).on('click','.comparre_status_btn',function(){
            var id = $(this).attr("id");
            var id_array = id.split("_");
            console.log(id_array);
            $("#liststatus_"+id_array[1]).css("display","block");
            $("#gridstatus_"+id_array[1]).css("display","block");
            open_status_id = "gridstatus_"+id_array[1];
        });
//        $(document).click(function(e){
//            if(open_status_id){
//                var id_array = open_status_id.split("_");
//                var status = $("#status_"+id_array[1]);
//                //console.log(id_array[1]);
//                if(!status.is(e.target) && status.has(e.target).length===0){
//                    $('.seller_info_dropdown').hide(); //hide the button
//
//                }else{
//                    $('.seller_info_dropdown').css("display","block");
//                }
//            }
//        });





    });
</script>
<?php include 'footer.php'; ?>
