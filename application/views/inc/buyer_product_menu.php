<section class="col-sm-2" style="border-right: 1px solid #ddd;padding-right: 0;">
    <div class="product_menu">
    <h4>Buyers Profile</h4>
            <ul>
                <li id="buyer_home"><a href="<?= base_url() ?>index.php/buyer/buyer_profile">Home</a></li>
                <li id="buying_request"><a href="<?= base_url() ?>index.php/buyer/buying_requests">Buying Request</a></li>
                <li id="view_rfqs"><a href="<?= base_url() ?>index.php/seller/manage_products/">Message & Contacts</a></li>

            </ul>
    </div>
    <br/>
    <hr/>
    <div style="margin-top: 15px">
        <a href="" class="button_blue">Get Verified</a><br/><br/>
        <p style="text-align: justify;font-size: 12px;padding-right: 10px">Become a  Verified member to get higher rankings for your products in search result as a supplier; and enhanced sourcing service as a buyer.</p>
    </div>
    <div style="margin-top: 15px">
        <a href="" class="button_blue">Upgrade Now</a><br/><br/>
        <p style="text-align: justify;font-size: 12px;padding-right: 10px">Sign up for Gold Supplier to get 22x more inquiries than Free Members.</p>
    </div>

</section>