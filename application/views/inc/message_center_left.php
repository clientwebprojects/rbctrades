<a href="<?= base_url() ?>/index.php/message_center/message_center_new_inquiry" class="btn button_black btn-sm btn-block" role="button"><i class="glyphicon glyphicon-edit"></i> New Inquiry</a>
<hr>
<ul class="nav nav-pills nav-stacked">
    <li id="inbox"><a href="<?= base_url() ?>/index.php/seller/message_center"> All Inquiries </a></li>
    <li id="important_box"><a href="<?= base_url() ?>/index.php/message_center/get_important_inquiries">Important</a></li>
    <li id="sent_box"><a href="<?= base_url() ?>/index.php/message_center/get_sent_inquiries">Sent Box</a></li>
    <li id="trash_box"><a href="<?= base_url() ?>/index.php/message_center/get_trash_inquiries">Trash</a></li>
</ul>