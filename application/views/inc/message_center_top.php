<div class="btn-group">
                                <select class="form-control">
                                    <option value="1"><a href="#">All</a></option>
                                <option value="2"><a href="#">Read</a></option>
                                <option value="3"><a href="#">Unread</a></option>
                                </select>
                            </div>
                            <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Refresh">
                                <span class="glyphicon glyphicon-refresh"></span></button>
<button type="button" class="btn btn-default btn-sm" data-toggle="Delete" id="trash_all" title="Delete">Delete</button>
                            <div class="btn-group">
                                <select class="form-control" id="more_functions">
                                <option value="0"><a href="#" >More</a></option>
                                <option value="1" id="important_selected">Mark Important</option>
                                <option value="2" id="mark_read">Mark as Read</option>
                                <option value="3">Mark as Unread</option>
                                </select>
                            </div>
                            <div class="pull-right pagination_footer">
                                <lable>Page <lable id="page_number"></lable> of <lable id="total_page"></lable></lable>
                                <div class="btn-group btn-group-sm">
                                    <button type="button" id="previous" class="btn btn-default">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </button>
                                    <button type="button" id="next" class="btn btn-default">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </button>
                                </div>
                            </div>