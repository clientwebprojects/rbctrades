<section class="col-sm-3" style="background: #f9f9f9;padding:10px;border: 1px solid #dae2ed;border-radius: 3px 3px 0 0;">
    <div class="product_menu">
        <h4>Categories</h4>
        <select name="category_list" id="ajaxcall_select_category" class="form-control" >
            <option value="0" id="ajaxcall_select_category"  >All Category</option>
            <?php
            foreach ($categories as $category) {?>
         
            <option value = "<?= $category->category_id; ?>"  ><?= $category->category_name; ?></option>
                    <?php
            }
            ?>
        </select>
    </div>
    <br/>
    <hr>
    <br/>
    <div class="product_menu">
        <h4>Region/Country</h4>
        <ul style="max-height: 162px;overflow: hidden;overflow-y: auto;">
            <li><a class="country-list bold" id="0" style="cursor:pointer">All Countries</a></li>
            <?php
            foreach ($countries as $country) {
                ?>

            <li><a class="country-list" style="cursor:pointer" id="<?= $country->country_id; ?>"><?= $country->country_name; ?></a></li>

                <?php
            }
            ?>
        </ul>

    </div>

<!--    <section class = "section_offset">

        <h3>Tags</h3>

        <div class = "tags_container">

            <ul class = "tags_cloud">

                <li><a href = "#" class = "button_grey">allergy</a></li>
                <li><a href = "#" class = "button_grey">baby</a></li>
                <li><a href = "#" class = "button_grey">beauty</a></li>
                <li><a href = "#" class = "button_grey">ear care</a></li>
                <li><a href = "#" class = "button_grey">for her</a></li>
                <li><a href = "#" class = "button_grey">for him</a></li>
                <li><a href = "#" class = "button_grey">first aid</a></li>
                <li><a href = "#" class = "button_grey">gift sets</a></li>
                <li><a href = "#" class = "button_grey">spa</a></li>
                <li><a href = "#" class = "button_grey">hair care</a></li>
                <li><a href = "#" class = "button_grey">herbs</a></li>
                <li><a href = "#" class = "button_grey">medicine</a></li>
                <li><a href = "#" class = "button_grey">natural</a></li>
                <li><a href = "#" class = "button_grey">oral care</a></li>
                <li><a href = "#" class = "button_grey">pain</a></li>
                <li><a href = "#" class = "button_grey">pedicure</a></li>
                <li><a href = "#" class = "button_grey">personal care</a></li>
                <li><a href = "#" class = "button_grey">probiotics</a></li>

            </ul>

        </div>

    </section>-->
</section>