<section class="col-sm-2" style="border-right: 1px solid #ddd;padding-right: 0;">
    <div class="product_menu">
    <h4>Personal Information</h4>
            <ul>
                <li id="my_profile"><a href="<?= base_url() ?>index.php/seller/account_setting/my_profile/">My Profile</a></li>
                <li id="member_profile"><a href="<?= base_url() ?>index.php/seller/account_setting/member_profile/">Member Profile</a></li>
                <li id="privacy_setting"><a href="<?= base_url() ?>index.php/seller/account_setting/privacy_setting/">Privacy Setting</a></li>

            </ul>
    </div>
    <br/>
    <hr/>
    <div class="product_menu">
    <h4>Account Security</h4>
            <ul>
                <li id="change_email_address"><a href="<?= base_url() ?>index.php/seller/account_setting/change_email_address/">Change Email Address</a></li>
                <li id="change_password"><a href="<?= base_url() ?>index.php/seller/account_setting/change_password/">Change Password</a></li>
                <li id="set_security_question"><a href="<?= base_url() ?>index.php/seller/account_setting/security_question/">Set Security Question</a></li>

            </ul>
    </div>
    <br/>
    <hr/>
    <div style="margin-top: 15px">
        <a href="" class="button_blue">Get Verified</a><br/><br/>
        <p style="text-align: justify;font-size: 12px;padding-right: 10px">Become a  Verified member to get higher rankings for your products in search result as a supplier; and enhanced sourcing service as a buyer.</p>
    </div>
    <div style="margin-top: 15px">
        <a href="" class="button_blue">Upgrade Now</a><br/><br/>
        <p style="text-align: justify;font-size: 12px;padding-right: 10px">Sign up for Gold Supplier to get 22x more inquiries than Free Members.</p>
    </div>

</section>