<section class="col-sm-2" style="border-right: 1px solid #ddd;padding-right: 0;">
    <div class="product_menu">
    <h4>My Products</h4>
            <ul>
                <li id="manage_company_profile"><a href="<?= base_url() ?>index.php/seller/manage_company/">Manage Company Profile</a></li>
            </ul>
    </div>
    <br/>
    <hr/>
    <br/>
    <div class="product_menu">
    <h4>Verification</h4>
            <ul>
                <li id="verify_status"><a href="<?php// base_url('index.php/seller/verify_company/') ?>">Verification Status</a></li>
            </ul>
    </div>
    <br/>
    <hr/>
    <div style="margin-top: 15px">
        <a href="<?= base_url('index.php/seller/verify_company/') ?>" class="button_blue">Get Verified</a><br/><br/>
        <p style="text-align: justify;font-size: 12px;padding-right: 10px">Become a  Verified member to get higher rankings for your products in search result as a supplier; and enhanced sourcing service as a buyer.</p>
    </div>
    <div style="margin-top: 15px">
        <a href="" class="button_blue">Upgrade Now</a><br/><br/>
        <p style="text-align: justify;font-size: 12px;padding-right: 10px">Sign up for Gold Supplier to get 22x more inquiries than Free Members.</p>
    </div>

</section>