 <div class="table_row">
                            <?php
                            $count =1;
                            foreach ($category_products as $row) {
                                ?>
                                <!-- - - - - - - - - - - - - - Product - - - - - - - - - - - - - - - - -->

                                <div class="table_cell">

                                    <div class="product_item">

                                        <!-- - - - - - - - - - - - - - Thumbmnail - - - - - - - - - - - - - - - - -->

                                        <div class="image_wrap">

                                            <img src="<?= $row->image_url; ?>" alt="" width="243" height="243">

                                            <!-- - - - - - - - - - - - - - Product actions - - - - - - - - - - - - - - - - -->

                                            <div class="actions_wrap">

                                                <div class="centered_buttons">

                                                    <a href="<?= base_url() ?>index.php/buyer/product_detail" class="button_dark_grey middle_btn quick_view" data-modal-url="modals/quick_view.html">Quick View</a>



                                                </div><!--/ .centered_buttons -->





                                            </div><!--/ .actions_wrap-->

                                            <!-- - - - - - - - - - - - - - End of product actions - - - - - - - - - - - - - - - - -->

                                        </div><!--/. image_wrap-->

                                        <!-- - - - - - - - - - - - - - End thumbmnail - - - - - - - - - - - - - - - - -->

                                        <!-- - - - - - - - - - - - - - Product title & price - - - - - - - - - - - - - - - - -->

                                        <div class="description">

                                            <a href="#"><?= $row->product_name; ?></a>

                                            <div class="clearfix product_info">

                                                <p class="product_price alignleft"><b>$5.99</b></p>

                                                <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                <ul class="rating alignright">

                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li></li>

                                                </ul>

                                                <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                            </div>

                                        </div>

                                        <!-- - - - - - - - - - - - - - End of product title & price - - - - - - - - - - - - - - - - -->

                                        <!-- - - - - - - - - - - - - - Full description (only for list view) - - - - - - - - - - - - - - - - -->

                                        <div class="full_description">

                                            <a href="#" class="product_title"><?= $row->product_keyword; ?></a>

                                            <a href="#" class="product_category"><?= $row->product_category; ?></a>

                                            <div class="v_centered product_reviews">

                                                <!-- - - - - - - - - - - - - - Product rating - - - - - - - - - - - - - - - - -->

                                                <ul class="rating">

                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li class="active"></li>
                                                    <li></li>

                                                </ul>

                                                <!-- - - - - - - - - - - - - - End of product rating - - - - - - - - - - - - - - - - -->

                                                <!-- - - - - - - - - - - - - - Reviews menu - - - - - - - - - - - - - - - - -->

                                                <ul class="topbar">

                                                    <li><a href="#">3 Review(s)</a></li>
                                                    <li><a href="#">Add Your Review</a></li>

                                                </ul>

                                                <!-- - - - - - - - - - - - - - End of reviews menu - - - - - - - - - - - - - - - - -->

                                            </div>

                                            <p><?= $row->product_description; ?></p>

                                            <a href="#" class="learn_more">Learn More</a>

                                        </div>

                                        <!-- - - - - - - - - - - - - - End of full description (only for list view) - - - - - - - - - - - - - - - - -->

                                        <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                        <div class="actions">

                                            <p class="product_price bold">$5.99</p>

                                            <ul class="seller_stats">

                                                <li>Shipping: <span class="success">Free Shipping</span></li>
                                                <li>Availability: <span class="success">in stock</span></li>
                                                <li class="seller_info_wrap">

                                                    Seller: <span class="seller_name">johnsmith</span>

                                                    <div class="seller_info_dropdown">

                                                        <ul class="seller_stats">

                                                            <li>

                                                                <ul class="topbar">

                                                                    <li>China (Mainland)</li>

                                                                    <li><a href="#">Contact Details</a></li>

                                                                </ul>

                                                            </li>

                                                            <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                        </ul>

                                                        <div class="v_centered">

                                                            <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                            <a href="#" class="small_link">Chat Now</a>

                                                        </div>

                                                    </div>

                                                </li>

                                            </ul>


                                        </div>

                                        <!-- - - - - - - - - - - - - - Product price & actions (only for list view) - - - - - - - - - - - - - - - - -->

                                    </div><!--/ .product_item-->

                                </div>

                                <?php
                                if($count % 3 == 0 ){
                                    echo '</div><div class="table_row">';
                                }
                                $count++;
                            }
                            ?>
                        </div>