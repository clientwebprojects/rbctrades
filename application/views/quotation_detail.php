<?php
include 'header.php';
//echo '<pre>';
//print_r($quote);
//exit();
?>
<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="<?= base_url() ?>">Home</a></li>
            <li>Quotation</li>

        </ul>

        <div class="row">

            <main class="col-md-9 col-sm-8">

                <!-- - - - - - - - - - - - - - Product images & description - - - - - - - - - - - - - - - - -->

                <section class="section_offset">
                    <div class="single_product">
                    <div class="image_preview_container" style="height: 300px;border: 1px solid #eee;padding: 5px;margin-bottom: 15px;overflow-y: hidden;">
                                    
                                            <img src="<?= $quote->items[0]->rfq_product_image; ?>" alt="" style="height: 300px;padding: 5px;">

                                </div>
                    </div>
                    <div class="clearfix">

                        
                        <div class="single_product_description">
                           
                            <h3 style="font-size: 22px"><?= $quote->items[0]->rfq_product_name; ?></h3>
                            <div class="description_section">

                                <table class="product_info">

                                    <tbody>

                                                                                <tr>

                                            <td class="specs_cell"><p class="color-999 font13">Quantity:</p></td>
                                            <td class="color-000 font13"><?= $quote->items[0]->rfq_product_quantity; ?> &nbsp; <?=$quote->items[0]->uom_name; ?>	</td>

                                        </tr>

                                        <tr>

                                            <td class="specs_cell"><p class="color-999 font13">Date Posted:</p> </td>
                                            <td class="color-000 font13"><?= $quote->rfq_quotation_date; ?></td>

                                        </tr>

                                        <tr>

                                            <td class="specs_cell"><p class="color-999 font13">Expiry Date:</p> </td>
                                            <td class="color-000 font13"><?= $quote->rfq_expiry_date; ?></td>

                                        </tr>
                                         <tr>

                                            <td class="specs_cell"><p class="color-999 font13">Posted in:</p> </td>
                                            <td class="color-000 font13"><img src="<?= base_url('images/flags/'.$quote->country_code.'.png'); ?>"/> <?= $quote->country_name; ?></td>

                                        </tr>

                                        <tr>

                                            <td class="specs_cell"><p class="color-999 font13">Availability:</p> </td>
                                            <td class="color-000 font13"><?php 
                                            if($quote->rfq_quotes_left== '0'){ 
                                                echo " Quota reached ";
                                            
                                            }else
                                                { echo $quote->rfq_quotes_left;
                                            
                                            }    ?></td>
                                                
                                              
                                        </tr>
                                        <tr>

                                            <td class="specs_cell"><p class="color-999 font13">Description:</p> </td>
                                            <td class="color-000 font13"><?= $quote->items[0]->rfq_product_description; ?></td>

                                        </tr>

                                        <tr>
                                            <td>
                                                <div class="buttons_row">

                                                    <a class="button_blue btn-lg" href="<?= base_url() ?>index.php/seller/seller_quotation/<?= $quote->id; ?>">Get Quotation</a>

                                                </div>
                                            </td>
                                            
                                        </tr>

                                    </tbody>

                                </table>

                            </div>

                            <hr>

                        </div>
                    </div>

                </section>

                <div class="section_offset">
                    <h3 class="bold" style="font-size: 22px">RFQ Details</h3>

                    <h4>Detailed Description</h4>
                    
                    <?= $quote->rfq_quotation_description ?>

                </div><!--/ .section_offset -->

                <hr>
            </main><!--/ [col]-->

            <aside class="col-md-3 col-sm-4">

                <!-- - - - - - - - - - - - - - Seller Information - - - - - - - - - - - - - - - - -->

                <section class="section_offset">

                    <h3>Buyer Information</h3>

                    <div class="theme_box">

                        <div class="seller_info clearfix">

                            <a href="#" class="alignleft photo image_div">
                                <?php
                                        if (!empty($quote->logo_url)) {
                                            echo "<img src='" . $values->logo_url . "' alt='' width='100' height='100' >";
                                        } else {
                                            echo "<img src='" . base_url() . "images/no_image.jpg'  width='100' height='100'>";
                                        }
                                    ?>
                             
                            </a>

                            <div class="wrapper">

                                <a href="#"><b><?= $quote->company_name; ?></b></a>

                                

                            </div>

                        </div><!--/ .seller_info-->

                        <ul class="seller_stats">

                            <li>

                                <ul class="topbar">

                                    <li><a href="#">Contact Details</a></li><br>

                                    <li> <?= $product->country_name; ?></li>



                                </ul>

                            </li>
                            <!--
                                                        <li><span class="bold">99.8%</span> Positive Feedback</li>

                                                        <li><span class="bold">7606</span> Transactions</li>-->

                        </ul>

                        <!--                        <div class="v_centered">

                                                    <a href="#" class="button_blue mini_btn">Contact Seller</a>

                                                    <a href="#" class="small_link">Chat Now</a>

                                                </div>-->

                    </div><!--/ .theme_box -->


                </section>

            </aside><!--/ [col]-->

        </div>
        
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <section class="section_offset">

                    <h3 class="offset_title bold">Quotation Record</h3>
                    
                    <div class="quotations-content">
  	<table class="record-content" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th class="record-name first-col">Company Name </th>
				<th style="width: 18%;" class="record-type">Business Type</th>
				<th style="width: 15%;" class="record-location">Location</th>
				<th class="record-time">Time Quoted</th>
				<th style="width: 13%;">Buyer's Reaction</th>
			</tr>
		</thead>
		<tbody>
                    
                    <?php foreach ($seller_details as $values) { ?>
                    <tr>
                    <td class="first-col">
                   <span class="company"><?= $values->company_name; ?></span>
                    </td>
                    <td>Manufacturer, Trading Company</td>
                    <td><?= $values->country_name;?>(<?= $values->operational_address_city;?>)</td>
                    <td><?= $values->quotation_date; ?></td>
                    <td ></td>
		</tr>
			<?php }?>		
		</tbody>
	</table>
	</div>
                    
                    

                    

                </section>
                
            </div>
            
        </div>

    </div>

</div>

<?php include 'footer.php'; ?>