<?php include 'header2.php';?>
<script>
    document.getElementById("account_setting").className = "active-seller-menu";
</script>
<div class="page_wrapper type_2" >
    <div class="container">
            <div class="row">
                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                    <?php include 'inc/seller_account_menu.php'; ?>
                    <script>
                        document.getElementById("member_profile").className = "active";
                    </script>
                    <div class="col-md-10">

                            <form id="account_setting_member_profile" class="form-horizontal" method="post" action="<?= base_url('index.php/seller/update_member_profile')?>" name="member_profile_form" enctype="">

                                    <h4 class="heading">Edit Member Profile <span class="small-desc"></span></h4>

                                    <fieldset style="border: 1px dotted #bebebe;border-radius: 3px;padding: 10px">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Name</label>
                                <div class="col-md-6">
                                    <input name="member_name" type="text" placeholder="Full Name" class="form-control input-md"  value="<?= $user->user_name;?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Gender</label>
                                <div class="col-md-6 radio" style="padding-left: 25px">
                                    <label style="padding-top:0px;">
                                        <input name="gender" type="radio" value="male" style="width: 30px;margin-top: 1px" <?php if($user->gender == "male"){echo "checked=checked";}?>> Male
                                    </label>
                                    <label style="margin-left: 20px;padding-top:0px;">
                                        <input name="gender" type="radio" value="female" style="width: 30px;margin-top: 1px" <?php if($user->gender == "female"){echo "checked=checked";}?>> Female
                                    </label>
                                </div>
                            </div>
                            <div id="job_title" class="form-group">
                                <label class="col-md-3 control-label color-333">Job Title</label>
                                <div class="col-md-6">
                                    <input name="job_title" type="text" placeholder="Job Title" class="form-control input-md" value="<?= $user->job_title;?>"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333">Email</label>
                                <div class="col-md-6 color-333 font12" style="padding-top: 3px;">
                                    <?= $user->user_email;?> <a href="" class="link">Change email address</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Contact Address</label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Street</label>
                                <div class="col-md-4">
                                    <input  name="street_adddress" type="text" class="form-control input-md"  value="<?= $user->street_adddress;?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">City</label>
                                <div class="col-md-4">
                                    <input  name="city" type="text" class="form-control input-md" value="<?= $user->city;?>"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Province/State/Country</label>
                                <div class="col-md-4">
                                    <input  name="province" type="text" class="form-control input-md"  value="<?= $user->province;?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Country/Region</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="country">
                                        <option value="">--Please Select One</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?= $country->country_id ?>" <?php if($user->country == $country->country_id ){echo "selected=selected";}?>><?= $country->country_name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Zip Code</label>
                                <div class="col-md-4">
                                    <input  name="zip_code" type="text" class="form-control input-md"  value="<?= $user->zip_code;?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Tel</label>
                                <div class="col-md-2 font12">
                                    Country Code
                                    <input  name="phone[]" type="text" class="form-control input-md" value="<?= $user_phone[0]?>"  >
                                </div>
                                <div class="col-md-2 font12">
                                    Area Code
                                    <input  name="phone[]" type="text" class="form-control input-md"  value="<?= $user_phone[1]?>" >
                                </div>
                                <div class="col-md-3 font12">
                                    Number
                                    <input  name="phone[]" type="text" class="form-control input-md" value="<?= $user_phone[2]?>"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333">Fax</label>
                                <div class="col-md-2 font12">
                                    Country Code
                                    <input  name="fax[]" type="text" class="form-control input-md" value="<?= $user_fax[0]?>"  >
                                </div>
                                <div class="col-md-2 font12">
                                    Area Code
                                    <input  name="fax[]" type="text" class="form-control input-md" value="<?= $user_fax[1]?>"  >
                                </div>
                                <div class="col-md-3 font12">
                                    Number
                                    <input  name="fax[]" type="text" class="form-control input-md"  value="<?= $user_fax[2]?>" >
                                </div>
                            </div>
                                        <div class="form-group">
                                <label class="col-md-3 control-label color-333">Mobile</label>
                                <div class="col-md-4 font12">
                                    <input  name="mobile" type="text" class="form-control input-md" value="<?= $user->phone;?>">
                                </div>
                            </div>
                                        <div class="form-group">
                                <label class="col-md-3 control-label color-333">Department</label>
                                <div class="col-md-4 font12">
                                    <select class="form-control" name="department">
                                        <option value="">--Please Select One</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?= $country->country_id ?>" <?php if($user->department == $country->country_id){echo "selected=selected";}?>><?= $country->country_name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                                    <br/>
                                    <div class="form-group">
                                    <div class="col-md-12">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9" style="margin-top: 15px;">
                                        <input type="submit" class="button_blue" id="submit_company_information" name="submit_member_profile" value="Submit">
                                    </div>
                                    </div>
                        </div>
                            </form>

                        </div>
                </div><!--/ [col]-->
                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
            </div><!--/ .row-->
        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>