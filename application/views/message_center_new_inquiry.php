<?php include 'header2.php'; ?>
<script>
    document.getElementById("seller_messages").className = "active-seller-menu";
</script>
<script src="<?= base_url() ?>js/ckeditor.js"></script>
<script>

    // The instanceReady event is fired, when an instance of CKEditor has finished
    // its initialization.
    CKEDITOR.on('instanceReady', function (ev) {
        // Show the editor name and description in the browser status bar.
        document.getElementById('eMessage').innerHTML = 'Instance <code>' + ev.editor.name + '<\/code> loaded.';

        // Show this sample buttons.
        document.getElementById('eButtons').style.display = 'block';
    });

    function InsertHTML() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        var value = document.getElementById('htmlArea').value;

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg')
        {
            // Insert HTML code.
            // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-insertHtml
            editor.insertHtml(value);
        }
        else
            alert('You must be in WYSIWYG mode!');
    }

    function InsertText() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        var value = document.getElementById('txtArea').value;

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg')
        {
            // Insert as plain text.
            // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-insertText
            editor.insertText(value);
        }
        else
            alert('You must be in WYSIWYG mode!');
    }

    function SetContents() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        var value = document.getElementById('htmlArea').value;

        // Set editor contents (replace current contents).
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-setData
        editor.setData(value);
    }

    function GetContents() {
        // Get the editor instance that you want to interact with.
        var editor = CKEDITOR.instances.editor1;

        // Get editor contents
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-getData
        alert(editor.getData());
    }

    function ExecuteCommand(commandName) {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg')
        {
            // Execute the command.
            // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-execCommand
            editor.execCommand(commandName);
        }
        else
            alert('You must be in WYSIWYG mode!');
    }

    function CheckDirty() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        // Checks whether the current editor contents present changes when compared
        // to the contents loaded into the editor at startup
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-checkDirty
        alert(editor.checkDirty());
    }

    function ResetDirty() {
        // Get the editor instance that we want to interact with.
        var editor = CKEDITOR.instances.editor1;
        // Resets the "dirty state" of the editor (see CheckDirty())
        // http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-resetDirty
        editor.resetDirty();
        alert('The "IsDirty" status has been reset');
    }

    function Focus() {
        CKEDITOR.instances.editor1.focus();
    }

    function onFocus() {
        document.getElementById('eMessage').innerHTML = '<b>' + this.name + ' is focused </b>';
    }

    function onBlur() {
        document.getElementById('eMessage').innerHTML = this.name + ' lost focus';
    }

</script>
<div class="page_wrapper type_2" >

    <div class="container">

        <div class="section_offset">


            <div class="container" style="background-color:#FFF;padding-top: 20px;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-2">
                            <?php include 'inc/message_center_left.php'; ?>
                        </div>
                        <div class="col-sm-10">
                            <?php include 'inc/message_center_top.php'; ?>
                            <br/>
                            <form class="form-horizontal" action="" method="post">
                                <fieldset style="border: 1px solid #bebebe;border-radius: 3px;padding: 10px 4px">
                                    <div class="form-group">
                                        <label href="" class="col-md-1" for="user_id_to" style="padding-top: 4px;/*color: #0076A3;text-decoration: underline;*/">To</label>
                                        <div class="col-md-11">
                                            <input id="user_id_to" name="user_id_to" type="text" placeholder="Full Name" class="form-control input-md" value="<?= $user->user_name ?>"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label href="" class="col-md-1" for="subject" style="padding-top: 4px;">Subject</label>
                                        <div class="col-md-11">
                                            <input id="subject" name="subject" type="text" placeholder="Full Name" class="form-control input-md" value="<?= $user->user_name ?>"  >
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-md-12" id="eButtons" style="padding: 10px 10px !important;">
                                            <textarea id="editor1" name="editor1" rows="10"></textarea>

                                            <script>
                                                // Replace the <textarea id="editor1"> with an CKEditor instance.
                                                CKEDITOR.replace('editor1', {
                                                    on: {
                                                        focus: onFocus,
                                                        blur: onBlur,
                                                        // Check for availability of corresponding plugins.
                                                        pluginsLoaded: function (evt) {
                                                            var doc = CKEDITOR.document, ed = evt.editor;
                                                            if (!ed.getCommand('bold'))
                                                                doc.getById('exec-bold').hide();
                                                            if (!ed.getCommand('link'))
                                                                doc.getById('exec-link').hide();
                                                        }
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <button  id="message_send_btn" value="message_send_btn" name="message_send_btn" class="btn button_blue">Send</button>
                                            <button  id="message_cancel_btn" value="message_cancel_btn" name="message_cancel_btn" class="btn btn-default">Cancel</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/ .section_offset-->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>