<?php include 'header2.php'; ?>

<script>
    document.getElementById("seller_messages").className = "active-seller-menu";
</script>

<div class="page_wrapper type_2" >

    <div class="container">

        <div class="section_offset">


            <div class="container" style="background-color:#FFF;padding-top: 20px;">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-2">
                            <?php include 'inc/message_center_left.php'; ?>
                        </div>
                        <div class="col-sm-10">
                            <?php include 'inc/message_center_top_reply.php'; ?>
                            <hr>
                            
                                       
                                <?php
                                foreach ($message_data as $value) {
                                    ?>

                                    

                                            <b><p style="font-size: 18px; padding-bottom: 10px;padding-top: 10px"> <?= $value->subject; ?> </p></b>                                                          

                                            <hr>
                                            <div class="form-group">
                                                
                                                <div class="col-md-12" style="padding-bottom: 12px !important">
                                                    From: <b ><?= $value->temp_user_email; ?></b>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12" style="padding-bottom: 12px !important">
                                                    <label>To:</label> <?= $value->main_user_email; ?></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12" style="padding-bottom: 12px !important">
                                                    <label>Date:</label> <?= $value->date; ?></div>
                                            </div>
                                            <div class="form-group">
                                                
                                                <div class="col-md-12" style="padding-bottom: 12px !important">
                                                    <?= $value->body; ?>
                                                </div>
                                            </div>
<!--                                            <a href="<?php echo base_url() ?>/index.php/message_center/message_center_reply_inquiry/<?php echo $value->message_id; ?>" >Reply</a>-->
                                       
                                    <?php
                                }
                                ?>

                            
                                    
                        </div>
                    </div>
                </div>
            </div><!--/ .section_offset-->

        </div><!--/ .container-->

    </div><!--/ .page_wrapper-->

    <!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
    <?php include 'footer.php'; ?>