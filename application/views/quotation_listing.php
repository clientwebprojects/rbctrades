<?php
include 'header.php';
//echo '<pre>';
//print_r($quots);
//exit();
?>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
<script type="text/javascript">
    var base_url = "<?= base_url(); ?>";
    var checkbox_enabled = false;
    var quantity_fil_pagination = false;
    var country_filter = false;
    var country_id;
    var page_number = 0;
    var total_page = null;
    var old;
</script>
<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="<?= base_url() ?>">Home</a></li>
            <li class="active">Buying Leads</li>
        </ul>

        <div class="row">
            <?php include 'inc/quotation_filter.php'; ?>

            <main class = "col-md-9 col-sm-9">

                <div class="section_offset">

                    <header class="top_box on_the_sides">

                        <div class="left_side clearfix v_centered type_2">

                            <span>Filter by:</span>
                            <div class="form-group">
                                <label class="checkbox-inline"><input value="open-quotes" id="open-quotes" type="checkbox" name="open-quotes" style=" width: 30px"/> Open Quotes</label>
                                <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Gold Supplier</label>
                                <label class="checkbox-inline"><input type="checkbox" name="" style=" width: 30px"/> Assessed Supplier</label>
                            </div>

                        </div>

                    </header>

                    <div class="table_layout list_view list_view_products" id="products_container">
                        <div class="ajax-content-loading">
                            <img src="<?php echo base_url()?>images/icon_loader.gif" alt="">
                        </div>

                        <div id="outerdiv">
                        </div>
                    </div>
                    <footer class="bottom_box on_the_sides" id="footer_id">

                        <div class="left_side">

                            <lable>Page <lable id="page_number"></lable> of <lable id="total_page"></lable></lable>
                        </div>

                        <div class="right_side">

                            <div class="btn-group btn-group-sm">
                                <button type="button" id="previous" class="btn btn-default">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </button>
                                <button type="button" id="next" class="btn btn-default">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </button>
                            </div>

                        </div>

                    </footer>
                </div>
                <!-- - - - - - - - - - - - - - End of products - - - - - - - - - - - - - - - - -->
            </main>
        </div><!--/ .row -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->

<script>
    $(document).ready(function () {

        $(".country-list").on('click', (function (e) {

           country_id=$(this).attr("id");
           country_filter =  true;
           checkbox_enabled = false;
           quantity_fil_pagination = false;
           page_number=0;
           getReport(page_number);


        }));



    });



</script>
<script>
    $(document).ready(function () {

        $("#open-quotes").change(function () {
            console.log(document.getElementById("open-quotes").checked);
            checkbox_enabled = true;
            quantity_fil_pagination = false;
            country_filter =  false;
            page_number=0;
            //$(this).is(':checked');
            getReport(page_number);

        });

    });

</script>
<script>
    $(document).ready(function () {

        $("#qty-filter-btn").on('click', (function (e) {
            quantity_fil_pagination = true;
            checkbox_enabled=false;
            country_filter =  false;
            page_number=0;
            getReport(page_number);

        }));
    });

</script>
<script>
    var start_html = '<div id="product-row"> ';
    var end_html = '</div></div>';
    var product_row = '<div class="table_row">';
    var count = 1;

    var getReport = function (page_number) {



        if (page_number == 0) {
            $("#previous").prop('disabled', true);
        }
        else {
            $("#previous").prop('disabled', false);
        }

        if (page_number == (total_page - 1)) {
            $("#next").prop('disabled', true);
        }
        else {
            $("#next").prop('disabled', false);
        }

        $("#page_number").text(page_number + 1);
         $('#footer_id').hide();
         $('#outerdiv').empty();
        $('#product-row').remove();
        $(".ajax-content-loading").show();
        var min = $("#minimum_qty").val();
        var max = $("#maximum_qty").val();
        var params = {
            type: "POST",
            cache: false,
            dataType: 'json',
            success: function (data) {

                $('#outerdiv').append(start_html);
                $('#product-row').append(product_row);
                $(".ajax-content-loading").hide();
                $('#footer_id').show();
                window.mydata = data;
                total_page = mydata[0].TotalRows;
//                console.log('Total Pages = '+total_page);
//                console.log('Total Pages = '+data);
                $("#total_page").text(total_page);
                var record_par_page = mydata[0].Rows;
                if (page_number == 0) {
                    $("#previous").prop('disabled', true);
                }
                else {
                    $("#previous").prop('disabled', false);
                }

                if (page_number == (total_page - 1)) {
                    $("#next").prop('disabled', true);
                }
                else {
                    $("#next").prop('disabled', false);
                }

                $("#page_number").text(page_number + 1);

                $.each(record_par_page, function (i, v) {
//                    console.log(v);
                      var subCategory_html =
                                '<div class="table_cell">' +
                                '<div class="product_item">' +
                                '<div class="image_wrap">' +
                                '<img src="' + image_src(v.rfq_product_image) + '" alt="" width="245" height="243">' +
                                '<div class="actions_wrap">' +
                                '<div class="centered_buttons">' +
                                '<a href="' + base_url + 'index.php/main/quotation_detail/' + v.rfq_id + '" class="btn btn-sm btn-default quick_view" data-modal-url="modals/quick_view.html">View</a>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '<div class="full_description">' +
                                '<a href="#" class="product_title">' + v.rfq_product_name + '</a>' +
                                '<p class="font12 color-333"> ' + v.rfq_product_description + '</p>' +
                                '<p class="font12 color-999">Quantity Required: <span class="color-333">' + v.rfq_product_quantity + ' ' + v.uom_name + '</span></p>' +
                                '<span class="font12 color-999">Date Posted: ' + v.rfq_quotation_date + '</span>' +
                                '</div>' +
                                '<div class="actions">' +
                                '<ul class="seller_stats">' +
                                '<li><img src="' +base_url+'images/flags/'+ v.country_code + '.png"/> ' + v.country_name + '</li>' +
                                '<li><a href="' + base_url + 'index.php/seller/seller_quotation/' + v.rfq_id + '" '+ allowed(v.rfq_quotes_left)+' class="btn btn-xs btn-primary">Quote Now</a></li>' +
                                '<li class="font12"> Quotes Left :  <span class="success" style="font-weight:bold">' + v.rfq_quotes_left + ' </span></li>' +
                                '</ul>' +
                                '</div>' +
                                '</div>' +
                                '</div>' + new_row(count);
                        $('#product-row').append(subCategory_html);
                        count++;


                    $('#outerdiv').append(end_html);
                });

            },
            error: function () {
                            var error_msg = '<div style="padding:30px 0 30px 10px;border:1px solid #eaeaea"> <b>Sorry, we could not find any results that matched your query.</b><br/>Use the options above or main menu to redefine your search. </div>';
                            $(".ajax-content-loading").hide();
                            $("#outerdiv").append(error_msg);
                }
        };
        if (checkbox_enabled == true) {
            params.url = "<?php echo base_url('index.php/main/quotation_listing_open_quotes_filtration'); ?>";
            params.data = {"quotes": document.getElementById("open-quotes").checked, "page_number": page_number};
        }
        else if ( quantity_fil_pagination == true) {
            params.url = "<?php echo base_url('index.php/main/quotation_listing_quantity_filtration'); ?>";
            params.data = {"min_qty": min, "max_qty": max, "page_number": page_number};
        }
        else if(country_filter == true){

            console.log(country_id);
            params.url = "<?php echo base_url('index.php/main/quotation_listing_country_filtration'); ?>";
            params.data = {"country": country_id, "page_number": page_number};
        }
        else {

            params.url = "<?php echo base_url('index.php/main/quotation_listing_all_quotes_filtration'); ?>";
            params.data = 'page_number=' + page_number;
        }

        $.ajax(params);


    };
    $(document).ready(function (e) {

        $(".ajax-content-loading").hide();

        getReport(page_number);

        $("#next").on("click", function () {
            $("#product-row").html("");
            page_number = (page_number + 1);
            getReport(page_number);

        });

        $("#previous").on("click", function () {
            $("#product-row").html("");
            page_number = (page_number - 1);
            getReport(page_number);
        });
    });


</script>



<script type="text/javascript">
    function image_src(src) {
        if (jQuery.isEmptyObject(src)) {
            return base_url + "images/no_image.jpg";
        } else {
            return src;
        }
    }
    function allowed(value) {
        if (value>0) {
            return "";
        } else {
            return "disabled ";
        }
    }
    function new_row(count) {
//        console.log(count % 3);
        if (count % 3 === 0) {

            return '</div><div class="table-row">';
        } else {
            return "";
        }
    }
</script>
<script src="<?= base_url() ?>js/invoicescript.js" type="text/javascript"></script>
<?php include 'footer.php'; ?>
