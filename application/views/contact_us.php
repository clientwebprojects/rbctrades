<?php include 'header.php'; ?>
<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">


            <div class="row">

                <div class="col-md-7 col-sm-7"  >
                    <div class="well well-sm" style="background-color:#FFF;padding-top: 20px;">
                        <form  class="form-horizontal" action="" method="post" id="contact-us-form">
                            <?php if ($this->session->flashdata('email_sent')) { ?>
                                <div class='alert alert-success alert-dismissible' role='alert' >
                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close' style="margin-top:8%;margin-right:5%"></button><strong>Congrats!</strong><?= $this->session->flashdata('email_sent'); ?>
                                </div>
                            <?php }?>
                            <fieldset>
                                <legend class="heading">Contact us</legend>

                                <!-- Name input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="fname">First Name</label>
                                    <div class="col-md-7">
                                        <input id="name" name="fname" type="text" placeholder="Your First name" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="lname">Last Name</label>
                                    <div class="col-md-7">
                                        <input id="name" name="lname" type="text" placeholder="Your Last name" class="form-control">
                                    </div>
                                </div>

                                <!-- Email input-->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="email">E-mail</label>
                                    <div class="col-md-7">
                                        <input  name="email" type="text" placeholder="Your email" class="form-control">
                                    </div>
                                </div>

                                <!-- Message body -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="message">Your message</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                                    </div>
                                </div>

                                <!-- Form actions -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-3">
                                        <input type="submit" name="contact_us_submit" value="submit" class="button_blue btn" />
                                    </div>
                                    </div>
                            </fieldset>
                        </form>
                    </div>
                    </div>
                                 <div class="col-md-5 col-sm-5">

                    <section>

                        <div class="theme_box" style="background: #f0f0f0">
                            <legend class="heading">Feel free to drop by or call to say Hello!</legend>
                            <p class="pb_title">If you have any issues regarding RBC Trades, please contact us here. </p><br>
                            <p class="pb_title"><span style="font-weight: 600">Service:</span> <span style="color: red">24</span> hrs./<span style="color: red">7</span> days a week. </p>
                        </div>

                    </section>

                 </div>
                </div>

            </div>
        </div>
    </div>
</div>
<?php include 'footer.php'; ?>
