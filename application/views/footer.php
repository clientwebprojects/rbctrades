
<footer id="footer">

    <div class="footer_section">

        <div class="container">

            <div class="row">

                <div class="col-md-5 col-sm-6">

                    <section class="widget">

                        <h4>Sign Up to Our Newsletter</h4>

                        <p class="form_caption" style="font-size: 13px;">Sign up our newsletter and get exclusive deals you will not find anywhere else straight to your inbox!</p>

                        <form class="newsletter subscribe clearfix" novalidate>

                            <input type="email" name="sc_email" placeholder="Enter your email address">

                            <button class="button_blue def_icon_btn"></button>

                        </form>

                    </section>

                    <section class="widget">

                        <h4>Follow Us</h4>

                        <ul class="social_btns">

                            <li>
                                <a href="#" class="icon_btn middle_btn social_facebook tooltip_container"><i class="icon-facebook-1"></i><span class="tooltip top">Facebook</span></a>
                            </li>

                            <li>
                                <a href="#" class="icon_btn middle_btn social_twitter tooltip_container"><i class="icon-twitter"></i><span class="tooltip top">Twitter</span></a>
                            </li>

                            <li>
                                <a href="#" class="icon_btn middle_btn social_googleplus tooltip_container"><i class="icon-gplus-2"></i><span class="tooltip top">GooglePlus</span></a>
                            </li>

                            <li>
                                <a href="#" class="icon_btn middle_btn social_linkedin tooltip_container"><i class="icon-linkedin-5"></i><span class="tooltip top">LinkedIn</span></a>
                            </li>

                        </ul>

                    </section>

                </div>

                <div class="col-md-2 col-sm-6">

                    <section class="widget">

                        <h4>Information</h4>

                        <ul class="list_of_links">

                            <li><a href="<?= base_url('index.php/seller/seller_profile/') ?>">My Account</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="<?= base_url('index.php/main/terms_conditions') ?>">Terms &amp; Conditions</a></li>
                            <li><a href="<?= base_url('index.php/login') ?>">Login</a></li>
                            <li><a href="<?= base_url('index.php/main/register_user/') ?>">Register Free</a></li>
                            <li><a href="<?= base_url('index.php/main/site_map/') ?>">Site Map</a></li>
                            <li><a href="#">FAQ</a></li>

                        </ul>

                    </section>

                </div>

                <div class="col-md-2 col-sm-6">
                    <section class="widget">

                        <h4>For Buyers</h4>

                        <ul class="list_of_links">

                            <li><a href="<?= base_url() ?>index.php/buyer/request_for_quotation">Get Quotation</a></li>
                            <li><a href="<?= base_url() ?>index.php/buyer/company_listing/">Find Sellers</a></li>
                            <li><a href="<?= base_url() ?>index.php/buyer/view_selling_leeds/">View Selling Leads</a></li>
                            <li><a href="<?= base_url() ?>index.php/main/how_to_buy/">How to Buy</a></li>

                        </ul>

                    </section>
                </div>
                <div class="col-md-2 col-sm-6">

                    <section class="widget">

                        <h4>For Sellers</h4>

                        <ul class="list_of_links">
                            <li><a href="#">Seller Memberships</a></li>
                            <li><a href="<?= base_url() ?>index.php/seller/add_new_product/">Display New Product</a></li>
                            <li><a href="<?= base_url() ?>index.php/seller/manage_products/">Manage Products</a></li>
                            <li><a href="<?= base_url() ?>index.php/seller/view_buying_leeds/">View Buying Leads</a></li>
                            <li><a href="<?= base_url() ?>index.php/main/how_to_sell/">How to Sell</a></li>

                        </ul>

                    </section>


                </div>

            </div>
        </div>
            <hr/>
            <br/>
            <div class="container">
            <div class="row">
                <p class="align_center">&copy; 2015 <a href="index.php">RBC TRADES</a>. All Rights Reserved.</p>
            </div>
            </div>

    </div>

</footer>

</div>
<script src="<?= base_url() ?>js/jquery-2.1.1.min.js"></script>
<!--<script src="<?= base_url() ?>/js/bootstrap.min.js"></script>-->
<script>
    $(document).ready(function () {
        $("#tab-1-btn").click(function () {
            $('#tab-1').show();
            $('#tab-2').hide();
            $('#tab-3').hide();
            $('#tab-4').hide();
            $('#tab-5').hide();
        });
        $("#tab-2-btn").click(function () {
            $('#tab-1').hide();
            $('#tab-2').show();
            $('#tab-3').hide();
            $('#tab-4').hide();
            $('#tab-5').hide();
        });
        $("#tab-3-btn").click(function () {
            $('#tab-1').hide();
            $('#tab-3').show();
            $('#tab-2').hide();
            $('#tab-4').hide();
            $('#tab-5').hide();
        });
        $("#tab-4-btn").click(function () {
            $('#tab-1').hide();
            $('#tab-4').show();
            $('#tab-2').hide();
            $('#tab-3').hide();
            $('#tab-5').hide();
        });
        $("#tab-5-btn").click(function () {
            $('#tab-1').hide();
            $('#tab-5').show();
            $('#tab-3').hide();
            $('#tab-4').hide();
            $('#tab-2').hide();
        });
    });
</script>

<script src="<?= base_url() ?>js/jquery.bootstrap.wizard.js"></script>
<script src="<?= base_url() ?>js/prettify.js"></script>
<script src="<?= base_url() ?>js/jquery-ui.js"></script>
<script src="<?= base_url() ?>js/jquery.appear.js"></script>
<script src="<?= base_url() ?>js/jquery.countdown.plugin.min.js"></script>
<script src="<?= base_url() ?>js/jquery.countdown.min.js"></script>
<script src="<?= base_url() ?>js/owlcarousel/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>js/jquery.tweet.min.js"></script>
<script src="<?= base_url() ?>js/retina.min.js"></script>
<script src="<?= base_url() ?>js/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="<?= base_url() ?>js/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?= base_url() ?>js/fancybox/source/helpers/jquery.fancybox-media.js"></script>
<script src="<?= base_url() ?>js/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
<script src="<?= base_url() ?>js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="<?= base_url() ?>js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="<?= base_url() ?>js/formValidation.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/framework/bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/theme.plugins.js"></script>
<script src="<?= base_url() ?>js/theme.core.js"></script>
<script src="<?= base_url() ?>js/modernizr.js"></script>
<script src="<?= base_url() ?>js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>js/validation.js"></script>
<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_upload_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#company_logo").change(function () {
            readURL(this);
        });
    });
</script>
<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_company_preview').attr('src', e.target.result);
                }


                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#company_photo").change(function () {

            //$('#company-photo-div').height( $("#company-photo-div").height() + 50 );
            //$('#company-photo-div').css("clear","both");
            readURL(this);
        });

    });
</script>
<script>
    $(document).ready(function () {
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_certificate_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#certificate_image").change(function () {
            readURL(this);
        });
    });
</script>

<script>

    $(document).ready(function () {

        $('#myCarousel').carousel({
            interval: 0

        });

        $('#myCarousel').on('slid.bs.carousel', function () {

            //alert("slid");

        });

        $(".tabs_nav li.disabled a").click(function () {

            return false;

        });

        $("#submit").click(function () {

            $("#tab-submit").removeClass('active');

            $("#tab-submit").addClass('disabled');

            $("#tab-varify").removeClass('disabled');

            $("#tab-varify").addClass('active');

        });
        $(".compress").hover(function () {

            $(".image").show();

        });

        $('#rootwizard').bootstrapWizard({onTabShow: function (tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index + 1;
                var $percent = ($current / $total) * 100;
                $('#rootwizard .progress-bar').css({width: $percent + '%'});
            }});
        window.prettyPrint && prettyPrint();

        $("#agriculture-food-btn").click(function () {
            $('html, body').animate({
                scrollTop: $("#agriculture-food").offset().top
            }, 1000);
        });

        $("#supplier").click(function () {
            $('#supplier-div').show(500);
        });

        $("#both").click(function () {
            $('#supplier-div').show(500);
        });

        $("#buyer").click(function () {
            $('#supplier-div').hide(500);
        });

        $(".change-password-btn").click(function () {
            $('#change-password-div').show(500);
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function(e){

    $("#search-select").on('change', (function (e) {
        $("#search-input").val('');
        if($('#search-select').val()=== 'products'){
            //console.log('products');
            $("#search-input").autocomplete({
              source: "<?php echo base_url('index.php/main/get_product_search'); ?>" // path to the get_birds method
            });
            }
        else if($('#search-select').val()=== 'suppliers'){
        //console.log('suppliers');
            $("#search-input").autocomplete({
              source: "<?php echo base_url('index.php/main/get_supplier_search'); ?>" // path to the get_birds method
            });

            }
        else{
        //console.log('buyer');
            $("#search-input").autocomplete({
              source: "<?php echo base_url('index.php/main/get_buyer_search'); ?>" // path to the get_birds method
            });
            }


        }));
    $(function(){

            $("#search-input").autocomplete({
              source: "<?php echo base_url('index.php/main/get_product_search'); ?>" // path to the get_birds method
            });


        });

    });



</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#more_functions').on('change', function () {
            console.log($(this).attr("value"));
            if (this.value == '1')
                console.log(this.value);
            {

                $("#mark_read").on('click', function (e) {
                    e.preventDefault();
                    var checkValues = $('#checkbox_id:checked').map(function ()
                    {
                        return $(this).val();
                    }).get();
                    console.log(checkValues);
                    $.each(checkValues, function (i, val) {
                        $("#" + val).remove();
                    });
//                    return  false;
                    $.ajax({
                        url: '<?= base_url() ?>/index.php/message_center/mark_as_readed_inquiries',
                        type: 'post',
                        data: 'ids=' + checkValues
                    }).done(function (data) {
                        $("#outerdiv").html(data);
                        //$('#selecctall').attr('checked', false);
                        $('#outerdiv').html(data);
                    });
                });
            }
        });
    });
</script>

<!--<script src="<?= base_url() ?>js/modernizr.js"></script>-->

</body>
</html>
