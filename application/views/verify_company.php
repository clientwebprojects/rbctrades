<?php include 'header2.php'; ?>
<script>
    document.getElementById("seller_company").className = "active-seller-menu";
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="page_wrapper type_2" >

    <div class="container" >

        <div class="section_offset">

            <div class="row">
                <!-- - - - - - - - - - - - - - Main slider - - - - - - - - - - - - - - - - -->

                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px;">
                    <?php include 'inc/seller_company_menu.php'; ?>
                    <script>
                        document.getElementById("verify_status").className = "active";
                    </script>

                    <div class="col-md-10" style="background-color:#FFF;padding-top: 20px;">

                        <div>
                            <div id="rootwizard">
                                <div class="navbar">
                                    <div class="navbar-inner">
                                        <div class="container">
                                            <ul class="tabs_nav">
                                                <li id="submit" class="<?php echo ($id == 'tab1') ? 'active' : 'disabled'; ?>"><a href="#tab1" data-toggle="tab">Submit</a></li>
                                                <li id="verification" class="<?php echo ($id == 'tab2') ? 'active' : 'disabled'; ?>"><a href="#tab2" data-toggle="tab">Verification</a></li>
                                                <li id="complete" class="<?php echo ($id == 'tab3') ? 'active' : 'disabled'; ?>"><a href="#tab3" data-toggle="tab">Complete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div id="bar" class="progress_bar" style="height: 20px;">
                                    <div class="progress-bar pb_inner" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;height: 15px;"></div>
                                </div>
                                <div class="tab-content">
                                    <div class="tab-pane" id="tab1" style="display: <?php if ($id == 'tab1') {
                        echo 'block';
                    } ?>">
                                        <form class="form-horizontal" action="" method="post" id="register-company-form">
                                            <fieldset>

                                                <!-- Text input-->
                                                <br>
                                                <h4>Personal Information</h4>
                                                <br>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="name">Name</label>  
                                                    <div class="col-md-6">
                                                        <input id="name" name="name" type="text" placeholder="Full Name" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="job_title">Job Title</label>  
                                                    <div class="col-md-6">
                                                        <input id="job_title" name="job_title" type="text" placeholder="Job Title" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <!-- File Button --> 
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="phone">Phone</label>
                                                    <div class="col-md-4">
                                                        <input id="phone" name="phone" type="text" placeholder="e.g. 92-111111-111" class="form-control input-md" required="">
                                                    </div>
                                                </div>
                                                <hr>

                                                <br>
                                                <h4>Registration Company Information</h4>
                                                <br>
                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="c_registration">Registration No</label>  
                                                    <div class="col-md-6">
                                                        <input id="registration_no" name="c_registration" type="text" placeholder="" class="form-control input-md">

                                                    </div>
                                                </div>

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="c_name">Company Name</label>  
                                                    <div class="col-md-6">
                                                        <input id="company_name" name="c_name" type="text" placeholder="" class="form-control input-md">

                                                    </div>
                                                </div>

                                                <!-- Select Basic -->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="country">Country</label>
                                                    <div class="col-md-6">
                                                        <select id="country" name="country_id" class="form-control">
                                                            <option value="">---- Select----</option>
                                                            <?php foreach ($countries as $country) { ?>
                                                                <option value="<?= $country->country_id ?>"><?= strtoupper($country->country_name) ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <!-- Password input-->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="c_address">Street Address</label>
                                                    <div class="col-md-6">
                                                        <textarea class="form-control" id="street_address" rows="5" name="c_address"></textarea>

                                                    </div>
                                                </div>

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="c_zip_code">Zip Code</label>  
                                                    <div class="col-md-6">
                                                        <input id="zip_code" name="c_zip_code" type="text" placeholder="" class="form-control input-md">

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="state">Region/State</label>  
                                                    <div class="col-md-6">
                                                        <select id="state" name="state" class="form-control">
                                                            <option value="1">Option one</option>
                                                            <option value="2">Option two</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="c_verify_by">Verify By</label>  
                                                    <div class="col-md-2">
                                                        <select id="verify_by" name="c_verify_by" class="form-control">
                                                            <option value="1">NTN</option>
                                                            <option value="2">Other</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="ntn" name="ntn" type="text" placeholder="Number" class="form-control input-md">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label" for="c_phone">Phone</label>  
                                                    <div class="col-md-6">
                                                        <input id="phone" name="c_phone" type="text" placeholder="e.g. 92111111111" class="form-control input-md">
                                                    </div>
                                                </div>
                                                <hr>

                                                <div class="form-group pull-left" style="clear:left;clear: right">
                                                    <label class="col-md-6 control-label" for="submit"></label>
                                                    <div class="col-md-8">
                                                        <button type="submit" value="submit" name="submit" class="button_blue middle_btn register-company-btn" >Submit</button>
                                                    </div>
                                                </div>

                                                <!-- Button -->


                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="tab-pane" id="tab2" style="display: <?php if ($id == 'tab2') {
                                                                echo 'block';
                                                            } ?>">

                                        <img src="<?= base_url() ?>images/green_tick.png" alt="" class="alignright" height="100" width="100">

                                        <p>Thank You For Registering your company on RBC Trades. Our Technical <br>Team will verify your company and reply you soon. </p>

                                        <p>This may take 2-4 working days due to large requests of verifications. </p>

                                    </div>

                                    <div class="tab-pane" id="tab3" style="display: <?php if ($id == 'tab3') {
                                                                echo 'block';
                                                            } ?>">

                                        <p>Donec porta diam eu massa. Quisque diam lorem, interdum vitae, dapibus ac, scelerisque vitae, pede. Donec eget tellus non erat lacinia fermentum. </p>

                                        <p>Donec in velit vel ipsum auctor pulvinar. Vestibulum iaculis lacinia est. Proin dictum elementum velit. </p>

                                    </div>

                                    <ul class="pager wizard">

                                        <li class="previous first" style="display:none;"><a href="#">First</a></li>
                                        <!--                                    <li class="previous"><a href="#">Previous</a></li>-->
                                        <li class="next last" style="display:none;"><a href="#">Last</a></li>
                                        <!--                                    <li class="next " ><a href="#" style="float:left">Submit</a></li>-->
                                    </ul>
                                </div>
                            </div>

                            <!-- - - - - - - - - - - - - - End of Revolution slider - - - - - - - - - - - - - - - - -->

                        </div><!--/ .animated.transparent-->

                    </div><!--/ [col]-->

                    <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

                </div><!--/ [col]-->

                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->

            </div><!--/ .row-->

        </div><!--/ .section_offset-->

        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->




    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<!-- - - - - - - - - - - - - - End Page Wrapper - - - - - - - - - - - - - - - - -->
<?php include 'footer.php'; ?>
