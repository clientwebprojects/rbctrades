<?php include 'header2.php'; ?>
<script>
    document.getElementById("seller_products").className = "active-seller-menu";
</script>
<script>
// assumes you're using jQuery
    $(document).ready(function () {
        $('.confirm-div').hide();
<?php if ($this->session->flashdata('msg')) { ?>
            $('#successmessage').html("<div class='alert alert-success alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('msg'); ?>.</div>").show().delay(5000).fadeOut();
        });
<?php } ?>
</script>
<div class="page_wrapper type_2" >
    <div class="container">
        <div class="">
            <div class="row">
                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                    <?php include 'inc/seller_product_menu.php'; ?>
                    <script>
                        document.getElementById("manage_product").className = "active";
                    </script>
                    <div>
                        <section class="col-sm-10">
                            <h3 class="heading">Manage Products</h3>
                            <!-- - - - - - - - - - - - - - Tabs v2 - - - - - - - - - - - - - - - - -->
                            <div id="successmessage"></div>
                            <div class="tabs type_2">
                                <!-- - - - - - - - - - - - - - Navigation of tabs - - - - - - - - - - - - - - - - -->
                                <ul class="tabs_nav clearfix" style="margin-right: 0px;">
                                    <li class="active"><a href="#tab-1">Approval Pending (<?php echo sizeof($pending_products); ?>)</a></li>
                                    <li ><a href="#tab-2">Active (<?php echo sizeof($active_products); ?>)</a></li>

                                </ul>
                                <!-- - - - - - - - - - - - - - End navigation of tabs - - - - - - - - - - - - - - - - -->
                                <!-- - - - - - - - - - - - - - Tabs container - - - - - - - - - - - - - - - - -->
                                <div class="tab_containers_wrap">
                                    <!-- - - - - - - - - - - - - - Tab - - - - - - - - - - - - - - - - -->
                                    <div id="tab-1" class="tab_container" style="margin-top: 20px;">
                                        <table class="table_type_1" cellpadding="0" cellspacing="0" border="0" class="display" id="table1" style="clear: both">
                                            <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Model</th>
                                                    <th>Owner</th>
                                                    <th>Last Updated</th>
                                                    <th>Created Date</th>
                                                    <th style="width: 110px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count = 0;
                                                foreach ($pending_products as $pen_product) {
                                                    //                                                   print_r($pending_products);exit();
                                                    ?>
                                                    <tr>
                                                        <td data-title="Products Name"><?= $pen_product->product_name ?></td>
                                                        <td data-title="Model" ><?= $pen_product->product_model ?></td>
                                                        <td data-title="Owner" ><?=$owner->member_name?></td>
                                                        <td data-title="Last Updated"><?= $pen_product->product_last_updated ?></td>
                                                        <td data-title="Created Date" ><?= $pen_product->product_add_date ?></td>
                                                        <td data-title="Action" style="text-align: center;">
                                                            <div class="btn-group">
                                                                <a href="<?php echo base_url() ?>index.php/seller/edit_product/<?= $pen_product->product_id ?>" class="btn btn-primary btn-xs" title="Edit">Edit</i></a>
                                                                <a href="<?php echo base_url() ?>index.php/seller/delete_product/<?= $pen_product->product_id ?>" class="btn btn-danger btn-xs" title="delete" onclick="return confirm('Are you sure you want to delete this product?')">Delete</a>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>

<!--                                        <section class="section_offset pull-right">
                                            <ul class="pags">
                                                <li><a href="#"></a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#"></a></li>
                                            </ul>
                                        </section>-->
                                    </div>
                                    <!--/ #tab-1-->
                                    <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                    <div id="tab-2" class="tab_container">
                                        <table class="table_type_1" cellpadding="0" cellspacing="0" border="0" class="display" id="table2" style="clear:both">
                                            <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Model</th>
                                                    <th>Owner</th>
                                                    <th>Last Updated</th>
                                                    <th>Created Date</th>
                                                    <th style="width: 110px;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($active_products != null) {
                                                    foreach ($active_products as $product) {
//                                                   print_r($product);exit();
                                                        ?>
                                                        <tr>
                                                            <td data-title="Products Name"><?= $product->product_name ?></td>
                                                            <td data-title="Model"><?= $product->product_model ?></td>
                                                            <td data-title="Owner"><?=$owner->member_name?></td>
                                                            <td data-title="Last Updated"><?= $product->product_last_updated ?></td>
                                                            <td data-title="Created Date" ><?= $product->product_add_date ?></td>
                                                            <td data-title="Action" style="text-align: center;">
                                                                <div class="btn-group">
                                                                    <a href="<?php echo base_url() ?>index.php/seller/edit_product/<?= $product->product_id ?>" class="btn btn-primary btn-xs" title="Edit">Edit</i></a>
                                                                    <a href="<?php echo base_url() ?>index.php/seller/delete_product/<?= $product->product_id ?>" class="btn btn-danger btn-xs" title="delete" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                                                </div>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <section class="section_offset pull-right">
                                            <ul class="pags">
                                                <li><a href="#"></a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#"></a></li>
                                            </ul>
                                        </section>
                                    </div>
                                    <!--/ #tab-1-->
                                    <!-- - - - - - - - - - - - - - End tab - - - - - - - - - - - - - - - - -->
                                </div><!--/ .tab_containers_wrap -->
                                <!-- - - - - - - - - - - - - - End of tabs containers - - - - - - - - - - - - - - - - -->
                            </div><!--/ .tabs-->
                            <!-- - - - - - - - - - - - - - End of tabs v2 - - - - - - - - - - - - - - - - -->
                        </section><!--/ [col]-->
                        <!--/well-->
                    </div><!--/ .animated.transparent-->
                </div><!--/ [col]-->
                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
            </div><!--/ .row-->
        </div><!--/ .section_offset-->
        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datatable/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#table1').dataTable().columnFilter();
        $('#table2').dataTable().columnFilter();

    });
</script>