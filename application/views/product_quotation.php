<?php include 'header.php'; ?>
<script>
// assumes you're using jQuery
    $(document).ready(function () {
<?php if ($this->session->flashdata('msg')) { ?>
            $('#successmessage').html("<div class='alert alert-success alert-dismissible' role='alert' ><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><?php echo $this->session->flashdata('msg'); ?>.</div>").show().delay(10000).fadeOut();
        });
<?php } ?>
</script>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->
<style>

    .glyphicon{
        line-height: 5;
    }

    div > img:hover{
        cursor:pointer;
    }

    .carousel-inner.onebyone-carosel { margin: auto; width: 90%; }
    .onebyone-carosel .active.left { left: -33.33%; }
    .onebyone-carosel .active.right { left: 33.33%; }
    .onebyone-carosel .next { left: 33.33%; }
    .onebyone-carosel .prev { left: -33.33%; }

</style>
<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="<?= base_url() ?>">Home</a></li>
            <li>Product</li>

        </ul>

        <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->

        <div class="row">

            <main class="col-md-12 col-sm-12">

                <!-- - - - - - - - - - - - - - Product images & description - - - - - - - - - - - - - - - - -->

                <section class="section_offset">

                    <div class="clearfix">

                        <!-- - - - - - - - - - - - - - Product image column - - - - - - - - - - - - - - - - -->

                         <div class="single_product">
                            <?php
                            if ($product_images == NULL) {
                                echo '<img src="' . base_url() . 'images/no_image.jpg"/>';
                            } else {
                                ?>
                                <div class="image_preview_container" style="height: 300px;border: 1px solid #eee;padding: 5px;margin-bottom: 15px;overflow-y: hidden;">
                                    <?php
                                    foreach ($product_images as $row) {
                                        if ($row->is_main == 1) {
                                            ?>
                                            <img id="img_zoom" data-zoom-image="<?= $row->image_url ?>" src="<?= $row->image_url ?>" alt="" style="height: 300px;padding: 5px;">

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="product_preview">

                                    <div class="owl_carousel" id="thumbnails">
                                        <?php
                                        foreach ($product_images as $row) {
                                            ?>
                                            <a href="#" data-image="<?= $row->image_url ?>" data-zoom-image="<?= $row->image_url ?>" style="text-align: center;
                                               float: left;
                                               position: relative;
                                               vertical-align: middle;
                                               border: 1px solid #eee;
                                               padding: 5px;border-radius: 3px;">

                                                <img src="<?= $row->image_url ?>" data-large-image="<?= $row->image_url ?>" alt="" style="max-height: 70px;">

                                            </a>
                                        <?php } ?>

                                    </div>

                                </div>
                            <?php } ?>
                        </div>

                        <!-- - - - - - - - - - - - - - End of product image column - - - - - - - - - - - - - - - - -->
                        <form class="form-horizontal" method="post" action="" name="rfqp_form" id="rfqp_form">

                    <fieldset style="background: #f9f9f9;padding: 10px;border-radius: 3px;">



                        <!-- Form Name -->
                        <div class="col-md-12">
                            
                            <div id="successmessage"></div>
                            
                            <div class="heading">I want quotes for the product described below:</div>
                        </div>
                        <div id="rfq_container" class="rfq_container">

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="rfq_product_name">Product Name <span class="color-red">*</span></label>
                                <div class="col-md-6">
                                    <input id="rfq_product_name" name="rfq_product_name" type="text" class="form-control input-md" readonly value="<?=$product->product_name?>" >
                                    <input id="rfq_seller_id" name="rfq_seller_id" type="hidden" class="form-control input-md"  value="<?=$product->member_id?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="rfq_product_description">Product Details <span class="color-red">*</span></label>
                                <div class="col-md-6">
                                    <textarea id="rfq_product_description" name="rfq_product_description" type="text" rows="5" class="form-control input-md" value="<?=$product->product_description?>"></textarea>


                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Attachment</label>
                                <div class="col-md-3">
                                    <input class="btn btn-default btn-sm" type="file" name="rfq_product_image">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="rfq_product_quantity">Product Quantity <span class="color-red">*</span></label>
                                <div class="col-md-3">
                                    <input id="rfq_product_quantity" name="rfq_product_quantity" type="text" class="form-control input-md">
                                </div>
                                <div class="col-md-3">
                                    <select name="rfq_product_uom" class="form-control">
                                        <?php foreach ($uom as $uoms) {
                                            ?>

                                            <option value="<?php echo $uoms->uom_id; ?>" name="<?php echo $uoms->uom_name; ?>"><?php echo $uoms->uom_name; ?></option>

                                        <?php } ?>
                                    </select>
                                </div>
                            </div>


                        </div>




                      <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <label class="col-md-9 checkbox-inline font14 color-333" for="">
                            <input type="checkbox" name="" value=""   style=" width: 30px">
                            I agree to share my Business Card with quoted suppliers.
                        </label>
                        <br/>
                        <label class="col-md-3 control-label"></label>
                        <label class="col-md-9 checkbox-inline font14 color-333" for="">
                            <input type="checkbox" name="" value=""   style=" width: 30px">
                            I have read, understood and agree to abide by the Buying Request Posting Rules
                        </label>
                    </div>
                    <div class="form-group" style="clear:left;clear: right">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-6">
                            <button type='submit' id="add-rfq" value="rfqp-submit" name="rfqp-submit" class="btn button_blue">Get Quotation</button>
                        </div>
                    </div>


                    </fieldset>

                </form>


                    </div>

                </section><!--/ .section_offset -->

                <!-- - - - - - - - - - - - - - End of product images & description - - - - - - - - - - - - - - - - -->


                <!-- - - - - - - - - - - - - - Related products - - - - - - - - - - - - - - - - -->
                <section class="section_offset">

                    <h3 class="offset_title">Other Products From This Seller</h3>

                    <div class="owl_carousel five_items">

                        <?php foreach ($products as $product) { ?>
                                <div class="product_item">
                                    <a href="" class="image_wrap">
                                        <?php
                                        if (!empty($product->images_url)) {
                                            echo "<img src='" . $product->images_url[0]->image_url . "' alt='' >";
                                        } else {
                                            echo "<img src='" . base_url() . "images/no_image.jpg'>";
                                        }
                                        ?>
                                    </a>
                                    <div class="description">

                                        <div class="clearfix product_info">
                                            <a class="font14 alignleft" href="#"><?php if (strlen($product->product_name) > 36) {
                                        echo substr($product->product_name, 0, 36) . '...';
                                    } else {
                                        echo $product->product_name;
                                    } ?></a><br/>
                                            <p class="font12 alignleft"><b><?= $product->product_minimun_order ?> </b> <?= $product->uom_name ?> <span class="color-999">(Min. Order)</span></p>
                                        </div>
                                    </div>
                                </div>
<?php } ?>

                    </div><!--/ .owl_carousel -->

                </section><!--/ .section_offset -->

                <!-- - - - - - - - - - - - - - End of related products - - - - - - - - - - - - - - - - -->

            </main><!--/ [col]-->

        </div><!--/ .row-->

    </div><!--/ .container-->

</div><!--/ .page_wrapper-->

<script>
    $('.selected-img').on('mouseenter', function () {
        var me = $(this).attr('src');
        var link = 'url(' + me + ')';
        console.log(link);
        $('.img_zoom').attr("src", me);
        $('.img_zoom').attr("data-zoom-image", me);
        $('.zoomWindowContainer div:last').css('background-image', link);
//        var tar = $('.preview');
//
//        me.siblings('.active').removeClass('active');
//        tar.attr('src', me.attr('src'));
    });
</script>
<script>
    $(document).ready(function () {
        $('#myCarousel').carousel({
            interval: 10000
        })
        $('.fdi-Carousel .item').each(function () {
            var next = $(this).next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            //next.children(':first-child').clone().appendTo($(this));

            //if (next.next().length > 0) {
            //next.next().children(':first-child').clone().appendTo($(this));
            //}
            //else {
            //   $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
            // }
        });
    });
</script>
<script src="<?= base_url() ?>js/invoicescript.js" type="text/javascript"></script>
<?php include 'footer.php'; ?>
