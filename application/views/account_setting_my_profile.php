<?php include 'header2.php'; ?>
<script>
    document.getElementById("account_setting").className = "active-seller-menu";</script>
<div class="page_wrapper type_2" >
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                <?php include 'inc/seller_account_menu.php'; ?>
                <script>
                    document.getElementById("my_profile").className = "active";</script>
                <div class="col-md-10">

                    <form id="account_setting_my_profile" class="form-horizontal" method="post" action="<?= base_url('index.php/seller/update_my_profile');?>" name="my_proflie_form" enctype="multipart/form-data">

                        <h4 class="heading" style="background: #f5f5f5;padding-left: 10px;">Key Information <span class="small-desc">(This information will be used to create your Business Card.)</span></h4>

                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Business Pattern</label>
                            <div class="col-md-6">
                                <div class="radio" style="min-height: 5px">
                                    <label id="business_1" style="padding-top: 0px">
                                        <input name="business_pattern" type="radio" style=" width: 30px;margin-top: 1px" checked/> I represent a company
                                    </label>
                                </div>
                                <br/>
                                <div class="radio" style="min-height: 5px">
                                    <label id="business_2" style="padding-top: 0px">
                                        <input name="business_pattern" type="radio" style=" width: 30px;margin-top: 1px" /> I do not represent any company
                                    </label>
                                </div>

                            </div>
                        </div>

                        <fieldset style="border: 1px dotted #bebebe;border-radius: 3px;padding: 10px">
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Name</label>
                                <div class="col-md-6">
                                    <input name="member_name" type="text" placeholder="Full Name" class="form-control input-md" value="<?= $user->user_name?>"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Gender</label>
                                <div class="col-md-6 radio" style="padding-left: 25px">
                                    <label style="padding-top: 0px">
                                        <input name="gender" type="radio" value="male" style="width: 30px;margin-top: 1px" <?php if($user->gender== "male"){echo "checked=checked";}?>> Male
                                    </label >
                                    <label style="margin-left: 20px;padding-top: 0px">
                                        <input name="gender" type="radio" value="female" style="width: 30px;margin-top: 1px" <?php if($user->gender == "female"){echo "checked=checked";}?>> Female
                                    </label>
                                </div>
                            </div>
                            <div id="job_title" class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Job Title</label>
                                <div class="col-md-6">
                                    <input name="job_title" type="text" placeholder="Job Title" class="form-control input-md" value="<?= $user->job_title;?>"  >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333">Email</label>
                                <div class="col-md-6 color-333 font12" style="padding-top: 3px;">
                                    <?= $user->user_email;?> <a href="<?= base_url() ?>index.php/seller/account_setting/change_email_address/" class="color-blue underline"> Change email address</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Contact Address</label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Street</label>
                                <div class="col-md-4">
                                    <input  name="street_adddress" type="text" class="form-control input-md" value="<?= $user->street_adddress?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">City</label>
                                <div class="col-md-4">
                                    <input  name="city" type="text" class="form-control input-md" value="<?= $user->city?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Province/State/Country</label>
                                <div class="col-md-4">
                                    <input  name="province" type="text" class="form-control input-md"  value="<?= $user->province ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Country/Region</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="country">
                                        <option value="">--Please Select One</option>
                                        <?php foreach ($countries as $country) { ?>
                                            <option value="<?= $country->country_id ?>" <?php if($user->country == $country->country_id ){echo "selected=selected";}?>><?= $country->country_name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"></label>
                                <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Zip Code</label>
                                <div class="col-md-4">
                                    <input  name="zip_code" type="text" class="form-control input-md" value="<?= $user->zip_code?>"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Tel</label>
                                <div class="col-md-2 font12">
                                    Country Code
                                    <input  name="phone[]" type="text" class="form-control input-md" value="<?= $user_phone[0]?>" >
                                </div>
                                <div class="col-md-2 font12">
                                    Area Code
                                    <input  name="phone[]" type="text" class="form-control input-md" value="<?= $user_phone[1]?>"  >
                                </div>
                                <div class="col-md-3 font12">
                                    Number
                                    <input  name="phone[]" type="text" class="form-control input-md"  value="<?= $user_phone[2]?>" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label color-333">Fax</label>
                                <div class="col-md-2 font12">
                                    Country Code
                                    <input  name="fax[]" type="text" class="form-control input-md" value="<?= $user_fax[0]?>"  >
                                </div>
                                <div class="col-md-2 font12">
                                    Area Code
                                    <input  name="fax[]" type="text" class="form-control input-md" value="<?= $user_fax[1]?>"  >
                                </div>
                                <div class="col-md-3 font12">
                                    Number
                                    <input  name="fax[]" type="text" class="form-control input-md" value="<?= $user_fax[2]?>"  >
                                </div>
                            </div>
                        </fieldset>
                        <br/>
                        <div id="company_name" class="form-group">
                            <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Company Name</label>
                            <div class="col-md-4">
                                <input  name="company_name" type="text" class="form-control input-md" value="<?= $company->company_name?>">
                            </div>
                        </div>
                        <div id="business_type" class="form-group">
                            <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Business Type</label>
                            <div class="col-md-4">
                                <select class="form-control" name="business_type">
                                    <option value="">--Please Select One</option>
                                    <?php foreach ($business_types as $b_types) { ?>
                                        <option value="<?= $b_types->business_type_id ?>" <?php if($user->business_type == $b_types->business_type_id){ echo "selected";}?>><?= $b_types->business_type_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"><span style="color:red">*</span> Website</label>
                            <div class="col-md-4 radio" style="padding-left: 25px">
                                <label style="margin-top:0px">
                                    <input id="website_yes" name="website_opt" type="radio" checked style="width: 30px;padding-top: 1px;"> I have one
                                </label>
                                <label style="margin-left: 20px;margin-top:0px">
                                    <input id="website_no" name="website_opt" type="radio" style="width: 30px;padding-top: 1px"> I don't have one
                                </label>
                            </div>
                        </div>
                        <div id="website_field" class="form-group">
                            <label class="col-md-3 control-label color-333"></label>
                            <div class="col-md-4">
                                <input name="website_url" type="text" class="form-control input-md" value="<?php if(!empty($company->website_url)){echo $company->website_url;}else{ echo "http://";} ?>">
                                <span class="small-desc">Example : http://www.example.com</span>
                            </div>
                        </div>
                        <h4 class="heading" style="background: #f5f5f5;padding-left: 10px;">More Information <span class="small-desc">(A complete profile will allow Rbctrades.com to provide you with better overall service.)</span></h4>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333">Industry we are in</label>
                            <div class="col-md-4">
                                <input name="in_industry" type="text" class="form-control input-md" value="<?= $company->in_industry;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333">Purchasing Frequency</label>
                            <div class="col-md-4">
                                <select class="form-control" name="purchasing_frequency">
                                    <option value="">--Please Select One</option>
                                    <?php foreach($purchasing_fre as $values){?>
                                            <option value="<?= $values->id;?>" <?php if($values->id == $company->purchasing_frequency[0]){echo "selected=selected";}?>><?= $values->purchasing_frequency;?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333">Annual Purchase Volume</label>
                            <div class="col-md-4">
                                <select class="form-control" name="annual_purchase_volume">
                                    <option value="">--Please Select One</option>
                                    <option value="below_10k" <?php if($company->annual_purchase_volume == "below_10k"){echo "selected=selected";}?>>Below US$10,000</option>
                                    <option value="10k-100K" <?php if($company->annual_purchase_volume == "10k-100K"){echo "selected=selected";}?>>US$10,000 - US$100,000</option>
                                    <option value="100k-300k" <?php if($company->annual_purchase_volume == "100k-300k"){echo "selected=selected";}?>>US$100,000 - US$300,000</option>
                                    <option value="300k-500k" <?php if($company->annual_purchase_volume == "300k-500k"){echo "selected=selected";}?>>US$300,000 - US$500,000</option>
                                    <option value="500k-1m" <?php if($company->annual_purchase_volume == "500k-1m"){echo "selected=selected";}?>>US$500,000 - US$1 Million</option>
                                    <option value="1m-5m" <?php if($company->annual_purchase_volume == "1m-5m"){echo "selected=selected";}?>>US$1 Million - US$5 Million</option>
                                    <option value="above_5m" <?php if($company->annual_purchase_volume == "above_5m"){echo "selected=selected";}?>>Above US$5 Million</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333">Preferred Seller Location</label>
                            <div class="col-md-4">
                                <select class="form-control" name="preffered_seller_location[]">
                                    <option value="">--Please Select One</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?= $country->country_id ?>" <?php if($country->country_id == $company->preffered_seller_location[0]){echo "selected=selected";}?>><?= $country->country_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"></label>
                            <div class="col-md-4">
                                <select class="form-control" name="preffered_seller_location[]">
                                    <option value="">--Please Select One</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?= $country->country_id?>" <?php if($company->preffered_seller_location[1]==$country->country_id){echo "selected=selected";}?>><?= $country->country_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"></label>
                            <div class="col-md-4">
                                <select class="form-control" name="preffered_seller_location[]">
                                    <option value="">--Please Select One</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?= $country->country_id?>" <?php if($company->preffered_seller_location[2]==$country->country_id){echo "selected=selected";}?>><?= $country->country_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label color-333">Preferred Seller Type</label>
                            <div class="col-sm-6"><?php foreach ($business_types as $type) { ?>
                                <div class="checkbox">
                                        <label for="business_<?= $type->business_type_id; ?>" style="padding-top:0px;">
                                            <input name="preffered_seller_type[]" id="business_<?= $type->business_type_id; ?>" value="<?= $type->business_type_id; ?>" type="checkbox" style=" width: 30px;margin-top: 1px;" <?php
                                            if (in_array($type->business_type_id, $company->preffered_seller_type)) {
                                                echo "checked=checked";
                                            }
                                            ?>><?= $type->business_type_name; ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333">Business Experiences</label>
                            <div class="col-md-6">
                                <textarea name="business_experiences" rows="10" class="form-control"><?= $company->business_experiences; ?></textarea>
                            </div>
                        </div>
                        <div id="company_information">
                        <h4 class="heading" style="background: #f5f5f5;padding-left: 10px;">Company Information<span class="small-desc"></span></h4>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="company_logo">Company logo:</label>
                            <div class="col-md-4">
                                <input id="company_logo" type="file" name="company_logo"><br/>
                                <?php if($company->logo_url){?>
                                    <input type="hidden" name="old_logo_name" value="<?= $company->logo_url?>">
                                <?php }?>
                                <div class="img-div">
                                <img id="image_upload_preview" src="<?php if($company->logo_url){echo $company->logo_url;}else{ echo base_url()."images/default.png";}?>"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333">Company Introduction</label>
                            <div class="col-md-6">
                                <textarea name="company_description" rows="10" class="form-control" ><?= $company->company_description;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"> Company Address</label>
                            <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Street</label>
                            <div class="col-md-4">
                                <input  name="operational_street_address" type="text" class="form-control input-md" value="<?= $company->operational_street_address?>"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"></label>
                            <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">City</label>
                            <div class="col-md-4">
                                <input  name="operational_address_city" type="text" class="form-control input-md" value="<?= $company->operational_address_city?>"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"></label>
                            <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Province/State/Country</label>
                            <div class="col-md-4">
                                <input  name="operational_address_province" type="text" class="form-control input-md" value="<?= $company->operational_address_province?>"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"></label>
                            <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Country/Region</label>
                            <div class="col-md-4">
                                <select class="form-control" name="operational_location">
                                    <option value="">--Please Select One</option>
                                    <?php foreach ($countries as $country) { ?>
                                        <option value="<?= $country->country_id ?>" <?php if($company->operational_location==$country->country_id){echo "selected=selected";}?>><?= $country->country_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label color-333"></label>
                            <label class="col-md-2 control-label" style="text-align: left !important;font-weight: normal;">Zip Code</label>
                            <div class="col-md-4">
                                <input  name="operational_address_zip_code" type="text" class="form-control input-md" value="<?= $company->operational_address_zip_code;?>"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-5 control-label color-333"></label>
                            <div class="col-md-6" style="text-align: left !important;font-weight: normal;">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" style=" width: 30px"> My company address is the same as my contact address.
                                    </label>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="dash-line"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-9" style="margin-top: 15px;">
                                        <input type="submit" class="button_blue" id="submit_company_information" name="my_profile_submit" value="Submit">
                                    </div>
                                    </div>
                        </div>
                    </form>
                </div>
            </div><!--/ [col]-->
            <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
        </div><!--/ .row-->
        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->
<script>
    $("#business_1").click(function() {
        $('#job_title').show(500);
        $('#company_name').show();
        $('#business_type').show();
        $('#company_information').show();
    });
    $("#business_2").click(function() {
        $('#job_title').hide(500);
        $('#company_name').hide();
        $('#business_type').hide();
        $('#company_information').hide();
    });
    $("#website_yes").click(function() {
        $('#website_field').show(500);
    });
    $("#website_no").click(function() {
        $('#website_field').hide(500);
    });
</script>
<?php include 'footer.php'; ?>