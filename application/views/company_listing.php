<?php include 'header.php'; 
//echo '<pre>';
//print_r($companies);
//exit();


$count = 1;?>
<!-- - - - - - - - - - - - - - Page Wrapper - - - - - - - - - - - - - - - - -->

<div class="secondary_page_wrapper">

    <div class="container">

        <!-- - - - - - - - - - - - - - Breadcrumbs - - - - - - - - - - - - - - - - -->

        <ul class="breadcrumbs">

            <li><a href="<?= base_url() ?>">Home</a></li>
            <li><a href="#">Buyers</a></li>
            <li>Company List</li>

        </ul>

        <!-- - - - - - - - - - - - - - End of breadcrumbs - - - - - - - - - - - - - - - - -->

        <div class="row">
            <?php include 'inc/company_filter.php'; ?>
            <div class="col-md-9 col-sm-8">

                <h1>Manufacturers</h1>

                <div class="table_layout">

                    <div class="table_row">
                        
                        <?php foreach ($companies as $values) { ?>
                                          
                        <div class="table_cell">

                            <!-- - - - - - - - - - - - - - Manufacturer - - - - - - - - - - - - - - - - -->

                            <figure class="manufacturer">

                                <a href="shop_manufacturer_page.html" class="thumbnail">
                                    <?php
                                        if (!empty($values->logo_url)) {
                                            echo "<img src='" . $values->logo_url . "' alt='' >";
                                        } else {
                                            echo "<img src='" . base_url() . "images/no_image.jpg'>";
                                        }
                                    ?>
                                   

                                </a>

                                <figcaption>

                                    <a href="shop_manufacturer_page.html"><?=$values->company_name ?> </a>

                                </figcaption>

                            </figure>

                            <!-- - - - - - - - - - - - - - End of manufacturer - - - - - - - - - - - - - - - - -->

                        </div>
                                                
                                                <?php if($count==4){
                                                    $count=0
                                                    ?>
                                            </div>
                                        <div class="table_row">
                                          <?php } $count++; }?>
                        
                       <!--/ .table_row -->

                </div><!--/ .table_layout -->

            </div><!--/ [col]-->

            <footer class="bottom_box on_the_sides" id="footer_id">

                                    <div class="left_side">

                                        <lable>Page <lable id="page_number"></lable> of <lable id="total_page"></lable></lable>
                                    </div>

                                    <div class="right_side">

                                        <div class="btn-group btn-group-sm">
                                            <button type="button" id="previous" class="btn btn-default">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                            </button>
                                            <button type="button" id="next" class="btn btn-default">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                            </button>
                                        </div>

                                    </div>

            </footer>
        </div><!--/ .row-->

    </div><!--/ .container-->
    

</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>