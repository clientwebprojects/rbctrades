<?php include 'header2.php'; ?>
<script>
    document.getElementById("account_setting").className = "active-seller-menu";
</script>
<div class="page_wrapper type_2" >
    <div class="container">
            <div class="row">
                <div class="col-md-12" style="background-color:#FFF;padding-top: 20px; float: right;padding-bottom: 30px;">
                    <?php include 'inc/seller_account_menu.php'; ?>
                    <script>
                        document.getElementById("my_profile").className = "active";
                    </script>
                    <div class="col-md-10">

                            <form id="seller-add-new-product" class="form-horizontal" method="post" action="" name="add_new_product" enctype="multipart/form-data">

                                    <h4 class="heading">Key Information <span class="small-desc">This information will be used to create your Business Card.</span></h4>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Business Pattern</label>
                                        <div class="col-md-6">
                                            <div class="radio">
                                                    <label>
                                                        <input id="business_1" name="business_pattern" type="radio" style=" width: 30px" /> I represent a company
                                                    </label>
                                                <br/>
                                                <label>
                                                    <input id="business_2" name="business_pattern" type="radio" style=" width: 30px" /> I do not represent any company
                                                </label>
                                                </div>

                                        </div>
                                    </div>

                                    <fieldset style="border: 1px dotted #bebebe;border-radius: 3px;padding: 10px">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name</label>
                                        <div class="col-md-3">
                                            <input id="product_keyword" name="product_keyword" type="text" placeholder="First Name" class="form-control input-md"   >
                                        </div>
                                        <div class="col-md-3">
                                            <input id="product_keyword" name="product_keyword" type="text" placeholder="LAst Name" class="form-control input-md"   >
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

                        </div>
                </div><!--/ [col]-->
                <!-- - - - - - - - - - - - - - End of main slider - - - - - - - - - - - - - - - - -->
            </div><!--/ .row-->
        <!-- - - - - - - - - - - - - - Infoblocks - - - - - - - - - - - - - - - - -->
    </div><!--/ .container-->
</div><!--/ .page_wrapper-->
<?php include 'footer.php'; ?>