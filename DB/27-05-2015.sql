-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 27, 2015 at 01:59 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rbctrades`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_type`
--

CREATE TABLE IF NOT EXISTS `business_type` (
  `business_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`business_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `business_type`
--

INSERT INTO `business_type` (`business_type_id`, `business_type_name`) VALUES
(1, 'Manufacturer'),
(2, 'Trading Company'),
(3, 'Buying Office'),
(4, 'Agent'),
(5, 'Distributor/Wholesaler'),
(6, 'Government ministry/Bureau/Commission'),
(7, 'Association'),
(8, 'Business Service (Transportation, finance, travel, Ads, etc) '),
(9, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `company_certification`
--

CREATE TABLE IF NOT EXISTS `company_certification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certification_type` varchar(100) DEFAULT NULL,
  `reference_no` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `issued_by` varchar(100) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `ending_date` date DEFAULT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  `scope` text,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_master`
--

CREATE TABLE IF NOT EXISTS `company_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_type` text,
  `business_pattern` tinyint(1) NOT NULL DEFAULT '1',
  `company_name` varchar(100) NOT NULL,
  `contact_no` varchar(100) DEFAULT NULL,
  `registration_location` int(11) NOT NULL,
  `operational_street_address` varchar(100) DEFAULT NULL,
  `operational_address_city` varchar(100) DEFAULT NULL,
  `operational_address_province` varchar(100) DEFAULT NULL,
  `operational_location` int(11) DEFAULT NULL,
  `operational_address_zip_code` varchar(100) DEFAULT NULL,
  `main_products` text,
  `other_products` text,
  `registered_year` varchar(4) DEFAULT NULL,
  `total_employees` varchar(100) DEFAULT NULL,
  `website_url` varchar(100) DEFAULT NULL,
  `website_opt` tinyint(1) NOT NULL DEFAULT '1',
  `legal_owner` varchar(100) DEFAULT NULL,
  `office_size` varchar(100) DEFAULT NULL,
  `company_advantages` text,
  `annual_sales_volume` varchar(100) DEFAULT NULL,
  `export_percentage` varchar(100) DEFAULT NULL,
  `exporting_started` varchar(4) DEFAULT NULL,
  `trade_staff_num` int(11) DEFAULT NULL,
  `cross_industry` tinyint(1) DEFAULT '0',
  `rnd_staff_num_trade` varchar(100) DEFAULT NULL,
  `qc_staff_num_trade` varchar(100) DEFAULT NULL,
  `export_port` text,
  `avg_delivery_time` int(11) DEFAULT NULL,
  `have_oversea_office` tinyint(1) DEFAULT '0',
  `delievery_terms` text,
  `support_currency` text,
  `payment_methods` text,
  `language_skills` text,
  `logo_url` varchar(100) DEFAULT NULL,
  `photo_url` varchar(100) DEFAULT NULL,
  `company_description` text,
  `in_industry` varchar(100) DEFAULT NULL,
  `purchasing_frequency` varchar(50) DEFAULT NULL,
  `annual_purchase_volume` varchar(50) DEFAULT NULL,
  `preffered_seller_location` varchar(50) DEFAULT NULL,
  `preffered_seller_type` text,
  `business_experiences` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company_master`
--

INSERT INTO `company_master` (`id`, `business_type`, `business_pattern`, `company_name`, `contact_no`, `registration_location`, `operational_street_address`, `operational_address_city`, `operational_address_province`, `operational_location`, `operational_address_zip_code`, `main_products`, `other_products`, `registered_year`, `total_employees`, `website_url`, `website_opt`, `legal_owner`, `office_size`, `company_advantages`, `annual_sales_volume`, `export_percentage`, `exporting_started`, `trade_staff_num`, `cross_industry`, `rnd_staff_num_trade`, `qc_staff_num_trade`, `export_port`, `avg_delivery_time`, `have_oversea_office`, `delievery_terms`, `support_currency`, `payment_methods`, `language_skills`, `logo_url`, `photo_url`, `company_description`, `in_industry`, `purchasing_frequency`, `annual_purchase_volume`, `preffered_seller_location`, `preffered_seller_type`, `business_experiences`) VALUES
(1, NULL, 1, 'Noriyah', '03217254041', 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_partner`
--

CREATE TABLE IF NOT EXISTS `company_partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_factory_name` varchar(100) DEFAULT NULL,
  `coorporation_contract` varchar(100) DEFAULT NULL,
  `partner_duration` varchar(100) DEFAULT NULL,
  `partner_annual_amount` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_privacy_setting`
--

CREATE TABLE IF NOT EXISTS `company_privacy_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `basic_information` int(11) NOT NULL,
  `contact_information` int(11) NOT NULL,
  `sourcing_information` int(11) NOT NULL,
  `activity_summary` int(11) NOT NULL,
  `watched_industries` int(11) NOT NULL,
  `most_searched_keywords` int(11) NOT NULL,
  `recently_searched_products` int(11) NOT NULL,
  `transactions` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_master`
--

CREATE TABLE IF NOT EXISTS `member_master` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_name` varchar(500) NOT NULL,
  `member_type` varchar(50) NOT NULL COMMENT 'buyer, seller, both',
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`member_id`),
  UNIQUE KEY `company_id` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `member_master`
--

INSERT INTO `member_master` (`member_id`, `member_name`, `member_type`, `company_id`) VALUES
(1, 'Muhammad Asad Saleem', 'both', 1);

-- --------------------------------------------------------

--
-- Table structure for table `message_center`
--

CREATE TABLE IF NOT EXISTS `message_center` (
  `message_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` text,
  `body` text,
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_center_mapped`
--

CREATE TABLE IF NOT EXISTS `message_center_mapped` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `main_user_email` varchar(100) NOT NULL,
  `temp_user_email` varchar(100) NOT NULL,
  `placeholder_id` int(11) NOT NULL COMMENT '1-Inbox, 2- Important, 3-Sent, 4-Trash',
  `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-Not Read, 1-Read',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_placeholder`
--

CREATE TABLE IF NOT EXISTS `message_placeholder` (
  `placeholder_id` int(11) NOT NULL AUTO_INCREMENT,
  `placeholder_name` varchar(100) NOT NULL,
  PRIMARY KEY (`placeholder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `message_placeholder`
--

INSERT INTO `message_placeholder` (`placeholder_id`, `placeholder_name`) VALUES
(1, 'All Inquiries'),
(2, 'Important'),
(3, 'Sent Box'),
(4, 'Trash');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `is_main` tinyint(1) NOT NULL,
  PRIMARY KEY (`image_id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `product_id`, `image_url`, `is_main`) VALUES
(1, 1, 'http://localhost/bitbucket/rbctrades/uploads/product_images/zzytgq.jpg', 1),
(2, 1, 'http://localhost/bitbucket/rbctrades/uploads/product_images/b79uux.png', 0),
(3, 2, 'http://localhost/bitbucket/rbctrades/uploads/product_images/5r5rsh.png', 1),
(4, 3, 'http://localhost/bitbucket/rbctrades/uploads/product_images/kdc9na.jpeg', 1),
(5, 3, 'http://localhost/bitbucket/rbctrades/uploads/product_images/khso82.png', 0),
(6, 4, 'http://localhost/bitbucket/rbctrades/uploads/product_images/xd1az1.jpg', 1),
(7, 9, 'http://localhost/bitbucket/rbctrades/uploads/product_images/t2hnfr.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

CREATE TABLE IF NOT EXISTS `product_master` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` int(11) NOT NULL,
  `product_code` varchar(100) DEFAULT NULL,
  `product_model` varchar(100) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `images` int(11) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `product_description` varchar(500) DEFAULT NULL,
  `product_keyword` text,
  `uom_id` int(11) DEFAULT NULL,
  `product_status` varchar(100) NOT NULL DEFAULT 'P' COMMENT 'P-Pending, A-Approved, R-Rejected',
  `product_origin` varchar(100) NOT NULL,
  `product_type` varchar(100) DEFAULT NULL,
  `product_use` varchar(100) DEFAULT NULL,
  `product_minimun_order` int(11) DEFAULT NULL,
  `minimun_order_unit` int(11) DEFAULT NULL,
  `product_port` varchar(100) DEFAULT NULL,
  `product_payment_type` varchar(100) DEFAULT NULL,
  `product_supply_ability` int(11) DEFAULT NULL,
  `product_delivery_time` varchar(100) DEFAULT NULL,
  `product_delivery_unit` int(11) DEFAULT NULL,
  `packaging_detail` text,
  `product_detailed_description` text,
  `product_fob_price` int(11) DEFAULT NULL,
  `product_fob_unit` int(11) DEFAULT NULL,
  `product_slug` varchar(60) DEFAULT NULL,
  `product_fob_currency` int(11) DEFAULT NULL,
  `product_fob_price_from` float DEFAULT NULL,
  `product_fob_price_to` float DEFAULT NULL,
  `product_add_date` datetime NOT NULL,
  `product_last_updated` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `product_master`
--

INSERT INTO `product_master` (`product_id`, `product_category`, `product_code`, `product_model`, `brand_id`, `images`, `product_name`, `product_description`, `product_keyword`, `uom_id`, `product_status`, `product_origin`, `product_type`, `product_use`, `product_minimun_order`, `minimun_order_unit`, `product_port`, `product_payment_type`, `product_supply_ability`, `product_delivery_time`, `product_delivery_unit`, `packaging_detail`, `product_detailed_description`, `product_fob_price`, `product_fob_unit`, `product_slug`, `product_fob_currency`, `product_fob_price_from`, `product_fob_price_to`, `product_add_date`, `product_last_updated`) VALUES
(1, 29, '13123', '2013', 23123, NULL, 'Angoor', 'asdas', 'angoor, malta, apple', 1, 'A', '1', 'asd', NULL, 123, 1, 'dasdsa', '1|2|3|4|', 213213, '123', 1, 'adsasd', '<p>asdasdadssad</p>\r\n', NULL, 1, 'angoor-----1cgaqfe', 0, 123123, 213123000, '2015-05-25 11:17:21', '2015-05-27 04:30:49'),
(3, 11, '21321321311122', '123', 123213, NULL, 'Bnana', '213213', 'Bnana', 1, 'A', '11', '123', NULL, 213, 2, 'sfddsf', '1|2|', 213, '1231', 2, '123213', '<p>asdasd</p>\r\n', NULL, 2, 'bnana-----1itb6y9', 0, 123, 1222, '2015-05-25 12:03:26', '2015-05-27 04:36:53'),
(4, 5, '23432', '32424', 234, NULL, 'asdasdasdadsdsa', '2344', 'asdasdasdads', 1, 'A', '1', '324', NULL, 324, 1, '234', '1|', 234, '234', 1, '234', NULL, NULL, 0, 'asdasdasdadsdsa-----1z7h6o7', 1, 234, 324, '2015-05-25 12:01:53', '0000-00-00 00:00:00'),
(7, 3, '123123123123', '123213213312', 123, NULL, 'ttt', '213321', 'ttt', 3, 'A', '14', '123', NULL, 121, 0, 'fdfds', '1|', 443, '33', 2, 'EWQ', '<p>sdas</p>\r\n', NULL, 2, 'ttt-----1gtf247', 0, 11, 22, '2015-05-25 12:12:42', '2015-05-27 05:13:05'),
(8, 3, '123123123123', '123213213312', 123, NULL, 'ttt', '213321', 'ttt', 3, 'A', '14', '123', NULL, 121, 0, 'fdfds', '1|', 443, '33', 2, 'EWQ', NULL, NULL, 2, 'ttt-----17981ge', 2, 11, 22, '2015-05-25 12:13:48', '0000-00-00 00:00:00'),
(9, 4, 'qw', 'qw', 0, NULL, '7 up', 'qw', '7 up', 1, 'A', '1', 'qw', NULL, 12, 1, 'df', '1|', 12, '12', 1, '12', '<p>saddasdeee</p>\r\n', NULL, 1, '7-up----1opsgz7', 0, 112222, 1222, '2015-05-27 04:49:12', '2015-05-27 05:05:39'),
(10, 1, 'ads', 'as', 0, NULL, 'asdsadasdasd asds asddasdsa sadsadads asdadsasdads saddasd sadsad', '', 'asddasdas', 2, 'A', '2', '', NULL, 1, 1, '123', '1|', 231, '213', 1, '213', NULL, NULL, 1, 'asdsadasdasd-asds-asddasdsa-sadsadads-asdadsasdads-1uy0fnf', 1, 21, 2132, '2015-05-27 05:07:29', '0000-00-00 00:00:00'),
(11, 6, '', 'saddas', 0, NULL, 'assad', '', 'saddas', 1, 'A', '2', '', NULL, 34, 2, '', '7|', 0, '34', 2, '', '<p>It links</p>\r\n', NULL, 0, 'assad-----1dkub2m', 0, 0, 0, '2015-05-27 04:23:41', '2015-05-27 04:29:31');

-- --------------------------------------------------------

--
-- Table structure for table `product_member`
--

CREATE TABLE IF NOT EXISTS `product_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `product_member`
--

INSERT INTO `product_member` (`id`, `product_id`, `member_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_specification`
--

CREATE TABLE IF NOT EXISTS `product_specification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `product_specification`
--

INSERT INTO `product_specification` (`id`, `product_id`, `attribute`, `value`) VALUES
(1, 6, '123', 'dd'),
(10, 7, 'asasd', '2222'),
(11, 9, 'coloe', '11'),
(20, 9, 'rrr', '666'),
(21, 9, 'asad', '555'),
(22, 10, '123', '213'),
(23, 10, '123', '23'),
(24, 11, 'asd', 'asd'),
(25, 11, 'color', 'red'),
(26, 1, 'type', 'fruit'),
(27, 1, 'size', 'MEDIUM'),
(28, 3, 'color', 'red'),
(30, 3, 'type', 'fruit');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

CREATE TABLE IF NOT EXISTS `quotation_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quot_no` int(11) NOT NULL DEFAULT '0',
  `product_name` varchar(100) NOT NULL,
  `description` tinytext,
  `unit_price` double NOT NULL DEFAULT '0',
  `uom_id` int(11) NOT NULL,
  `quantity` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sorder` (`quot_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_master`
--

CREATE TABLE IF NOT EXISTS `quotation_master` (
  `quot_no` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL DEFAULT '0',
  `quotation_date` date NOT NULL DEFAULT '0000-00-00',
  `quotation_validity_date` date NOT NULL DEFAULT '0000-00-00',
  `currency_id` int(11) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `tax` float(10,2) NOT NULL,
  PRIMARY KEY (`quot_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_annual_purchasing_volume`
--

CREATE TABLE IF NOT EXISTS `rbc_annual_purchasing_volume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annual_purchasing_volume` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_banners`
--

CREATE TABLE IF NOT EXISTS `rbc_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `category_location` text NOT NULL,
  `banner_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_banner_locations`
--

CREATE TABLE IF NOT EXISTS `rbc_banner_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_location` text NOT NULL,
  `location_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_category`
--

CREATE TABLE IF NOT EXISTS `rbc_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=145 ;

--
-- Dumping data for table `rbc_category`
--

INSERT INTO `rbc_category` (`category_id`, `category_name`, `parent_id`) VALUES
(1, 'Agriculture', 0),
(2, 'Automobiles & Transportation', 0),
(3, 'Chemicals', 0),
(4, 'Computers', 0),
(5, 'Construction & Real Estate', 0),
(6, 'Consumer Electronics', 0),
(7, 'Electrical Equipment', 0),
(9, 'Agricultural Equipment', 1),
(10, 'Grain', 1),
(11, 'Fresh Fruit', 1),
(12, 'Plant & Animal Oil', 1),
(13, 'Fresh Vegetables', 1),
(14, 'Animal Feed', 1),
(15, 'Nuts & Kernels', 1),
(16, 'Plant Seeds & Bulbs', 1),
(17, 'Beans', 1),
(18, 'Timber Raw Materials', 1),
(19, 'Ornamental Plants', 1),
(20, 'Coffee Beans', 1),
(21, 'Mushrooms & Truffles', 1),
(22, 'Animal Products', 1),
(23, 'Auto Electrical System', 2),
(24, 'Auto Chassis Parts', 2),
(25, 'Auto Engine', 2),
(26, 'Auto Lighting System', 2),
(27, 'Interior Accessories', 2),
(28, 'Exterior Accessories', 2),
(29, 'Car Care & Cleaning', 2),
(30, 'Vehicle Tools', 2),
(31, 'Vehicle Equipment', 2),
(32, 'Motorcycles', 2),
(33, 'Motorcycle Parts', 2),
(34, 'Motorcycle Accessories', 2),
(35, 'Additives', 3),
(36, 'Adhesives & Sealants', 3),
(37, 'Agrochemicals', 3),
(38, 'Basic Organic Chemicals', 3),
(39, 'Catalysts & Chemical Auxiliary Agents', 3),
(40, 'Chemical Machinery & Equipment', 3),
(41, 'Petrochemical Products', 3),
(42, 'Chemical Reagent Products', 3),
(43, 'Chemical Waste', 3),
(44, 'All-In-One PC', 4),
(45, 'Hard Drives', 4),
(46, 'Keyboards', 4),
(47, 'Laptops', 4),
(48, 'Monitors', 4),
(49, 'Motherboards', 4),
(50, 'Tablet PCs', 4),
(51, 'USB Flash Drives', 4),
(52, 'CCTV Products', 4),
(53, 'Access Control Systems & Products', 4),
(54, 'Locks', 4),
(55, 'Police & Military Products', 4),
(56, 'Agriculture Machinery', 5),
(57, 'Apparel & Textile Machinery', 5),
(58, 'Building Material Machinery', 5),
(59, 'Chemical Machinery & Equipment', 5),
(60, 'Energy & Mineral Equipment', 5),
(61, 'Engineering & Construction Machinery', 5),
(62, 'Filling Machinery', 5),
(63, 'Food & Beverage Machinery', 5),
(64, 'General Industrial Equipment', 5),
(65, 'Industry Laser Equipment', 5),
(66, 'Electronic Measuring Instruments', 5),
(67, 'Testing Equipment', 5),
(68, 'Optical Instruments', 5),
(69, 'Temperature Instruments', 5),
(70, 'Mobile Phone Chargers', 6),
(71, 'Mobile Phone Flex Cables', 6),
(72, 'Mobile Phone Holders', 6),
(73, 'Mobile Phone Housings', 6),
(74, 'Mobile Phone Keypads', 6),
(75, 'Mobile Phone LCDs', 6),
(76, 'Mobile Phone Covers', 6),
(77, 'Mobile Phone Straps', 6),
(78, 'Mobile Phones', 6),
(79, 'Portable CD Player', 6),
(80, 'MP4 Players', 6),
(81, 'MP3 Players', 6),
(82, 'Card Readers', 6),
(83, 'Chargers', 6),
(84, 'Generators', 7),
(85, 'Power Supplies', 7),
(86, 'Transformers', 7),
(87, 'Switches, Plugs & Sockets', 7),
(88, 'Wires & Cables', 7),
(89, 'Solar Cells & Panels', 7),
(90, 'Diesel Generators', 7),
(91, 'Inverters & Converters', 7),
(92, 'Rechargeable Batteries', 7),
(93, 'Alternative Energy Generators', 7),
(94, 'Connectors & Terminals', 7),
(95, 'Electronic & Instrument Enclosures', 7),
(96, 'Power Distribution Equipment', 7),
(97, 'Packaging & Printing', 7),
(112, 'Bicycle Parts', 2),
(113, 'Boats & Ships', 2),
(114, 'Agrochemicals', 3),
(115, 'Petrochemical Products', 3),
(116, 'Chemical Waste', 3),
(117, 'Basic Organic Chemicals', 3),
(118, 'Additives', 3),
(119, 'Self Deffense', 4),
(120, 'Alarms', 4),
(121, 'Tractors', 9),
(122, 'Hardware', 0),
(123, 'Health & Medical', 0),
(124, 'Abrasive Tools', 122),
(125, 'Brackets', 122),
(126, 'Fasteners', 122),
(127, 'Chains', 122),
(128, 'Screws', 122),
(129, 'Nails', 122),
(130, 'Bolts', 122),
(131, 'Nuts', 122),
(132, 'Washers', 122),
(133, 'Anchors', 122),
(134, 'Body Weight', 123),
(135, 'Plant Extracts', 123),
(136, 'Bath Supplies', 123),
(137, 'Feminine Hygiene', 123),
(138, 'Hair Salon Equipment', 123),
(139, 'Nail Supplies', 123),
(140, 'Makeup', 123),
(141, 'Oral Hygiene', 123),
(142, 'Sanitary Paper', 123),
(143, 'Skin Care', 123),
(144, 'Beauty Equipment', 123);

-- --------------------------------------------------------

--
-- Table structure for table `rbc_city`
--

CREATE TABLE IF NOT EXISTS `rbc_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_countries`
--

CREATE TABLE IF NOT EXISTS `rbc_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(50) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=243 ;

--
-- Dumping data for table `rbc_countries`
--

INSERT INTO `rbc_countries` (`country_id`, `country_code`, `country_name`) VALUES
(1, 'pk', 'Pakistan'),
(2, 'ca', 'Canada'),
(3, 'af', 'Afghanistan'),
(4, 'al', 'Albania'),
(5, 'dz', 'Algeria'),
(6, 'cn', 'China'),
(7, 'ad', 'Andorra'),
(8, 'ao', 'Angola'),
(9, 'ai', 'Anguilla'),
(10, 'aq', 'Antarctica'),
(11, 'ag', 'Antigua and/or Barbuda'),
(12, 'ar', 'Argentina'),
(13, 'am', 'Armenia'),
(14, 'aw', 'Aruba'),
(15, 'au', 'Australia'),
(16, 'at', 'Austria'),
(17, 'az', 'Azerbaijan'),
(18, 'bs', 'Bahamas'),
(19, 'bh', 'Bahrain'),
(20, 'bd', 'Bangladesh'),
(21, 'bb', 'Barbados'),
(22, 'by', 'Belarus'),
(23, 'be', 'Belgium'),
(24, 'bz', 'Belize'),
(25, 'bj', 'Benin'),
(26, 'bm', 'Bermuda'),
(27, 'bt', 'Bhutan'),
(28, 'bo', 'Bolivia'),
(29, 'ba', 'Bosnia and Herzegovina'),
(30, 'bw', 'Botswana'),
(31, 'bv', 'Bouvet Island'),
(32, 'br', 'Brazil'),
(33, 'io', 'British lndian Ocean Territory'),
(34, 'bn', 'Brunei Darussalam'),
(35, 'bg', 'Bulgaria'),
(36, 'bf', 'Burkina Faso'),
(37, 'bi', 'Burundi'),
(38, 'kh', 'Cambodia'),
(39, 'cm', 'Cameroon'),
(40, 'cv', 'Cape Verde'),
(41, 'ky', 'Cayman Islands'),
(42, 'cf', 'Central African Republic'),
(43, 'td', 'Chad'),
(44, 'cl', 'Chile'),
(45, 'us', 'United States'),
(46, 'cx', 'Christmas Island'),
(47, 'cc', 'Cocos (Keeling) Islands'),
(48, 'co', 'Colombia'),
(49, 'km', 'Comoros'),
(50, 'cg', 'Congo'),
(51, 'ck', 'Cook Islands'),
(52, 'cr', 'Costa Rica'),
(53, 'hr', 'Croatia (Hrvatska)'),
(54, 'cu', 'Cuba'),
(55, 'cy', 'Cyprus'),
(56, 'cz', 'Czech Republic'),
(57, 'dk', 'Denmark'),
(58, 'dj', 'Djibouti'),
(59, 'dm', 'Dominica'),
(60, 'do', 'Dominican Republic'),
(61, 'tp', 'East Timor'),
(62, 'ec', 'Ecuador'),
(63, 'eg', 'Egypt'),
(64, 'sv', 'El Salvador'),
(65, 'gq', 'Equatorial Guinea'),
(66, 'er', 'Eritrea'),
(67, 'ee', 'Estonia'),
(68, 'et', 'Ethiopia'),
(69, 'fk', 'Falkland Islands (Malvinas)'),
(70, 'fo', 'Faroe Islands'),
(71, 'fj', 'Fiji'),
(72, 'fi', 'Finland'),
(73, 'fr', 'France'),
(74, 'fx', 'France, Metropolitan'),
(75, 'gf', 'French Guiana'),
(76, 'pf', 'French Polynesia'),
(77, 'tf', 'French Southern Territories'),
(78, 'ga', 'Gabon'),
(79, 'gm', 'Gambia'),
(80, 'ge', 'Georgia'),
(81, 'de', 'Germany'),
(82, 'gh', 'Ghana'),
(83, 'gi', 'Gibraltar'),
(84, 'gr', 'Greece'),
(85, 'gl', 'Greenland'),
(86, 'gd', 'Grenada'),
(87, 'gp', 'Guadeloupe'),
(88, 'gu', 'Guam'),
(89, 'gt', 'Guatemala'),
(90, 'gn', 'Guinea'),
(91, 'gw', 'Guinea-Bissau'),
(92, 'gy', 'Guyana'),
(93, 'ht', 'Haiti'),
(94, 'hm', 'Heard and Mc Donald Islands'),
(95, 'hn', 'Honduras'),
(96, 'hk', 'Hong Kong'),
(97, 'hu', 'Hungary'),
(98, 'is', 'Iceland'),
(99, 'in', 'India'),
(100, 'id', 'Indonesia'),
(101, 'ir', 'Iran (Islamic Republic of)'),
(102, 'iq', 'Iraq'),
(103, 'ie', 'Ireland'),
(104, 'il', 'Israel'),
(105, 'it', 'Italy'),
(106, 'ci', 'Ivory Coast'),
(107, 'jm', 'Jamaica'),
(108, 'jp', 'Japan'),
(109, 'jo', 'Jordan'),
(110, 'kz', 'Kazakhstan'),
(111, 'ke', 'Kenya'),
(112, 'ki', 'Kiribati'),
(113, 'kp', 'Korea, Democratic People''s Republic of'),
(114, 'kr', 'Korea, Republic of'),
(115, 'xk', 'Kosovo'),
(116, 'kw', 'Kuwait'),
(117, 'kg', 'Kyrgyzstan'),
(118, 'la', 'Lao People''s Democratic Republic'),
(119, 'lv', 'Latvia'),
(120, 'lb', 'Lebanon'),
(121, 'ls', 'Lesotho'),
(122, 'lr', 'Liberia'),
(123, 'ly', 'Libyan Arab Jamahiriya'),
(124, 'li', 'Liechtenstein'),
(125, 'lt', 'Lithuania'),
(126, 'lu', 'Luxembourg'),
(127, 'mo', 'Macau'),
(128, 'mk', 'Macedonia'),
(129, 'mg', 'Madagascar'),
(130, 'mw', 'Malawi'),
(131, 'my', 'Malaysia'),
(132, 'mv', 'Maldives'),
(133, 'ml', 'Mali'),
(134, 'mt', 'Malta'),
(135, 'mh', 'Marshall Islands'),
(136, 'mq', 'Martinique'),
(137, 'mr', 'Mauritania'),
(138, 'mu', 'Mauritius'),
(139, 'ty', 'Mayotte'),
(140, 'mx', 'Mexico'),
(141, 'fm', 'Micronesia, Federated States of'),
(142, 'md', 'Moldova, Republic of'),
(143, 'mc', 'Monaco'),
(144, 'mn', 'Mongolia'),
(145, 'me', 'Montenegro'),
(146, 'ms', 'Montserrat'),
(147, 'ma', 'Morocco'),
(148, 'mz', 'Mozambique'),
(149, 'mm', 'Myanmar'),
(150, 'na', 'Namibia'),
(151, 'nr', 'Nauru'),
(152, 'np', 'Nepal'),
(153, 'nl', 'Netherlands'),
(154, 'an', 'Netherlands Antilles'),
(155, 'nc', 'New Caledonia'),
(156, 'nz', 'New Zealand'),
(157, 'ni', 'Nicaragua'),
(158, 'ne', 'Niger'),
(159, 'ng', 'Nigeria'),
(160, 'nu', 'Niue'),
(161, 'nf', 'Norfork Island'),
(162, 'mp', 'Northern Mariana Islands'),
(163, 'no', 'Norway'),
(164, 'om', 'Oman'),
(165, 'ds', 'American Samoa'),
(166, 'pw', 'Palau'),
(167, 'pa', 'Panama'),
(168, 'pg', 'Papua New Guinea'),
(169, 'py', 'Paraguay'),
(170, 'pe', 'Peru'),
(171, 'ph', 'Philippines'),
(172, 'pn', 'Pitcairn'),
(173, 'pl', 'Poland'),
(174, 'pt', 'Portugal'),
(175, 'pr', 'Puerto Rico'),
(176, 'qa', 'Qatar'),
(177, 're', 'Reunion'),
(178, 'ro', 'Romania'),
(179, 'ru', 'Russian Federation'),
(180, 'rw', 'Rwanda'),
(181, 'kn', 'Saint Kitts and Nevis'),
(182, 'lc', 'Saint Lucia'),
(183, 'vc', 'Saint Vincent and the Grenadines'),
(184, 'ws', 'Samoa'),
(185, 'sm', 'San Marino'),
(186, 'st', 'Sao Tome and Principe'),
(187, 'sa', 'Saudi Arabia'),
(188, 'sn', 'Senegal'),
(189, 'rs', 'Serbia'),
(190, 'sc', 'Seychelles'),
(191, 'sl', 'Sierra Leone'),
(192, 'sg', 'Singapore'),
(193, 'sk', 'Slovakia'),
(194, 'si', 'Slovenia'),
(195, 'sb', 'Solomon Islands'),
(196, 'so', 'Somalia'),
(197, 'za', 'South Africa'),
(198, 'gs', 'South Georgia South Sandwich Islands'),
(199, 'es', 'Spain'),
(200, 'lk', 'Sri Lanka'),
(201, 'sh', 'St. Helena'),
(202, 'pm', 'St. Pierre and Miquelon'),
(203, 'sd', 'Sudan'),
(204, 'sr', 'Suriname'),
(205, 'sj', 'Svalbarn and Jan Mayen Islands'),
(206, 'sz', 'Swaziland'),
(207, 'se', 'Sweden'),
(208, 'ch', 'Switzerland'),
(209, 'sy', 'Syrian Arab Republic'),
(210, 'tw', 'Taiwan'),
(211, 'tj', 'Tajikistan'),
(212, 'tz', 'Tanzania, United Republic of'),
(213, 'th', 'Thailand'),
(214, 'tg', 'Togo'),
(215, 'tk', 'Tokelau'),
(216, 'to', 'Tonga'),
(217, 'tt', 'Trinidad and Tobago'),
(218, 'tn', 'Tunisia'),
(219, 'tr', 'Turkey'),
(220, 'tm', 'Turkmenistan'),
(221, 'tc', 'Turks and Caicos Islands'),
(222, 'tv', 'Tuvalu'),
(223, 'ug', 'Uganda'),
(224, 'ua', 'Ukraine'),
(225, 'ae', 'United Arab Emirates'),
(226, 'gb', 'United Kingdom'),
(227, 'um', 'United States minor outlying islands'),
(228, 'uy', 'Uruguay'),
(229, 'uz', 'Uzbekistan'),
(230, 'vu', 'Vanuatu'),
(231, 'va', 'Vatican City State'),
(232, 've', 'Venezuela'),
(233, 'vn', 'Vietnam'),
(234, 'vg', 'Virgin Islands (British)'),
(235, 'vi', 'Virgin Islands (U.S.)'),
(236, 'wf', 'Wallis and Futuna Islands'),
(237, 'eh', 'Western Sahara'),
(238, 'ye', 'Yemen'),
(239, 'yu', 'Yugoslavia'),
(240, 'zr', 'Zaire'),
(241, 'zm', 'Zambia'),
(242, 'zw', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_currency`
--

CREATE TABLE IF NOT EXISTS `rbc_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(100) NOT NULL,
  `currency_code` varchar(50) NOT NULL,
  `currency_symbol` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rbc_currency`
--

INSERT INTO `rbc_currency` (`id`, `currency_name`, `currency_code`, `currency_symbol`) VALUES
(1, 'Pound', 'GBP', '£'),
(2, 'Dollar', 'USD', '$'),
(3, 'Euro', 'EUR', '€');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_department`
--

CREATE TABLE IF NOT EXISTS `rbc_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_help_us`
--

CREATE TABLE IF NOT EXISTS `rbc_help_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) DEFAULT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `rbc_help_us`
--

INSERT INTO `rbc_help_us` (`id`, `status`, `message`, `user_id`) VALUES
(1, 'neutral', 'asdasd', 1),
(2, 'neutral', 'sadasdsad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rbc_home_slider`
--

CREATE TABLE IF NOT EXISTS `rbc_home_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` text NOT NULL,
  `image_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_membership`
--

CREATE TABLE IF NOT EXISTS `rbc_membership` (
  `membership_id` int(11) NOT NULL AUTO_INCREMENT,
  `membership_name` varchar(100) DEFAULT NULL,
  `products_limit` int(11) NOT NULL,
  PRIMARY KEY (`membership_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rbc_membership`
--

INSERT INTO `rbc_membership` (`membership_id`, `membership_name`, `products_limit`) VALUES
(1, 'Basic', 25),
(2, 'Standard', 50),
(3, 'Premium', 200);

-- --------------------------------------------------------

--
-- Table structure for table `rbc_news`
--

CREATE TABLE IF NOT EXISTS `rbc_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(100) NOT NULL,
  `news_description` varchar(255) NOT NULL,
  `news_date` date NOT NULL,
  `news_image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rbc_news`
--

INSERT INTO `rbc_news` (`id`, `news_title`, `news_description`, `news_date`, `news_image`) VALUES
(3, 'news title 3', 'ji han ji han', '2015-05-21', 'http://localhost/rbc-trades-cms/uploads/news_images/sxdfos.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_payment_type`
--

CREATE TABLE IF NOT EXISTS `rbc_payment_type` (
  `p_term_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_term_description` varchar(100) NOT NULL,
  PRIMARY KEY (`p_term_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `rbc_payment_type`
--

INSERT INTO `rbc_payment_type` (`p_term_id`, `p_term_description`) VALUES
(1, 'L/C'),
(2, 'D/A'),
(3, 'D/P'),
(4, 'T/T'),
(5, 'Western Union'),
(6, 'Money Gram'),
(7, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_purchasing_frequency`
--

CREATE TABLE IF NOT EXISTS `rbc_purchasing_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchasing_frequency` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `rbc_purchasing_frequency`
--

INSERT INTO `rbc_purchasing_frequency` (`id`, `purchasing_frequency`) VALUES
(1, 'Less than one month'),
(2, 'One month'),
(3, 'One quarter'),
(4, 'Half a year'),
(5, 'One year'),
(6, 'Long than one year');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_security_question`
--

CREATE TABLE IF NOT EXISTS `rbc_security_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `security_question` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `rbc_security_question`
--

INSERT INTO `rbc_security_question` (`id`, `security_question`) VALUES
(1, 'What''s your pet''s name?'),
(2, 'What''s your grandma''s Name?'),
(3, 'What''s your grandpa''s Name?'),
(4, 'What''s primary school Name?'),
(5, 'What''s primary school Name?'),
(6, 'What''s your daughter/son''s nick name?'),
(7, 'What''s your birthday? (Example: 1980/01/01)'),
(8, 'What''s your hometown?'),
(9, 'What''s your Car Number?');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_uom`
--

CREATE TABLE IF NOT EXISTS `rbc_uom` (
  `uom_id` int(11) NOT NULL AUTO_INCREMENT,
  `uom_name` varchar(100) NOT NULL,
  PRIMARY KEY (`uom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rbc_uom`
--

INSERT INTO `rbc_uom` (`uom_id`, `uom_name`) VALUES
(1, 'Box'),
(2, 'Container'),
(3, 'Pieces'),
(4, 'KG'),
(5, 'Feet');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_user`
--

CREATE TABLE IF NOT EXISTS `rbc_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `membership_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(100) DEFAULT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `alternative_email` varchar(100) DEFAULT NULL,
  `street_adddress` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `job_title` varchar(100) DEFAULT NULL,
  `business_type` int(11) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `facebook_uid` varchar(500) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer` varchar(200) DEFAULT NULL,
  `hash_key` varchar(10) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 Active, 0 Inactive',
  `hash_key_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `rbc_user`
--

INSERT INTO `rbc_user` (`user_id`, `member_id`, `membership_type`, `user_name`, `user_password`, `user_email`, `alternative_email`, `street_adddress`, `city`, `province`, `country`, `zip_code`, `phone`, `fax`, `mobile`, `gender`, `job_title`, `business_type`, `department`, `facebook_uid`, `question_id`, `answer`, `hash_key`, `is_active`, `hash_key_time`) VALUES
(1, 1, 1, 'Muhammad Asad Saleem', 'b7e26c2e1aa1ecf0f4f54d5a4db9e795', 'asad.saleem50@gmail.com', NULL, NULL, 'sadasd', 'asasdads', 3, 3132, '23-23-232', '', NULL, 'male', 'sadd', 0, NULL, NULL, NULL, NULL, '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rfq_details`
--

CREATE TABLE IF NOT EXISTS `rfq_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) NOT NULL,
  `rfq_product_name` varchar(100) NOT NULL,
  `rfq_product_quantity` int(11) NOT NULL,
  `rfq_product_uom` int(11) NOT NULL,
  `rfq_product_description` text NOT NULL,
  `rfq_product_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_master`
--

CREATE TABLE IF NOT EXISTS `rfq_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_seller_id` int(11) DEFAULT NULL,
  `rfq_buyer_id` int(11) NOT NULL,
  `rfq_quotation_date` date NOT NULL DEFAULT '0000-00-00',
  `rfq_expiry_date` date DEFAULT '0000-00-00',
  `rfq_quotation_description` varchar(100) DEFAULT NULL,
  `rfq_quotes_left` int(11) DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `security_setting_master`
--

CREATE TABLE IF NOT EXISTS `security_setting_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `security_question_id` int(11) NOT NULL,
  `security_answer` varchar(500) NOT NULL,
  PRIMARY KEY (`id`,`member_id`,`security_question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
