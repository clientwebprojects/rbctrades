-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2015 at 12:59 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rbctrades`
--

-- --------------------------------------------------------

--
-- Table structure for table `business_type`
--

CREATE TABLE IF NOT EXISTS `business_type` (
  `business_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`business_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `business_type`
--

INSERT INTO `business_type` (`business_type_id`, `business_type_name`) VALUES
(1, 'Manufacturer'),
(2, 'Trading Company'),
(3, 'Buying Office'),
(4, 'Agent'),
(5, 'Distributor/Wholesaler'),
(6, 'Government ministry/Bureau/Commission'),
(7, 'Association'),
(8, 'Business Service (Transportation, finance, travel, Ads, etc) '),
(9, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `company_certification`
--

CREATE TABLE IF NOT EXISTS `company_certification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certification_type` varchar(100) DEFAULT NULL,
  `reference_no` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `issued_by` varchar(100) DEFAULT NULL,
  `starting_date` date DEFAULT NULL,
  `ending_date` date DEFAULT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  `scope` text,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_master`
--

CREATE TABLE IF NOT EXISTS `company_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_type` text,
  `business_pattern` tinyint(1) NOT NULL DEFAULT '1',
  `company_name` varchar(100) NOT NULL,
  `contact_no` varchar(100) DEFAULT NULL,
  `registration_location` int(11) NOT NULL,
  `operational_street_address` varchar(100) DEFAULT NULL,
  `operational_address_city` varchar(100) DEFAULT NULL,
  `operational_address_province` varchar(100) DEFAULT NULL,
  `operational_location` int(11) DEFAULT NULL,
  `operational_address_zip_code` varchar(100) DEFAULT NULL,
  `main_products` text,
  `other_products` text,
  `registered_year` varchar(4) DEFAULT NULL,
  `total_employees` varchar(100) DEFAULT NULL,
  `website_url` varchar(100) DEFAULT NULL,
  `website_opt` tinyint(1) NOT NULL DEFAULT '1',
  `legal_owner` varchar(100) DEFAULT NULL,
  `office_size` varchar(100) DEFAULT NULL,
  `company_advantages` text,
  `annual_sales_volume` varchar(100) DEFAULT NULL,
  `export_percentage` varchar(100) DEFAULT NULL,
  `exporting_started` varchar(4) DEFAULT NULL,
  `trade_staff_num` int(11) DEFAULT NULL,
  `cross_industry` tinyint(1) DEFAULT '0',
  `rnd_staff_num_trade` varchar(100) DEFAULT NULL,
  `qc_staff_num_trade` varchar(100) DEFAULT NULL,
  `export_port` text,
  `avg_delivery_time` int(11) DEFAULT NULL,
  `have_oversea_office` tinyint(1) DEFAULT '0',
  `delievery_terms` text,
  `support_currency` text,
  `payment_methods` text,
  `language_skills` text,
  `logo_url` varchar(100) DEFAULT NULL,
  `photo_url` varchar(100) DEFAULT NULL,
  `company_description` text,
  `in_industry` varchar(100) DEFAULT NULL,
  `purchasing_frequency` varchar(50) DEFAULT NULL,
  `annual_purchase_volume` varchar(50) DEFAULT NULL,
  `preffered_seller_location` varchar(50) DEFAULT NULL,
  `preffered_seller_type` text,
  `business_experiences` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company_master`
--

INSERT INTO `company_master` (`id`, `business_type`, `business_pattern`, `company_name`, `contact_no`, `registration_location`, `operational_street_address`, `operational_address_city`, `operational_address_province`, `operational_location`, `operational_address_zip_code`, `main_products`, `other_products`, `registered_year`, `total_employees`, `website_url`, `website_opt`, `legal_owner`, `office_size`, `company_advantages`, `annual_sales_volume`, `export_percentage`, `exporting_started`, `trade_staff_num`, `cross_industry`, `rnd_staff_num_trade`, `qc_staff_num_trade`, `export_port`, `avg_delivery_time`, `have_oversea_office`, `delievery_terms`, `support_currency`, `payment_methods`, `language_skills`, `logo_url`, `photo_url`, `company_description`, `in_industry`, `purchasing_frequency`, `annual_purchase_volume`, `preffered_seller_location`, `preffered_seller_type`, `business_experiences`) VALUES
(1, NULL, 1, 'Noriyah', '03217254041', 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company_partner`
--

CREATE TABLE IF NOT EXISTS `company_partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_factory_name` varchar(100) DEFAULT NULL,
  `coorporation_contract` varchar(100) DEFAULT NULL,
  `partner_duration` varchar(100) DEFAULT NULL,
  `partner_annual_amount` varchar(100) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `company_privacy_setting`
--

CREATE TABLE IF NOT EXISTS `company_privacy_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `basic_information` int(11) NOT NULL,
  `contact_information` int(11) NOT NULL,
  `sourcing_information` int(11) NOT NULL,
  `activity_summary` int(11) NOT NULL,
  `watched_industries` int(11) NOT NULL,
  `most_searched_keywords` int(11) NOT NULL,
  `recently_searched_products` int(11) NOT NULL,
  `transactions` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `member_master`
--

CREATE TABLE IF NOT EXISTS `member_master` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_name` varchar(500) NOT NULL,
  `member_type` varchar(50) NOT NULL COMMENT 'buyer, seller, both',
  `company_id` int(11) NOT NULL,
  PRIMARY KEY (`member_id`),
  UNIQUE KEY `company_id` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `member_master`
--

INSERT INTO `member_master` (`member_id`, `member_name`, `member_type`, `company_id`) VALUES
(1, 'Muhammad Asad Saleem', 'both', 1);

-- --------------------------------------------------------

--
-- Table structure for table `message_center`
--

CREATE TABLE IF NOT EXISTS `message_center` (
  `message_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject` text,
  `body` text,
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_center_mapped`
--

CREATE TABLE IF NOT EXISTS `message_center_mapped` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `main_user_email` varchar(100) NOT NULL,
  `temp_user_email` varchar(100) NOT NULL,
  `placeholder_id` int(11) NOT NULL COMMENT '1-Inbox, 2- Important, 3-Sent, 4-Trash',
  `is_read` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-Not Read, 1-Read',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message_placeholder`
--

CREATE TABLE IF NOT EXISTS `message_placeholder` (
  `placeholder_id` int(11) NOT NULL AUTO_INCREMENT,
  `placeholder_name` varchar(100) NOT NULL,
  PRIMARY KEY (`placeholder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `message_placeholder`
--

INSERT INTO `message_placeholder` (`placeholder_id`, `placeholder_name`) VALUES
(1, 'All Inquiries'),
(2, 'Important'),
(3, 'Sent Box'),
(4, 'Trash');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `image_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `image_url` text NOT NULL,
  `is_main` tinyint(1) NOT NULL,
  PRIMARY KEY (`image_id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`image_id`, `product_id`, `image_url`, `is_main`) VALUES
(1, 1, 'http://localhost/bitbucket/rbctrades/uploads/product_images/zzytgq.jpg', 1),
(2, 1, 'http://localhost/bitbucket/rbctrades/uploads/product_images/b79uux.png', 0),
(3, 2, 'http://localhost/bitbucket/rbctrades/uploads/product_images/5r5rsh.png', 1),
(4, 3, 'http://localhost/bitbucket/rbctrades/uploads/product_images/kdc9na.jpeg', 1),
(5, 3, 'http://localhost/bitbucket/rbctrades/uploads/product_images/khso82.png', 0),
(6, 4, 'http://localhost/bitbucket/rbctrades/uploads/product_images/xd1az1.jpg', 1),
(7, 9, 'http://localhost/bitbucket/rbctrades/uploads/product_images/t2hnfr.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_master`
--

CREATE TABLE IF NOT EXISTS `product_master` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` int(11) NOT NULL,
  `product_code` varchar(100) DEFAULT NULL,
  `product_model` varchar(100) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `images` int(11) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `product_description` varchar(500) DEFAULT NULL,
  `product_keyword` text,
  `uom_id` int(11) DEFAULT NULL,
  `product_status` varchar(100) NOT NULL DEFAULT 'P' COMMENT 'P-Pending, A-Approved, R-Rejected',
  `product_origin` varchar(100) NOT NULL,
  `product_type` varchar(100) DEFAULT NULL,
  `product_use` varchar(100) DEFAULT NULL,
  `product_minimun_order` int(11) DEFAULT NULL,
  `minimun_order_unit` int(11) DEFAULT NULL,
  `product_port` varchar(100) DEFAULT NULL,
  `product_payment_type` varchar(100) DEFAULT NULL,
  `product_supply_ability` int(11) DEFAULT NULL,
  `product_delivery_time` varchar(100) DEFAULT NULL,
  `product_delivery_unit` int(11) DEFAULT NULL,
  `packaging_detail` text,
  `product_detailed_description` text,
  `product_fob_price` int(11) DEFAULT NULL,
  `product_fob_unit` int(11) DEFAULT NULL,
  `product_slug` varchar(60) DEFAULT NULL,
  `product_fob_currency` int(11) DEFAULT NULL,
  `product_fob_price_from` float DEFAULT NULL,
  `product_fob_price_to` float DEFAULT NULL,
  `product_add_date` datetime NOT NULL,
  `product_last_updated` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `product_master`
--

INSERT INTO `product_master` (`product_id`, `product_category`, `product_code`, `product_model`, `brand_id`, `images`, `product_name`, `product_description`, `product_keyword`, `uom_id`, `product_status`, `product_origin`, `product_type`, `product_use`, `product_minimun_order`, `minimun_order_unit`, `product_port`, `product_payment_type`, `product_supply_ability`, `product_delivery_time`, `product_delivery_unit`, `packaging_detail`, `product_detailed_description`, `product_fob_price`, `product_fob_unit`, `product_slug`, `product_fob_currency`, `product_fob_price_from`, `product_fob_price_to`, `product_add_date`, `product_last_updated`) VALUES
(1, 29, '13123', '2013', 23123, NULL, 'Angoor', 'asdas', 'angoor, malta, apple', 1, 'A', '1', 'asd', NULL, 123, 1, 'dasdsa', '1|2|3|4|', 213213, '123', 1, 'adsasd', '<p>asdasdadssad</p>\r\n', NULL, 1, 'angoor-----1cgaqfe', 0, 123123, 213123000, '2015-05-25 11:17:21', '2015-05-27 04:30:49'),
(3, 11, '21321321311122', '123', 123213, NULL, 'Bnana', '213213', 'Bnana', 1, 'A', '11', '123', NULL, 213, 2, 'sfddsf', '1|2|', 213, '1231', 2, '123213', '<p>asdasd</p>\r\n', NULL, 2, 'bnana-----1itb6y9', 0, 123, 1222, '2015-05-25 12:03:26', '2015-05-27 04:36:53'),
(4, 5, '23432', '32424', 234, NULL, 'asdasdasdadsdsa', '2344', 'asdasdasdads', 1, 'A', '1', '324', NULL, 324, 1, '234', '1|', 234, '234', 1, '234', NULL, NULL, 0, 'asdasdasdadsdsa-----1z7h6o7', 1, 234, 324, '2015-05-25 12:01:53', '0000-00-00 00:00:00'),
(7, 3, '123123123123', '123213213312', 123, NULL, 'ttt', '213321', 'ttt', 3, 'A', '14', '123', NULL, 121, 0, 'fdfds', '1|', 443, '33', 2, 'EWQ', '<p>sdas</p>\r\n', NULL, 2, 'ttt-----1gtf247', 0, 11, 22, '2015-05-25 12:12:42', '2015-05-27 05:13:05'),
(8, 3, '123123123123', '123213213312', 123, NULL, 'ttt', '213321', 'ttt', 3, 'A', '14', '123', NULL, 121, 0, 'fdfds', '1|', 443, '33', 2, 'EWQ', NULL, NULL, 2, 'ttt-----17981ge', 2, 11, 22, '2015-05-25 12:13:48', '0000-00-00 00:00:00'),
(9, 4, 'qw', 'qw', 0, NULL, '7 up', 'qw', '7 up', 1, 'A', '1', 'qw', NULL, 12, 1, 'df', '1|', 12, '12', 1, '12', '<p>saddasdeee</p>\r\n', NULL, 1, '7-up----1opsgz7', 0, 112222, 1222, '2015-05-27 04:49:12', '2015-05-27 05:05:39'),
(10, 1, 'ads', 'as', 0, NULL, 'asdsadasdasd asds asddasdsa sadsadads asdadsasdads saddasd sadsad', '', 'asddasdas', 2, 'A', '2', '', NULL, 1, 1, '123', '1|', 231, '213', 1, '213', NULL, NULL, 1, 'asdsadasdasd-asds-asddasdsa-sadsadads-asdadsasdads-1uy0fnf', 1, 21, 2132, '2015-05-27 05:07:29', '0000-00-00 00:00:00'),
(11, 6, '', 'saddas', 0, NULL, 'assad', '', 'saddas', 1, 'A', '2', '', NULL, 34, 2, '', '7|', 0, '34', 2, '', '<p>It links</p>\r\n', NULL, 0, 'assad-----1dkub2m', 0, 0, 0, '2015-05-27 04:23:41', '2015-05-27 04:29:31');

-- --------------------------------------------------------

--
-- Table structure for table `product_member`
--

CREATE TABLE IF NOT EXISTS `product_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `product_member`
--

INSERT INTO `product_member` (`id`, `product_id`, `member_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1),
(10, 10, 1),
(11, 11, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_specification`
--

CREATE TABLE IF NOT EXISTS `product_specification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`,`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `product_specification`
--

INSERT INTO `product_specification` (`id`, `product_id`, `attribute`, `value`) VALUES
(1, 6, '123', 'dd'),
(10, 7, 'asasd', '2222'),
(11, 9, 'coloe', '11'),
(20, 9, 'rrr', '666'),
(21, 9, 'asad', '555'),
(22, 10, '123', '213'),
(23, 10, '123', '23'),
(24, 11, 'asd', 'asd'),
(25, 11, 'color', 'red'),
(26, 1, 'type', 'fruit'),
(27, 1, 'size', 'MEDIUM'),
(28, 3, 'color', 'red'),
(30, 3, 'type', 'fruit');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

CREATE TABLE IF NOT EXISTS `quotation_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quot_no` int(11) NOT NULL DEFAULT '0',
  `product_name` varchar(100) NOT NULL,
  `description` tinytext,
  `unit_price` double NOT NULL DEFAULT '0',
  `uom_id` int(11) NOT NULL,
  `quantity` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sorder` (`quot_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_master`
--

CREATE TABLE IF NOT EXISTS `quotation_master` (
  `quot_no` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) NOT NULL,
  `seller_id` int(11) NOT NULL DEFAULT '0',
  `quotation_date` date NOT NULL DEFAULT '0000-00-00',
  `quotation_validity_date` date NOT NULL DEFAULT '0000-00-00',
  `currency_id` int(11) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `tax` float(10,2) NOT NULL,
  PRIMARY KEY (`quot_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_annual_purchasing_volume`
--

CREATE TABLE IF NOT EXISTS `rbc_annual_purchasing_volume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annual_purchasing_volume` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_banners`
--

CREATE TABLE IF NOT EXISTS `rbc_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` text NOT NULL,
  `location_id` int(11) NOT NULL,
  `category_location` text NOT NULL,
  `banner_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_banner_locations`
--

CREATE TABLE IF NOT EXISTS `rbc_banner_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_location` text NOT NULL,
  `location_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_category`
--

CREATE TABLE IF NOT EXISTS `rbc_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1024 ;

--
-- Dumping data for table `rbc_category`
--

INSERT INTO `rbc_category` (`category_id`, `category_name`, `parent_id`) VALUES
(1, 'Agriculture', 0),
(2, 'Automobiles & Transportation', 0),
(3, 'Chemicals', 0),
(4, 'Computers', 0),
(5, 'Construction & Real Estate', 0),
(6, 'Consumer Electronics', 0),
(7, 'Electrical Equipment', 0),
(8, 'Hardware', 0),
(9, 'Health & Medical', 0),
(10, 'Food & Beverage', 0),
(11, 'Apperal', 0),
(12, 'Textile & Leather Product ', 0),
(13, 'Fashion Accessories ', 0),
(14, 'Timepieces, Jewelry, Eyewear ', 0),
(15, 'Transportation ', 0),
(16, 'Luggage, Bags & Cases ', 0),
(17, 'Shoes & Accessories ', 0),
(18, 'Computer Hardware & Software ', 0),
(19, 'Home Appliance ', 0),
(20, 'Consumer Electronic ', 0),
(21, 'Security & Protection ', 0),
(22, 'Electrical Equipment & Supplies ', 0),
(23, 'Electronic Compnents & Supplies ', 0),
(24, 'Telecommunication ', 0),
(25, 'Sports & Entertainment ', 0),
(26, 'Gifts & Crafts ', 0),
(27, 'Toys & Hobbies ', 0),
(28, 'Health & Medical ', 0),
(29, 'Beauty & Personal Care ', 0),
(30, 'Construction & Real Estate ', 0),
(31, 'Home & Garden ', 0),
(32, 'Lights & Lighting ', 0),
(33, 'Furniture ', 0),
(34, 'Machinery ', 0),
(35, 'Industrial Parts & Fabrication Services ', 0),
(36, 'Tools ', 0),
(37, 'Hardware ', 0),
(38, 'Measurement & Analysis Instruments ', 0),
(39, 'Minerals & Metallurgy ', 0),
(40, 'Chemicals ', 0),
(41, 'Rubber & Plastics ', 0),
(42, 'Energy ', 0),
(43, 'Environment ', 0),
(44, 'Packaging & Printing ', 0),
(45, 'Office & School Supplies ', 0),
(46, 'Service Equipment ', 0),
(47, 'Agricultural Growing Media', 1),
(48, 'Animal Products', 1),
(49, 'Cocoa Beans', 1),
(50, 'Farm Machinery & Equipment', 1),
(51, 'Fresh Seafood', 1),
(52, 'Grain', 1),
(53, 'Mushrooms & Truffles', 1),
(54, 'Organic Produce', 1),
(55, 'Other Agriculture Products', 1),
(56, 'Plant Seeds & Bulbs', 1),
(57, 'Agricultural Waste', 1),
(58, 'Beans', 1),
(59, 'Coffee Beans', 1),
(60, 'Feed', 1),
(61, 'Fruit', 1),
(62, 'Herbal Cigars & Cigarettes', 1),
(63, 'Nuts & Kernels', 1),
(64, 'Ornamental Plants', 1),
(65, 'Plant & Animal Oil', 1),
(66, 'Timber Raw Materials', 1),
(67, 'Vegetables', 1),
(68, 'Vanilla Beans', 1),
(69, 'Alcoholic Beverage', 10),
(70, 'Baked Goods', 10),
(71, 'Canned Food', 10),
(72, 'Confectionery', 10),
(73, 'Drinking Water', 10),
(74, 'Food Ingredients', 10),
(75, 'Grain Products', 10),
(76, 'Instant Food', 10),
(77, 'Other Food & Beverage', 10),
(78, 'Seasonings & Condiments', 10),
(79, 'Slimming Food', 10),
(80, 'Seafood', 10),
(81, 'Meat & Poultry', 10),
(82, 'Honey Products', 10),
(83, 'Fruit Products', 10),
(84, 'Egg & Egg Products', 10),
(85, 'Dairy', 10),
(86, 'Coffee', 10),
(87, 'Bean Products', 10),
(88, 'Baby Food', 10),
(89, 'Snack Food', 10),
(90, 'Soft Drinks', 10),
(91, 'Vegetable Products', 10),
(92, 'Tea', 10),
(93, 'Apparel Design Services', 11),
(94, 'Apparel Stock', 11),
(95, 'Children’s Clothing', 11),
(96, 'Costumes', 11),
(97, 'Ethnic Clothing', 11),
(98, 'Girls’ Clothing', 11),
(99, 'Hosiery', 11),
(100, 'Jackets', 11),
(101, 'Ladies’ Blouses & Tops', 11),
(102, 'Maternity Clothing', 11),
(103, 'Apparel Processing Services', 11),
(104, 'Boy’s Clothing', 11),
(105, 'Coats', 11),
(106, 'Dresses', 11),
(107, 'Garment Accessories', 11),
(108, 'Hoodies & Sweatshirts', 11),
(109, 'Infant & Toddlers Clothing', 11),
(110, 'Jeans', 11),
(111, 'Mannequins', 11),
(112, 'Men’s Clothing', 11),
(113, 'Men’s Shirts', 11),
(114, 'Organic Cotton Clothing', 11),
(115, 'Other Apparel', 11),
(116, 'Pants & Trousers', 11),
(117, 'Plus Size Clothing', 11),
(118, 'Sewing Supplies', 11),
(119, 'Shorts', 11),
(120, 'Skirts', 11),
(121, 'Sleepwear', 11),
(122, 'Sportswear', 11),
(123, 'Stage & Dance Wear', 11),
(124, 'Suits & Tuxedo', 11),
(125, 'Sweaters', 11),
(126, 'Tag Guns', 11),
(127, 'Tank Tops', 11),
(128, 'T-Shirts', 11),
(129, 'Underwear', 11),
(130, 'Uniforms', 11),
(131, 'Used Clothes', 11),
(132, 'Vests & Waistcoats', 11),
(133, 'Wedding Apparel & Accessories', 11),
(134, 'Women’s Clothing', 11),
(135, 'Workwear', 11),
(136, 'Down & Feather', 12),
(137, 'Fabric', 12),
(138, 'Fiber', 12),
(139, 'Fur', 12),
(140, 'Grey Fabric', 12),
(141, 'Home Textile', 12),
(142, 'Leather', 12),
(143, 'Leather Product', 12),
(144, 'Textile Accessories', 12),
(145, 'Textile Processing', 12),
(146, 'Textile Stock', 12),
(147, 'Thread', 12),
(148, 'Yarn', 12),
(149, '100% Cotton Fabric', 12),
(150, '100% Polyester Fabric', 12),
(151, 'Bedding Set', 12),
(152, 'Towel', 12),
(153, 'Chair Cover', 12),
(154, 'Genuine Leather', 12),
(155, 'Belt Accessories', 13),
(156, 'Belts', 13),
(157, 'Fashion Accessories Stock', 13),
(158, 'Fashion Accessories Processing Services ', 13),
(159, 'Fashion Accessories Design Services ', 13),
(160, 'Gloves & Mittens', 13),
(161, 'Headwear', 13),
(162, 'Neckwear', 13),
(163, 'Scarf, Hat & Glove Sets', 13),
(164, 'Hats & Caps', 13),
(165, 'Scarves & Shawls', 13),
(166, 'Hair Accessories', 13),
(167, 'Genuine Leather Belts', 13),
(168, 'Leather Gloves & Mittens', 13),
(169, 'Ties & Accessories', 13),
(170, 'Belt Buckles', 13),
(171, 'PU Belts', 13),
(172, 'Belt Chains', 13),
(173, 'Metal Belts', 13),
(174, 'Suspenders', 13),
(175, 'Eyewear', 14),
(176, 'Jewelry', 14),
(177, 'Watches', 14),
(178, 'Eyeglasses Frames', 14),
(179, 'Sunglasses', 14),
(180, 'Sports Eyewear', 14),
(181, 'Body Jewelry', 14),
(182, 'Bracelets & Bangles', 14),
(183, 'Brooches', 14),
(184, 'Cuff Links & Tie Clips', 14),
(185, 'Earrings', 14),
(186, 'Jewelry Boxes', 14),
(187, 'Jewelry Sets', 14),
(188, 'Jewelry Tools & Equipment', 14),
(189, 'Loose Beads', 14),
(190, 'Loose Gemstone', 14),
(191, 'Necklaces', 14),
(192, 'Pendants & Charms', 14),
(193, 'Rings', 14),
(194, 'Wristwatches', 14),
(195, 'Automobiles & Motorcycles ', 0),
(196, 'Air Intakes', 195),
(197, 'ATV', 195),
(198, 'ATV Parts', 195),
(199, 'Auto Chassis Parts', 195),
(200, 'Auto Clutch', 195),
(201, 'Auto Electrical System', 195),
(202, 'Auto Electronics', 195),
(203, 'Auto Engine', 195),
(204, 'Auto Ignition System', 195),
(205, 'Auto Steering System', 195),
(206, 'Automobiles', 195),
(207, 'Axles', 195),
(208, 'Body Parts', 195),
(209, 'Brake System', 195),
(210, 'Car Care & Cleaning', 195),
(211, 'Cooling System', 195),
(212, 'Crank Mechanism', 195),
(213, 'Exhaust System', 195),
(214, 'Exterior Accessories', 195),
(215, 'Fuel System', 195),
(216, 'Interior Accessories', 195),
(217, 'Lubrication System', 195),
(218, 'Motorcycle Accessories', 195),
(219, 'Motorcycle Parts', 195),
(220, 'Motorcycles', 195),
(221, 'Other Auto Parts', 195),
(222, 'Suspension System', 195),
(223, 'Transmission', 195),
(224, 'Tricycles', 195),
(225, 'Universal Parts', 195),
(226, 'UTV', 195),
(227, 'Valve Train', 195),
(228, 'Vehicle Equipment', 195),
(229, 'Vehicle Tools', 195),
(230, 'Transportation ', 0),
(231, 'Aircraft', 230),
(232, 'Aviation Parts', 230),
(233, 'Aviation Accessories', 230),
(234, 'Bicycle', 230),
(235, 'Bicycle Accessories', 230),
(236, 'Bicycle Parts', 230),
(237, 'Boats & Ships', 230),
(238, 'Bus', 230),
(239, 'Bus Accessories', 230),
(240, 'Bus Parts', 230),
(241, 'Container', 230),
(242, 'Electric Bicycle', 230),
(243, 'Electric Bicycle Part', 230),
(244, 'Emergency Vehicles', 230),
(245, 'Golf Carts', 230),
(246, 'Locomotive', 230),
(247, 'Marine Supplies', 230),
(248, 'Personal Watercraft', 230),
(249, 'Railway Supplies', 230),
(250, 'Snowmobile', 230),
(251, 'Special Transportation', 230),
(252, 'Trailers', 230),
(253, 'Train Carriage', 230),
(254, 'Train Parts', 230),
(255, 'Truck', 230),
(256, 'Truck Accessories', 230),
(257, 'Truck Parts', 230),
(258, 'Bag & Luggage Making Materials', 16),
(259, 'Bag Parts & Accessories', 16),
(260, 'Business Bags & Cases', 16),
(261, 'Digital Gear & Camera Bags', 16),
(262, 'Handbags & Messenger Bags', 16),
(263, 'Luggage & Travel Bags', 16),
(264, 'Luggage Cart', 16),
(265, 'Other Luggage, Bags & Cases', 16),
(266, 'Special Purpose Bags & Cases', 16),
(267, 'Sports & Leisure Bags', 16),
(268, 'Wallets & Holders', 16),
(269, 'Carry-on Luggage', 16),
(270, '', 0),
(271, 'Luggage Sets', 16),
(272, 'Trolley Bags', 16),
(273, 'Briefcases', 16),
(274, 'Cosmetic Bags & Cases', 16),
(275, 'Shopping Bags', 16),
(276, 'Handbags', 16),
(277, 'Backpacks', 16),
(278, 'Wallets', 16),
(279, 'Baby Shoes', 17),
(280, 'Boots', 17),
(281, 'Casual Shoes', 17),
(282, 'Children’s Shoes', 17),
(283, 'Clogs', 17),
(284, 'Dance Shoes', 17),
(285, 'Dress Shoes', 17),
(286, 'Genuine Leather Shoes', 17),
(287, 'Men’s Shoes', 17),
(288, 'Other Shoes', 17),
(289, 'Sandals', 17),
(290, 'Shoe Materials', 17),
(291, 'Shoe Parts & Accessories', 17),
(292, 'Shoe Repairing Equipment', 17),
(293, 'Shoes Design Services', 17),
(294, 'Shoes Processing Services', 17),
(295, 'Shoes Stock', 17),
(296, 'Slippers', 17),
(297, 'Special Purpose Shoes', 17),
(298, 'Sports Shoes', 17),
(299, 'Used Shoes', 17),
(300, 'Women’s Shoes', 17),
(301, 'All-In-One PC', 18),
(302, 'Barebone System', 18),
(303, 'Blank Media', 18),
(304, 'Computer Cables & Connectors', 18),
(305, 'Computer Cases & Towers', 18),
(306, 'Computer Cleaners', 18),
(307, 'Desktops', 18),
(308, 'Fans & Cooling', 18),
(309, 'Firewall & VPN', 18),
(310, 'Floppy Drives', 18),
(311, 'Graphics Cards', 18),
(312, 'Hard Drives', 18),
(313, 'HDD Enclosure', 18),
(314, 'Industrial Computer & Accessories ', 18),
(315, 'Keyboard Covers', 18),
(316, 'KVM Switches', 18),
(317, 'Laptop Accessories', 18),
(318, 'Laptop Cooling Pads', 18),
(319, 'Laptops', 18),
(320, 'Memory', 18),
(321, 'Modems', 18),
(322, 'Monitors', 18),
(323, 'Motherboards', 18),
(324, 'Mouse & Keyboards', 18),
(325, 'Mouse Pads', 18),
(326, 'Netbooks & UMPC', 18),
(327, 'Network Cabinets', 18),
(328, 'Network Cards', 18),
(329, 'Network Hubs', 18),
(330, 'Network Switches', 18),
(331, 'Networking Storage', 18),
(332, 'Optical Drives', 18),
(333, 'Other Computer Accessories', 18),
(334, 'Other Computer Parts', 18),
(335, 'Other Computer Products', 18),
(336, 'Other Drive & Storage Devices', 18),
(337, 'Other Networking Devices', 18),
(338, 'PC Stations', 18),
(339, 'PDAs', 18),
(340, 'Power Supply Units', 18),
(341, 'Printers', 18),
(342, 'Processors', 18),
(343, 'Routers', 18),
(344, 'Scanners', 18),
(345, 'Servers', 18),
(346, 'Software', 18),
(347, 'Sound Cards', 18),
(348, 'Tablet PC', 18),
(349, 'Tablet PC Stands', 18),
(350, 'Tablet Stylus Pen', 18),
(351, 'USB Flash Drives', 18),
(352, 'USB Gadgets', 18),
(353, 'USB Hubs', 18),
(354, 'Used Computers & Accessories', 18),
(355, 'Webcams', 18),
(356, 'Wireless Networking', 18),
(357, 'Workstations', 18),
(358, 'Air Conditioning Appliances', 19),
(359, 'Cleaning Appliances', 19),
(360, 'Hand Dryers', 19),
(361, 'Home Appliance Parts', 19),
(362, 'Home Appliances Stocks', 19),
(363, 'Home Heaters', 19),
(364, 'Kitchen Appliances', 19),
(365, 'Laundry Appliances', 19),
(366, 'Other Home Appliances', 19),
(367, 'Refrigerators & Freezers', 19),
(368, 'Water Heaters', 19),
(369, 'Water Treatment Appliances', 19),
(370, 'Wet Towel Dispensers', 19),
(371, 'Air Conditioners', 19),
(372, 'Fans', 19),
(373, 'Vacuum Cleaners', 19),
(374, 'Solar Water Heaters', 19),
(375, 'Cooking Appliances', 19),
(376, 'Coffee Makers', 19),
(377, 'Blenders', 19),
(378, 'Accessories & Parts', 20),
(379, 'Camera, Photo & Accessories', 20),
(380, 'Electronic Publications', 20),
(381, 'Home Audio, Video & Accessories ', 20),
(382, 'Mobile Phone & Accessories', 20),
(383, 'Other Consumer Electronics', 20),
(384, 'Portable Audio, Video & Accessories ', 20),
(385, 'Video Game & Accessories', 20),
(386, 'Mobile Phones', 20),
(387, 'Earphone & Headphone', 20),
(388, 'Power Banks', 20),
(389, 'Digital Camera', 20),
(390, 'Radio & TV Accessories', 20),
(391, 'Speaker', 20),
(392, 'Television', 20),
(393, 'Cables', 20),
(394, 'Charger', 20),
(395, 'Digital Battery', 20),
(396, 'Digital Photo Frame', 20),
(397, '3D Glasses', 20),
(398, 'Access Control Systems & Products ', 21),
(399, 'Alarm', 21),
(400, 'CCTV Products', 21),
(401, 'Firefighting Supplies', 21),
(402, 'Key', 21),
(403, 'Lock Parts', 21),
(404, 'Locks', 21),
(405, 'Locksmith Supplies', 21),
(406, 'Other Security & Protection Products ', 21),
(407, 'Police & Military Supplies', 21),
(408, 'Roadway Safety', 21),
(409, 'Safes', 21),
(410, 'Security Services', 21),
(411, 'Self Defense Supplies', 21),
(412, 'Water Safety Products', 21),
(413, 'Workplace Safety Supplies', 21),
(414, 'CCTV Camera', 21),
(415, 'Bullet Proof Vest', 21),
(416, 'Alcohol Tester', 21),
(417, 'Fire Alarm', 21),
(418, 'Batteries', 22),
(419, 'Circuit Breakers', 22),
(420, 'Connectors & Terminals', 22),
(421, 'Contactors', 22),
(422, 'Electrical Plugs & Sockets', 22),
(423, 'Electronic & Instrument Enclosures ', 22),
(424, 'Fuse Components', 22),
(425, 'Fuses', 22),
(426, 'Generators', 22),
(427, 'Other Electrical Equipment', 22),
(428, 'Power Accessories', 22),
(429, 'Power Distribution Equipment', 22),
(430, 'Power Supplies', 22),
(431, 'Professional Audio, Video & Lighting ', 22),
(432, 'Relays', 22),
(433, 'Switches', 22),
(434, 'Transformers', 22),
(435, 'Wires, Cables & Cable Assemblies ', 22),
(436, 'Wiring Accessories', 22),
(437, 'Solar Cells, Solar Panel', 22),
(438, 'Active Components', 23),
(439, 'EL Products', 23),
(440, 'Electronic Accessories & Supplies ', 23),
(441, 'Electronic Data Systems', 23),
(442, 'Electronic Signs', 23),
(443, 'Electronics Production Machinery ', 23),
(444, 'Electronics Stocks', 23),
(445, 'Optoelectronic Displays', 23),
(446, 'Other Electronic Components', 23),
(447, 'Passive Components', 23),
(448, 'LCD Modules', 23),
(449, 'LED Displays', 23),
(450, 'PCB & PCBA', 23),
(451, 'Keypads & Keyboards', 23),
(452, 'Insulation Materials & Elements', 23),
(453, 'Integrated Circuits', 23),
(454, 'Diodes', 23),
(455, 'Transistors', 23),
(456, 'Capacitors', 23),
(457, 'Resistors', 23),
(458, 'Antennas for Communications', 24),
(459, 'Communication Equipment', 24),
(460, 'Telephones & Accessories', 24),
(461, 'Communication Cables', 24),
(462, 'Fiber Optic Equipment', 24),
(463, 'Fixed Wireless Terminals', 24),
(464, 'WiFi Finder', 24),
(465, 'Telephone Accessories', 24),
(466, 'Corded Telephones', 24),
(467, 'Cordless Telephones', 24),
(468, 'Wireless Networking Equipment', 24),
(469, 'Telephone Headsets', 24),
(470, 'VoIP Products', 24),
(471, 'Repeater', 24),
(472, 'PBX', 24),
(473, 'Telecom Parts', 24),
(474, 'Phone Cards', 24),
(475, 'Telephone Cords', 24),
(476, 'Answering Machines', 24),
(477, 'Caller ID Boxes', 24),
(478, 'Amusement Park', 25),
(479, 'Artificial Grass & Sports Flooring', 25),
(480, 'Fitness & Body Building', 25),
(481, 'Gambling', 25),
(482, 'Golf', 25),
(483, 'Indoor Sports', 25),
(484, 'Musical Instruments', 25),
(485, 'Other Sports & Entertainment Products ', 25),
(486, 'Outdoor Sports', 25),
(487, 'Sports Gloves', 25),
(488, 'Sports Safety', 25),
(489, 'Sports Souvenirs', 25),
(490, 'Team Sports', 25),
(491, 'Tennis', 25),
(492, 'Water Sports', 25),
(493, 'Winter Sports', 25),
(494, 'Camping & Hiking', 25),
(495, 'Scooters', 25),
(496, 'Gym Equipment', 25),
(497, 'Swimming & Diving', 25),
(498, 'Antique Imitation Crafts', 26),
(499, 'Art & Collectible', 26),
(500, 'Artificial Crafts', 26),
(501, 'Arts & Crafts Stocks', 26),
(502, 'Bamboo Crafts', 26),
(503, 'Carving Crafts', 26),
(504, 'Clay Crafts', 26),
(505, 'Cross Stitch', 26),
(506, 'Crystal Crafts', 26),
(507, 'Embroidery Crafts', 26),
(508, 'Feng Shui Crafts', 26),
(509, 'Festive & Party Supplies', 26),
(510, 'Flags, Banners & Accessories', 26),
(511, 'Folk Crafts', 26),
(512, 'Gift Sets', 26),
(513, 'Glass Crafts', 26),
(514, 'Holiday Gifts', 26),
(515, 'Home Decoration', 26),
(516, 'Key Chains', 26),
(517, 'Knitting & Crocheting', 26),
(518, 'Lacquerware', 26),
(519, 'Lanyard', 26),
(520, 'Leather Crafts', 26),
(521, 'Metal Crafts', 26),
(522, 'Money Boxes', 26),
(523, 'Music Boxes', 26),
(524, 'Natural Crafts', 26),
(525, 'Nautical Crafts', 26),
(526, 'Other Gifts & Crafts', 26),
(527, 'Paper Crafts', 26),
(528, 'Plastic Crafts', 26),
(529, 'Pottery & Enamel', 26),
(530, 'Religious Crafts', 26),
(531, 'Resin Crafts', 26),
(532, 'Sculptures', 26),
(533, 'Semi-Precious Stone Crafts', 26),
(534, 'Souvenirs', 26),
(535, 'Stickers', 26),
(536, 'Stone Crafts', 26),
(537, 'Textile & Fabric Crafts', 26),
(538, 'Wedding Decorations & Gifts', 26),
(539, 'Wicker Crafts', 26),
(540, 'Wood Crafts', 26),
(541, 'Action Figure', 27),
(542, 'Baby Toys', 27),
(543, 'Balloons', 27),
(544, 'Candy Toys', 27),
(545, 'Classic Toys', 27),
(546, 'Dolls', 27),
(547, 'Educational Toys', 27),
(548, 'Electronic Toys', 27),
(549, 'Glass Marbles', 27),
(550, 'Inflatable Toys', 27),
(551, 'Light-Up Toys', 27),
(552, 'Noise Maker', 27),
(553, 'Other Toys & Hobbies', 27),
(554, 'Outdoor Toys & Structures', 27),
(555, 'Plastic Toys', 27),
(556, 'Pretend Play & Preschool', 27),
(557, 'Solar Toys', 27),
(558, 'Toy Accessories', 27),
(559, 'Toy Animal', 27),
(560, 'Toy Guns', 27),
(561, 'Toy Parts', 27),
(562, 'Toy Robots', 27),
(563, 'Toy Vehicle', 27),
(564, 'Wind Up Toys', 27),
(565, 'Wooden Toys', 27),
(566, 'Animal Extract', 28),
(567, 'Plant Extracts', 28),
(568, 'Body Weight', 28),
(569, 'Health Care Supplement', 28),
(570, 'Health Care Supplies', 28),
(571, 'Crude Medicine', 28),
(572, 'Prepared Drugs In Pieces', 28),
(573, 'Traditional Patented Medicines', 28),
(574, 'Body Fluid-Processing & Circulation Devices ', 28),
(575, 'Clinical Analytical Instruments', 28),
(576, 'Dental Equipment', 28),
(577, 'Emergency & Clinics Apparatuses ', 28),
(578, 'Equipments of Traditional Chinese Medicine ', 28),
(579, 'General Assay & Diagnostic Apparatuses ', 28),
(580, 'Implants & Interventional Materials ', 28),
(581, 'Medical Consumable', 28),
(582, 'Medical Cryogenic Equipments', 28),
(583, 'Medical Software', 28),
(584, 'Physical Therapy Equipments', 28),
(585, 'Sterilization Equipments', 28),
(586, 'Surgical Instrument', 28),
(587, 'Ward Nursing Equipments', 28),
(588, 'Medicines', 28),
(589, 'Veterinary Instrument', 28),
(590, 'Veterinary Medicine', 28),
(591, 'Baby Care', 29),
(592, 'Bath Supplies', 29),
(593, 'Beauty Equipment', 29),
(594, 'Body Art', 29),
(595, 'Breast Care', 29),
(596, 'Feminine Hygiene', 29),
(597, 'Fragrance & Deodorant', 29),
(598, 'Hair Care', 29),
(599, 'Hair Extensions & Wigs', 29),
(600, 'Hair Salon Equipment', 29),
(601, 'Makeup', 29),
(602, 'Makeup Tools', 29),
(603, 'Men Care', 29),
(604, 'Nail Supplies', 29),
(605, 'Oral Hygiene', 29),
(606, 'Other Beauty & Personal Care Products ', 29),
(607, 'Sanitary Paper', 29),
(608, 'Shaving & Hair Removal', 29),
(609, 'Skin Care', 29),
(610, 'Skin Care Tool', 29),
(611, 'Spa Supplies', 29),
(612, 'Weight Loss', 29),
(613, 'Aluminum Composite Panels', 30),
(614, 'Balustrades & Handrails', 30),
(615, 'Bathroom', 30),
(616, 'Boards', 30),
(617, 'Building Glass', 30),
(618, 'Ceilings', 30),
(619, 'Corner Guards', 30),
(620, 'Curtain Walls & Accessories', 30),
(621, 'Decorative Films', 30),
(622, 'Door & Window Accessories', 30),
(623, 'Doors & Windows', 30),
(624, 'Earthwork Products', 30),
(625, 'Escalators & Escalator Parts', 30),
(626, 'Faucets, Mixers & Taps', 30),
(627, 'Fiberglass Wall Meshes', 30),
(628, 'Fireplaces,Stoves', 30),
(629, 'Flooring & Accessories', 30),
(630, 'Formwork', 30),
(631, 'Gates', 30),
(632, 'Heat Insulation Materials', 30),
(633, 'HVAC Systems & Parts', 30),
(634, 'Kitchen', 30),
(635, 'Ladders & Scaffoldings', 30),
(636, 'Landscaping Stone', 30),
(637, 'Masonry Materials', 30),
(638, 'Metal Building Materials', 30),
(639, 'Mosaics', 30),
(640, 'Mouldings', 30),
(641, 'Multifunctional Materials', 30),
(642, 'Other Construction & Real Estate', 30),
(643, 'Plastic Building Materials', 30),
(644, 'Quarry Stone & Slabs', 30),
(645, 'Real Estate', 30),
(646, 'Soundproofing Materials', 30),
(647, 'Stairs & Stair Parts', 30),
(648, 'Stone Carvings and Sculptures', 30),
(649, 'Sunrooms & Glass Houses', 30),
(650, 'Tiles & Accessories', 30),
(651, 'Timber', 30),
(652, 'Tombstones and Monuments', 30),
(653, 'Wallpapers/Wall Coating', 30),
(654, 'Waterproofing Materials', 30),
(655, 'Bakeware', 31),
(656, 'Barware', 31),
(657, 'Bathroom Products', 31),
(658, 'Cooking Tools', 31),
(659, 'Cookware', 31),
(660, 'Garden Supplies', 31),
(661, 'Home Decor', 31),
(662, 'Home Storage & Organization', 31),
(663, 'Household Chemicals', 31),
(664, 'Household Sundries', 31),
(665, 'Kitchen Knives & Accessories', 31),
(666, 'Laundry Products', 31),
(667, 'Pet Products', 31),
(668, 'Tableware', 31),
(669, 'Dinnerware', 31),
(670, 'Drinkware', 31),
(671, 'Baby Supplies & Products', 31),
(672, 'Rain Gear', 31),
(673, 'Lighters & Smoking Accessories', 31),
(674, 'Emergency Lighting', 32),
(675, 'Holiday Lighting', 32),
(676, 'Indoor Lighting', 32),
(677, 'LED Lighting', 32),
(678, 'Lighting Accessories', 32),
(679, 'Lighting Bulbs & Tubes', 32),
(680, 'Other Lights & Lighting Products', 32),
(681, 'Outdoor Lighting', 32),
(682, 'Professional Lighting', 32),
(683, 'LED Residential Lighting', 32),
(684, 'LED Outdoor Lighting', 32),
(685, 'Chandeliers & Pendant Lights', 32),
(686, 'Ceiling Lights', 32),
(687, 'Crystal Lights', 32),
(688, 'Stage Lights', 32),
(689, 'Street Lights', 32),
(690, 'Energy Saving & Fluorescent', 32),
(691, 'LED Professional Lighting', 32),
(692, 'LED Encapsulation Series', 32),
(693, 'Antique Furniture', 33),
(694, 'Baby Furniture', 33),
(695, 'Bamboo Furniture', 33),
(696, 'Children Furniture', 33),
(697, 'Commercial Furniture', 33),
(698, 'Folding Furniture', 33),
(699, 'Furniture Accessories', 33),
(700, 'Furniture Hardware', 33),
(701, 'Furniture Parts', 33),
(702, 'Glass Furniture', 33),
(703, 'Home Furniture', 33),
(704, 'Inflatable Furniture', 33),
(705, 'Metal Furniture', 33),
(706, 'Other Furniture', 33),
(707, 'Outdoor Furniture', 33),
(708, 'Plastic Furniture', 33),
(709, 'Rattan / Wicker Furniture', 33),
(710, 'Wood Furniture', 33),
(711, 'Living Room Furniture', 33),
(712, 'Bedroom Furniture', 33),
(713, 'Building Material Machinery', 34),
(714, 'Apparel & Textile Machinery', 34),
(715, 'Electronic Products Machinery', 34),
(716, 'Energy & Mineral Equipment', 34),
(717, 'General Industrial Equipment', 34),
(718, 'Machine Tool Equipment', 34),
(719, 'Metal & Metallurgy Machinery', 34),
(720, 'Paper Production Machinery', 34),
(721, 'Packaging Machine', 34),
(722, 'Plastic & Rubber Machinery', 34),
(723, 'Pharmaceutical Machinery', 34),
(724, 'Printing Machine', 34),
(725, 'Woodworking Machinery', 34),
(726, 'Used Machinery & Equipment', 34),
(727, 'Ball Valves', 35),
(728, 'Bearing Accessory', 35),
(729, 'Bearings', 35),
(730, 'Brass Valves', 35),
(731, 'Butterfly Valves', 35),
(732, 'Ceramic Valves', 35),
(733, 'Check Valves', 35),
(734, 'Custom Fabrication Services', 35),
(735, 'Diaphragm Valves', 35),
(736, 'Filter Supplies', 35),
(737, 'Flanges', 35),
(738, 'Gaskets', 35),
(739, 'Gate Valves', 35),
(740, 'Machine Tools Accessory', 35),
(741, 'Linear Motion', 35),
(742, 'Manual Valves', 35),
(743, 'Motor Parts', 35),
(744, 'Motors', 35),
(745, 'Moulds', 35),
(746, 'Needle Valves', 35),
(747, 'Other Mechanical Parts', 35),
(748, 'Pipe Fittings', 35),
(749, 'Pneumatic & Hydraulic', 35),
(750, 'Power Transmission', 35),
(751, 'Pumps & Parts', 35),
(752, 'Seals', 35),
(753, 'Shafts', 35),
(754, 'Solenoid Valves', 35),
(755, 'Vacuum Valves', 35),
(756, 'Valve Parts', 35),
(757, 'Valves', 35),
(758, 'Welding & Soldering Supplies', 35),
(759, 'Construction Tools', 36),
(760, 'Garden Tools', 36),
(761, 'Hand Tools', 36),
(762, 'Lifting Tools', 36),
(763, 'Material Handling Tools', 36),
(764, 'Other Tools', 36),
(765, 'Power Tool Accessories', 36),
(766, 'Power Tools', 36),
(767, 'Tool Design Services', 36),
(768, 'Tool Parts', 36),
(769, 'Tool Processing Services', 36),
(770, 'Tool Sets', 36),
(771, 'Tool Stock', 36),
(772, 'Tools Packaging', 36),
(773, 'Used Tools', 36),
(774, 'Electric Drill', 36),
(775, 'Knife', 36),
(776, 'Hand Carts & Trolleys', 36),
(777, 'Lawn Mower', 36),
(778, 'Sander', 36),
(779, 'Abrasive Tools', 37),
(780, 'Abrasives', 37),
(781, 'Brackets', 37),
(782, 'Chains', 37),
(783, 'Clamps', 37),
(784, 'Fasteners', 37),
(785, 'Hardware Stock', 37),
(786, 'Other Hardware', 37),
(787, 'Springs', 37),
(788, 'Used Hardware', 37),
(789, 'Bolts', 37),
(790, 'Screws', 37),
(791, 'Nuts', 37),
(792, 'Nails', 37),
(793, 'Anchors', 37),
(794, 'Rivets', 37),
(795, 'Washers', 37),
(796, 'Other Fasteners', 37),
(797, 'Analyzers', 38),
(798, 'Counters', 38),
(799, 'Electrical Instruments', 38),
(800, 'Electronic Measuring Instruments ', 38),
(801, 'Flow Measuring Instruments', 38),
(802, 'Instrument Parts & Accessories', 38),
(803, 'Lab Supplies', 38),
(804, 'Level Measuring Instruments', 38),
(805, 'Measuring & Analysing Instrument Design Services ', 38),
(806, 'Measuring & Analysing Instrument Processing Services ', 38),
(807, 'Measuring & Analysing Instrument Stocks ', 38),
(808, 'Optical Instruments', 38),
(809, 'Physical Measuring Instruments', 38),
(810, 'Pressure Measuring Instruments', 38),
(811, 'Temperature Instruments', 38),
(812, 'Testing Equipment', 38),
(813, 'Timers', 38),
(814, 'Weighing Scales', 38),
(815, 'Used Measuring & Analysing Instruments ', 38),
(816, 'Aluminum', 39),
(817, 'Asbestos Products', 39),
(818, 'Asbestos Sheets', 39),
(819, 'Barbed Wire', 39),
(820, 'Billets', 39),
(821, 'Carbon', 39),
(822, 'Carbon Fiber', 39),
(823, 'Cast & Forged', 39),
(824, 'Cemented Carbide', 39),
(825, 'Ceramic Fiber Products', 39),
(826, 'Ceramics', 39),
(827, 'Copper', 39),
(828, 'Copper Forged', 39),
(829, 'Fiberglass Products', 39),
(830, 'Glass', 39),
(831, 'Graphite Products', 39),
(832, 'Ingots', 39),
(833, 'Iron', 39),
(834, 'Lead', 39),
(835, 'Lime', 39),
(836, 'Magnetic Materials', 39),
(837, 'Metal Scrap', 39),
(838, 'Metal Slabs', 39),
(839, 'Mineral Wool', 39),
(840, 'Molybdenum', 39),
(841, 'Nickel', 39),
(842, 'Non-Metallic Mineral Deposit', 39),
(843, 'Ore', 39),
(844, 'Other Metals & Metal Products', 39),
(845, 'Pig Iron', 39),
(846, 'Quartz Products', 39),
(847, 'Rare Earth & Products', 39),
(848, 'Rare Earth Magnets', 39),
(849, 'Refractory', 39),
(850, 'Steel', 39),
(851, 'Titanium', 39),
(852, 'Tungsten', 39),
(853, 'Wire Mesh', 39),
(854, 'Zinc', 39),
(855, 'Gas Disposal', 40),
(856, 'Noise Reduction Device', 40),
(857, 'Other Environmental Products', 40),
(858, 'Other Excess Inventory', 40),
(859, 'Recycling', 40),
(860, 'Sewer', 40),
(861, 'Waste Management', 40),
(862, 'Water Treatment', 40),
(863, 'Textile Waste', 40),
(864, 'Waste Paper', 40),
(865, 'Other Recycling Products', 40),
(866, 'Plastic Processing Service', 41),
(867, 'Plastic Products', 41),
(868, 'Plastic Projects', 41),
(869, 'Plastic Raw Materials', 41),
(870, 'Plastic Stocks', 41),
(871, 'Recycled Plastic', 41),
(872, 'Recycled Rubber', 41),
(873, 'Rubber Processing Service', 41),
(874, 'Rubber Products', 41),
(875, 'Rubber Projects', 41),
(876, 'Rubber Raw Materials', 41),
(877, 'Rubber Stocks', 41),
(878, 'Plastic Cards', 41),
(879, 'PVC', 41),
(880, 'Plastic Tubes', 41),
(881, 'HDPE', 41),
(882, 'Rubber Hoses', 41),
(883, 'Plastic Sheets', 41),
(884, 'LDPE', 41),
(885, 'Agricultural Rubber', 41),
(886, 'Biodiesel', 42),
(887, 'Biogas', 42),
(888, 'Charcoal', 42),
(889, 'Coal', 42),
(890, 'Coal Gas', 42),
(891, 'Coke Fuel', 42),
(892, 'Crude Oil', 42),
(893, 'Electricity Generation', 42),
(894, 'Petrochemical Products', 41),
(895, 'Solar Energy Products', 42),
(896, 'Industrial Fuel', 42),
(897, 'Natural Gas', 42),
(898, 'Other Energy Related Products', 42),
(899, 'Wood Pellets', 42),
(900, 'Solar Energy Systems', 42),
(901, 'Lubricant', 42),
(902, 'Diesel Fuel', 42),
(903, 'Solar Chargers', 42),
(904, 'Solar Collectors', 42),
(905, 'Bitumen', 42),
(906, 'Additives', 43),
(907, 'Adhesives & Sealants', 43),
(908, 'Agrochemicals', 43),
(909, 'Basic Organic Chemicals', 43),
(910, 'Chemical Reagent Products', 43),
(911, 'Chemical Waste', 43),
(912, 'Custom Chemical Services', 43),
(913, 'Daily Chemical Raw Materials', 43),
(914, 'Flavour & Fragrance', 43),
(915, 'Inorganic Chemicals', 43),
(916, 'Non-Explosive Demolition Agents', 43),
(917, 'Organic Intermediates', 43),
(918, 'Other Chemicals', 43),
(919, 'Paints & Coatings', 43),
(920, 'Pharmaceuticals', 43),
(921, 'Pigment & Dyestuff', 43),
(922, 'Polymer', 43),
(923, 'Food Additive Products', 43),
(924, 'Fertilizer', 43),
(925, 'Adhesive Tape', 44),
(926, 'Agricultural Packaging', 44),
(927, 'Aluminum Foil', 44),
(928, 'Apparel Packaging', 44),
(929, 'Blister Cards', 44),
(930, 'Bottles', 44),
(931, 'Cans', 44),
(932, 'Chemical Packaging', 44),
(933, 'Composite Packaging Materials', 44),
(934, 'Cosmetics Packaging', 44),
(935, 'Electronics Packaging', 44),
(936, 'Food Packaging', 44),
(937, 'Gift Packaging', 44),
(938, 'Handles', 44),
(939, 'Hot Stamping Foil', 44),
(940, 'Jars', 44),
(941, 'Lids, Bottle Caps, Closures', 44),
(942, 'Media Packaging', 44),
(943, 'Metallized Film', 44),
(944, 'Other Packaging Applications', 44),
(945, 'Other Packaging Applications', 44),
(946, 'Other Packaging Materials', 44),
(947, 'Packaging Bags', 44),
(948, 'Packaging Boxes', 44),
(949, 'Packaging Labels', 44),
(950, 'Packaging Product Stocks', 44),
(951, 'Packaging Rope', 44),
(952, 'Packaging Rope', 44),
(953, 'Packaging Trays', 44),
(954, 'Packaging Tubes', 44),
(955, 'Paper & Paperboard', 44),
(956, 'Paper Packaging', 44),
(957, 'Pharmaceutical Packaging', 44),
(958, 'Plastic Film', 44),
(959, 'Plastic Packaging', 44),
(960, 'Printing Materials', 44),
(961, 'Printing Services', 44),
(962, 'Protective Packaging', 44),
(963, 'Pulp', 44),
(964, 'Shrink Film', 44),
(965, 'Strapping', 44),
(966, 'Stretch Film', 44),
(967, 'Tobacco Packaging', 44),
(968, 'Transport Packaging', 44),
(969, 'Art Supplies', 45),
(970, 'Badge Holder & Accessories', 45),
(971, 'Board', 45),
(972, 'Board Eraser', 45),
(973, 'Book Cover', 45),
(974, 'Books', 45),
(975, 'Calculator', 45),
(976, 'Calendar', 45),
(977, 'Clipboard', 45),
(978, 'Correction Supplies', 45),
(979, 'Desk Organizer', 45),
(980, 'Drafting Supplies', 45),
(981, 'Easels', 45),
(982, 'Educational Supplies', 45),
(983, 'Filing Products', 45),
(984, 'Letter Pad / Paper', 45),
(985, 'Magazines', 45),
(986, 'Map', 45),
(987, 'Notebooks & Writing Pads', 45),
(988, 'Office Adhesives & Tapes', 45),
(989, 'Office Binding Supplies', 45),
(990, 'Office Cutting Supplies', 45),
(991, 'Office Equipment', 45),
(992, 'Office Paper', 45),
(993, 'Other Office & School Supplies', 45),
(994, 'Paper Envelopes', 45),
(995, 'Pencil Cases & Bags', 45),
(996, 'Pencil Sharpeners', 45),
(997, 'Printer Supplies', 45),
(998, 'Stamps', 45),
(999, 'Stationery Set', 45),
(1000, 'Stencils', 45),
(1001, 'Writing Accessories', 45),
(1002, 'Writing Instruments', 45),
(1003, 'Yellow Pages', 45),
(1004, 'Advertising Equipment', 46),
(1005, 'Cargo & Storage Equipment', 46),
(1006, 'Commercial Laundry Equipment', 46),
(1007, 'Financial Equipment', 46),
(1008, 'Funeral Supplies', 46),
(1009, 'Other Service Equipment', 46),
(1010, 'Restaurant & Hotel Supplies', 46),
(1011, 'Store & Supermarket Supplies', 46),
(1012, 'Trade Show Equipment', 46),
(1013, 'Vending Machines', 46),
(1014, 'Wedding Supplies', 46),
(1015, 'Display Racks', 46),
(1016, 'Advertising Players', 46),
(1017, 'Advertising Light Boxes', 46),
(1018, 'Hotel Amenities', 46),
(1019, 'POS Systems', 46),
(1020, 'Supermarket Shelves', 46),
(1021, 'Stacking Racks & Shelves', 46),
(1022, 'Refrigeration Equipment', 46),
(1023, 'Trade Show Tent', 46);

-- --------------------------------------------------------

--
-- Table structure for table `rbc_city`
--

CREATE TABLE IF NOT EXISTS `rbc_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_countries`
--

CREATE TABLE IF NOT EXISTS `rbc_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(50) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=243 ;

--
-- Dumping data for table `rbc_countries`
--

INSERT INTO `rbc_countries` (`country_id`, `country_code`, `country_name`) VALUES
(1, 'pk', 'Pakistan'),
(2, 'ca', 'Canada'),
(3, 'af', 'Afghanistan'),
(4, 'al', 'Albania'),
(5, 'dz', 'Algeria'),
(6, 'cn', 'China'),
(7, 'ad', 'Andorra'),
(8, 'ao', 'Angola'),
(9, 'ai', 'Anguilla'),
(10, 'aq', 'Antarctica'),
(11, 'ag', 'Antigua and/or Barbuda'),
(12, 'ar', 'Argentina'),
(13, 'am', 'Armenia'),
(14, 'aw', 'Aruba'),
(15, 'au', 'Australia'),
(16, 'at', 'Austria'),
(17, 'az', 'Azerbaijan'),
(18, 'bs', 'Bahamas'),
(19, 'bh', 'Bahrain'),
(20, 'bd', 'Bangladesh'),
(21, 'bb', 'Barbados'),
(22, 'by', 'Belarus'),
(23, 'be', 'Belgium'),
(24, 'bz', 'Belize'),
(25, 'bj', 'Benin'),
(26, 'bm', 'Bermuda'),
(27, 'bt', 'Bhutan'),
(28, 'bo', 'Bolivia'),
(29, 'ba', 'Bosnia and Herzegovina'),
(30, 'bw', 'Botswana'),
(31, 'bv', 'Bouvet Island'),
(32, 'br', 'Brazil'),
(33, 'io', 'British lndian Ocean Territory'),
(34, 'bn', 'Brunei Darussalam'),
(35, 'bg', 'Bulgaria'),
(36, 'bf', 'Burkina Faso'),
(37, 'bi', 'Burundi'),
(38, 'kh', 'Cambodia'),
(39, 'cm', 'Cameroon'),
(40, 'cv', 'Cape Verde'),
(41, 'ky', 'Cayman Islands'),
(42, 'cf', 'Central African Republic'),
(43, 'td', 'Chad'),
(44, 'cl', 'Chile'),
(45, 'us', 'United States'),
(46, 'cx', 'Christmas Island'),
(47, 'cc', 'Cocos (Keeling) Islands'),
(48, 'co', 'Colombia'),
(49, 'km', 'Comoros'),
(50, 'cg', 'Congo'),
(51, 'ck', 'Cook Islands'),
(52, 'cr', 'Costa Rica'),
(53, 'hr', 'Croatia (Hrvatska)'),
(54, 'cu', 'Cuba'),
(55, 'cy', 'Cyprus'),
(56, 'cz', 'Czech Republic'),
(57, 'dk', 'Denmark'),
(58, 'dj', 'Djibouti'),
(59, 'dm', 'Dominica'),
(60, 'do', 'Dominican Republic'),
(61, 'tp', 'East Timor'),
(62, 'ec', 'Ecuador'),
(63, 'eg', 'Egypt'),
(64, 'sv', 'El Salvador'),
(65, 'gq', 'Equatorial Guinea'),
(66, 'er', 'Eritrea'),
(67, 'ee', 'Estonia'),
(68, 'et', 'Ethiopia'),
(69, 'fk', 'Falkland Islands (Malvinas)'),
(70, 'fo', 'Faroe Islands'),
(71, 'fj', 'Fiji'),
(72, 'fi', 'Finland'),
(73, 'fr', 'France'),
(74, 'fx', 'France, Metropolitan'),
(75, 'gf', 'French Guiana'),
(76, 'pf', 'French Polynesia'),
(77, 'tf', 'French Southern Territories'),
(78, 'ga', 'Gabon'),
(79, 'gm', 'Gambia'),
(80, 'ge', 'Georgia'),
(81, 'de', 'Germany'),
(82, 'gh', 'Ghana'),
(83, 'gi', 'Gibraltar'),
(84, 'gr', 'Greece'),
(85, 'gl', 'Greenland'),
(86, 'gd', 'Grenada'),
(87, 'gp', 'Guadeloupe'),
(88, 'gu', 'Guam'),
(89, 'gt', 'Guatemala'),
(90, 'gn', 'Guinea'),
(91, 'gw', 'Guinea-Bissau'),
(92, 'gy', 'Guyana'),
(93, 'ht', 'Haiti'),
(94, 'hm', 'Heard and Mc Donald Islands'),
(95, 'hn', 'Honduras'),
(96, 'hk', 'Hong Kong'),
(97, 'hu', 'Hungary'),
(98, 'is', 'Iceland'),
(99, 'in', 'India'),
(100, 'id', 'Indonesia'),
(101, 'ir', 'Iran (Islamic Republic of)'),
(102, 'iq', 'Iraq'),
(103, 'ie', 'Ireland'),
(104, 'il', 'Israel'),
(105, 'it', 'Italy'),
(106, 'ci', 'Ivory Coast'),
(107, 'jm', 'Jamaica'),
(108, 'jp', 'Japan'),
(109, 'jo', 'Jordan'),
(110, 'kz', 'Kazakhstan'),
(111, 'ke', 'Kenya'),
(112, 'ki', 'Kiribati'),
(113, 'kp', 'Korea, Democratic People''s Republic of'),
(114, 'kr', 'Korea, Republic of'),
(115, 'xk', 'Kosovo'),
(116, 'kw', 'Kuwait'),
(117, 'kg', 'Kyrgyzstan'),
(118, 'la', 'Lao People''s Democratic Republic'),
(119, 'lv', 'Latvia'),
(120, 'lb', 'Lebanon'),
(121, 'ls', 'Lesotho'),
(122, 'lr', 'Liberia'),
(123, 'ly', 'Libyan Arab Jamahiriya'),
(124, 'li', 'Liechtenstein'),
(125, 'lt', 'Lithuania'),
(126, 'lu', 'Luxembourg'),
(127, 'mo', 'Macau'),
(128, 'mk', 'Macedonia'),
(129, 'mg', 'Madagascar'),
(130, 'mw', 'Malawi'),
(131, 'my', 'Malaysia'),
(132, 'mv', 'Maldives'),
(133, 'ml', 'Mali'),
(134, 'mt', 'Malta'),
(135, 'mh', 'Marshall Islands'),
(136, 'mq', 'Martinique'),
(137, 'mr', 'Mauritania'),
(138, 'mu', 'Mauritius'),
(139, 'ty', 'Mayotte'),
(140, 'mx', 'Mexico'),
(141, 'fm', 'Micronesia, Federated States of'),
(142, 'md', 'Moldova, Republic of'),
(143, 'mc', 'Monaco'),
(144, 'mn', 'Mongolia'),
(145, 'me', 'Montenegro'),
(146, 'ms', 'Montserrat'),
(147, 'ma', 'Morocco'),
(148, 'mz', 'Mozambique'),
(149, 'mm', 'Myanmar'),
(150, 'na', 'Namibia'),
(151, 'nr', 'Nauru'),
(152, 'np', 'Nepal'),
(153, 'nl', 'Netherlands'),
(154, 'an', 'Netherlands Antilles'),
(155, 'nc', 'New Caledonia'),
(156, 'nz', 'New Zealand'),
(157, 'ni', 'Nicaragua'),
(158, 'ne', 'Niger'),
(159, 'ng', 'Nigeria'),
(160, 'nu', 'Niue'),
(161, 'nf', 'Norfork Island'),
(162, 'mp', 'Northern Mariana Islands'),
(163, 'no', 'Norway'),
(164, 'om', 'Oman'),
(165, 'ds', 'American Samoa'),
(166, 'pw', 'Palau'),
(167, 'pa', 'Panama'),
(168, 'pg', 'Papua New Guinea'),
(169, 'py', 'Paraguay'),
(170, 'pe', 'Peru'),
(171, 'ph', 'Philippines'),
(172, 'pn', 'Pitcairn'),
(173, 'pl', 'Poland'),
(174, 'pt', 'Portugal'),
(175, 'pr', 'Puerto Rico'),
(176, 'qa', 'Qatar'),
(177, 're', 'Reunion'),
(178, 'ro', 'Romania'),
(179, 'ru', 'Russian Federation'),
(180, 'rw', 'Rwanda'),
(181, 'kn', 'Saint Kitts and Nevis'),
(182, 'lc', 'Saint Lucia'),
(183, 'vc', 'Saint Vincent and the Grenadines'),
(184, 'ws', 'Samoa'),
(185, 'sm', 'San Marino'),
(186, 'st', 'Sao Tome and Principe'),
(187, 'sa', 'Saudi Arabia'),
(188, 'sn', 'Senegal'),
(189, 'rs', 'Serbia'),
(190, 'sc', 'Seychelles'),
(191, 'sl', 'Sierra Leone'),
(192, 'sg', 'Singapore'),
(193, 'sk', 'Slovakia'),
(194, 'si', 'Slovenia'),
(195, 'sb', 'Solomon Islands'),
(196, 'so', 'Somalia'),
(197, 'za', 'South Africa'),
(198, 'gs', 'South Georgia South Sandwich Islands'),
(199, 'es', 'Spain'),
(200, 'lk', 'Sri Lanka'),
(201, 'sh', 'St. Helena'),
(202, 'pm', 'St. Pierre and Miquelon'),
(203, 'sd', 'Sudan'),
(204, 'sr', 'Suriname'),
(205, 'sj', 'Svalbarn and Jan Mayen Islands'),
(206, 'sz', 'Swaziland'),
(207, 'se', 'Sweden'),
(208, 'ch', 'Switzerland'),
(209, 'sy', 'Syrian Arab Republic'),
(210, 'tw', 'Taiwan'),
(211, 'tj', 'Tajikistan'),
(212, 'tz', 'Tanzania, United Republic of'),
(213, 'th', 'Thailand'),
(214, 'tg', 'Togo'),
(215, 'tk', 'Tokelau'),
(216, 'to', 'Tonga'),
(217, 'tt', 'Trinidad and Tobago'),
(218, 'tn', 'Tunisia'),
(219, 'tr', 'Turkey'),
(220, 'tm', 'Turkmenistan'),
(221, 'tc', 'Turks and Caicos Islands'),
(222, 'tv', 'Tuvalu'),
(223, 'ug', 'Uganda'),
(224, 'ua', 'Ukraine'),
(225, 'ae', 'United Arab Emirates'),
(226, 'gb', 'United Kingdom'),
(227, 'um', 'United States minor outlying islands'),
(228, 'uy', 'Uruguay'),
(229, 'uz', 'Uzbekistan'),
(230, 'vu', 'Vanuatu'),
(231, 'va', 'Vatican City State'),
(232, 've', 'Venezuela'),
(233, 'vn', 'Vietnam'),
(234, 'vg', 'Virgin Islands (British)'),
(235, 'vi', 'Virgin Islands (U.S.)'),
(236, 'wf', 'Wallis and Futuna Islands'),
(237, 'eh', 'Western Sahara'),
(238, 'ye', 'Yemen'),
(239, 'yu', 'Yugoslavia'),
(240, 'zr', 'Zaire'),
(241, 'zm', 'Zambia'),
(242, 'zw', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_currency`
--

CREATE TABLE IF NOT EXISTS `rbc_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_name` varchar(100) NOT NULL,
  `currency_code` varchar(50) NOT NULL,
  `currency_symbol` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rbc_currency`
--

INSERT INTO `rbc_currency` (`id`, `currency_name`, `currency_code`, `currency_symbol`) VALUES
(1, 'Pound', 'GBP', '£'),
(2, 'Dollar', 'USD', '$'),
(3, 'Euro', 'EUR', '€');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_department`
--

CREATE TABLE IF NOT EXISTS `rbc_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_help_us`
--

CREATE TABLE IF NOT EXISTS `rbc_help_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) DEFAULT NULL,
  `message` text NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `rbc_help_us`
--

INSERT INTO `rbc_help_us` (`id`, `status`, `message`, `user_id`) VALUES
(1, 'neutral', 'asdasd', 1),
(2, 'neutral', 'sadasdsad', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rbc_home_slider`
--

CREATE TABLE IF NOT EXISTS `rbc_home_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` text NOT NULL,
  `image_description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rbc_membership`
--

CREATE TABLE IF NOT EXISTS `rbc_membership` (
  `membership_id` int(11) NOT NULL AUTO_INCREMENT,
  `membership_name` varchar(100) DEFAULT NULL,
  `products_limit` int(11) NOT NULL,
  PRIMARY KEY (`membership_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rbc_membership`
--

INSERT INTO `rbc_membership` (`membership_id`, `membership_name`, `products_limit`) VALUES
(1, 'Basic', 25),
(2, 'Standard', 50),
(3, 'Premium', 200);

-- --------------------------------------------------------

--
-- Table structure for table `rbc_news`
--

CREATE TABLE IF NOT EXISTS `rbc_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(100) NOT NULL,
  `news_description` varchar(255) NOT NULL,
  `news_date` date NOT NULL,
  `news_image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rbc_news`
--

INSERT INTO `rbc_news` (`id`, `news_title`, `news_description`, `news_date`, `news_image`) VALUES
(3, 'news title 3', 'ji han ji han', '2015-05-21', 'http://localhost/rbc-trades-cms/uploads/news_images/sxdfos.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_payment_type`
--

CREATE TABLE IF NOT EXISTS `rbc_payment_type` (
  `p_term_id` int(11) NOT NULL AUTO_INCREMENT,
  `p_term_description` varchar(100) NOT NULL,
  PRIMARY KEY (`p_term_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `rbc_payment_type`
--

INSERT INTO `rbc_payment_type` (`p_term_id`, `p_term_description`) VALUES
(1, 'L/C'),
(2, 'D/A'),
(3, 'D/P'),
(4, 'T/T'),
(5, 'Western Union'),
(6, 'Money Gram'),
(7, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_purchasing_frequency`
--

CREATE TABLE IF NOT EXISTS `rbc_purchasing_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchasing_frequency` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `rbc_purchasing_frequency`
--

INSERT INTO `rbc_purchasing_frequency` (`id`, `purchasing_frequency`) VALUES
(1, 'Less than one month'),
(2, 'One month'),
(3, 'One quarter'),
(4, 'Half a year'),
(5, 'One year'),
(6, 'Long than one year');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_security_question`
--

CREATE TABLE IF NOT EXISTS `rbc_security_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `security_question` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `rbc_security_question`
--

INSERT INTO `rbc_security_question` (`id`, `security_question`) VALUES
(1, 'What''s your pet''s name?'),
(2, 'What''s your grandma''s Name?'),
(3, 'What''s your grandpa''s Name?'),
(4, 'What''s primary school Name?'),
(5, 'What''s primary school Name?'),
(6, 'What''s your daughter/son''s nick name?'),
(7, 'What''s your birthday? (Example: 1980/01/01)'),
(8, 'What''s your hometown?'),
(9, 'What''s your Car Number?');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_uom`
--

CREATE TABLE IF NOT EXISTS `rbc_uom` (
  `uom_id` int(11) NOT NULL AUTO_INCREMENT,
  `uom_name` varchar(100) NOT NULL,
  PRIMARY KEY (`uom_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rbc_uom`
--

INSERT INTO `rbc_uom` (`uom_id`, `uom_name`) VALUES
(1, 'Box'),
(2, 'Container'),
(3, 'Pieces'),
(4, 'KG'),
(5, 'Feet');

-- --------------------------------------------------------

--
-- Table structure for table `rbc_user`
--

CREATE TABLE IF NOT EXISTS `rbc_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `membership_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(100) DEFAULT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `alternative_email` varchar(100) DEFAULT NULL,
  `street_adddress` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `job_title` varchar(100) DEFAULT NULL,
  `business_type` int(11) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `facebook_uid` varchar(500) DEFAULT NULL,
  `question_id` int(11) DEFAULT NULL,
  `answer` varchar(200) DEFAULT NULL,
  `hash_key` varchar(10) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 Active, 0 Inactive',
  `hash_key_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `rbc_user`
--

INSERT INTO `rbc_user` (`user_id`, `member_id`, `membership_type`, `user_name`, `user_password`, `user_email`, `alternative_email`, `street_adddress`, `city`, `province`, `country`, `zip_code`, `phone`, `fax`, `mobile`, `gender`, `job_title`, `business_type`, `department`, `facebook_uid`, `question_id`, `answer`, `hash_key`, `is_active`, `hash_key_time`) VALUES
(1, 1, 1, 'Muhammad Asad Saleem', 'b7e26c2e1aa1ecf0f4f54d5a4db9e795', 'asad.saleem50@gmail.com', NULL, NULL, 'sadasd', 'asasdads', 3, 3132, '23-23-232', '', NULL, 'male', 'sadd', 0, NULL, NULL, NULL, NULL, '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rfq_details`
--

CREATE TABLE IF NOT EXISTS `rfq_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_id` int(11) NOT NULL,
  `rfq_product_name` varchar(100) NOT NULL,
  `rfq_product_quantity` int(11) NOT NULL,
  `rfq_product_uom` int(11) NOT NULL,
  `rfq_product_description` text NOT NULL,
  `rfq_product_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rfq_master`
--

CREATE TABLE IF NOT EXISTS `rfq_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rfq_seller_id` int(11) DEFAULT NULL,
  `rfq_buyer_id` int(11) NOT NULL,
  `rfq_quotation_date` date NOT NULL DEFAULT '0000-00-00',
  `rfq_expiry_date` date DEFAULT '0000-00-00',
  `rfq_quotation_description` varchar(100) DEFAULT NULL,
  `rfq_quotes_left` int(11) DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `security_setting_master`
--

CREATE TABLE IF NOT EXISTS `security_setting_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `security_question_id` int(11) NOT NULL,
  `security_answer` varchar(500) NOT NULL,
  PRIMARY KEY (`id`,`member_id`,`security_question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
