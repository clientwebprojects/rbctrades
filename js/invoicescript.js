/* Shivving (IE8 is not supported, but at least it won't look as awful)
/* ========================================================================== */

(function (document) {
	var
	head = document.head = document.getElementsByTagName('head')[0] || document.documentElement,
	elements = 'article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output picture progress section summary time video x'.split(' '),
	elementsLength = elements.length,
	elementsIndex = 0,
	element;

	while (elementsIndex < elementsLength) {
		element = document.createElement(elements[++elementsIndex]);
	}

	element.innerHTML = 'x<style>' +
		'article,aside,details,figcaption,figure,footer,header,hgroup,nav,section{display:block}' +
		'audio[controls],canvas,video{display:inline-block}' +
		'[hidden],audio{display:none}' +
		'mark{background:#FF0;color:#000}' +
	'</style>';

	return head.insertBefore(element.lastChild, head.firstChild);
})(document);

/* Prototyping
/* ========================================================================== */

(function (window, ElementPrototype, ArrayPrototype, polyfill) {
	function NodeList() { [polyfill] }
	NodeList.prototype.length = ArrayPrototype.length;

	ElementPrototype.matchesSelector = ElementPrototype.matchesSelector ||
	ElementPrototype.mozMatchesSelector ||
	ElementPrototype.msMatchesSelector ||
	ElementPrototype.oMatchesSelector ||
	ElementPrototype.webkitMatchesSelector ||
	function matchesSelector(selector) {
		return ArrayPrototype.indexOf.call(this.parentNode.querySelectorAll(selector), this) > -1;
	};

	ElementPrototype.ancestorQuerySelectorAll = ElementPrototype.ancestorQuerySelectorAll ||
	ElementPrototype.mozAncestorQuerySelectorAll ||
	ElementPrototype.msAncestorQuerySelectorAll ||
	ElementPrototype.oAncestorQuerySelectorAll ||
	ElementPrototype.webkitAncestorQuerySelectorAll ||
	function ancestorQuerySelectorAll(selector) {
		for (var cite = this, newNodeList = new NodeList; cite = cite.parentElement;) {
			if (cite.matchesSelector(selector)) ArrayPrototype.push.call(newNodeList, cite);
		}

		return newNodeList;
	};

	ElementPrototype.ancestorQuerySelector = ElementPrototype.ancestorQuerySelector ||
	ElementPrototype.mozAncestorQuerySelector ||
	ElementPrototype.msAncestorQuerySelector ||
	ElementPrototype.oAncestorQuerySelector ||
	ElementPrototype.webkitAncestorQuerySelector ||
	function ancestorQuerySelector(selector) {
		return this.ancestorQuerySelectorAll(selector)[0] || null;
	};
})(this, Element.prototype, Array.prototype);

/* Helper Functions
/* ========================================================================== */

function description(value , id){
    console.log(value);
    console.log(id);
    $.each(product, function (i, v) {
            

                    if (parseInt(v.product_id) === parseInt(value)) {
//                        console.log(id);
                        console.log(v.product_description);
                           
                            $('#'+id+'description').val(v.product_description);
                    }
                   
                });
    
    
   
    
}

function generateTableRow() {
	var emptyColumn = document.createElement('tr');

	emptyColumn.innerHTML =
                '<td><a class="cut">-</a><span contenteditable id="newselect'+count+'">'+
                '</span></td>' +
		'<td><input id="select'+count+'description" name="selectdescription[]" type="text" ></td>' +
		'<td><input id="rate'+count+'" name="rate[]" type="text"></td>' +
		'<td><input id="quantity'+count+'" name="quantity[]" type="text"></td>' +
                '<td id="uom'+count+'"></td>' +
		'<td><input id="price'+count+'" name="price[]" type="text" disabled></td>';

	return emptyColumn;
}

function parseFloatHTML(element) {
	return parseFloat(element.innerHTML.replace(/[^\d\.\-]+/g, '')) || 0;
}

function parsePrice(number) {
	return number.toFixed(2).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
}

/* Update Number
/* ========================================================================== */

function updateNumber(e) {
//	var
//	activeElement = document.activeElement,
//	value = parseFloat(activeElement.innerHTML),
//	wasPrice = activeElement.innerHTML == parsePrice(parseFloatHTML(activeElement));
//
//	if (!isNaN(value) && (e.keyCode == 38 || e.keyCode == 40 || e.wheelDeltaY)) {
//		e.preventDefault();
//
//		value += e.keyCode == 38 ? 1 : e.keyCode == 40 ? -1 : Math.round(e.wheelDelta * 0.025);
//		value = Math.max(value, 0);
//
//		activeElement.innerHTML = wasPrice ? parsePrice(value) : value;
//	}

	updateInvoice();
}

/* Update Invoice
/* ========================================================================== */

function updateInvoice() {
	var total = 0;
        var due = 0;
        var tax = 0;
        var discount = 0;
	var rate, qty,price,i;

	// update inventory cells
	// ======================

//	for (var a = document.querySelectorAll('table.inventory tbody tr'), i = 0; a[i]; ++i) {
                
                for(i=0;i<count;i++){
                rate = $('#rate'+i).val();
                qty = $('#quantity'+i).val();
                price = rate*qty;
                  
                  if(isNaN(price)){
                      price=0;
                  }
                total+=price;


                $('#price'+i).val(price);
            
            }
            $('#total').val(total);
            tax = $('#tax').val()*total/100;
            discount = $('#discount').val()*total/100;
            due=total+tax-discount;
            $('#balance_due').val(due);
//            $('#amount_due').val(due);
//	}

	// update balance cells
	// ====================

	// get balance cells
//	cells = document.querySelectorAll('table.balance td:last-child span:last-child');

	// set total
//	cells[0].innerHTML = total;

	// set balance and meta balance
//	cells[2].innerHTML = document.querySelector('table.meta tr:last-child td:last-child span:last-child').innerHTML = parsePrice(total - parseFloatHTML(cells[1]));

	// update prefix formatting
	// ========================

//	var prefix = document.querySelector('#prefix').innerHTML;
//	for (a = document.querySelectorAll('[data-prefix]'), i = 0; a[i]; ++i) a[i].innerHTML = prefix;

	// update price formatting
	// =======================

//	for (a = document.querySelectorAll('span[data-prefix] + span'), i = 0; a[i]; ++i) if (document.activeElement != a[i]) a[i].innerHTML = parsePrice(parseFloatHTML(a[i]));
}

/* On Content Load
/* ========================================================================== */

function onContentLoad() {
	updateInvoice();

	var
	input = document.querySelector('input'),
	image = document.querySelector('img');

	function onClick(e) {
		var element = e.target.querySelector('[contenteditable]'), row;

		element && e.target != document.documentElement && e.target != document.body && element.focus();

		if (e.target.matchesSelector('.add')) {
			document.querySelector('table.inventory tbody').appendChild(generateTableRow());
                        
                        var newSelect=document.createElement('select');
                        var newUoms=document.createElement('select')
                        newSelect.setAttribute('id','select'+count);
                        newSelect.setAttribute('name','select[]');
                        newUoms.setAttribute('id','selectuom'+count);
                        newUoms.setAttribute('name','selectuom[]');
                        
                        newSelect.setAttribute('onchange','description(this.value,this.id)');
                        var selectHTML="";
                           for(i=0; i<product.length; i=i+1){
                               selectHTML+= "<option value='"+product[i].product_id+"'>"+product[i].product_name+"</option>";
                           }
                           newSelect.innerHTML= selectHTML;
                               document.getElementById('newselect'+count).appendChild(newSelect);
                               
                               
                         var selectHTMLUom="";
                           for(i=0; i<uom.length; i=i+1){
                               
                               selectHTMLUom+= "<option value='"+uom[i].uom_id+"'>"+uom[i].uom_name+"</option>";
                           }
                           newUoms.innerHTML= selectHTMLUom;
                               document.getElementById('uom'+count).appendChild(newUoms);
                               
                               
                               
                               
                               
                                               count++;
		}
		else if (e.target.className == 'cut') {
			row = e.target.ancestorQuerySelector('tr');

			row.parentNode.removeChild(row);
//                        count--;
		}

//		updateInvoice();
	}

	function onEnterCancel(e) {
		e.preventDefault();

		image.classList.add('hover');
	}

	function onLeaveCancel(e) {
		e.preventDefault();

		image.classList.remove('hover');
	}

//	function onFileInput(e) {
//		image.classList.remove('hover');
//
//		var
//		reader = new FileReader(),
//		files = e.dataTransfer ? e.dataTransfer.files : e.target.files,
//		i = 0;
//
//		reader.onload = onFileLoad;
//
//		while (files[i]) reader.readAsDataURL(files[i++]);
//	}

	function onFileLoad(e) {
		var data = e.target.result;

		image.src = data;
	}

	if (window.addEventListener) {
		document.addEventListener('click', onClick);

		document.addEventListener('mousewheel', updateNumber);
		document.addEventListener('keydown', updateNumber);

		document.addEventListener('keydown', updateInvoice);
		document.addEventListener('keyup', updateInvoice);

		input.addEventListener('focus', onEnterCancel);
		input.addEventListener('mouseover', onEnterCancel);
		input.addEventListener('dragover', onEnterCancel);
		input.addEventListener('dragenter', onEnterCancel);

		input.addEventListener('blur', onLeaveCancel);
		input.addEventListener('dragleave', onLeaveCancel);
		input.addEventListener('mouseout', onLeaveCancel);
//
//		input.addEventListener('drop', onFileInput);
//		input.addEventListener('change', onFileInput);
	}
}

window.addEventListener && document.addEventListener('DOMContentLoaded', onContentLoad);