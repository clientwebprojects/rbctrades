/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {
    $('#contact-form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fname: {
                message: 'The firstname is not valid',
                validators: {
                    notEmpty: {
                        message: 'The FirstName is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The FirstName must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z]+$/,
                        message: 'The FirstName can only consist of alphabets'
                    }
                }
            },
            lname: {
                message: 'The LastName is not valid',
                validators: {
                    notEmpty: {
                        message: 'The LastName is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'The LastName must be more than 6 and less than 30 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z]+$/,
                        message: 'The LastName can only consist of alphabets'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            refid: {
                validators: {
                    notEmpty: {
                        message: 'The Ref Number is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 9,
                        max: 9,
                        message: 'The Reference Number must be more than 9 characters long'
                    }
                }
            }
            
        }
    });
});