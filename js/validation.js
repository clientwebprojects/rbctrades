/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#login-form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'The email is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Email address is not valid'
                    }
                }
            },
            password: {
                message: 'Password is not valid',
                validators: {
                    notEmpty: {
                        message: 'Password is required and can\'t be empty'
                    },
                }
            }

        }
    });
    $('#register-user-form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            user_email: {
                validators: {
                    notEmpty: {
                        message: 'The email is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Email address is invalid'
                    }
                }
            },
            create_password: {
                message: 'The password is not valid',
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
//                    stringLength: {
//                        min: 8,
//                        max: 100,
//                        message: 'The password must be more than 8 characters long'
//                    },
                    identical: {
                        field: 'confirm_password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
            confirm_password: {
                message: 'The password is not valid',
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'create_password',
                        message: 'Password Does not match'
                    }
                }
            },
            business_location: {
                validators: {
                    notEmpty: {
                        message: 'The Business Location is required and can\'t be empty'
                    }
                }
            },
            contact_name: {
                message: 'Contact Name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Contact Name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Contact Name must be under 100 characters'
                    }
                }
            },
            company_name: {
                message: 'The Company Name is not valid',
                validators: {
                    notEmpty: {
                        message: 'The Company Name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Company Name must be under 100 characters'
                    }
                }
            },
            contact_no: {
                message: 'The Contact Number is not valid',
                validators: {
                    notEmpty: {
                        message: 'The Contact Number is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }

                }
            }

        }
    });
    $('#register-company-form').formValidation({
        message: 'This value is not valid',
        icon: {
            
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: 'Name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Name required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Name must be under 100 characters'
                    }
                }
            },
            job_title: {
                message: 'Job title is not valid',
                validators: {
                    notEmpty: {
                        message: 'Job title is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Job title must be under 100 characters'
                    }
                }
            },
            phone: {
                message: 'Phone number is not valid',
                validators: {
                    notEmpty: {
                        message: 'Phone number is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }

                }
            },
            c_registration: {
                message: 'Registration No is not valid',
                validators: {
                    notEmpty: {
                        message: 'Registration No is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Registration No be under 100 characters'
                    }
                }
            },
            c_name: {
                validators: {
                    notEmpty: {
                        message: 'Company name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Company name must be under 100 characters'
                    }
                }
            },
            country: {
                message: 'Country is not valid',
                validators: {
                    notEmpty: {
                        message: 'Country is required and can\'t be empty'
                    }
                }
            },
            c_address: {
                message: 'Company Address is not valid',
                validators: {
                    notEmpty: {
                        message: 'Company Address is required and can\'t be empty'
                    }
                }
            },
            c_zip_code: {
                message: 'Zip code is not valid',
                validators: {
                    notEmpty: {
                        message: 'Zip code is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }

                }
            },
            state: {
                message: 'State is not valid',
                validators: {
                    notEmpty: {
                        message: 'State is required and can\'t be empty'
                    }
                }
            },
            ntn: {
                message: 'Verification is not valid',
                validators: {
                    notEmpty: {
                        message: 'Verification is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Verification must be under 100 characters'
                    }
                }
            },
            c_phone: {
                message: 'Company phone number is not valid',
                validators: {
                    notEmpty: {
                        message: 'Company phone number is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }

                }
            }

        }
    });
    $('#membership-registration-form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            mrfFirstNameTextField: {
                message: 'First name is not valid',
                validators: {
                    notEmpty: {
                        message: 'First name required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'First name must be under 100 characters'
                    }
                }
            },
            mrfLastNameTextField: {
                message: 'First name is not valid',
                validators: {
                    notEmpty: {
                        message: 'First name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'First name must be under 100 characters'
                    }
                }
            },
            mrfJobTitleTextField: {
                message: 'Job Title is not valid',
                validators: {
                    notEmpty: {
                        message: 'Job Title is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Job Title must be under 100 characters'
                    }

                }
            },
            mrfPhoneNumberTextField: {
                message: 'Phone No is not valid',
                validators: {
                    notEmpty: {
                        message: 'Phone No is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            mrfFreeTaxNoTextField: {
                validators: {
                    notEmpty: {
                        message: 'NTN/Free tax number is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'NTN/Free tax number must be under 100 characters'
                    }
                }
            },
            mrfSaleTaxRegistrationNoTextField: {
                message: 'Tax Registration No is not valid',
                validators: {
                    notEmpty: {
                        message: 'Tax Registration No is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'NTN/Free tax number must be under 100 characters'
                    }
                }
            },
            mrfPTCLAccountIdTextField: {
                message: 'PTCL account id is not valid',
                validators: {
                    notEmpty: {
                        message: 'PTCL account id is required and can\'t be empty'
                    }
                },
                stringLength: {
                    min: 0,
                    max: 100,
                    message: 'PTCL account id must be under 100 characters'
                }
            },
            mrfRegisteredCompanyNameTextField: {
                message: 'Company name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Company name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Company name must be under 100 characters'
                    }

                }
            },
            mrfCompanyStreetTextField: {
                message: 'Street address is not valid',
                validators: {
                    notEmpty: {
                        message: 'Street address is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Street address name must be under 100 characters'
                    }
                }
            },
            mrfCompanyCityTextField: {
                message: 'City is not valid',
                validators: {
                    notEmpty: {
                        message: 'City is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'City must be under 100 characters'
                    }
                }
            },
            mrfCompanyStateTextField: {
                message: 'State is not valid',
                validators: {
                    notEmpty: {
                        message: 'State is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'State must be under 100 characters'
                    }

                }
            },
            mrfCompanyZipCodeTextField: {
                message: 'Zip code is not valid',
                validators: {
                    notEmpty: {
                        message: 'Zip code is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }

                }
            },
            mrfCompanyTelephoneNoTextField: {
                message: 'Company phone number is not valid',
                validators: {
                    notEmpty: {
                        message: 'Company phone number is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }

                }
            }

        }
    });
    $('#contact-us-form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            fname: {
                message: 'First name is not valid',
                validators: {
                    notEmpty: {
                        message: 'First name required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Name must be under 100 characters'
                    }
                }
            },
            lname: {
                message: 'Last name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Last name is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 0,
                        max: 100,
                        message: 'Last name must be under 100 characters'
                    }
                }
            },
            email: {
                message: 'The email is not valid',
                validators: {
                    notEmpty: {
                        message: 'The email is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            message: {
                message: 'Message is not valid',
                validators: {
                    notEmpty: {
                        message: 'Message can\'t be empty'
                    }
                }
            }
        }
    });
    $('#rfq_form').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'rfq_product_name[]': {
                message: 'Product name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Product name is required and can\'t be empty'
                    }
                }
            },
            'rfq_product_description[]': {
                message: 'Product description is not valid',
                validators: {
                    notEmpty: {
                        message: 'Product description is required and can\'t be empty'
                    }
                }
            },
            'rfq_product_quantity[]': {
                message: 'Product quantity is not valid',
                validators: {
                    notEmpty: {
                        message: 'Product quantity is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            rfq_quotation_description: {
                message: 'Description is not valid',
                validators: {
                    notEmpty: {
                        message: 'Quotation information is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 5,
                        max: 100,
                        message: 'Description must be under 100 characters'
                    }
                }
            },
            share_info_check: {
                validators: {
                    notEmpty: {
                        message: 'Please mark this'
                    }
                }
            },
            agreement_check: {
                validators: {
                    notEmpty: {
                        message: 'Please mark this'
                    }
                }
            }

        }
    });
    $("#basic_company_details").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'business_type[]': {
                message: 'Business Type is not valid',
                validators: {
                    notEmpty: {
                        message: 'Business Type is required and can\'t be empty'
                    }
                }
            },
            company_name: {
                message: 'Company Name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Company Name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Company Name should be in alphabet'
                    }
                }

            },
            registration_location: {
                message: 'Registration location is not valid',
                validators: {
                    notEmpty: {
                        message: 'Please select your registration location.'
                    }
                }
            },
            operational_street_address: {
                message: 'Operational Address is not valid',
                validators: {
                    notEmpty: {
                        message: 'Street Address is required and can\'t be empty'
                    }
                }

            },
            operational_address_city: {
                message: 'Operational Address is not valid',
                validators: {
                    notEmpty: {
                        message: 'City Name is required and can\'t be empty'
                    }
                }


            },
            operational_location: {
                message: 'Operational Address is not valid',
                validators: {
                    notEmpty: {
                        message: 'Operational Location is required and can\'t be empty'
                    }
                }
            },
            operational_address_zip_code: {
                message: 'Operational Address is not valid',
                validators: {
                    notEmpty: {
                        message: 'Zip Code is required and can\'t be empty'
                    }
                }
            }

//                    'main_products[]': {
//
//                        validators: {
//                            notEmpty: {
//                                message: 'One Main product is required '
//                            }
//                        }
//
//                    }
        }


    });
    $("#certification-form").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            certification_type: {
                validators: {
                    notEmpty: {
                        message: 'Certificate type is required and can\'t be empty'
                    }
                }
            },
            reference_no: {
                message: "Refference number is not valid",
                validators: {
                    notEmpty: {
                        message: 'Refference number is required and can\'t be empty'
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            name: {
                message: "Certification name is not valid",
                validators: {
                    notEmpty: {
                        message: 'Certification name is required and can\'t be empty'
                    }

                }
            },
            issued_by: {
                message: "Certificate issued by is not valid",
                validators: {
                    notEmpty: {
                        message: 'Certificate issued by is required and can\'t be empty'
                    }

                }
            },
            starting_date: {
                message: "Certificate starting date is not valid",
                validators: {
                    notEmpty: {
                        message: 'Certificate starting date is required and can\'t be empty'
                    }

                }
            },
            scope: {
                message: "Scope is not valid",
                validators: {
                    notEmpty: {
                        message: 'Scope is required and can\'t be empty'
                    }

                }
            }
        }
    });
    $("#partner-factory-form").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            partner_factory_name: {
                message: "Factory name is not valid",
                validators: {
                    notEmpty: {
                        message: 'Factory name is required and can\'t be empty'
                    }
                }
            }

        }
    });

    $("#add_product_form").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            select: {
                validators: {
                    notEmpty: {
                        message: "Please select category"
                    }
                }
            },
            product_name: {
                validators: {
                    notEmpty: {
                        message: "Please enter product name"
                    }
                }


            },
            product_keyword: {
                validators: {
                    notEmpty: {
                        message: "Please enter product keyword"
                    }
                }
            },
            'product_image[]': {
                validators: {
                    notEmpty: {
                        message: "Please enter product image"
                    }
                }
            },
            product_origin: {
                validators: {
                    notEmpty: {
                        message: "Please provide place of origin"
                    }
                }
            },
            uom_id: {
                validators: {
                    notEmpty: {
                        message: "Please provide unit of measurement"
                    }
                }
            },
            product_minimun_order: {
                validators: {
                    notEmpty: {
                        message: "Please provide minimum order"
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            product_fob_price: {
                validators: {
                    notEmpty: {
                        message: "Please provide freight on board price"
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            'product_payment_type[]': {
                validators: {
                    notEmpty: {
                        message: "Please provide payment type"
                    }
                }
            },
            product_delivery_time: {
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            product_fob_currency:{
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    }
                }
            },
            product_fob_price_from:{
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    }
                }
                
            },
            product_fob_price_to:{
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    },
                    inclusive:{
                        field: 'product_fob_price_from',
                        message: "Should be greater than FOB price from"
                    }
                }
            }
        }

    });
    $("#edit_product_form").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            product_name: {
                validators: {
                    notEmpty: {
                        message: "Please enter product name"
                    }
                }


            },
            product_keyword: {
                validators: {
                    notEmpty: {
                        message: "Please enter product keyword"
                    }
                }
            },
            'product_image[]': {
                validators: {
                    notEmpty: {
                        message: "Please enter product image"
                    }
                }
            },
            product_origin: {
                validators: {
                    notEmpty: {
                        message: "Please provide place of origin"
                    }
                }
            },
            uom_id: {
                validators: {
                    notEmpty: {
                        message: "Please provide unit of measurement"
                    }
                }
            },
            product_minimun_order: {
                validators: {
                    notEmpty: {
                        message: "Please provide minimum order"
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            product_fob_price: {
                validators: {
                    notEmpty: {
                        message: "Please provide freight on board price"
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            'product_payment_type[]': {
                validators: {
                    notEmpty: {
                        message: "Please provide payment type"
                    }
                }
            },
            product_delivery_time: {
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            product_fob_currency:{
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    }
                }
            },
            product_fob_price_from:{
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    }
                }
                
            },
            product_fob_price_to:{
                validators: {
                    notEmpty: {
                        message: "Please provide delievery time"
                    },
                    inclusive:{
                        field: 'product_fob_price_from',
                        message: "Should be greater than FOB price from"
                    }
                }
            }
        }

    });
    $("#account_setting_my_profile").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            business_pattern: {
                validators: {
                    notEmpty: {
                        message: "Please provide Business Location"
                    }
                }
            },
            member_name: {
                validators: {
                    notEmpty: {
                        message: "Please provide member name"
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: "Please provide gender"
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: "Please provide city time"
                    }
                }
            },
            province: {
                validators: {
                    notEmpty: {
                        message: "Please provide province time"
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: "Please provide country time"
                    }
                }
            },
            zip_code: {
                validators: {
                    notEmpty: {
                        message: "Please provide zip code"
                    },
                    digits: {
                        message: 'Digits Only'
                    }
                }
            },
            'phone[]': {
                validators: {
                    notEmpty: {
                        message: "Please provide complete phone no."
                    }
                }
            },
            company_name: {
                validators: {
                    notEmpty: {
                        message: "Please provide company name"
                    }
                }
            },
            'business_type[]': {
                validators: {
                    notEmpty: {
                        message: "Please provide business type"
                    }
                }
            },
            website_opt: {
                validators: {
                    notEmpty: {
                        message: "Please select website option"
                    }
                }
            },
            website_url: {
                validators: {
                    notEmpty: {
                        message: "Please provide website url"
                    },
                    uri: {
                        message: 'The URL is not valid'
                    }
                }
            },
            job_title: {
                validators: {
                    notEmpty: {
                        message: "Please provide job title"
                    }
                }
            }
        }

    });
    $("#account_setting_member_profile").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            member_name: {
                validators: {
                    notEmpty: {
                        message: "Please provide name"
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: "Please provide gender"
                    }
                }
            },
            street_adddress: {
                validators: {
                    notEmpty: {
                        message: "Please provide street address"
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: "Please provide city name"
                    }
                }
            },
            province: {
                validators: {
                    notEmpty: {
                        message: "Please provide province"
                    }
                }
            },
            country: {
                validators: {
                    notEmpty: {
                        message: "Please provide country"
                    }
                }
            },
            zip_code: {
                validators: {
                    notEmpty: {
                        message: "Please provide zip code"
                    }
                },
                digits: {
                    message: 'Digits Only'
                }
            },
            'phone[]': {
                validators: {
                    notEmpty: {
                        message: "Please provide phone no."
                    }
                }
            }
        }
    });
    $("#account_setting_change_email").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: "Please provide email"
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Email address is invalid'
                    },
                    identical: {
                        field: 'confirm_email',
                        message: 'Email and confirm email not matched'
                    }
                }
            },
            confirm_email: {
                validators: {
                    notEmpty: {
                        message: "Please provide email"
                    },
                    regexp: {
                        regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                        message: 'Email address is invalid'
                    },
                    identical: {
                        field: 'email',
                        message: 'Email and confirm email not matched'
                    }
                }

            },
            security_question_id: {
                validators: {
                    notEmpty: {
                        message: "Please provide your security question"
                    }
                }
            },
            security_answer: {
                validators: {
                    notEmpty: {
                        message: "Please provide your security question"
                    }
                }
            }

        }
    });
    $("#account_setting_change_pas").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            user_password: {
                validators: {
                    notEmpty: {
                        message: "Please provide password"
                    }
                },
                identical: {
                    field: 'confirm_password',
                    message: 'Password and confirm password not matched'
                }
            },
            confirm_password: {
                validators: {
                    notEmpty: {
                        message: "Please provide confirm password"
                    }
                },
                identical: {
                    field: 'user_password',
                    message: 'Password and confirm password not matched'
                }
            },
            security_question_id: {
                validators: {
                    notEmpty: {
                        message: "Please provide your security question"
                    }
                }
            },
            security_answer: {
                validators: {
                    notEmpty: {
                        message: "Please provide your security question"
                    }
                }
            }
        }
    });
    $("#account_setting_set_sec_q").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            security_question_id: {
                validators: {
                    notEmpty: {
                        message: "Please provide your security question"
                    }
                }
            },
            security_answer: {
                validators: {
                    notEmpty: {
                        message: "Please provide your security question"
                    }
                }
            }
        }
    });
    $("#build_buyer_profile").formValidation({
        message: 'This value is not valid',
        excluded: [':disabled'],
        icon: {
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            company_name:{
                validators: {
                    notEmpty: {
                        message: "Please provide company name"
                    }
                }
            }
        }
    });
    
    
    //console.log("aaaaaaaaaaa");

});

